WITH IEE_METER_DET
     AS (SELECT DISTINCT s.servicepointid AS NMI,
                         MSS.SUFFIX AS RegisterId,
                         MSS.SUFFIX AS Suffix,
                         spc.spcnodeid,
                         UPPER (M.METERNUMBER ) AS SerialNumber,
                         U.UNITID AS UnitOfMeasure,
                         UOM.UNITOFMEASUREID,
                         nl2.effectivestartdate + 10 / 24 AS nodelink_start,
                         nl2.effectiveenddate + 10 / 24 AS nodelink_end,
                         es.statusid,
                         m.effectivestartdate + 10 / 24 AS meter_start,
                         m.effectiveenddate + 10 / 24 AS meter_end,
                         M.MULTIPLIER AS MULTIPLIER,
                         'C' AS STATUS,
                         SUBSTR (M.MANUFACTURER, 1, 15) AS MANUFACTURER,
                         M.MODEL AS MODEL,
                         NVL(IUAD.DATAVALUE,' ') AS ControlledLoad,
                         NVL(IUAD2.DATAVALUE, ' ') AS NetworkTariffCode,
                         'INTERVAL' AS TimeOfDay /*HARDCODED AS THESE ARE INTERVAL METERS*/
                         ,IC.PULSEMISCMULTIPLIER AS ConstantCAT
           FROM servicepoint s
                JOIN nodelink nl ON nl.leftnodekey = s.nodekey
                JOIN servicepointchannel spc ON spc.nodekey = nl.rightnodekey
                JOIN nodelink nl2 ON nl2.leftnodekey = spc.nodekey
                JOIN intervalchannel ic ON ic.nodekey = nl2.rightnodekey
                JOIN nodelink nl3 ON nl3.rightnodekey = ic.nodekey
                JOIN meter m ON m.nodekey = nl3.leftnodekey
                INNER JOIN MTS_SPC_SUFFIX_MAP MSS
                   ON MSS.SPC_ID = spc.channelnumber
                JOIN spcvalidationset svs
                   ON svs.servicepointchannelkey = spc.servicepointchannelkey
                JOIN validationset vs
                   ON vs.validationsetkey = svs.validationsetkey
                JOIN entitystatus es
                   ON es.entitystatuskey = m.entitystatuskey
                 JOIN IEE_SERVICEPOINTCHANNELUDAS iuad
                   ON IUAD.SERVICEPOINT = S.SERVICEPOINTID
                 JOIN IEE_SERVICEPOINTCHANNELUDAS iuad2
                   ON IUAD2.SERVICEPOINT = S.SERVICEPOINTID
                JOIN UNITOFMEASURE uom
                   ON UOM.UNITOFMEASUREKEY = SPC.UNITOFMEASUREKEY
                JOIN UNIT u ON UOM.UNITKEY = U.UNITKEY
          WHERE     1 = 1
                AND (    svs.effectivestartdate < nl2.effectiveenddate
                     AND svs.effectiveenddate > nl2.effectivestartdate)
                AND (    m.effectivestartdate < nl2.effectiveenddate
                     AND m.effectiveenddate > nl2.effectivestartdate)
                AND S.SERVICEPOINTID = '<NMI>'
                AND M.EFFECTIVEENDDATE > SYSDATE
                AND UPPER (IUAD.UDANAME) = 'CONTROLLED_LOAD'
                AND UPPER (IUAD2.UDANAME) = 'NTC'
                AND IUAD.SERVICEPOINTCHANNELNUMBER = spc.channelnumber
                AND IUAD2.SERVICEPOINTCHANNELNUMBER = SPC.CHANNELNUMBER
                AND IUAD.EFFECTIVEENDLINKDATE > SYSDATE
                AND IUAD2.EFFECTIVEENDLINKDATE > SYSDATE
                AND 1 = 1),
     LEVELONE
     AS (SELECT NMI,
                SERIALNUMBER,
                MANUFACTURER,
                MODEL,
                CONSTANTCAT,
                REGISTERID AS REGISTERID1,
                LEAD (REGISTERID, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS REGISTERID2,
                SUFFIX AS SUFFIX1,
                LEAD (SUFFIX, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS SUFFIX2,
                NETWORKTARIFFCODE AS NETWORKTARIFFCODE1,
                LEAD (NETWORKTARIFFCODE, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS NETWORKTARIFFCODE2,
                CONTROLLEDLOAD AS CONTROLLEDLOAD1,
                LEAD (CONTROLLEDLOAD, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS CONTROLLEDLOAD2,
                TIMEOFDAY,
                UNITOFMEASURE AS UNITOFMEASURE1,
                LEAD (UNITOFMEASURE, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS UNITOFMEASURE2,
                MULTIPLIER AS MULTIPLIER1,
                LEAD (MULTIPLIER, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS MULTIPLIER2
           FROM IEE_METER_DET),
     SEL_LEVELONE
     AS (SELECT *
           FROM LEVELONE
          WHERE REGISTERID2 IS NOT NULL),
     LEVELTWO
     AS (SELECT NMI,
                SERIALNUMBER AS SERIALNUMBER1,
                MANUFACTURER AS MANUFACTURER1,
                MODEL AS MODEL1,
                CONSTANTCAT AS CONSTANTCAT1,
                REGISTERID1,
                SUFFIX1,
                NETWORKTARIFFCODE1,
                CONTROLLEDLOAD1,
                UNITOFMEASURE1,
                MULTIPLIER1, REGISTERID2,
                SUFFIX2,
                NETWORKTARIFFCODE2,
                CONTROLLEDLOAD2,
                UNITOFMEASURE2,
				'D' as STATUS,
                MULTIPLIER2,
                LEAD (SERIALNUMBER, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS SERIALNUMBER2,
                LEAD (MANUFACTURER, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS MANUFACTURER2,
                LEAD (MODEL, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS MODEL2,
                LEAD (CONSTANTCAT, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS CONSTANTCAT2,
                LEAD (REGISTERID1, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS REGISTERID3,
                LEAD (SUFFIX1, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS SUFFIX3,
                LEAD (NETWORKTARIFFCODE1, 1)
                   OVER (PARTITION BY NMI ORDER BY NMI)
                   AS NETWORKTARIFFCODE3,
                LEAD (CONTROLLEDLOAD1, 1)
                   OVER (PARTITION BY NMI ORDER BY NMI)
                   AS CONTROLLEDLOAD3,
                LEAD (UNITOFMEASURE1, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS UNITOFMEASURE3,
                LEAD (MULTIPLIER1, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS MULTIPLIER3,
                LEAD (REGISTERID2, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS REGISTERID4,
                LEAD (SUFFIX2, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS SUFFIX4,
                LEAD (NETWORKTARIFFCODE2, 1)
                   OVER (PARTITION BY NMI ORDER BY NMI)
                   AS NETWORKTARIFFCODE4,
                LEAD (CONTROLLEDLOAD2, 1)
                   OVER (PARTITION BY NMI ORDER BY NMI)
                   AS CONTROLLEDLOAD4,
                LEAD (UNITOFMEASURE2, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS UNITOFMEASURE4,
                LEAD (MULTIPLIER2, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS MULTIPLIER4
           FROM SEL_LEVELONE)
SELECT *
  FROM LEVELTWO
 WHERE SERIALNUMBER2 IS NOT NULL