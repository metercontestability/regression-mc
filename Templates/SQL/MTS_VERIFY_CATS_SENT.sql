'select CCR.TRANSACTION_DETAIL_ID
       , ccr.nmid
       , crc.crcode      
       , s.name         AS Txn_Status
       , CCR.INITIATED_DATE
       , ccr.updated_dt AS CR_Update_Dt
       , ts.UPDATED_DT  AS Txn_Update_Dt
       , ccr.CR_DATA_ID
       , td.transaction_dt
from cat_change_request     ccr   /* <<=== this is where the row      */
     , transaction_status     ts  /*   is created after the pkg runs  */
     , status                 s
     , cat_change_reason_code crc
     , transaction_detail td
where ccr.change_reason_code_id = crc.id
  and ts.transaction_detail_id  = ccr.transaction_detail_id
  and TS.TRANSACTION_DETAIL_ID = TD.ID
  and ts.status_id = s.id
  and ccr.nmid = '<NMI>'
  and ccr.UPDATED_DT > trunc(sysdate) 
order by Txn_update_dt desc, 2, 4