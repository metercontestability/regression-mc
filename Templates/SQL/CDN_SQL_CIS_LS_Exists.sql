select replace(rtrim(xmlagg(xmlelement(NMI, ':'||Txt_Nat_Supp_Point||':,')).extract('//text()'),','),':',chr(39)) as CIS_NMI_LIST
from (
Select Distinct T056.Txt_Nat_Supp_Point
    From Tvp056servprov T056      
       , TVP054SERVPROVRESP t054
       , TVP024CUSTACCTROLE t024
       , Tvp064lename T064
       , TVP036LEGALENTITY T036       
       , Tvp097propscalert T097
      Where T056.No_Property = T054.No_Property
      And T056.No_Property = T097.No_Property
      And T056.Cd_Company_System = T024.Cd_Company_System
      And T056.Cd_Company_System = T064.Cd_Company_System
      And T056.Cd_Company_System = T097.Cd_Company_System     
      And T056.Cd_Company_System = T054.Cd_Company_System      
      And T054.No_Account = T024.No_Account
      And T024.No_Legal_Entity = T064.No_Legal_Entity
      And T024.No_Legal_Entity = T036.No_Legal_Entity  
      And T056.Cd_Service_Prov       = 'E'
      And T056.St_Serv_Prov          = 'A'
      And T024.Tp_Cust_Acct_Role     = '3'      
      And Nvl(T024.Dt_End,Sysdate+1) > Sysdate
        And T036.Ind_Legal_Entity = 'I'      
      And T064.Ind_Name              = 'I'     
      and t064.NM_LE_C4              <> 'UNKN' 
      And (Trim(T097.Cd_Spec_Cond_Tp) = 'LIFE' and t097.dt_end is null)
      AND not exists ( select 1 from TVP109WORKORDER t109
                       where T109.CD_COMPANY_SYSTEM = T056.CD_COMPANY_SYSTEM
                       and T109.NO_PROPERTY = T056.NO_PROPERTY
                       and T109.ST_WORK_ORDER in ('PR','OP','W')
                     )
       And T056.Txt_Nat_Supp_Point NOT IN (<NMI_LIST_FROM_KDR>)	
       And Rownum < 40
	   
     )
