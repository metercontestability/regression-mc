WITH IEE_METER_DET
     AS ( select distinct
                          s.servicepointid AS NMI
                        , MSS.SUFFIX as RegisterId
                        ,MSS.SUFFIX as Suffix
                        , spc.spcnodeid
                        , upper(m.meternumber) as SerialNumber
                        , U.UNITID as UnitOfMeasure
                        , UOM.UNITOFMEASUREID
                        , nl2.effectivestartdate + 10/24 as nodelink_start  
                        , nl2.effectiveenddate + 10/24   as nodelink_end  
                        , es.statusid
                        ,M.METERKEY
                        , m.effectivestartdate + 10/24 as meter_start
                        , m.effectiveenddate + 10/24 as meter_end
                        ,M.MULTIPLIER as MULTIPLIER
                        ,'C' as STATUS
                        ,SUBSTR(M.MANUFACTURER,1,15) as MANUFACTURER
                        ,M.MODEL as MODEL
                        ,IUAD.DATAVALUE as ControlledLoad
                        ,IUAD2.DATAVALUE as NetworkTariffCode
                        ,'INTERVAL' as TimeOfDay /*HARDCODED AS THESE ARE INTERVAL METERS*/
                        ,IC.PULSEMISCMULTIPLIER as ConstantCAT
                          ,SPG.SERVICEPOINTPROGRAMID as PROGRAMID
                   from servicepoint s
                   join nodelink nl on nl.leftnodekey = s.nodekey
                   join servicepointchannel spc on spc.nodekey = nl.rightnodekey
                   join MTS_SPC_SUFFIX_MAP MSS on MSS.SPC_ID=spc.channelnumber
                   join nodelink nl2 on nl2.leftnodekey = spc.nodekey
                   join intervalchannel ic on ic.nodekey = nl2.rightnodekey
                   join nodelink nl3 on nl3.rightnodekey = ic.nodekey
                   join meter m on m.nodekey = nl3.leftnodekey
                   join spcvalidationset svs on svs.servicepointchannelkey = spc.servicepointchannelkey
                                      JOIN SERVICEPOINTSPPGM SPSG on SPSG.SERVICEPOINTKEY=S.SERVICEPOINTKEY
                   JOIN SERVICEPOINTPROGRAM SPG on SPG.SERVICEPOINTPROGRAMKEY=SPSG.SERVICEPOINTPROGRAMKEY
                   join validationset vs on vs.validationsetkey = svs.validationsetkey
                   join entitystatus es
                   on es.entitystatuskey = m.entitystatuskey
                   join IEE_SERVICEPOINTCHANNELUDAS iuad on IUAD.SERVICEPOINT=S.SERVICEPOINTID
                   join IEE_SERVICEPOINTCHANNELUDAS iuad2 on IUAD2.SERVICEPOINT=S.SERVICEPOINTID
                   join UNITOFMEASURE uom on UOM.UNITOFMEASUREKEY = SPC.UNITOFMEASUREKEY
                   join UNIT u on UOM.UNITKEY=U.UNITKEY
                   where 1=1
                   and (svs.effectivestartdate < nl2.effectiveenddate and svs.effectiveenddate > nl2.effectivestartdate)
                   and (m.effectivestartdate < nl2.effectiveenddate and m.effectiveenddate > nl2.effectivestartdate)
                  and S.SERVICEPOINTID = '<NMI>'
                   and M.EFFECTIVEENDDATE>SYSDATE
                   and UPPER(IUAD.UDANAME)='CONTROLLED_LOAD'
                   and UPPER(IUAD2.UDANAME)='NTC'
                          AND IUAD.SERVICEPOINTCHANNELNUMBER = spc.channelnumber
                AND IUAD2.SERVICEPOINTCHANNELNUMBER = SPC.CHANNELNUMBER
                AND IUAD.EFFECTIVEENDLINKDATE > SYSDATE
                AND IUAD2.EFFECTIVEENDLINKDATE > SYSDATE
                 AND SPSG.EFFECTIVEENDDATE>SYSDATE
                AND 1 = 1),
     LEVELONE
     AS (SELECT NMI,
                SERIALNUMBER,
                MANUFACTURER,
                    PROGRAMID,
                MODEL,
                CONSTANTCAT,
                REGISTERID AS REGISTERID1,
                LEAD (REGISTERID, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS REGISTERID2,
                SUFFIX AS SUFFIX1,
                LEAD (SUFFIX, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS SUFFIX2,
                NETWORKTARIFFCODE AS NETWORKTARIFFCODE1,
                LEAD (NETWORKTARIFFCODE, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS NETWORKTARIFFCODE2,
                CONTROLLEDLOAD AS CONTROLLEDLOAD1,
                LEAD (CONTROLLEDLOAD, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS CONTROLLEDLOAD2,
                TIMEOFDAY,
                UNITOFMEASURE AS UNITOFMEASURE1,
                LEAD (UNITOFMEASURE, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS UNITOFMEASURE2,
                MULTIPLIER AS MULTIPLIER1,
                LEAD (MULTIPLIER, 1)
                OVER (PARTITION BY NMI, SERIALNUMBER ORDER BY SERIALNUMBER)
                   AS MULTIPLIER2
           FROM IEE_METER_DET),
     SEL_LEVELONE
     AS (SELECT *
           FROM LEVELONE
          WHERE REGISTERID2 IS NOT NULL),
     LEVELTWO
     AS (SELECT NMI,
                SERIALNUMBER AS SERIALNUMBER1,
                MANUFACTURER AS MANUFACTURER1,
                MODEL AS MODEL1,
                CONSTANTCAT AS CONSTANTCAT1,
                 PROGRAMID,
                'C' as STATUS,
                REGISTERID1,
                SUFFIX1,
                NETWORKTARIFFCODE1,
                CONTROLLEDLOAD1,
                UNITOFMEASURE1,
                MULTIPLIER1, REGISTERID2,
                SUFFIX2,
                NETWORKTARIFFCODE2,
                CONTROLLEDLOAD2,
                UNITOFMEASURE2,
                MULTIPLIER2,
                LEAD (SERIALNUMBER, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS SERIALNUMBER2,
                LEAD (MANUFACTURER, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS MANUFACTURER2,
                LEAD (MODEL, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS MODEL2,
                LEAD (CONSTANTCAT, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS CONSTANTCAT2,
                LEAD (REGISTERID1, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS REGISTERID3,
                LEAD (SUFFIX1, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS SUFFIX3,
                LEAD (NETWORKTARIFFCODE1, 1)
                   OVER (PARTITION BY NMI ORDER BY NMI)
                   AS NETWORKTARIFFCODE3,
                LEAD (CONTROLLEDLOAD1, 1)
                   OVER (PARTITION BY NMI ORDER BY NMI)
                   AS CONTROLLEDLOAD3,
                LEAD (UNITOFMEASURE1, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS UNITOFMEASURE3,
                LEAD (MULTIPLIER1, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS MULTIPLIER3,
                LEAD (REGISTERID2, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS REGISTERID4,
                LEAD (SUFFIX2, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS SUFFIX4,
                LEAD (NETWORKTARIFFCODE2, 1)
                   OVER (PARTITION BY NMI ORDER BY NMI)
                   AS NETWORKTARIFFCODE4,
                LEAD (CONTROLLEDLOAD2, 1)
                   OVER (PARTITION BY NMI ORDER BY NMI)
                   AS CONTROLLEDLOAD4,
                LEAD (UNITOFMEASURE2, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS UNITOFMEASURE4,
                LEAD (MULTIPLIER2, 1) OVER (PARTITION BY NMI ORDER BY NMI)
                   AS MULTIPLIER4
           FROM SEL_LEVELONE)
SELECT *
  FROM LEVELTWO
 WHERE SERIALNUMBER2 IS NOT NULL