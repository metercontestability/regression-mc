SELECT 
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//mts:WorkOrderType', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS WORKORDERTYPE,
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//mts:WorkOrderTask', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS WORKORDERTASK,
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//mts:ServiceOrderChargeCode', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS SERVICEORDERCHARGECODE,
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//mts:Description', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS NOTE
FROM MTS.SQT_INTERFACE_OUT T
WHERE
DBMS_LOB.INSTR(T.USER_DATA.PAYLOAD_DATA.GETCLOBVAL(), '<Ret_ServiceOrderNumber>') > 0
ORDER BY T.ENQ_TIME ASC