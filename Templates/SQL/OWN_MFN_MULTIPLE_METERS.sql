WITH RESERVED_NMIS
     AS (SELECT DISTINCT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE     (   TDR.IS_RETIRED_YN = 'Y'
                     OR TDR.IS_IN_USE_YN = 'Y'
                     OR TDR.IS_RESERVED_YN = 'Y')
                AND TDR.CREATED_TS > TRUNC (SYSDATE)),
     NMIFILTER
     AS (SELECT SERVICEPOINT AS NMI
           FROM STATUS ST, SERVICEPOINT_STATUS SS
          WHERE SS.STATUS_ID = ST.ID AND ST.NAME = 'A'
         MINUS
         SELECT NMI FROM RESERVED_NMIS),
    NMI_DETAILS
    AS (select distinct NF.NMI, meternumber
        from iee_physicalconfigtreebymeter TREE, NMIFILTER NF, SERVICEPOINT_MP_ROLE SMRMPB, SERVICEPOINT_REF_ATTRIBUTE SRACLS
        where NF.NMI = TREE.SERVICEPOINT
        and NF.NMI = SMRMPB.SERVICEPOINT
        and NF.NMI = sracls.servicepoint
        and smrmpb.role_id in (6, 1)
        and sracls.attribute_type_id in (5, 1)
        and sracls.end_dt > sysdate
        AND metereffectiveenddate > sysdate
        AND CHANNELEFFECTIVESTARTDATE <= SYSDATE
        AND CHANNELEFFECTIVEENDDATE > SYSDATE
        AND SPCTODATASTREAMLINKSTARTDATE <= SYSDATE
        AND SPCTODATASTREAMLINKENDDATE > SYSDATE
        AND CHANNELTOMETERLINKSTARTDATE <= SYSDATE
        AND CHANNELTOMETERLINKENDDATE > SYSDATE),
    NMI_METER_COUNT
    AS (select NMI, count(NMI) as NUM_METERS from NMI_DETAILS
    GROUP BY NMI),
	METER_JOIN
	AS (
		select * from
		(select
		ND.NMI,
		rtrim(xmlagg(xmlelement(e, ND.meternumber, ' ')).extract('//text()').getclobval(), ' ') as METERNUMBER
		from NMI_DETAILS ND
		join NMI_METER_COUNT NC on ND.NMI = NC.NMI
		where NC.NUM_METERS > 1
		group by ND.NMI)
		where rownum <= 1)
select 
NMI,
METERNUMBER,
(select PARTICIPANT_NAME from MARKET_PARTICIPANT MP, SERVICEPOINT_MP_ROLE SLNSP where MP.ID = SLNSP.MARKET_PARTICIPANT_ID and ROLE_ID = 1 and servicepoint = NMI and rownum < 2) as INITIATOR, 
'LNSP' AS INITIATOR_Role,
(select PARTICIPANT_NAME from MARKET_PARTICIPANT MP, SERVICEPOINT_MP_ROLE SLNSP where MP.ID = SLNSP.MARKET_PARTICIPANT_ID and ROLE_ID = 6 and servicepoint = NMI and rownum < 2) as RECIPIENT  
from METER_JOIN