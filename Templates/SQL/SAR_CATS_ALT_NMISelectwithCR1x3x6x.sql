/* Formatted on 14/06/2017 9:59:39 AM (QP5 v5.256.13226.35510) */
WITH RECEIVED_TODAY
     AS (SELECT DISTINCT SAR.NMI || SAR.NMI_CHECKSUM AS NMI
           FROM SITE_ACCESS_REQ SAR, TRANSACTION_DETAIL TD
          WHERE     SAR.TRANSACTION_DETAIL_ID = TD.ID
                AND TD.GW_FILE_TIMESTAMP > TRUNC (SYSDATE)),
     RESERVED_NMIS
     AS (SELECT DISTINCT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE     (   TDR.IS_RETIRED_YN = 'Y'
                     OR TDR.IS_IN_USE_YN = 'Y'
                     OR TDR.IS_RESERVED_YN = 'Y')
                AND TDR.CREATED_TS > TRUNC (SYSDATE)),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID),
     NMI_DETAILS
     AS (SELECT SS.SERVICEPOINT AS NMI
           FROM STATUS ST,
                SERVICEPOINT_STATUS SS,
                SERVICEPOINT_REF_ATTRIBUTE SRA,
                SERVICEPOINT_REF_ATTRIBUTE SRA2,
                SERVICEPOINT_REF_ATTRIBUTE SRA3
          WHERE     SS.STATUS_ID = ST.ID
                AND ST.NAME = 'A'
                AND SS.END_DT > SYSDATE
                AND SRA.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA.VALUE = '<Meter_Type>'
                AND SRA.END_DT > SYSDATE
                AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA2.VALUE = '<NMI_Size>'
                AND SRA3.VALUE = 'RWD'
                AND SRA3.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA2.END_DT > SYSDATE
                AND SRA3.END_DT > SYSDATE
         MINUS
         SELECT NMI FROM RECEIVED_TODAY
         MINUS
         SELECT NMI FROM RESERVED_NMIS),
     CRSelect
     AS (  SELECT nmid, MAX (ccd.id) AS CR_ID
             FROM cat_cr_data ccd, NMI_DETAILS ND
            WHERE     ccd.nmid = SUBSTR (ND.NMI, 1, 10)
                  AND ccd.change_reason_code_id IN ('1',
                                                    '2',
                                                    '3',
                                                    '72',
                                                    '73',
                                                    '75',
                                                    '74',
                                                    '76',
                                                    '77',
                                                    '78',
                                                    '79',
                                                    '80',
                                                    '4',
                                                    '5',
                                                    '81',
                                                    '82',
                                                    '6',
                                                    '7',
                                                    '8',
                                                    '9',
                                                    '10',
                                                    '90',
                                                    '91',
                                                    '92',
                                                    '93',
                                                    '43',
                                                    '44',
                                                    '49',
                                                    '50',
                                                    '86',
                                                    '87')
         GROUP BY nmid),
     NMI_DETAILS_FIN
     AS (SELECT DISTINCT t.*, ND.NMI, st.name
           FROM CRSelect t,
                cat_notification cn,
                status st,
                NMI_DETAILS ND
          WHERE     CN.CR_DATA_ID = t.CR_ID
                AND CN.STATUS_ID = st.id
                AND nmid = SUBSTR (ND.NMI, 1, 10)
                AND ST.NAME NOT IN ('REQ', 'PEND'))
SELECT SRA.VALUE AS NMI_SIZE,
       'MDP' AS Static_MDP,
       'RP' AS Static_RP,
       'MPB' AS Static_MPB,
       'LNSP' AS Static_LNSP,
       MPRP.PARTICIPANT_NAME AS ROLE_RP,
       MPMDP.PARTICIPANT_NAME AS ROLE_MDP,
       MPMPB.PARTICIPANT_NAME AS ROLE_MPB,
       MPMPC.PARTICIPANT_NAME AS ROLE_MPC,
       MPLNSP.PARTICIPANT_NAME AS ROLE_LNSP,
       MPFRMP.PARTICIPANT_NAME AS ROLE_FRMP,
       (SELECT MPAF.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPAF, MARKET_PARTICIPANT_ROLE MPRAF
         WHERE     MPAF.ID = MPRAF.MARKET_PARTICIPANT_ID
               AND MPRAF.ROLE_ID = 6                            /* 6 = FRMP */
               AND MPAF.ID <> SMRFRMP.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
          AS ROLE_ALTFRMP,
       (SELECT MPARP.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPARP, MARKET_PARTICIPANT_ROLE MPRARP
         WHERE     MPARP.ID = MPRARP.MARKET_PARTICIPANT_ID
               AND MPRARP.ROLE_ID = 2
               AND MPARP.ID <> SMRRP.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
          AS ROLE_ALTRP,
       (SELECT MPAMD.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPAMD, MARKET_PARTICIPANT_ROLE MPRAMD
         WHERE     MPAMD.ID = MPRAMD.MARKET_PARTICIPANT_ID
               AND MPRAMD.ROLE_ID = 5                            /* 5 = MDP */
               AND MPAMD.ID <> SMRMDP.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
          AS ROLE_ALTMDP,
       (SELECT MPAMB.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPAMB, MARKET_PARTICIPANT_ROLE MPRAMB
         WHERE     MPAMB.ID = MPRAMB.MARKET_PARTICIPANT_ID
               AND MPRAMB.ROLE_ID = 3                             /* 3= MPB */
               AND MPAMB.ID <> SMRMPB.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
          AS ROLE_ALTMPB,
       S.NAME AS NMI_STATUS,
       SS.SERVICEPOINT AS NMI,
       'zCol' AS zCol
  FROM SERVICEPOINT_STATUS SS
       JOIN NMI_DETAILS_FIN NDF ON SS.SERVICEPOINT = NDF.NMI
       JOIN STATUS S ON S.ID = SS.STATUS_ID
       JOIN SERVICEPOINT_REF_ATTRIBUTE SRA
          ON SRA.SERVICEPOINT = SS.SERVICEPOINT
       JOIN SERVICEPOINT_REF_ATTRIBUTE SRA2
          ON SRA2.SERVICEPOINT = SS.SERVICEPOINT
       JOIN SERVICEPOINT_MP_ROLE SMRRP
          ON SMRRP.SERVICEPOINT = SS.SERVICEPOINT AND SMRRP.ROLE_ID = 2 /* 2 = RP   */
       JOIN MARKET_PARTICIPANT MPRP
          ON     MPRP.ID = SMRRP.MARKET_PARTICIPANT_ID
             AND EXISTS
                    (SELECT 1
                       FROM ORGANISATION_PARTICIPANT
                      WHERE PARTICIPANT_ID = MPRP.ID)
       JOIN SERVICEPOINT_MP_ROLE SMRMDP
          ON SMRMDP.SERVICEPOINT = SS.SERVICEPOINT AND SMRMDP.ROLE_ID = 5 /* 5 = MDP  */
       JOIN MARKET_PARTICIPANT MPMDP
          ON     MPMDP.ID = SMRMDP.MARKET_PARTICIPANT_ID
             AND EXISTS
                    (SELECT 1
                       FROM ORGANISATION_PARTICIPANT
                      WHERE PARTICIPANT_ID = MPMDP.ID)
       JOIN SERVICEPOINT_MP_ROLE SMRMPB
          ON SMRMPB.SERVICEPOINT = SS.SERVICEPOINT AND SMRMPB.ROLE_ID = 3 /* 3 = MPB  */
       JOIN MARKET_PARTICIPANT MPMPB
          ON     MPMPB.ID = SMRMPB.MARKET_PARTICIPANT_ID
             AND EXISTS
                    (SELECT 1
                       FROM ORGANISATION_PARTICIPANT
                      WHERE PARTICIPANT_ID = MPMPB.ID)
       JOIN SERVICEPOINT_MP_ROLE SMRMPC
          ON SMRMPC.SERVICEPOINT = SS.SERVICEPOINT AND SMRMPC.ROLE_ID = 4 /* 4 = MPC  */
       JOIN MARKET_PARTICIPANT MPMPC
          ON MPMPC.ID = SMRMPC.MARKET_PARTICIPANT_ID
       JOIN SERVICEPOINT_MP_ROLE SMRLNSP
          ON SMRLNSP.SERVICEPOINT = SS.SERVICEPOINT AND SMRLNSP.ROLE_ID = 1 /* 1 = LNSP */
       JOIN MARKET_PARTICIPANT MPLNSP
          ON MPLNSP.ID = SMRLNSP.MARKET_PARTICIPANT_ID
       JOIN SERVICEPOINT_MP_ROLE SMRFRMP
          ON SMRFRMP.SERVICEPOINT = SS.SERVICEPOINT AND SMRFRMP.ROLE_ID = 6 /* 6 = FRMP */
       JOIN MARKET_PARTICIPANT MPFRMP
          ON MPFRMP.ID = SMRFRMP.MARKET_PARTICIPANT_ID
 WHERE     1 = 1
       AND S.NAME <> 'X'
       AND SRA.VALUE = '<NMI_Size>'
       AND SRA.END_DT > SYSDATE
       AND SRA2.VALUE = '<Meter_Type>'
       AND SRA2.END_DT > SYSDATE
       AND SS.END_DT > SYSDATE
       AND SMRRP.END_DT > SYSDATE
       AND SMRMDP.END_DT > SYSDATE
       AND SMRMPB.END_DT > SYSDATE
       AND SMRMPC.END_DT > SYSDATE
       AND SMRLNSP.END_DT > SYSDATE
       AND SMRFRMP.END_DT > SYSDATE
       AND ROWNUM <= 1
       AND 1 = 1