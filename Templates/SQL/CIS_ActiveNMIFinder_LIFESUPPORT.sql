select replace(rtrim(xmlagg(xmlelement(NMI, ':'||NMI||':,')).extract('//text()'),','),':',chr(39)) as CIS_NMI_LIST
from (
    select distinct t056.TXT_NAT_SUPP_POINT as NMI
    from TVP056SERVPROV t056,
         TVP202SERVPROVEQP t202,
         TVP054SERVPROVRESP t054,
         TVP024CUSTACCTROLE t024,
         TVP163EQUIPINST t163,
         TVP063EQUIPMENT t063,
         (select t163_count.NO_PROPERTY,
                 count(t163_count.NO_EQUIPMENT) as METER_COUNT
          from TVP163EQUIPINST t163_count
          where t163_count.ST_EQUIP_INST = 'A'  
            and t163_count.TP_EQUIPMENT  = '<Meter_Type_CIS>' 
          group by t163_count.NO_PROPERTY) equip_count
    where t056.CD_COMPANY_SYSTEM  = t202.CD_COMPANY_SYSTEM 
      and t056.NO_PROPERTY        = t202.NO_PROPERTY
      and t056.NO_SERV_PROV       = t202.NO_SERV_PROV
      and t056.CD_SERVICE_PROV    = 'E'
      and t056.ST_SERV_PROV       = 'A'     
      and t056.CD_COMPANY_SYSTEM  = t054.CD_COMPANY_SYSTEM
      and t056.NO_PROPERTY        = t054.NO_PROPERTY
      and t056.NO_SERV_PROV       = t054.NO_SERV_PROV
      and t054.DT_END is null
      and t054.CD_COMPANY_SYSTEM  = t024.CD_COMPANY_SYSTEM
      and t054.NO_ACCOUNT         = t024.NO_ACCOUNT
      and t024.TP_CUST_ACCT_ROLE  = 'P'     
      and nvl(t024.DT_END,sysdate+1) > sysdate  
      and t202.CD_COMPANY_SYSTEM  = t163.CD_COMPANY_SYSTEM
      and t202.NO_COMBINE_163     = t163.NO_COMBINE_163
      and t163.CD_COMPANY_SYSTEM  = t063.CD_COMPANY_SYSTEM
      and t163.NO_EQUIPMENT       = t063.NO_EQUIPMENT
      and t163.ST_EQUIP_INST      = 'A'    
      and t163.TP_EQUIPMENT       = '<Meter_Type_CIS>'    
      and equip_count.NO_PROPERTY = t056.NO_PROPERTY
      and equip_count.METER_COUNT = 1       
      and t056.TXT_NAT_SUPP_POINT  in (
                select t056.TXT_NAT_SUPP_POINT
                from TVP097PROPSCALERT t097
                where t056.CD_COMPANY_SYSTEM  = t097.CD_COMPANY_SYSTEM
                  and t056.NO_PROPERTY        = t097.NO_PROPERTY
                  and t097.CD_SPEC_COND_TP = 'LIFE'
                  and t097.DT_END is null)
      and t056.TXT_NAT_SUPP_POINT  not in (
                select t056.TXT_NAT_SUPP_POINT
                from TVP109WORKORDER t109
                where t056.CD_COMPANY_SYSTEM = t109.CD_COMPANY_SYSTEM
                  and t056.NO_PROPERTY       = t109.NO_PROPERTY
                  and t109.ST_WORK_ORDER in ('PR', 'W', 'OP'))
     AND t056.TXT_NAT_SUPP_POINT  NOT IN        
           (SELECT NMI FROM TEST_DATA_KEY_REGISTER 
             WHERE IS_RETIRED_YN = 'N'
               AND IS_IN_USE_YN  = 'Y') 
      and rownum < 30
)