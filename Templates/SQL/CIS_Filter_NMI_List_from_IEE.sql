SELECT DISTINCT(replace(rtrim(xmlagg(xmlelement(NMI, ':'||t056.TXT_NAT_SUPP_POINT||':,')).extract('//text()'),','),':',chr(39))) as NMI
            FROM TVP056SERVPROV t056,
                 TVP202SERVPROVEQP t202,
                 TVP163EQUIPINST t163,
                 TVP063EQUIPMENT t063,
                 TVP054SERVPROVRESP t054,
                 TVP808METRDATASTRM t808,
                 TVP024CUSTACCTROLE t024,
                 (  SELECT t163_count.NO_PROPERTY,
                           COUNT (DISTINCT t163_count.NO_EQUIPMENT)
                              AS METER_COUNT,
                           COUNT (t808_count.NO_DATASTREAM) AS TOTAL_DS_COUNT,
                           COUNT (DISTINCT t808_count.NO_DATASTREAM)
                              AS MAX_METER_DS_COUNT
                      FROM TVP163EQUIPINST t163_count,
                           TVP808METRDATASTRM t808_count
                     WHERE     t163_count.CD_COMPANY_SYSTEM =
                                  t808_count.CD_COMPANY_SYSTEM
                           AND t163_count.NO_EQUIPMENT = t808_count.NO_EQUIPMENT
                           AND t163_count.TP_EQUIPMENT = t808_count.TP_EQUIPMENT
                           AND t163_count.ST_EQUIP_INST = 'A'
                           AND t163_count.TP_EQUIPMENT = 'E1'
                           AND t808_count.TS_END IS NULL
                           AND t808_count.NO_METER_LOG <> ' '
                  GROUP BY t163_count.NO_PROPERTY) equip_count,
                 TVP058TARIFFASSGN t058,
                 TVP807INSTDSASSGN t807
           WHERE     t056.CD_COMPANY_SYSTEM = t202.CD_COMPANY_SYSTEM
                 AND t056.NO_PROPERTY = t202.NO_PROPERTY
                 AND t056.NO_SERV_PROV = t202.NO_SERV_PROV
                 AND t202.CD_COMPANY_SYSTEM = t163.CD_COMPANY_SYSTEM
                 AND t202.NO_COMBINE_163 = t163.NO_COMBINE_163
                 AND t163.CD_COMPANY_SYSTEM = t063.CD_COMPANY_SYSTEM
                 AND t163.NO_EQUIPMENT = t063.NO_EQUIPMENT
                 AND t163.ST_EQUIP_INST = 'A'
                 AND t163.TP_EQUIPMENT = 'E1'
                 AND t056.CD_COMPANY_SYSTEM = t054.CD_COMPANY_SYSTEM
                 AND t056.NO_PROPERTY = t054.NO_PROPERTY
                 AND t056.NO_SERV_PROV = t054.NO_SERV_PROV
                 AND t054.DT_END IS NULL
                 AND t054.NO_ACCOUNT = t024.NO_ACCOUNT
                 AND t024.TP_CUST_ACCT_ROLE = 'P'
                 AND NVL (t024.DT_END, SYSDATE + 1) > SYSDATE
                 AND equip_count.NO_PROPERTY = t054.NO_PROPERTY
                 AND equip_count.METER_COUNT = 1
                 AND equip_count.TOTAL_DS_COUNT = 1
                 AND equip_count.MAX_METER_DS_COUNT = 1
                 AND t163.CD_COMPANY_SYSTEM = t808.CD_COMPANY_SYSTEM
                 AND t163.NO_EQUIPMENT = t808.NO_EQUIPMENT
                 AND t163.TP_EQUIPMENT = t808.TP_EQUIPMENT
                 AND t808.CD_DSTREAM_SPEC = 'E'
                 AND t808.NO_METER_LOG = '1'
                 AND t054.CD_COMPANY_SYSTEM = t058.CD_COMPANY_SYSTEM
                 AND t054.NO_COMBINE_054 = t058.NO_COMBINE_054
                 AND t058.DT_END IS NULL
                 AND UPPER(t058.cd_tariff) = '<tariff_type>'||'I'
                 AND t807.CD_COMPANY_SYSTEM = t808.CD_COMPANY_SYSTEM
                 AND t807.NO_COMBINE_808 = t808.NO_COMBINE_808
                 AND 1 =
                        (SELECT COUNT (tvp056.TXT_NAT_SUPP_POINT)
                           FROM TVP056SERVPROV tvp056
                          WHERE     tvp056.ST_SERV_PROV = 'A'
                                AND tvp056.NO_PROPERTY = t056.NO_PROPERTY)
                 AND t056.TXT_NAT_SUPP_POINT NOT IN (SELECT t056.TXT_NAT_SUPP_POINT
                                                       FROM TVP097PROPSCALERT t097
                                                      WHERE     t056.CD_COMPANY_SYSTEM =
                                                                   t097.CD_COMPANY_SYSTEM
                                                            AND t056.NO_PROPERTY =
                                                                   t097.NO_PROPERTY
                                                            AND t097.DT_END
                                                                   IS NULL)
                 AND (SELECT COUNT (1)
                        FROM TVP805AGGRREQSHDR t805
                       WHERE     t805.NO_COMBINE_054 = t054.NO_COMBINE_054
                             AND t805.NO_ACCOUNT = t054.NO_ACCOUNT
                             AND t054.NO_PROPERTY = t056.NO_PROPERTY
                             AND t054.NO_SERV_PROV = t056.NO_SERV_PROV
                             AND t805.ST_AGGREGATION_422 IN ('REQ')) = 0
                 AND t056.TXT_NAT_SUPP_POINT IN (<Nmi_list>)
AND t056.TXT_NAT_SUPP_POINT NOT IN(Select NMI from TEST_DATA_KEY_REGISTER)