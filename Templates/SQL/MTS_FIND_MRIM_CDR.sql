select SERVICEPOINT AS RESULT
from (
    select ss.SERVICEPOINT 
    from SERVICEPOINT_STATUS ss
    join STATUS s on ss.STATUS_ID = s.ID
    join SERVICEPOINT_REF_ATTRIBUTE sra on sra.SERVICEPOINT = ss.SERVICEPOINT
    where sra.ATTRIBUTE_TYPE_ID = 1     
      and sra.VALUE = 'MRIM'
      and sra.END_DT > sysdate
      and s.NAME = 'A'     
      and ss.END_DT > sysdate
      and 0 = (select count(*)
               from SERVICEPOINT_REF_ATTRIBUTE sra_2
               where sra_2.SERVICEPOINT = ss.SERVICEPOINT
                 and sra_2.ATTRIBUTE_TYPE_ID = 2    
                 and sra_2.VALUE = 'RWD'
                 and sra_2.END_DT > sysdate)
      and sra.SERVICEPOINT in (select distinct frmp.SERVICEPOINT
                               from SERVICEPOINT_MP_ROLE frmp, 
                                    SERVICEPOINT_MP_ROLE lr
                               where frmp.SERVICEPOINT = lr.SERVICEPOINT
                                 and frmp.ROLE_ID = 6
                                 and lr.ROLE_ID = 8
                                 and frmp.END_DT > sysdate
                                 and lr.END_DT > sysdate                                 
                                 and frmp.MARKET_PARTICIPANT_ID = lr.MARKET_PARTICIPANT_ID)                                      
     and 0 = (select count(1)
                from B2B_CUSTOMER_DETAILS_REQUEST cdr
               where cdr.nmi = substr(ss.SERVICEPOINT,1,10)
                 and trunc(updated_dt) = trunc(sysdate))
      and rownum <= 1
)

