Select replace(rtrim(xmlagg(xmlelement(NMI, ':'||NMI||':,')).extract('//text()'),','),':',chr(39)) as CIS_NMI_LIST 
From ( SELECT DISTINCT TVP056.TXT_NAT_SUPP_POINT AS NMI
from VPO999.TVP056SERVPROV TVP056
where ST_SERV_PROV = 'X'
and CD_SERVICE_PROV = 'E'
and ROWNUM < 20)