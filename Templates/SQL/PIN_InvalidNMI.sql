WITH RESERVED_NMIS AS
     (SELECT NMI
        FROM TEST_DATA_KEY_REGISTER TDR
       WHERE (TDR.IS_RETIRED_YN = 'Y'
           OR TDR.IS_IN_USE_YN = 'Y'
           OR TDR.IS_RESERVED_YN = 'Y')
          AND TDR.CREATED_TS > TRUNC(SYSDATE)),
     NMIFILTER AS
     (SELECT SS.SERVICEPOINT AS NMI
        FROM SERVICEPOINT_STATUS SS
        JOIN STATUS ST ON ST.ID=SS.STATUS_ID AND ST.NAME='A'
       MINUS
      SELECT NMI FROM RESERVED_NMIS)
SELECT '12345678907' as NMI,
       MP.PARTICIPANT_NAME AS FROM_XML,
       MP2.PARTICIPANT_NAME AS TO_XML
  FROM NMIFILTER NF
  JOIN SERVICEPOINT_MP_ROLE SMR ON SMR.SERVICEPOINT=NF.NMI AND SMR.END_DT > SYSDATE
  JOIN MARKET_PARTICIPANT MP ON MP.ID=SMR.MARKET_PARTICIPANT_ID
  JOIN ROLE RO ON RO.ID=SMR.ROLE_ID AND RO.NAME='<FROM_ROLE>'
  JOIN SERVICEPOINT_MP_ROLE SMR2 ON SMR2.SERVICEPOINT=NF.NMI AND SMR2.END_DT > SYSDATE
  JOIN CONFIGURATION_VARIABLE CV ON CV.NAME='Organisation'
  JOIN ORGANISATION ORG ON ORG.NAME=CV.VALUE
  JOIN ORGANISATION_PARTICIPANT ORG_PARTI ON ORG_PARTI.ORGANISATION_ID=ORG.ID
  JOIN MARKET_PARTICIPANT MP2 ON MP2.ID=ORG_PARTI.PARTICIPANT_ID
  JOIN MARKET_PARTICIPANT_ROLE MPR2 ON MPR2.MARKET_PARTICIPANT_ID=MP2.ID
  JOIN ROLE RO2 ON RO2.ID=MPR2.ROLE_ID AND RO2.NAME='<TO_ROLE>'
WHERE ROWNUM <= 1