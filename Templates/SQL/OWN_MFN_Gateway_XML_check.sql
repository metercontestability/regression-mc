SELECT EXTRACTVALUE(VALUE(TBL_X), 'Transaction/@transactionID') AS TRANSACTIONID
     , EXTRACTVALUE(VALUE(TBL_X), 'Transaction/MeterFaultAndIssueNotification/NMI') AS Trans_Ack_NMI
     , EXTRACTVALUE(VALUE(TBL_X), 'Transaction/MeterFaultAndIssueNotification/DateIdentified') AS Trans_Ack_DATEIDENTIFIED
     , EXTRACTVALUE(VALUE(TBL_X), 'Transaction/MeterFaultAndIssueNotification/SupplyOn') AS Trans_Ack_SUPPLYON
	 , EXTRACTVALUE(VALUE(TBL_X), 'Transaction/MeterFaultAndIssueNotification/SupplyOff') AS Trans_Ack_SUPPLYOFF
	 , RTRIM(REPLACE(REPLACE(EXTRACT(VALUE(TBL_X), 'Transaction/MeterFaultAndIssueNotification/Meters/Meter/SerialNumber'), '<SerialNumber>', ''), '</SerialNumber>', ' ')) AS Trans_Ack_METERSERIALNUMBER
	 , EXTRACTVALUE(VALUE(TBL_X), 'Transaction/MeterFaultAndIssueNotification/ReasonForNotice') AS Trans_Ack_REASONFORNOTICE
     , EXTRACTVALUE(VALUE(TBL_X), 'Transaction/MeterFaultAndIssueNotification/Notes') AS Trans_Ack_NOTES
    , GD.CONTENTS
    FROM GW_DOCUMENT_CONTENTS GD
, TABLE(XMLSEQUENCE(EXTRACT(XMLTYPE(GD.CONTENTS), '*/Transactions/Transaction'))) TBL_X    
    WHERE GD.DOCUMENT_ID IN (SELECT DOCUMENT_ID FROM GW_MESSAGE
                            WHERE ID IN ( (SELECT MESSAGE_ID FROM GW_TRANSACTION GT
                                            WHERE GT.INIT_TRANSACTION_ID IN ('<INIT_TRANSACTION_ID>'))))
	AND EXTRACTVALUE(VALUE(TBL_X), 'Transaction/@transactionID') = '<INIT_TRANSACTION_ID>'