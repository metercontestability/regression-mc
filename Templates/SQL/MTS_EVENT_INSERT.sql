BEGIN
    PKG_MTS_XML_QUEUE.PRO_ENQUEUE(p_queue_name => 'SQ_INTERFACE_OUT'
    , p_xml_payload => XMLTYPE('<mts:EventAlert xmlns:mts="http://www.itron.com/mts/2008/08" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.itron.com/mts/2008/08 eventalert.xsd" correlationId="<INIT_TRANSACTION_ID>" dateTime="2017-05-30T14:04:22+10:00" eventType="SORD">
  <mts:MarketDocumentOriginator><INITIATOR></mts:MarketDocumentOriginator>
  <mts:TransactionID>SOR_<INIT_TRANSACTION_ID></mts:TransactionID>
  <mts:EventGroup><PARAM_EVENT_GROUP></mts:EventGroup>
</mts:EventAlert>'), 
    p_corrid => '<INIT_TRANSACTION_ID>');
    COMMIT;
END;