SELECT DISTINCT TVP036.NM_PREFERRED AS RETAILER ,
  TVP707.TXT_IDENTIFIER             AS RETAILER_CODE ,
  TVP358.DS_REF_TAB                 AS RETAILER_NAME
FROM TVP036LEGALENTITY TVP036,
  TVP707MARKPARTICIP TVP707,
  CIS_RETAILER CR,
  TVP358REFTAB TVP358
WHERE TVP036.NO_LEGAL_ENTITY          = TVP707.NO_LEGAL_ENTITY
AND TVP707.DT_END_ROLE               IS NULL
AND TVP707.CD_MARKET_ROLE             = 'FRMP'
AND NVL(TVP707.DT_END_ROLE,SYSDATE+1) > SYSDATE
AND TRIM(CR.RETAILER)                 = TRIM(TVP707.TXT_IDENTIFIER)
AND TRIM(CR.RETAILER_CODE)            = TRIM(TVP358.CD_REF_TAB)
AND TVP358.TP_REF_TAB                 = 800
AND TVP707.TXT_IDENTIFIER             = '<PARAM_RETAILER>'