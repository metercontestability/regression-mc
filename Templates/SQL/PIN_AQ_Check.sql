select aq.transaction_detail_id, aqr.name as Actual_AQ_Name, aq.nmid, es.name Actual_AQ_Status, s.name Actual_AQ_Severity, aq.created_dt, aq.updated_dt 
from activity_queue aq, activity_queue_reason_code aqr, event_status es, severity s
where aq.reason_code_id = aqr.id
and aq.event_status_id = es.id
and aq.severity_id = s.id
and aq.transaction_detail_id = (Select td.id from transaction_detail td where td.transactionid = '<INIT_TRANSACTION_ID>')