SELECT *
    from SERVICEPOINT_STATUS ss
    join STATUS s on s.ID = ss.STATUS_ID
    join SERVICEPOINT_REF_ATTRIBUTE sra3 on sra3.SERVICEPOINT = ss.SERVICEPOINT
    where s.NAME = 'D'    /*  -- Not Active NMI */
      and sra3.ATTRIBUTE_TYPE_ID = 5   /*  -- NMI class */
      and sra3.VALUE = '<NMI_Size>'  /*  <<===  'SMALL' 'LARGE' */
      and sra3.END_DT > sysdate
      and ss.DE_ENERGISED_METHOD_ID = 7
      and ss.end_dt > SYSDATE
      and ss.servicepoint NOT IN (SELECT SERVICEPOINTID
                                  FROM IEE_FLATCONFIGPHYSICAL)
      and ss.SERVICEPOINT not in      /* All NMIs are 10 characters and an 11th checksum-digit */
           (select NMI FROM TEST_DATA_KEY_REGISTER 
             WHERE IS_RETIRED_YN = 'N'
               AND IS_IN_USE_YN  = 'Y') /* the sub-query returns a row if the NMI is in use */
and SS.SERVICEPOINT NOT IN (SELECT DISTINCT (CCD.NMID) FROM CAT_CR_DATA CCD WHERE CCD.UPDATED_DT >= SYSDATE - 10)
      and rownum <= <rownum>