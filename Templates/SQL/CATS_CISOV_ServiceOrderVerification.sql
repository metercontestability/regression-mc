SELECT TVP109.CD_WORK_ORDER AS SONUM,
       TVP109.TP_WORK_ORDER AS WORKORDERTYPE,
       TVP523.CD_WORK_ORDER_TASK AS WORKORDERTASK,
       TVP109.CD_WO_REASON AS WORKORDERREASON,
       TO_CHAR (TVP523.DT_ORD_EFFECTIVE, 'DD-MON-YY') AS SOEFFECTIVEDATE,
       (SELECT T358.DS_REF_TAB
          FROM TVP358REFTAB T358
         WHERE     T358.CD_REF_TAB = TVP109.CD_WAIVE_RSN_428
               AND T358.TP_REF_TAB = 428)
          AS WAIVEREASON,
       TVP109.ST_WORK_ORDER AS WORKORDERSTATUS,
       TVP109.IND_WORK_COMPLETED AS WORKCOMPLETEDFLAG
  FROM TVP109WORKORDER TVP109
       LEFT JOIN TVP056SERVPROV TVP056
          ON     TVP109.NO_PROPERTY = TVP056.NO_PROPERTY
             AND TVP109.CD_COMPANY_SYSTEM = TVP056.CD_COMPANY_SYSTEM
       LEFT JOIN TVP523WORKORDDTL TVP523
          ON TVP109.CD_WORK_ORDER = TVP523.CD_WORK_ORDER      
 WHERE     TVP056.TXT_NAT_SUPP_POINT = '<NMI>'
       AND TVP523.NO_TASK = 1
       AND TRUNC(TVP109.TS_CREATED) = TRUNC(SYSDATE)
       AND TRUNC (TVP523.DT_ORD_EFFECTIVE) = TRUNC (SYSDATE)