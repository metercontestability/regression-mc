            select distinct
                          s.servicepointid AS NMI
                        , MSS.SUFFIX as RegisterId
                        ,MSS.SUFFIX as Suffix
                        , spc.spcnodeid
                        , upper(m.meterid) as SerialNumber
                        , U.UNITID as UnitOfMeasure
                        , UOM.UNITOFMEASUREID
                        , nl2.effectivestartdate + 10/24 as nodelink_start  
                        , nl2.effectiveenddate + 10/24   as nodelink_end  
                        , es.statusid
                        , m.effectivestartdate + 10/24 as meter_start
                        , m.effectiveenddate + 10/24 as meter_end
                        ,M.MULTIPLIER as MULTIPLIER
                        ,'D' as STATUS
                        ,SUBSTR(M.MANUFACTURER,1,15) as MANUFACTURER
                        ,M.MODEL as MODEL
                        ,IUAD.DATAVALUE as ControlledLoad
                        ,IUAD2.DATAVALUE as NetworkTariffCode
                        ,'INTERVAL' as TimeOfDay /*HARDCODED AS THESE ARE INTERVAL METERS*/
                        ,IC.PULSEMISCMULTIPLIER as ConstantCAT
                        ,NVL(IM.DATAVALUE,' ') as LOCATION
                          ,SPG.SERVICEPOINTPROGRAMID as PROGRAMID
                   from servicepoint s
                   join nodelink nl on nl.leftnodekey = s.nodekey
                   join servicepointchannel spc on spc.nodekey = nl.rightnodekey
                   join MTS_SPC_SUFFIX_MAP MSS on MSS.SPC_ID=spc.channelnumber
                   join nodelink nl2 on nl2.leftnodekey = spc.nodekey
                   join intervalchannel ic on ic.nodekey = nl2.rightnodekey
                   join nodelink nl3 on nl3.rightnodekey = ic.nodekey
                   join meter m on m.nodekey = nl3.leftnodekey
                   join spcvalidationset svs on svs.servicepointchannelkey = spc.servicepointchannelkey
                                      JOIN SERVICEPOINTSPPGM SPSG on SPSG.SERVICEPOINTKEY=S.SERVICEPOINTKEY
                   JOIN SERVICEPOINTPROGRAM SPG on SPG.SERVICEPOINTPROGRAMKEY=SPSG.SERVICEPOINTPROGRAMKEY
                   join validationset vs on vs.validationsetkey = svs.validationsetkey
                   join entitystatus es
                   on es.entitystatuskey = m.entitystatuskey
                   join IEE_SERVICEPOINTCHANNELUDAS iuad on IUAD.SERVICEPOINT=S.SERVICEPOINTID
                   join IEE_SERVICEPOINTCHANNELUDAS iuad2 on IUAD2.SERVICEPOINT=S.SERVICEPOINTID
                   join IEE_METERATTRIBUTES IM on IM.METERKEY = M.METERKEY
                   join UNITOFMEASURE uom on UOM.UNITOFMEASUREKEY = SPC.UNITOFMEASUREKEY
                   join UNIT u on UOM.UNITKEY=U.UNITKEY
                   where 1=1
                   and (svs.effectivestartdate < nl2.effectiveenddate and svs.effectiveenddate > nl2.effectivestartdate)
                   and (m.effectivestartdate < nl2.effectiveenddate and m.effectiveenddate > nl2.effectivestartdate)
                  and S.SERVICEPOINTID = '<NMI>'
                   and M.EFFECTIVEENDDATE>SYSDATE
                   and UPPER(IUAD.UDANAME)='CONTROLLED_LOAD'
                   and UPPER(IUAD2.UDANAME)='NTC'
                   and IM.METEREFFECTIVEENDDATE>SYSDATE
                   and UPPER(IM.UDANAME)='LOCATION'
                          AND IUAD.SERVICEPOINTCHANNELNUMBER = spc.channelnumber
                AND IUAD2.SERVICEPOINTCHANNELNUMBER = SPC.CHANNELNUMBER
                AND IUAD.EFFECTIVEENDLINKDATE > SYSDATE
                AND IUAD2.EFFECTIVEENDLINKDATE > SYSDATE
                 AND SPSG.EFFECTIVEENDDATE>SYSDATE
           AND ROWNUM <= 1