<CreateServiceOrderRequest xmlns="http://www.logica.com/CISOV/API">
	<RequestHeader>
		<Version>0</Version>
		<CompanyCode>^param_company_code_egCITI^</CompanyCode>
		<UserName>^param_user_name_eg_RECONECT^</UserName>
	</RequestHeader>
	<RequestBody>
		<NMI><param_NMI></NMI>
		<ServiceOrderType><param_DataPrep_CIS_SOType_Code></ServiceOrderType>
		<ServiceProvisionType>E</ServiceProvisionType>
		<EffectiveDate><param_Date-oneweekbefore-eg-2017-02-28></EffectiveDate>
		<ServiceOrderReason><param_DataPrep_CIS_SOReason></ServiceOrderReason>
		<Equipments>
			<Equipment>
				<EquipmentType><param_Meter_Type_MTS> Meter</EquipmentType>
				<UtilityEquipmentNumber><param_UtilityEquipmentNumber></UtilityEquipmentNumber>
				<TaskType>Remove Equipment</TaskType>
			</Equipment>
		</Equipments>
	</RequestBody>
</CreateServiceOrderRequest>
