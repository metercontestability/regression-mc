-- Retrieve CATS-IN and OBJ-Out GW_DOCUMENT_CONTENTS
SELECT GDC.CONTENTS AS INBOUND_XML, TD.TRANSACTIONID AS TRANSACTIONID_IN_XML
     , CN.ID    AS CN_ID, td.id as td_id
     , CCD.NMID, CRC.CRCODE
     , S1.NAME  AS TXN_STATUS, S2.NAME  AS NOTIF_STATUS
     , MP.PARTICIPANT_NAME AS PARTI_TO_NAME
     , R.NAME   AS RNAME, RS.NAME AS ROLE_STATUS
     , MRT.NAME AS MRTNAME, CCD.REQUEST_ID, MP2.PARTICIPANT_NAME AS CR_INIT_PARTI
     ,TD.GW_FILE_TIMESTAMP, CN.UPDATED_DT, CCD.PROPOSED_DATE, CCD.ACTUAL_CHANGE_DATE, CIS.CORRELATION_ID
--     , '## CN.* ##', cn.*, '## CCD.* ##', ccd.*, '## td.* ##', td.*, '##TS##', ts.*
     , GFOUT.FILENAME AS OUTBOUND_FILE, GDCOUT.CONTENTS AS OUTBOUND_XML, '## GDC.* ##', GDC.*, coc.name, COND.FUNCTION_NAME
--     , '## COR.* ##', COR.*, '## COC.* ##', COC.*
  FROM CAT_NOTIFICATION CN ,CAT_CR_DATA CCD ,CAT_METER_READ_TYPE MRT ,CAT_CHANGE_REASON_CODE CRC 
     ,TRANSACTION_DETAIL TD, TRANSACTION_STATUS TS, STATUS S1
     ,ROLE R ,ROLE_STATUS RS ,STATUS S2, MARKET_PARTICIPANT MP, MARKET_PARTICIPANT MP2
     , CIS_INSTRUCTION CIS
     , GW_TRANSACTION       GT
     , GW_MESSAGE           GM
     , GW_DOCUMENT_CONTENTS GDC
     , CAT_OBJECTION COR 
     , CATS_NOTIF_OBJ_CONDITION COND
     , CAT_OBJECTION_CODE COC
     , TRANSACTION_DETAIL     TDOUT
     , GW_TRANSACTION       GTOUT
     , GW_MESSAGE           GMOUT
     , GW_DOCUMENT_CONTENTS GDCOUT
     , GW_FILE    GFOUT
 WHERE CN.CR_DATA_ID = CCD.ID
   AND CN.TRANSACTION_DETAIL_ID = TD.ID
   AND TS.TRANSACTION_DETAIL_ID = TD.ID
   AND TS.STATUS_ID = S1.ID
   AND MRT.ID(+) = CCD.METER_READ_TYPE_CODE_ID
   AND CRC.ID = CCD.CHANGE_REASON_CODE_ID
   AND CN.ROLE_ID = R.ID
   AND CN.ROLE_STATUS_ID = RS.ID
   AND CN.STATUS_ID = S2.ID
   AND TD.TO_PARTICIPANT_ID = MP.ID
   AND CN.INITIATING_PARTICIPANT = MP2.ID (+)
   AND CIS.TRANSACTION_DETAIL_ID (+) = TD.ID
  AND TD.GW_TRANSACTION_ID = GT.ID
  AND GT.MESSAGE_ID = GM.ID
  AND GM.DOCUMENT_ID = GDC.DOCUMENT_ID    
  AND TD.ID > 306550000  --306500000 = 1/1/2014
  AND TD.GW_FILE_TIMESTAMP > SYSDATE - 35
--/**/
  AND COR.CAT_NOTIFICATION_ID = CN.ID        /*    CN_ID from inbound query.    */
  AND COR.OBJ_CONDITION_ID = COND.ID 
  AND COND.OBJECTION_CODE_ID = COC.ID
  AND TDOUT.ID = COR.TRANSACTION_DETAIL_ID
  AND TDOUT.GW_TRANSACTION_ID = GTOUT.ID
  AND GTOUT.MESSAGE_ID = GMOUT.ID
  AND GMOUT.DOCUMENT_ID = GDCOUT.DOCUMENT_ID
  AND GMOUT.DOCUMENT_ID = GFOUT.DOCUMENT_ID
--  and ccd.nmid = '6102387807'
  AND CCD.REQUEST_ID IN (20161216102622)
--  and s2.name = 'REQ'
--  and r.name = 'RP'
--  and rs.name = 'C'
--  and crc.crcode = '6301'
--  and MRT.NAME ='SP'
--  and TD.INITIATING_TRANSACTIONID = 'NOTF-1204380186'
--  and TD.TRANSACTIONID = 'CAT473025471'
  --and rownum <=3
--order by TD.GW_FILE_TIMESTAMP desc
;


--Retrieve CATS-IN GW_DOCUMENT_CONTENTS
SELECT gdc.CONTENTS, td.TRANSACTIONID
     , CN.ID    as CN_ID
     , ccd.nmid, crc.crcode
     , s1.name  as Txn_Status, s2.name  as Notif_Status
     , MP.PARTICIPANT_NAME as PARTI_TO_NAME
     , R.NAME   AS RNAME, RS.NAME as ROLE_STATUS
     , MRT.NAME AS MRTNAME, CCD.REQUEST_ID, MP2.PARTICIPANT_NAME as CR_INIT_PARTI
     ,TD.GW_FILE_TIMESTAMP, CN.UPDATED_DT, CCD.PROPOSED_DATE, CCD.ACTUAL_CHANGE_DATE, CIS.CORRELATION_ID
     , gdc.CONTENTS, td.TRANSACTIONID
     --     , '## CN.* ##', cn.*, '## CCD.* ##', ccd.*, '## td.* ##', td.*, '##TS##', ts.*
  FROM cat_notification cn ,CAT_CR_DATA ccd ,cat_meter_read_type mrt ,cat_change_reason_code crc 
       ,transaction_detail td, transaction_status ts, status s1
       ,role r ,role_status rs ,status s2, market_participant mp, market_participant mp2
       , cis_instruction cis
     , gw_transaction       gt
     , gw_message           gm
     , gw_document_contents gdc
 WHERE cn.cr_data_id = ccd.id
   AND cn.transaction_detail_id = td.id
   AND ts.transaction_detail_id = td.id
   AND ts.status_id = s1.id
   AND mrt.id(+) = ccd.meter_read_type_code_id
   AND crc.id = ccd.change_reason_code_id
   and cn.role_id = r.id
   and cn.role_status_id = rs.id
   and cn.status_id = s2.id
   and TD.TO_PARTICIPANT_ID = MP.ID
   and CN.INITIATING_PARTICIPANT = MP2.ID (+)
   and CIS.TRANSACTION_DETAIL_ID (+) = TD.ID
  and td.GW_TRANSACTION_ID = gt.ID
  and gt.MESSAGE_ID = gm.id
  and gm.DOCUMENT_ID = gdc.DOCUMENT_ID    
  and td.id > 306550000  --306500000 = 1/1/2014
  and TD.GW_FILE_TIMESTAMP > sysdate - 15
--  and ccd.nmid = '6102387807'
  and CCD.REQUEST_ID in (20161130170145)
--  and s2.name = 'REQ'
--  and r.name = 'RP'
--  and rs.name = 'C'
--  and crc.crcode = '6301'
--  and MRT.NAME ='SP'
--  and TD.INITIATING_TRANSACTIONID = 'NOTF-1204380186'
--  and TD.TRANSACTIONID = 'CAT473025471'
  --and rownum <=3
--order by TD.GW_FILE_TIMESTAMP desc
;

-- Outgoing CATS Objections ----
SELECT GF.FILENAME      
       , GDC.CONTENTS, COR.*, COND.*, COC.*, GDC.*
FROM CAT_OBJECTION COR 
     , CATS_NOTIF_OBJ_CONDITION COND
     , CAT_OBJECTION_CODE COC
     , TRANSACTION_DETAIL     TD
     , GW_TRANSACTION       GT
     , GW_MESSAGE           GM
     , GW_DOCUMENT_CONTENTS GDC
     , GW_FILE    GF
WHERE 1=1
   AND COR.OBJ_CONDITION_ID = COND.ID 
   AND COND.OBJECTION_CODE_ID = COC.ID
   AND TD.ID = COR.TRANSACTION_DETAIL_ID
   AND TD.GW_TRANSACTION_ID = GT.ID
   AND GT.MESSAGE_ID = GM.ID
   AND GM.DOCUMENT_ID = GDC.DOCUMENT_ID
   AND GM.DOCUMENT_ID = GF.DOCUMENT_ID
   AND COR.CAT_NOTIFICATION_ID           = 547210793        /*    CN_ID from inbound query.    */