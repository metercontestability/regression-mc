Select replace(rtrim(xmlagg(xmlelement(NMI, ':'||NMI||':,')).extract('//text()'),','),':',chr(39)) as CIS_NMI_LIST 
From ( SELECT DISTINCT T056.TXT_NAT_SUPP_POINT AS NMI
from VPO999.TVP056SERVPROV T056
where ST_SERV_PROV = 'F'
and CD_SERVICE_PROV = 'E'
and t056.TXT_NAT_SUPP_POINT not in (
                select t056.TXT_NAT_SUPP_POINT
                from TVP097PROPSCALERT t097
                where t056.CD_COMPANY_SYSTEM  = t097.CD_COMPANY_SYSTEM
                  and t056.NO_PROPERTY        = t097.NO_PROPERTY
                  and t097.DT_END is null
                union
             select t056.TXT_NAT_SUPP_POINT
                from TVP109WORKORDER t109
                where t056.CD_COMPANY_SYSTEM = t109.CD_COMPANY_SYSTEM
                  and t056.NO_PROPERTY       = t109.NO_PROPERTY
                  and t109.ST_WORK_ORDER in ('PR', 'W', 'OP'))
and ROWNUM < 20)