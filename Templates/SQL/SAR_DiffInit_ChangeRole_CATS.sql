WITH NMI_DETAILS_FIN as (
 SELECT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
WHERE TDR.USED_BY_CASE_NAME= '<Parent_Scenario_ID>'
MINUS
SELECT DISTINCT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE     (   TDR.IS_RETIRED_YN = 'Y'
                     OR TDR.IS_IN_USE_YN = 'Y'
                     OR TDR.IS_RESERVED_YN = 'Y')
                AND TDR.CREATED_TS > TRUNC (SYSDATE)
                AND USED_BY_CASE_NAME not like '%CATS%'),
   NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>' ),
     NMI_FROM_ROLE
     AS (SELECT NMI,
                MP.PARTICIPANT_NAME AS MP_FROM,
                RO.NAME AS FROM_ROLE,
                NTR.MP_TO
           FROM NMI_DETAILS_FIN ND,
                NMI_TO_ROLE NTR,
                SERVICEPOINT_MP_ROLE SMR,
                SERVICEPOINT_MP_ROLE SMR2,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE RO
          WHERE     SMR.SERVICEPOINT = ND.NMI
                AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
                AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
                AND SMR2.ROLE_ID = NTR.R_ID
                AND SMR2.end_dt > SYSDATE
                AND SMR.END_DT > SYSDATE
                AND MPR.ROLE_ID = RO.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.MARKET_PARTICIPANT_ID NOT IN
                                    (SELECT DISTINCT SM.MARKET_PARTICIPANT_ID
                                        FROM SERVICEPOINT_MP_ROLE sm, MARKET_PARTICIPANT_ROLE mpr
                                        WHERE     mpr.role_id = SM.ROLE_ID
                                         AND SM.ROLE_ID IN (2, 3, 6)
                                        AND SMR.SERVICEPOINT = SM.SERVICEPOINT
                                        AND SM.END_DT > SYSDATE
                                        UNION 
                                        select cat1.participant_id as MC_NAME from CAT_CR_ROLE_ASSIGNMENT cat1,MARKET_PARTICIPANT mp
                                        where  cat1.cr_data_id in (
                                        select max(id) from CAT_CR_DATA
                                        where  nmid=SUBSTR(ND.NMI,1,10)
                                        and CHANGE_REASON_CODE_ID ='<CATS_CR>'  )
                                        and CAT1.ROLE_ID=SMR.ROLE_ID
                                            and MP.ID=cat1.participant_id )
                AND RO.ID = SMR.ROLE_ID
                AND RO.NAME ='<From_Role>'  )
SELECT NFR.NMI AS NMI, NFR.MP_FROM AS FROM_XML, NFR.MP_TO AS TO_XML
  FROM NMI_FROM_ROLE NFR
 WHERE 1 = 1 AND ROWNUM <= 1