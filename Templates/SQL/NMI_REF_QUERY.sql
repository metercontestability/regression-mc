select TRANSACTION_TYPE
     , RESPONSE_STATUS
     , count(*) as TOTAL
from (
    select stt.TRANSACTION_TYPE 
        , case
                when dbms_lob.instr(std.CDF_RESPONSE, 'Error') > 0 then 'ERROR'
                when dbms_lob.instr(std.CDF_RESPONSE, 'Success') > 0 then 'SUCCESS'
                else 'NO RESPONSE'
          end as response_status
    from SOA_TRANSACTION st,
         SOA_TRANSACTION_DETAIL std,
         SOA_TRANSACTION_TYPE stt
    where st.ID = std.TRANSACTION_ID (+)
    and st.TRANSACTION_TYPE_ID = stt.ID
    and st.CREATED_DT > trunc(sysdate)-40
    and st.ID in (select frc.TRANSACTION_ID
                  from T_FRC_NMIPROPCHG frc
                  where frc.CONTEXT_ID = '<NMI>')
)
group by TRANSACTION_TYPE, RESPONSE_STATUS
having TRANSACTION_TYPE = '<temp_template_type>'
