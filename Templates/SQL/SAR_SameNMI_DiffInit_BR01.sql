/* Formatted on 6/06/2017 4:45:59 PM (QP5 v5.256.13226.35510) */
WITH RECEIVED_TODAY
     AS (SELECT DISTINCT SAR.NMI || SAR.NMI_CHECKSUM AS NMI,
                         TD.FROM_PARTICIPANT_ID,
                         MP.PARTICIPANT_NAME AS FROM_NAME,
                         TD.FROM_ROLE_ID
           FROM SITE_ACCESS_REQ SAR,
                TRANSACTION_DETAIL TD,
                MARKET_PARTICIPANT MP,
                ROLE R
          WHERE     SAR.TRANSACTION_DETAIL_ID = TD.ID
                AND TD.GW_FILE_TIMESTAMP > TRUNC (SYSDATE)
                AND TD.FROM_PARTICIPANT_ID = MP.ID),
     NMI_DETAILS
     AS (SELECT SS.SERVICEPOINT AS NMI
           FROM STATUS ST,
                SERVICEPOINT_STATUS SS,
                SERVICEPOINT_REF_ATTRIBUTE SRA,
                SERVICEPOINT_REF_ATTRIBUTE SRA2
          WHERE     SS.STATUS_ID = ST.ID
                AND ST.NAME = 'A'
                AND SS.END_DT > SYSDATE
                AND SRA.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA.VALUE = '<Meter_Type>'
                AND SRA.END_DT > SYSDATE
                AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA2.VALUE = '<NMI_Size>'
                AND SRA2.END_DT > SYSDATE),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>'),
     NMI_FROM_ROLE
     AS (SELECT ND.NMI,
                MP.PARTICIPANT_NAME AS MP_FROM,
                MP.ID AS FROM_MP_ID,
                RO.NAME AS FROM_ROLE,
                NTR.MP_TO,
                NTR.MP_ID AS TO_MP_IF,
                SAD.ACCESS_DETAILS AS ACCESS_DETAILS,         
                SCC.DESCRIPTION AS HAZARD,
                SC.UPDATED_DT AS LASTMODDATE,
                TO_CHAR(SC.UPDATED_DT,'DD/MM/YYYY HH24:MI:SS') AS LASTMODDATEGUI  				
           FROM NMI_DETAILS ND,
                NMI_TO_ROLE NTR,
                SERVICEPOINT_MP_ROLE SMR,
                SERVICEPOINT_MP_ROLE SMR2,
                MARKET_PARTICIPANT MP,
                ROLE RO,
                RECEIVED_TODAY RT,
                SITE_ACCESS_DETAILS SAD,
                SPECIAL_CONDITION SC,
                CIS_SPECIAL_CONDITION_CODE SCC				
          WHERE     SMR.SERVICEPOINT = ND.NMI
                AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
                AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
                AND SMR2.ROLE_ID = NTR.R_ID
                AND SMR2.end_dt > SYSDATE
                AND SMR.END_DT > SYSDATE
                AND SMR.MARKET_PARTICIPANT_ID = MP.ID
                AND RO.ID = SMR.ROLE_ID
                AND RT.NMI = SMR.SERVICEPOINT
                AND RO.ID IN (2, 3, 6)
                AND SMR.ROLE_ID NOT IN (SELECT DISTINCT FROM_ROLE_ID
                                          FROM RECEIVED_TODAY
                                         WHERE NMI = SMR.SERVICEPOINT)
                AND MP.PARTICIPANT_NAME NOT IN (SELECT DISTINCT FROM_NAME
                                                  FROM RECEIVED_TODAY
                                                 WHERE NMI = SMR.SERVICEPOINT)
                AND SAD.SERVICEPOINT = ND.NMI
                AND TRIM(SAD.ACCESS_DETAILS) IS NULL
                AND SAD.UPDATED_DT > '01-OCT-2004'
                AND SC.SERVICEPOINT = ND.NMI
                AND SC.CODE = SCC.SPECIAL_CONDITION_CODE            
                AND SCC.HAZARD_YN = 'Y'
                AND SC.UPDATED_DT < SAD.UPDATED_DT      
				AND SC.END_DT > SYSDATE				
			  AND MP.ID NOT IN (SELECT DISTINCT MP.ID
                                    FROM CONFIGURATION_VARIABLE CV,
                                         ORGANISATION ORG,
                                         ORGANISATION_PARTICIPANT ORG_PARTI,
                                         MARKET_PARTICIPANT MP,
                                         MARKET_PARTICIPANT_ROLE MPR,
                                         ROLE R
                                   WHERE     CV.NAME = 'Organisation'
                                         AND ORG.NAME = CV.VALUE
                                         AND ORG_PARTI.ORGANISATION_ID =
                                                ORG.ID
                                         AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                                         AND MP.ID =
                                                MPR.MARKET_PARTICIPANT_ID
                                         AND MPR.ROLE_ID = R.ID))
SELECT NFR.NMI AS NMI, NFR.MP_FROM AS FROM_XML, NFR.MP_TO AS TO_XML,'Customer reports no access requirements' AS ACCESS_DETAILS,NFR.HAZARD as HAZARD,TO_CHAR(NFR.LASTMODDATE,'YYYY-MM-DD"T"HH24:MI:SS')||'+10:00' AS LASTMODIFIEDDATE,LASTMODDATEGUI
  FROM NMI_FROM_ROLE NFR
 WHERE 1 = 1 AND ROWNUM <= 1