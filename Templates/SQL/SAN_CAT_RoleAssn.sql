WITH NMI_DETAILS_FIN
     AS (SELECT NMI
                FROM TEST_DATA_KEY_REGISTER TDR where row_id =(
     select max(row_id)
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE TDR.USED_BY_CASE_NAME ='<Parent_Scenario_ID>' and data_state='available')
         MINUS
       SELECT DISTINCT SAN.NMI || SAN.NMI_CHECKSUM as NMI
        FROM B2B_SITE_ACCESS_NOTIFICATION SAN
            WHERE SAN.UPDATED_DT > TRUNC(SYSDATE)),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>'),
     CRselect
     AS (SELECT MAX (ccd.id) AS CR_ID
           FROM cat_cr_data ccd, NMI_DETAILS_FIN ndf
          WHERE ccd.nmid = SUBSTR (ndf.NMI, 1, 10)),
     CRreqsel
     AS (SELECT CCD.REQUEST_ID
           FROM cat_cr_data ccd, CRselect crs
          WHERE CCD.ID = crs.cr_id),
     RoleAssnSel
     AS (SELECT DISTINCT CA.PARTICIPANT_ID MP_ID, CA.ROLE_ID, CCD.NMID
           FROM cat_cr_data ccd,
                CRreqsel crr,
                CAT_CR_ROLE_ASSIGNMENT ca,
                MARKET_PARTICIPANT mp,
                ROLE r
          WHERE     CCD.REQUEST_ID = crr.request_id
                AND CCD.ID = CA.CR_DATA_ID
                AND CA.PARTICIPANT_ID = mp.id
                AND CA.ROLE_ID = r.id),
     NMI_FROM_ROLE
     AS (SELECT NMI,
                MP.PARTICIPANT_NAME AS MP_FROM,
                RO.NAME AS FROM_ROLE,
                NTR.MP_TO
           FROM NMI_DETAILS_FIN ND,
                NMI_TO_ROLE NTR,
                MARKET_PARTICIPANT MP,
                ROLE RO, MARKET_PARTICIPANT_ROLE MPR,
                RoleAssnSel RAS
          WHERE   MP.ID=RAS.MP_ID
               AND RAS.ROLE_ID=RO.ID
                AND RO.NAME = '<From_Role>')
SELECT NFR.NMI AS NMI,NFR.NMI AS SERVICEPOINT, NFR.MP_FROM AS FROM_XML, NFR.MP_TO AS TO_XML
  FROM NMI_FROM_ROLE NFR
 WHERE 1 = 1 AND ROWNUM <= 1