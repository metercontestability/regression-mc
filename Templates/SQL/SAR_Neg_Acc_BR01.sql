WITH RECEIVED_TODAY
     AS (SELECT DISTINCT
                SAR.NMI || SAR.NMI_CHECKSUM AS NMI,
                TD.TO_PARTICIPANT_ID,
                TD.FROM_PARTICIPANT_ID
           FROM SITE_ACCESS_REQ SAR, TRANSACTION_DETAIL TD
          WHERE     SAR.TRANSACTION_DETAIL_ID = TD.ID
                AND TD.GW_FILE_TIMESTAMP > TRUNC (SYSDATE)),
     NMI_DETAILS
     AS (SELECT SS.SERVICEPOINT AS NMI
           FROM STATUS ST,
                SERVICEPOINT_STATUS SS,
                SERVICEPOINT_REF_ATTRIBUTE SRA,
                SERVICEPOINT_REF_ATTRIBUTE SRA2
          WHERE     SS.STATUS_ID = ST.ID
                AND ST.NAME = 'A'
                AND SS.END_DT > SYSDATE
                AND SRA.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA.VALUE ='<Meter_Type>'
                AND SRA.END_DT > SYSDATE
                AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA2.VALUE = '<NMI_Size>'
                AND SRA2.END_DT > SYSDATE),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>'),
     NMI_FROM_ROLE
     AS (SELECT NMI,
                MP.PARTICIPANT_NAME AS MP_FROM,
                MP.ID AS FROM_MP_ID,
                RO.NAME AS FROM_ROLE,
                NTR.MP_TO,
                NTR.MP_ID AS TO_MP_IF
           FROM NMI_DETAILS ND,
                NMI_TO_ROLE NTR,
                SERVICEPOINT_MP_ROLE SMR,
                SERVICEPOINT_MP_ROLE SMR2,
                MARKET_PARTICIPANT MP,
                ROLE RO
          WHERE     SMR.SERVICEPOINT = ND.NMI
                AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
                AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
                AND SMR2.ROLE_ID = NTR.R_ID
                AND SMR2.end_dt > SYSDATE
                AND SMR.END_DT > SYSDATE
                AND SMR.MARKET_PARTICIPANT_ID = MP.ID
                AND RO.ID = SMR.ROLE_ID
                AND RO.NAME =  '<From_Role>'
AND MP.ID not in (  select distinct MP.ID FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID) )
SELECT NFR.NMI AS NMI, NFR.MP_FROM AS FROM_XML, NFR.MP_TO AS TO_XML, NULL AS ACCESS_DETAILS,NULL as HAZARD,NULL AS LASTMODIFIEDDATE,NULL AS LASTMODDATEGUI
  FROM NMI_FROM_ROLE NFR, RECEIVED_TODAY RT
WHERE     1 = 1
       AND NFR.NMI = RT.NMI
       AND NFR.FROM_MP_ID = RT.FROM_PARTICIPANT_ID
       AND ROWNUM <= 1