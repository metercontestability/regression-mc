SELECT
EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//iee:MeterID', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04"') AS METERID,
EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//iee:MeterNumber', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04"') AS METERNUMBER,
SUBSTR(EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//iee:EffectiveStartDate', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04"'),1,10) AS METEREFFECTIVESTARTDATE,
EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//iee:MeterStatus', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04"') AS METERSTATUS
FROM MTS.SQT_INTERFACE_IN T
WHERE DBMS_LOB.INSTR(T.USER_DATA.PAYLOAD_DATA.GETCLOBVAL(), '<MTR_CHANGE_CORRID>') > 0