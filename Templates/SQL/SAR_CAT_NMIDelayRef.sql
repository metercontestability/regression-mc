WITH NMI_TDR
AS (SELECT TDR.NMI, MAX(ID) as CR_ID FROM TEST_DATA_KEY_REGISTER TDR, CAT_CR_DATA CCD
WHERE TDR.USED_BY_CASE_NAME ='CR_1080_SAR_036_BR08'
                              and NMID=SUBSTR(TDR.NMI,1,10)
                                group by nmi
                                       ),
           NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID),
SELALL as (
sELECT SRA.VALUE AS NMI_SIZE,
       'MDP' AS Static_MDP,
       'RP' AS Static_RP,
       'MPB' AS Static_MPB,
       'LNSP' AS Static_LNSP,
       MPRP.PARTICIPANT_NAME AS ROLE_RP,
       MPMPB.PARTICIPANT_NAME AS ROLE_MPB,
       MPLNSP.PARTICIPANT_NAME AS ROLE_LNSP,
       MPFRMP.PARTICIPANT_NAME AS ROLE_FRMP,
       (select REQUEST_ID from CAT_CR_DATA CCD, NMI_TDR NT
       where ccd.id=NT.cr_id) as REQUEST_ID,
       (SELECT MPAF.ID
          FROM MARKET_PARTICIPANT MPAF, MARKET_PARTICIPANT_ROLE MPRAF
         WHERE     MPAF.ID = MPRAF.MARKET_PARTICIPANT_ID
               AND MPRAF.ROLE_ID = 6                            /* 6 = FRMP */
               AND MPAF.ID <> SMRFRMP.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
          AS ROLE_ALTFRMP_ID,
       (SELECT MPAMB.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPAMB,
               MARKET_PARTICIPANT_ROLE MPRAMB,
               NMI_TO_ROLE nr
         WHERE     MPAMB.ID = MPRAMB.MARKET_PARTICIPANT_ID
               AND MPRAMB.ROLE_ID = 3                             /* 3= MPB */
               AND MPAMB.ID <> SMRMPB.MARKET_PARTICIPANT_ID
               AND mpamb.id <> nr.mp_id
               AND nr.r_id = 3
               AND ROWNUM <= 1)
          AS ROLE_ALTMPB,
       S.NAME AS NMI_STATUS,
       SS.SERVICEPOINT AS NMI,
       'zCol' AS zCol
  FROM SERVICEPOINT_STATUS SS
       JOIN NMI_TDR NDF ON SS.SERVICEPOINT = NDF.NMI
       JOIN STATUS S ON S.ID = SS.STATUS_ID
       JOIN SERVICEPOINT_REF_ATTRIBUTE SRA
          ON SRA.SERVICEPOINT = SS.SERVICEPOINT
       JOIN SERVICEPOINT_REF_ATTRIBUTE SRA2
          ON SRA2.SERVICEPOINT = SS.SERVICEPOINT
       JOIN SERVICEPOINT_MP_ROLE SMRRP
          ON SMRRP.SERVICEPOINT = SS.SERVICEPOINT AND SMRRP.ROLE_ID = 2 /* 2 = RP   */
       JOIN MARKET_PARTICIPANT MPRP
          ON     MPRP.ID = SMRRP.MARKET_PARTICIPANT_ID
             AND NOT EXISTS
                    (SELECT 1
                       FROM ORGANISATION_PARTICIPANT
                      WHERE PARTICIPANT_ID = MPRP.ID)
       JOIN SERVICEPOINT_MP_ROLE SMRMPB
          ON SMRMPB.SERVICEPOINT = SS.SERVICEPOINT AND SMRMPB.ROLE_ID = 3 /* 3 = MPB  */
       JOIN MARKET_PARTICIPANT MPMPB
          ON     MPMPB.ID = SMRMPB.MARKET_PARTICIPANT_ID
             AND NOT EXISTS
                    (SELECT 1
                       FROM ORGANISATION_PARTICIPANT
                      WHERE PARTICIPANT_ID = MPMPB.ID)
       JOIN SERVICEPOINT_MP_ROLE SMRLNSP
          ON SMRLNSP.SERVICEPOINT = SS.SERVICEPOINT AND SMRLNSP.ROLE_ID = 1 /* 1 = LNSP */
       JOIN MARKET_PARTICIPANT MPLNSP
          ON MPLNSP.ID = SMRLNSP.MARKET_PARTICIPANT_ID
       JOIN SERVICEPOINT_MP_ROLE SMRFRMP
          ON SMRFRMP.SERVICEPOINT = SS.SERVICEPOINT AND SMRFRMP.ROLE_ID = 6 /* 6 = FRMP */
       JOIN MARKET_PARTICIPANT MPFRMP
          ON MPFRMP.ID = SMRFRMP.MARKET_PARTICIPANT_ID
 WHERE     1 = 1
       AND S.NAME <> 'X'
       AND SRA.VALUE ='<NMI_Size>'
       AND SRA.END_DT > SYSDATE
       AND SRA2.VALUE like '<Meter_Type>%'
       AND SRA2.END_DT > SYSDATE
       AND SS.END_DT > SYSDATE
       AND SMRRP.END_DT > SYSDATE
       AND SMRMPB.END_DT > SYSDATE
       AND SMRLNSP.END_DT > SYSDATE
       AND SMRFRMP.END_DT > SYSDATE
       AND ROWNUM <= 1
       AND 1 = 1
       )
       select      
       s.*,
       (SELECT MPARP.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPARP,
               MARKET_PARTICIPANT_ROLE MPRARP,
               NMI_TO_ROLE nr, SELALL s
         WHERE     MPARP.ID = MPRARP.MARKET_PARTICIPANT_ID
               AND MPRARP.ROLE_ID = 2
               AND MPRARP.id <> nr.mp_id
               AND nr.r_id = 2
               and MPRARP.id<>ROLE_ALTFRMP_ID
               AND MPARP.ID <> SMP.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
                                                            AS ROLE_ALTRP,
          (Select MP.PARTICIPANT_NAME from SELALL s, MARKET_PARTICIPANT mp
          where s.ROLE_ALTFRMP_ID=MP.ID)
                                                             as ROLE_ALTFRMP
      from SELALL s, SERVICEPOINT_MP_ROLE smp
      where s.NMI=SMP.SERVICEPOINT
     and SMP.END_DT>SYSDATE
      and SMP.ROLE_ID=2
      and  ROWNUM <= 1