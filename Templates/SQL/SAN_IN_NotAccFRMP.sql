/* Formatted on 13/06/2017 5:04:32 PM (QP5 v5.256.13226.35510) */
WITH RECEIVED_TODAY
     AS (SELECT DISTINCT SAN.NMI || SAN.NMI_CHECKSUM AS NMI
           FROM B2B_SITE_ACCESS_NOTIFICATION SAN
          WHERE SAN.UPDATED_DT > TRUNC (SYSDATE)),
     RESERVED_NMIS
     AS (SELECT DISTINCT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE     (   TDR.IS_RETIRED_YN = 'Y'
                     OR TDR.IS_IN_USE_YN = 'Y'
                     OR TDR.IS_RESERVED_YN = 'Y')
                AND TDR.CREATED_TS > TRUNC (SYSDATE)),
     NMI_DETAILS
     AS (SELECT SS.SERVICEPOINT AS NMI
           FROM STATUS ST,
                SERVICEPOINT_STATUS SS,
                SERVICEPOINT_REF_ATTRIBUTE SRA,
                SERVICEPOINT_REF_ATTRIBUTE SRA2
          WHERE     SS.STATUS_ID = ST.ID
                AND ST.NAME = 'A'
                AND SS.END_DT > SYSDATE
                AND SRA.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA.VALUE = '<Meter_Type>'
                AND SRA.END_DT > SYSDATE
                AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA2.VALUE = '<NMI_Size>'
                AND SRA2.END_DT > SYSDATE
         MINUS
         SELECT NMI FROM RECEIVED_TODAY
         MINUS
         SELECT NMI FROM RESERVED_NMIS),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>'),
     NMI_FROM_ROLE
     AS (SELECT DISTINCT MP.PARTICIPANT_NAME AS MP_FROM
           FROM MARKET_PARTICIPANT MP, MARKET_PARTICIPANT_ROLE MPR, ROLE R
          WHERE     R.ID <> 6
                AND MPR.ROLE_ID = R.ID
                AND MPR.MARKET_PARTICIPANT_ID = MP.ID
                AND MP.PARTICIPANT_NAME NOT IN (SELECT MP.PARTICIPANT_NAME
                                                  FROM MARKET_PARTICIPANT_ROLE mprr,
                                                       ROLE ro,
                                                       MARKET_PARTICIPANT MP
                                                 WHERE     mprr.role_id =
                                                              ro.id
                                                       AND ro.ID = 6
                                                       AND MP.ID =
                                                              MPRR.MARKET_PARTICIPANT_ID
                                                       AND MP.END_DT >
                                                              SYSDATE)
                AND MP.END_DT > SYSDATE
         MINUS
         SELECT DISTINCT MP.PARTICIPANT_NAME
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID)
SELECT ND.NMI AS NMI, NFR.MP_FROM AS FROM_XML, NTR.MP_TO AS TO_XML
  FROM NMI_DETAILS ND, NMI_TO_ROLE NTR, NMI_FROM_ROLE NFR
 WHERE 1 = 1 AND ROWNUM <= 1