SELECT B.*
  FROM(
SELECT EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//iee:ConfigurationBatch/iee:ConfigurationTransfers/iee:ConfigurationTransfer/com:CorrelationID', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04", xmlns:com="http://www.itron.com/mdm/common/2008/04"' ) AS CORRID
, EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//iee:ConfigurationBatch/iee:ConfigurationTransfers/iee:ConfigurationTransfer/iee:ConfigurationTransaction/iee:ConfigurationUpdates/iee:ConfigurationUpdate[3]/iee:ServicePointID', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04", xmlns:com="http://www.itron.com/mdm/common/2008/04"' ) AS NMI
, EXTRACTVALUE(TBL_X.COLUMN_VALUE, '/a:Meter/a:MeterNumber', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04", xmlns:com="http://www.itron.com/mdm/common/2008/04", xmlns:a="http://www.itron.com/mdm/configuration/2008/04"') AS METERNUMBER
, EXTRACTVALUE(TBL_X.COLUMN_VALUE, '/a:Meter/a:MeterVersions/a:MeterVersion/a:MeterStatus', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04", xmlns:com="http://www.itron.com/mdm/common/2008/04", xmlns:a="http://www.itron.com/mdm/configuration/2008/04"') AS METERSTATUS
, SUBSTR(EXTRACTVALUE(TBL_X.COLUMN_VALUE, '/a:Meter/a:MeterVersions/a:MeterVersion/a:EffectiveDates/a:EffectiveStartDate', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04", xmlns:com="http://www.itron.com/mdm/common/2008/04", xmlns:a="http://www.itron.com/mdm/configuration/2008/04"'),1,10) AS METEREFFECTIVESTARTDATE
FROM MTS.SQT_INTERFACE_IN T
, TABLE(XMLSEQUENCE(EXTRACT(T.USER_DATA.PAYLOAD_DATA, '//iee:ConfigurationBatch/iee:ConfigurationTransfers/iee:ConfigurationTransfer/iee:ConfigurationTransaction/iee:ConfigurationUpdates/iee:ConfigurationUpdate[@xsi:type=''iee:ServicePoint'']/iee:LinkedMeters/a:LinkedMeter/a:Meter', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04", xmlns:com="http://www.itron.com/mdm/common/2008/04", xmlns:a="http://www.itron.com/mdm/configuration/2008/04", xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'))) TBL_X      
WHERE DBMS_LOB.INSTR(T.USER_DATA.PAYLOAD_DATA.GETCLOBVAL(), '<CORRID_CONFIGURATION>') > 0
) B
WHERE B.METERNUMBER = '<NEW_METER>'