SELECT SERVICEPOINT,
  CRCODE,
  CR_TXN_STATUS,
  INITIATED_DATE,
  PROPOSED_DATE,
  EXTRACTVALUE(value(F), 'DataStream/Suffix')            AS SUFFIX,
  EXTRACTVALUE(value(F), 'DataStream/ProfileName')       AS PROFILENAME,
  EXTRACTVALUE(value(F), 'DataStream/AveragedDailyLoad') AS AVERAGEDDAILYLOAD,
  EXTRACTVALUE(value(F), 'DataStream/DataStreamType')    AS DATASTREAMTYPE,
  EXTRACTVALUE(value(F), 'DataStream/Status')            AS DATASTREAM_STATUS
FROM
  (SELECT value(D) VAL1 ,
    CRC.CRCODE                                                                  AS CRCODE ,
    S.name                                                                      AS CR_TXN_STATUS ,
    CCR.INITIATED_DATE                                                          AS INITIATED_DATE ,
    CCD.PROPOSED_DATE                                                           AS PROPOSED_DATE,
    EXTRACTVALUE(value(D), 'Transaction/CATSChangeRequest/NMIStandingData/NMI') AS SERVICEPOINT
  FROM GW_DOCUMENT_CONTENTS GDC,
    CAT_CHANGE_REQUEST CCR ,
    TRANSACTION_STATUS TS ,
    STATUS S ,
    CAT_CHANGE_REASON_CODE CRC ,
    TRANSACTION_DETAIL TD ,
    GW_TRANSACTION GT ,
    GW_MESSAGE GM ,
    CAT_CR_DATA CCD ,
    GW_FILE GF ,
    TABLE(XMLSEQUENCE(extract(xmltype(GDC.contents), 'ase:aseXML/Transactions/Transaction', ' xmlns:ase="urn:aseXML:r35"'))) D
  WHERE CCR.NMID                = '<PARAM_NMID>'
  AND CRC.CRCODE                = '<CRCODE>'
  AND CCR.INITIATED_DATE       >= trunc(sysdate  <INITIATED_DATE>)
  AND TS.TRANSACTION_DETAIL_ID  = CCR.TRANSACTION_DETAIL_ID
  AND TS.STATUS_ID              = S.id
  AND CCR.CHANGE_REASON_CODE_ID = CRC.id
  AND CCR.TRANSACTION_DETAIL_ID = TD.id
  AND TD.GW_TRANSACTION_ID      = GT.id
  AND GT.MESSAGE_ID             = GM.id
  AND GM.DOCUMENT_ID            = GDC.DOCUMENT_ID
  AND CCR.CR_DATA_ID            = CCD.id
  AND GM.DOCUMENT_ID            = GF.DOCUMENT_ID
  ) GDC1,
  TABLE(XMLSEQUENCE(extract(GDC1.VAL1, 'Transaction/CATSChangeRequest/NMIStandingData/DataStreams/DataStream'))) F
WHERE SERVICEPOINT = '<PARAM_NMID>'
ORDER BY SUFFIX