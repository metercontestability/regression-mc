WITH RECEIVED_TODAY
     AS (
    SELECT DISTINCT SAN.NMI || SAN.NMI_CHECKSUM as NMI
        FROM B2B_SITE_ACCESS_NOTIFICATION SAN
            WHERE SAN.UPDATED_DT > TRUNC(SYSDATE)
        ),
     RESERVED_NMIS
     AS (
        SELECT DISTINCT NMI
        FROM TEST_DATA_KEY_REGISTER TDR
        WHERE   (TDR.IS_RETIRED_YN = 'Y'
            or TDR.IS_IN_USE_YN  = 'Y'
            or   TDR.IS_RESERVED_YN ='Y')
        ),
     NMI_DETAILS
     AS (
        SELECT SS.SERVICEPOINT as NMI
        FROM STATUS ST,
            SERVICEPOINT_STATUS SS,
            SERVICEPOINT_REF_ATTRIBUTE SRA,
            SERVICEPOINT_REF_ATTRIBUTE SRA2
        WHERE SS.STATUS_ID = ST.ID
            AND ST.NAME = 'A'
            AND SS.END_DT > SYSDATE
            AND SRA.SERVICEPOINT = SS.SERVICEPOINT
            AND SRA.VALUE =  '<Meter_Type>'
            AND SRA.END_DT > SYSDATE
            AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
            AND SRA2.VALUE = '<NMI_Size>'
            AND SRA2.END_DT > SYSDATE
        MINUS SELECT NMI FROM RECEIVED_TODAY
        MINUS SELECT NMI FROM RESERVED_NMIS
        ),
     NMI_TO_ROLE
     AS (
        SELECT MP.PARTICIPANT_NAME as MP_TO, R.NAME, MP.ID as MP_ID, r.id as R_ID
        FROM CONFIGURATION_VARIABLE CV,
             ORGANISATION ORG,
             ORGANISATION_PARTICIPANT ORG_PARTI,
             MARKET_PARTICIPANT MP,
             MARKET_PARTICIPANT_ROLE MPR,
             ROLE R
        WHERE  CV.NAME = 'Organisation'
            AND    ORG.NAME = CV.VALUE
            AND    ORG_PARTI.ORGANISATION_ID = ORG.ID
            AND    ORG_PARTI.PARTICIPANT_ID = MP.ID
            AND  MP.ID = MPR.MARKET_PARTICIPANT_ID
            AND  MPR.ROLE_ID = R.ID
            AND R.NAME = '<To_Role>'
        ),
     NMI_FROM_ROLE
     AS (
        SELECT
            NMI,
            MP.PARTICIPANT_NAME as MP_FROM, RO.NAME as FROM_ROLE
            , NTR.MP_TO
        FROM
            NMI_DETAILS ND,
            NMI_TO_ROLE NTR,
            SERVICEPOINT_MP_ROLE SMR,
            SERVICEPOINT_MP_ROLE SMR2,
            MARKET_PARTICIPANT MP,
            ROLE RO
        WHERE SMR.SERVICEPOINT = ND.NMI
            AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
            AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
            and SMR2.ROLE_ID = NTR.R_ID
            and SMR2.end_dt > SYSDATE
            AND SMR.END_DT > SYSDATE
            AND SMR.MARKET_PARTICIPANT_ID = MP.ID
            AND RO.ID = SMR.ROLE_ID
            AND RO.NAME = '<From_Role>' 
            AND MP.ID NOT IN (SELECT DISTINCT MP.ID FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                union SELECT MP.ID
                                        FROM MARKET_PARTICIPANT_ROLE mprr, ROLE ro, MARKET_PARTICIPANT MP
                                        WHERE     mprr.role_id = ro.id
                                         AND ro.ID=6
                                        AND MP.ID=MPRR.MARKET_PARTICIPANT_ID
                                        and MP.END_DT>SYSDATE)

         )
SELECT NFR.NMI as NMI, NFR.MP_FROM as FROM_XML, NFR.MP_TO as TO_XML
FROM  NMI_FROM_ROLE NFR
WHERE 1=1
AND ROWNUM <= 1