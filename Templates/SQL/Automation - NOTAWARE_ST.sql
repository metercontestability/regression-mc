/*NEW "NOTAWARE" NMI Search Query*/
SELECT SRA.SERVICEPOINT as NMI,       
       SRA.VALUE as NMI_CLASS,
       MP.PARTICIPANT_NAME as ROLE_LNSP,
       S.BUSINESS_STATUS as NMI_STATUS
--ss.*, s.*, sra.*, smr.*, mp.*
    from SERVICEPOINT_STATUS ss
    join STATUS s on s.ID = ss.STATUS_ID
    join SERVICEPOINT_REF_ATTRIBUTE sra on sra.SERVICEPOINT = ss.SERVICEPOINT
    join SERVICEPOINT_MP_ROLE smr on Smr.SERVICEPOINT = ss.SERVICEPOINT
    join MARKET_PARTICIPANT mp on mp.ID = SMR.MARKET_PARTICIPANT_ID
    join ROLE r on r.ID = SMR.ROLE_ID
    where 1=1
      and s.NAME <> 'X'
      and sra.ATTRIBUTE_TYPE_ID = 5   /*  -- NMI class */
      and sra.VALUE = 'SMALL'
      and sra.END_DT > SYSDATE
      and ss.END_DT > SYSDATE
      and R.ID = 1  --1 = 'LNSP'
      and ss.servicepoint not in (select servicepoint
                                    from special_condition sc
                                    where sc.code = 'EMBEDA'
                                      and sc.end_dt > SYSDATE) 
      AND SS.SERVICEPOINT NOT IN (SELECT DISTINCT (CCD.NMID) FROM CAT_CR_DATA CCD WHERE UPDATED_DT >= SYSDATE - 10)
      and rownum <= 10