/* Formatted on 29/04/2017 7:04:17 PM (QP5 v5.256.13226.35510) */
WITH NMI_DETAILS_FIN
     AS (SELECT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE TDR.USED_BY_CASE_NAME = '<Parent_Scenario_ID>'
         MINUS
         SELECT DISTINCT SAR.NMI || SAR.NMI_CHECKSUM AS NMI
           FROM SITE_ACCESS_REQ SAR, TRANSACTION_DETAIL TD
          WHERE     SAR.TRANSACTION_DETAIL_ID = TD.ID
                AND TD.GW_FILE_TIMESTAMP > TRUNC (SYSDATE)),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>'),
     NMI_FROM_ROLE
     AS (SELECT NMI,
                MP.PARTICIPANT_NAME AS MP_FROM,
                RO.NAME AS FROM_ROLE,
                NTR.MP_TO
           FROM NMI_DETAILS ND,
                NMI_TO_ROLE NTR,
                SERVICEPOINT_MP_ROLE SMR,
                SERVICEPOINT_MP_ROLE SMR2,
                MARKET_PARTICIPANT MP,
                ROLE RO
          WHERE     SMR.SERVICEPOINT = ND.NMI
                AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
                AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
                AND SMR2.ROLE_ID = NTR.R_ID
                AND SMR2.end_dt > SYSDATE
                AND SMR.END_DT > SYSDATE
                AND SMR.MARKET_PARTICIPANT_ID = MP.ID
                AND MP.ID NOT IN (SELECT cat1.participant_id AS MPID
                                    FROM CAT_CR_ROLE_ASSIGNMENT cat1,
                                         MARKET_PARTICIPANT mp
                                   WHERE     cat1.cr_data_id IN (SELECT MAX (
                                                                           id)
                                                                   FROM CAT_CR_DATA
                                                                  WHERE     nmid =
                                                                               SUBSTR (
                                                                                  ND.NMI,
                                                                                  1,
                                                                                  10)
                                                                        AND CHANGE_REASON_CODE_ID =
                                                                               '1026')
                                         AND CAT1.ROLE_ID = SMR.ROLE_ID
                                         AND MP.ID = cat1.participant_id)
                AND RO.ID = SMR.ROLE_ID
                AND RO.NAME = '<From_Role>')
SELECT NFR.NMI AS NMI, NFR.MP_FROM AS FROM_XML, NFR.MP_TO AS TO_XML
  FROM NMI_FROM_ROLE NFR
 WHERE 1 = 1 AND ROWNUM <= 1