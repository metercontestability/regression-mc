/* Formatted on 13/06/2017 2:31:35 PM (QP5 v5.256.13226.35510) */
WITH RECEIVED_TODAY
     AS (SELECT DISTINCT SAN.NMI || SAN.NMI_CHECKSUM AS NMI
           FROM B2B_SITE_ACCESS_NOTIFICATION SAN
          WHERE SAN.UPDATED_DT > TRUNC (SYSDATE)),
     RESERVED_NMIS
     AS (SELECT DISTINCT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE     (   TDR.IS_RETIRED_YN = 'Y'
                     OR TDR.IS_IN_USE_YN = 'Y'
                     OR TDR.IS_RESERVED_YN = 'Y')
                AND TDR.CREATED_TS > TRUNC (SYSDATE)),
     NMI_DETAILS
     AS (SELECT SS.SERVICEPOINT AS NMI
           FROM STATUS ST,
                SERVICEPOINT_STATUS SS,
                SERVICEPOINT_REF_ATTRIBUTE SRA,
                SERVICEPOINT_REF_ATTRIBUTE SRA2
          WHERE     SS.STATUS_ID = ST.ID
                AND ST.NAME = 'A'
                AND SS.END_DT > SYSDATE
                AND SRA.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA.VALUE = '<Meter_Type>'
                AND SRA.END_DT > SYSDATE
                AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA2.VALUE = '<NMI_Size>'
                AND SRA2.END_DT > SYSDATE
         MINUS
         SELECT NMI FROM RECEIVED_TODAY
         MINUS
         SELECT NMI FROM RESERVED_NMIS),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>'),
     NMI_FROM_ROLE
     AS (SELECT NMI,
                MP.PARTICIPANT_NAME AS MP_FROM,
                RO.NAME AS FROM_ROLE,
                NTR.MP_TO
           FROM NMI_DETAILS ND,
                NMI_TO_ROLE NTR,
                SERVICEPOINT_MP_ROLE SMR,
                SERVICEPOINT_MP_ROLE SMR2,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE RO
          WHERE     SMR.SERVICEPOINT = ND.NMI
                AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
                AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
                AND SMR2.ROLE_ID = NTR.R_ID
                AND SMR2.end_dt > SYSDATE
                AND SMR.END_DT > SYSDATE
                AND MPR.ROLE_ID = RO.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND SMR.MARKET_PARTICIPANT_ID = MP.ID
                AND MP.END_DT > SYSDATE
                AND MPR.MARKET_PARTICIPANT_ID NOT IN (SELECT DISTINCT MP.ID
                                                        FROM CONFIGURATION_VARIABLE CV,
                                                             ORGANISATION ORG,
                                                             ORGANISATION_PARTICIPANT ORG_PARTI,
                                                             MARKET_PARTICIPANT MP,
                                                             MARKET_PARTICIPANT_ROLE MPR,
                                                             ROLE R
                                                       WHERE     CV.NAME =
                                                                    'Organisation'
                                                             AND ORG.NAME =
                                                                    CV.VALUE
                                                             AND ORG_PARTI.ORGANISATION_ID =
                                                                    ORG.ID
                                                             AND ORG_PARTI.PARTICIPANT_ID =
                                                                    MP.ID
                                                             AND MP.ID =
                                                                    MPR.MARKET_PARTICIPANT_ID
                                                             AND MPR.ROLE_ID =
                                                                    R.ID)
                AND RO.ID = SMR.ROLE_ID
                AND RO.NAME =  '<From_Role>')
SELECT NFR.NMI AS NMI,
       NFR.NMI AS SERVICEPOINT,
       NFR.MP_FROM AS FROM_XML,
       NFR.MP_TO AS TO_XML
  FROM NMI_FROM_ROLE NFR,
       SPECIAL_CONDITION cond,
       CIS_SPECIAL_CONDS_MAP csm,
       SPEC_COND_GROUP scg,
       site_access_details san
 WHERE     COND.SERVICEPOINT = NFR.NMI
       AND csm.CIS_SPECIAL_COND_TYPE = COND.CODE
       AND csm.SPEC_COND_GRP_ID = scg.ID
       AND scg.HAZARD_YN = 'Y'
       AND COND.START_DT <= SYSDATE - 1
       AND COND.END_DT > SYSDATE
       AND nfr.nmi = SAN.SERVICEPOINT
       AND 1 = 1
       AND ROWNUM <= 1