select t.enq_time, t.deq_time, T.CORRID,
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//ns8:Status', 'xmlns:ns8="http://www.itron.com/mts/2008/08"') AS USB_TO_MTS_Status, 
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//ns8:Code', 'xmlns:ns8="http://www.itron.com/mts/2008/08"') AS USB_TO_MTS_Code,  
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//ns8:Message', 'xmlns:ns8="http://www.itron.com/mts/2008/08"') AS USB_TO_MTS_Message  
from mts.sqt_interface_in t
where t.corrid in 
(select distinct to_char(correlation_id) from cis_instruction
where transaction_detail_id = (Select id from transaction_detail td where td.transactionid = 
'<INIT_TRANSACTION_ID>'))
order by t.enq_time asc