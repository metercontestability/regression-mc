SELECT GF.FILENAME,
    GM.TRANSACTION_GRP
    ,GM.TO_MARKET_PART
    ,GM.FROM_MARKET_PART
    ,GPS.PROCESS_STEP
    ,GDC.CONTENTS
	,TD.TRANSACTIONID
    , XMLTYPE(GDC.CONTENTS).EXTRACT('//Header/From/text()').getStringVal() AS FROM_SAN_XML
    , XMLTYPE(GDC.CONTENTS).EXTRACT('//Header/To/text()').getStringVal() AS TO_SAN_XML	
    , REPLACE(XMLTYPE(GDC.CONTENTS).EXTRACT('//Transaction/AmendMeterRouteDetails/AmendSiteAccessDetails/AccessDetail/text()').getStringVal(),'amp;','') AS SAN_ACCESSDETAIL
    , XMLTYPE(GDC.CONTENTS).EXTRACT('//Transaction/AmendMeterRouteDetails/AmendSiteAccessDetails/Hazard/Description/text()').getStringVal() AS SAN_HAZARD        
    , XMLTYPE(GDC.CONTENTS).EXTRACT('//Transaction/AmendMeterRouteDetails/AmendSiteAccessDetails/LastModifiedDateTime/text()').getStringVal() AS SAN_LASTMODIFIEDDATE
FROM GW_DOCUMENT GD
    , GW_DOCUMENT_CONTENTS GDC
    ,GW_MESSAGE GM
    , GW_PROCESS_STEP GPS
    , GW_DOCUMENT_PROCESS_STEP GDPS
    , GW_FILE GF
    , GW_TRANSACTION GT
    , TRANSACTION_DETAIL TD
WHERE GD.ID > 200000003 
  AND GD.ID = GF.DOCUMENT_ID
  AND GD.UPDATED_DT BETWEEN SYSDATE-2 AND SYSDATE
  AND TRANSACTION_GRP IN ('SITE')      
  AND GD.DIRECTION = 'OUT'
  AND GM.DOCUMENT_ID = GD.ID
  AND GD.ID = GDC.DOCUMENT_ID
  AND GDPS.DOCUMENT_ID = GD.ID
  AND GDPS.PROCESS_STEP_ID = GPS.ID
  AND GT.MESSAGE_ID = GM.ID
  AND TD.GW_TRANSACTION_ID = GT.ID
  AND TD.INITIATING_TRANSACTIONID = '<INIT_TRANSACTION_ID>' 
ORDER BY GD.ID DESC