WITH RESERVED_NMIS
     AS (SELECT DISTINCT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE     (   TDR.IS_RETIRED_YN = 'Y'
                     OR TDR.IS_IN_USE_YN = 'Y'
                     OR TDR.IS_RESERVED_YN = 'Y')
                AND TDR.CREATED_TS > TRUNC (SYSDATE)),
     NMIFILTER
     AS (SELECT SERVICEPOINT AS NMI
           FROM STATUS ST, SERVICEPOINT_STATUS SS
          WHERE SS.STATUS_ID = ST.ID AND ST.NAME = 'A'
         MINUS
         SELECT NMI FROM RESERVED_NMIS),
     NMI_DETAILS
     AS (SELECT SS.SERVICEPOINT AS NMI,
                (SELECT DISTINCT
                        LTRIM (TRIM (METER.METERNUMBER), '0') METERNUMBER
                   FROM IEE_PHYSICALCONFIGTREEBYMETER TREE
                        JOIN IEE_METERATTRIBUTES METER
                           ON METER.METERID = TREE.METERID
                  WHERE     1 = 1
                        AND TREE.METEREFFECTIVESTARTDATE <= SYSDATE
                        AND TREE.METEREFFECTIVEENDDATE > SYSDATE
                        AND TREE.CHANNELEFFECTIVESTARTDATE <= SYSDATE
                        AND TREE.CHANNELEFFECTIVEENDDATE > SYSDATE
                        AND TREE.SPCTODATASTREAMLINKSTARTDATE <= SYSDATE
                        AND TREE.SPCTODATASTREAMLINKENDDATE > SYSDATE
                        AND TREE.CHANNELTOMETERLINKSTARTDATE <= SYSDATE
                        AND TREE.CHANNELTOMETERLINKENDDATE > SYSDATE
                        AND TREE.SERVICEPOINT = SS.SERVICEPOINT
                        AND ROWNUM < 2)
                   AS METERNUMBER
           FROM STATUS ST, SERVICEPOINT_STATUS SS, NMIFILTER NF,SERVICEPOINT_REF_ATTRIBUTE SRAMTR		   
          WHERE     SS.STATUS_ID = ST.ID
                AND NF.NMI = SS.SERVICEPOINT
                AND ST.NAME = 'A'
				      AND SRAMTR.ATTRIBUTE_TYPE_ID = 1
					  AND SRAMTR.SERVICEPOINT = SS.SERVICEPOINT
						AND SRAMTR.VALUE = '<Meter_Type_MTS>'
						AND SRAMTR.END_DT > SYSDATE				
				),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>'),
     NMI_FROM_ROLE
     AS (SELECT NMI,
                METERNUMBER,
                MP.PARTICIPANT_NAME AS MP_FROM,
                RO.NAME AS FROM_ROLE,
                NTR.MP_TO
           FROM NMI_DETAILS ND,
                NMI_TO_ROLE NTR,
                SERVICEPOINT_MP_ROLE SMR,
                SERVICEPOINT_MP_ROLE SMR2,
                MARKET_PARTICIPANT MP,
                ROLE RO
          WHERE     SMR.SERVICEPOINT = ND.NMI
                AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
                AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
                AND SMR2.ROLE_ID = NTR.R_ID
                AND SMR2.end_dt > SYSDATE
                AND SMR.END_DT > SYSDATE
                AND SMR.MARKET_PARTICIPANT_ID = MP.ID
                AND RO.ID = SMR.ROLE_ID
                AND meternumber IS NOT NULL
                AND RO.NAME = '<From_Role>')
SELECT NFR.NMI AS NMI,
       NFR.METERNUMBER AS METERNUMBER,
       NFR.MP_FROM AS FROM_XML,
       NFR.MP_TO AS TO_XML
  FROM NMI_FROM_ROLE NFR
WHERE 1 = 1 AND ROWNUM <= 1
