/* Formatted on 13/06/2017 5:01:36 PM (QP5 v5.256.13226.35510) */
WITH RECEIVED_TODAY
     AS (SELECT DISTINCT SAN.NMI || SAN.NMI_CHECKSUM AS NMI
           FROM B2B_SITE_ACCESS_NOTIFICATION SAN
          WHERE SAN.UPDATED_DT > TRUNC (SYSDATE)),
     RESERVED_NMIS
     AS (SELECT DISTINCT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE     (   TDR.IS_RETIRED_YN = 'Y'
                     OR TDR.IS_IN_USE_YN = 'Y'
                     OR TDR.IS_RESERVED_YN = 'Y')
                AND TDR.CREATED_TS > TRUNC (SYSDATE)),
     NMI_DETAILS
     AS (SELECT SS.SERVICEPOINT AS NMI
           FROM STATUS ST,
                SERVICEPOINT_STATUS SS,
                SERVICEPOINT_REF_ATTRIBUTE SRA,
                SERVICEPOINT_REF_ATTRIBUTE SRA2
          WHERE     SS.STATUS_ID = ST.ID
                AND ST.NAME = 'A'
                AND SS.END_DT > SYSDATE
                AND SRA.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA.VALUE = '<Meter_Type>'
                AND SRA.END_DT > SYSDATE
                AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA2.VALUE = '<NMI_Size>'
                AND SRA2.END_DT > SYSDATE
         MINUS
         SELECT NMI FROM RECEIVED_TODAY
         MINUS
         SELECT NMI FROM RESERVED_NMIS),
     EXCLUDE_CR1x
     AS (SELECT ND.NMI AS NMI
           FROM NMI_DETAILS ND
         MINUS
         (SELECT DISTINCT CCD.NMID || CCD.CHECKSUM AS NMI
            FROM CAT_CR_DATA CCD, cat_change_reason_code crc
           WHERE     crc.id = ccd.change_reason_code_id
                 AND CRC.CRCODE IN ('1000',
                                    '1010',
                                    '1020',
                                    '1030',
                                    '1040',
                                    '1021',
                                    '1022',
                                    '1023',
                                    '1024',
                                    '1025',
                                    '1026',
                                    '1027',
                                    '1028',
                                    '1029',
                                    '1050',
                                    '1051',
                                    '1080',
                                    '1081',
                                    '1082',
                                    '1083',
                                    '1084'))),
     NMI_DETAILS_FIN
     AS (SELECT DISTINCT a.NMI
           FROM NMI_DETAILS a, EXCLUDE_CR1x b
          WHERE a.NMI = b.NMI AND 1 = 1 AND ROWNUM <= 1),
     NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID
                AND R.NAME = '<To_Role>'),
     NMI_FROM_ROLE
     AS (SELECT NMI,
                MP.PARTICIPANT_NAME AS MP_FROM,
                RO.NAME AS FROM_ROLE,
                NTR.MP_TO
           FROM NMI_DETAILS_FIN ND,
                NMI_TO_ROLE NTR,
                SERVICEPOINT_MP_ROLE SMR,
                SERVICEPOINT_MP_ROLE SMR2,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE RO
          WHERE     SMR.SERVICEPOINT = ND.NMI
                AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
                AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
                AND SMR2.ROLE_ID = NTR.R_ID
                AND SMR2.end_dt > SYSDATE
                AND SMR.END_DT > SYSDATE
                AND MPR.ROLE_ID = RO.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.MARKET_PARTICIPANT_ID NOT IN (SELECT DISTINCT
                                                             SM.MARKET_PARTICIPANT_ID
                                                        FROM SERVICEPOINT_MP_ROLE sm,
                                                             MARKET_PARTICIPANT_ROLE mpr
                                                       WHERE     mpr.role_id =
                                                                    SM.ROLE_ID
                                                             AND SM.ROLE_ID IN (2,
                                                                                3,
                                                                                6)
                                                             AND SMR.SERVICEPOINT =
                                                                    SM.SERVICEPOINT
                                                             AND SM.END_DT >
                                                                    SYSDATE
                                                      UNION
                                                      SELECT DISTINCT MP.ID
                                                        FROM CONFIGURATION_VARIABLE CV,
                                                             ORGANISATION ORG,
                                                             ORGANISATION_PARTICIPANT ORG_PARTI,
                                                             MARKET_PARTICIPANT MP,
                                                             MARKET_PARTICIPANT_ROLE MPR,
                                                             ROLE R
                                                       WHERE     CV.NAME =
                                                                    'Organisation'
                                                             AND ORG.NAME =
                                                                    CV.VALUE
                                                             AND ORG_PARTI.ORGANISATION_ID =
                                                                    ORG.ID
                                                             AND ORG_PARTI.PARTICIPANT_ID =
                                                                    MP.ID
                                                             AND MP.ID =
                                                                    MPR.MARKET_PARTICIPANT_ID
                                                             AND MPR.ROLE_ID =
                                                                    R.ID)
                AND RO.ID = SMR.ROLE_ID
                AND RO.NAME = '<From_Role>')
SELECT NFR.NMI AS NMI,
       NFR.NMI AS SERVICEPOINT,
       NFR.MP_FROM AS FROM_XML,
       NFR.MP_TO AS TO_XML
  FROM NMI_FROM_ROLE NFR
 WHERE 1 = 1 AND ROWNUM <= 1