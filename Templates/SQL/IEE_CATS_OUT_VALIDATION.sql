SELECT DISTINCT S.SERVICEPOINTID AS SERVICEPOINT,
       UPPER (M.METERNUMBER ) AS METERSERIALNUMBER,
       MSS.SUFFIX AS REGISTERID,
       MSS.SUFFIX AS SUFFIX,
       U.UNITID AS UNITOFMEASURE,
       (CASE WHEN M.EFFECTIVEENDDATE > sysdate THEN 'C' ELSE 'R' END) AS METERSTATUS ,
       (CASE WHEN M.EFFECTIVEENDDATE > sysdate THEN 'C' ELSE 'R' END) AS REGSTATUS ,
       SUBSTR (M.MANUFACTURER, 1, 15) AS MANUFACTURER,
       M.MODEL AS MODELCODE,
       NVL(IUAD3.DATAVALUE,' ') AS TIMEOFDAY,
       NVL(IUAD.DATAVALUE,' ') AS CONTROLLEDLOAD,
       NVL(IUAD2.DATAVALUE, ' ') AS NETWORKTARIFFCODE
FROM SERVICEPOINT S
JOIN NODELINK NL ON NL.LEFTNODEKEY = S.NODEKEY
JOIN SERVICEPOINTCHANNEL SPC ON SPC.NODEKEY = NL.RIGHTNODEKEY
JOIN NODELINK NL2 ON NL2.LEFTNODEKEY = SPC.NODEKEY
JOIN INTERVALCHANNEL IC ON IC.NODEKEY = NL2.RIGHTNODEKEY
JOIN NODELINK NL3 ON NL3.RIGHTNODEKEY = IC.NODEKEY
JOIN METER M ON M.NODEKEY = NL3.LEFTNODEKEY
INNER JOIN MTS_SPC_SUFFIX_MAP MSS   ON MSS.SPC_ID = SPC.CHANNELNUMBER
JOIN SPCVALIDATIONSET SVS   ON SVS.SERVICEPOINTCHANNELKEY = SPC.SERVICEPOINTCHANNELKEY
JOIN VALIDATIONSET VS   ON VS.VALIDATIONSETKEY = SVS.VALIDATIONSETKEY
JOIN ENTITYSTATUS ES   ON ES.ENTITYSTATUSKEY = M.ENTITYSTATUSKEY
Left JOIN IEE_SERVICEPOINTCHANNELUDAS IUAD   ON IUAD.SERVICEPOINT = S.SERVICEPOINTID
  AND IUAD.SERVICEPOINTCHANNELNUMBER = SPC.CHANNELNUMBER
  AND IUAD.EFFECTIVEENDLINKDATE > SYSDATE
  AND UPPER (IUAD.UDANAME) = 'CONTROLLED_LOAD'
Left JOIN IEE_SERVICEPOINTCHANNELUDAS IUAD2  ON IUAD2.SERVICEPOINT = S.SERVICEPOINTID
  AND IUAD2.SERVICEPOINTCHANNELNUMBER = SPC.CHANNELNUMBER
  AND IUAD2.EFFECTIVEENDLINKDATE > SYSDATE
  AND UPPER (IUAD2.UDANAME) = 'NTC'
Left JOIN IEE_SERVICEPOINTCHANNELUDAS IUAD3   ON IUAD3.SERVICEPOINT = S.SERVICEPOINTID
  AND IUAD3.SERVICEPOINTCHANNELNUMBER = SPC.CHANNELNUMBER
  AND IUAD3.EFFECTIVEENDLINKDATE > SYSDATE
  AND UPPER (IUAD3.UDANAME) = 'TOD'
JOIN UNITOFMEASURE UOM   ON UOM.UNITOFMEASUREKEY = SPC.UNITOFMEASUREKEY
JOIN UNIT U ON UOM.UNITKEY = U.UNITKEY
WHERE     1 = 1
AND (    SVS.EFFECTIVESTARTDATE < NL2.EFFECTIVEENDDATE
     AND SVS.EFFECTIVEENDDATE > NL2.EFFECTIVESTARTDATE)
AND (    M.EFFECTIVESTARTDATE < NL2.EFFECTIVEENDDATE
     AND M.EFFECTIVEENDDATE > NL2.EFFECTIVESTARTDATE)
AND S.SERVICEPOINTID ='<NMI>'
AND (M.EFFECTIVEENDDATE + 10/24 >= sysdate <EFFECTIVE_DATE>  or M.EFFECTIVESTARTDATE + 10/24 >= sysdate <EFFECTIVE_DATE>)
ORDER BY METERSERIALNUMBER, REGISTERID