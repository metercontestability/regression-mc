select distinct ic.INTERVALLENGTH / 60 as INTERVAL_LENGTH
from METER m
  inner join NODELINK nl on m.NODEKEY = nl.LEFTNODEKEY
  inner join INTERVALCHANNEL ic on nl.RIGHTNODEKEY = ic.NODEKEY
where m.METERID = '<Meter_Id>'
