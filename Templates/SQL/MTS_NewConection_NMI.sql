SELECT DISTINCT SS.SERVICEPOINT as NMI
    , S.NAME AS NMI_STATUS
    , MPMPB.PARTICIPANT_NAME AS ROLE_MPB
    , MPMDP.PARTICIPANT_NAME AS ROLE_MPC
    , MPMDP.PARTICIPANT_NAME AS ROLE_MDP
    , MPLNSP.PARTICIPANT_NAME AS ROLE_LNSP 
    , MPFRMP.PARTICIPANT_NAME AS ROLE_FRMP
    , MPLNSP.PARTICIPANT_NAME AS ROLE_RP 
    , (SELECT MPAF.PARTICIPANT_NAME
        FROM MARKET_PARTICIPANT MPAF, MARKET_PARTICIPANT_ROLE MPRAF
        WHERE MPAF.ID = MPRAF.MARKET_PARTICIPANT_ID
          AND MPRAF.ROLE_ID = 6     
          AND MPAF.ID <> SMRFRMP.MARKET_PARTICIPANT_ID
          AND ROWNUM < 2  
          )     AS ROLE_ALTFRMP
 , '12345' AS METERSERIALNUMBER
 , 'GENR' AS PROPOSEDTARIFF
    FROM    SERVICEPOINT_STATUS SS
    JOIN STATUS S ON S.ID = SS.STATUS_ID
    JOIN SERVICEPOINT_MP_ROLE SMRMPB ON SMRMPB.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRMPB.ROLE_ID = 3  
    JOIN MARKET_PARTICIPANT MPMPB ON MPMPB.ID = SMRMPB.MARKET_PARTICIPANT_ID
           AND  EXISTS (SELECT 1 FROM ORGANISATION_PARTICIPANT
                    WHERE PARTICIPANT_ID = MPMPB.ID) 
 JOIN SERVICEPOINT_MP_ROLE SMRMDP ON SMRMDP.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRMDP.ROLE_ID = 5     /* 5 = MDP  */
    JOIN MARKET_PARTICIPANT MPMDP ON MPMDP.ID = SMRMDP.MARKET_PARTICIPANT_ID
    JOIN SERVICEPOINT_MP_ROLE SMRLNSP ON SMRLNSP.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRLNSP.ROLE_ID = 1
    JOIN MARKET_PARTICIPANT MPLNSP ON MPLNSP.ID = SMRLNSP.MARKET_PARTICIPANT_ID
    JOIN SERVICEPOINT_MP_ROLE SMRFRMP ON SMRFRMP.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRFRMP.ROLE_ID = 6  
    JOIN MARKET_PARTICIPANT MPFRMP ON MPFRMP.ID = SMRFRMP.MARKET_PARTICIPANT_ID
	   LEFT JOIN B2B_CIS_SERVICE_ORDER_DATA B2BCISSOD ON B2BCISSOD.SERVICEPOINT = SS.SERVICEPOINT AND B2BCISSOD.STATUS_ID IN (39,40)   
    LEFT OUTER JOIN TEST_DATA_KEY_REGISTER  TDR ON SS.SERVICEPOINT = TDR.NMI AND (TDR.IS_RETIRED_YN = 'Y' OR TDR.IS_IN_USE_YN  = 'Y' OR TDR.IS_RESERVED_YN ='Y')
    WHERE 1=1
      AND TDR.NMI IS NULL
      AND S.NAME = 'D'        
      AND SS.DE_ENERGISED_METHOD_ID = <Deen_Method_MTS>
      AND SS.END_DT > SYSDATE
      AND SMRMDP.END_DT > SYSDATE
      AND SMRMPB.END_DT > SYSDATE
      AND SMRLNSP.END_DT > SYSDATE
      AND SMRFRMP.END_DT > SYSDATE    
      AND B2BCISSOD.ID IS NULL
	    AND SS.SERVICEPOINT NOT IN (SELECT SH.NMI || SH.NMI_CHECKSUM FROM B2B_SERVICE_ORDER_HEADER SH
                                  JOIN B2B_SERVICE_ORDER SO ON SO.SERVICE_ORDER_HEADER_ID = SH.ID
                                  JOIN TRANSACTION_STATUS TS ON TS.TRANSACTION_DETAIL_ID = SO.TRANSACTION_DETAIL_ID
                                  JOIN STATUS ST ON ST.ID = TS.STATUS_ID
                                  WHERE 1=1
                                    AND SH.NMI = SUBSTR(SS.SERVICEPOINT,1,10)
                                    AND ST.BUSINESS_STATUS IN ('EXTERNAL', 'AQ', 'ERROR'))
      AND ROWNUM < 2