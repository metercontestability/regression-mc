select SERVICEPOINT AS RESULT
from (
select ss.SERVICEPOINT 
    from SERVICEPOINT_STATUS ss
    join STATUS s on ss.STATUS_ID = s.ID
    join SERVICEPOINT_REF_ATTRIBUTE sra on sra.SERVICEPOINT = ss.SERVICEPOINT
    join SERVICEPOINT_MP_ROLE smr on smr.SERVICEPOINT = ss.SERVICEPOINT 
    join SERVICEPOINT_MP_ROLE smr2 on smr2.SERVICEPOINT = ss.SERVICEPOINT
    where s.NAME = 'A'  
      and ss.END_DT > sysdate
      and sra.ATTRIBUTE_TYPE_ID = 1 
      and sra.VALUE = 'MRIM'
      and sra.END_DT > sysdate
      and 1 = (select count(*)      
               from SERVICEPOINT_REF_ATTRIBUTE sra_2
               where sra_2.SERVICEPOINT = ss.SERVICEPOINT
                 and sra_2.ATTRIBUTE_TYPE_ID = 2
                 and sra_2.VALUE = 'RWD'
                 and sra_2.END_DT > sysdate)
      and smr.ROLE_ID = 1       
      and smr.END_DT > sysdate
      and smr2.ROLE_ID = 13     
      and smr2.END_DT > sysdate      
      and ss.SERVICEPOINT not in (select cdn.NMI || cdn.NMI_CHECKSUM
                                  from B2B_CUST_DETAILS_NOTIFICATION cdn
                                  where 1=1
                                    and cdn.NMI = substr(ss.SERVICEPOINT,1,10)
                                    and trunc(CDN.UPDATED_DT) = trunc(sysdate))
      and rownum <= 1)