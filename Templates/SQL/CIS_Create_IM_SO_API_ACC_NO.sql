declare
  -- Local variables here
  queue_options      DBMS_AQ.ENQUEUE_OPTIONS_T;
  message_properties DBMS_AQ.MESSAGE_PROPERTIES_T;
  message_id         RAW(16);
  my_message         SYS.AQ$_JMS_TEXT_MESSAGE;
begin
  my_message := SYS.AQ$_JMS_TEXT_MESSAGE.construct;
  my_message.header.clear_properties();
  my_message.header.set_string_property('CISOVEnableDiagnostics', 'TRUE');
  message_properties.correlation := 'ETS_'||to_char(systimestamp,'dd_mm_yyyy_hh24_mi_ss');
  my_message.set_text('
<CreateServiceOrderRequest xmlns="http://www.logica.com/CISOV/API">
	<RequestHeader>
		<Version>0</Version>
		<CompanyCode>^param_company_code_egCITI^</CompanyCode>
		<UserName>^param_user_name_eg_RECONECT^</UserName>
	</RequestHeader>
	<RequestBody>
		<NMI><param_NMI></NMI>
		<ServiceOrderType><param_DataPrep_CIS_SOType_Code></ServiceOrderType>
		<ServiceProvisionType>E</ServiceProvisionType>
		<EffectiveDate><param_Date-oneweekbefore-eg-2017-02-28></EffectiveDate>
		<ServiceOrderReason><param_DataPrep_CIS_SOReason></ServiceOrderReason>
		<Requestor>
			<Retailer>
				<AccountNumber><NO_ACCOUNT></AccountNumber>
			</Retailer>
		</Requestor>
		<WorkCompleteFlag>Y</WorkCompleteFlag>
		<Equipments>
			<Equipment>
				<EquipmentType><param_Meter_Type_MTS> Meter</EquipmentType>
				<TaskType>Install New Equipment</TaskType>
			</Equipment>
		</Equipments>
	</RequestBody>
</CreateServiceOrderRequest>
');
  DBMS_AQ.ENQUEUE(queue_name         => 'WLAQ.CIS_IN_Q',
                  enqueue_options    => queue_options,
                  message_properties => message_properties,
                  payload            => my_message,
                  msgid              => message_id);
  commit;
end;