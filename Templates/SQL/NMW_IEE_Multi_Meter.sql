select distinct 
       s.servicepointid as NMI
     , spc.channelnumber
     , spc.spcnodeid              
     , map.suffix 
     , upper(m.meterid) as meterEquimentID
     , m.meternumber 
     , ph.phasecodeid as SupplyPhase
     , uom.unitofmeasureid
     , uom.description
     , pkg_ched_utils.fn_apply_timezone(nl2.effectivestartdate) as nl_start
     , pkg_ched_utils.fn_apply_timezone(nl2.effectiveenddate) as nl_end
     , vs.validationsetid
     , es.statusid
from servicepoint s
join nodelink nl on nl.leftnodekey = s.nodekey
join servicepointchannel spc on spc.nodekey = nl.rightnodekey
join mts_spc_suffix_map map on MAP.SPC_ID = SPC.CHANNELNUMBER
join nodelink nl2 on nl2.leftnodekey = spc.nodekey
left join intervalchannel ic on ic.nodekey = nl2.rightnodekey
left join registerchannel rc on rc.nodekey = nl2.rightnodekey
join nodelink nl3 on nl3.rightnodekey = ic.nodekey
join meter m on m.nodekey = nl3.leftnodekey
join entitystatus es on es.entitystatuskey = m.entitystatuskey 
join phasecode ph on ph.phasecodekey = m.phasecodeid
join unitofmeasure uom on uom.unitofmeasurekey = spc.unitofmeasurekey
join spcvalidationset svs on svs.servicepointchannelkey = spc.servicepointchannelkey
join validationset vs on vs.validationsetkey = svs.validationsetkey 
where 1=1
and (svs.effectivestartdate < nl2.effectiveenddate and svs.effectiveenddate > nl2.effectivestartdate)
and s.servicepointid = '<NMI>' /*AND S.SERVICEPOINTID = 'VAAA0021074'*/
