WITH  RESERVED_NMIS
     AS (SELECT DISTINCT NMI
           FROM TEST_DATA_KEY_REGISTER TDR
          WHERE     (   TDR.IS_RETIRED_YN = 'Y'
                     OR TDR.IS_IN_USE_YN = 'Y'
                     OR TDR.IS_RESERVED_YN = 'Y')
                AND TDR.CREATED_TS > TRUNC (SYSDATE)),
                        NMI_TO_ROLE
     AS (SELECT MP.PARTICIPANT_NAME AS MP_TO,
                R.NAME,
                MP.ID AS MP_ID,
                r.id AS R_ID
           FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID),
        MultiReg_Meter as(
            select METERID from (select distinct METERID, CHANNELNUMBER from IEE_FLATCONFIGPHYSICAL a
                 where CHANNELNUMBER is not NULL and meterid is not NULL and  EFFECTIVEENDDATE>SYSDATE) group by METERID having count(meterid)>=1),
      MultiMeter_MultiReg as (
                 select SERVICEPOINTID from (select distinct A.SERVICEPOINTID,A.METERID from IEE_FLATCONFIGPHYSICAL a, MultiReg_Meter SRM
                    where  A.METERID is not NULL 
                    and SRM.METERID= A.METERID
                    and EFFECTIVEENDDATE>SYSDATE)
                     group by SERVICEPOINTID
                      having count(servicepointid)=1),
         MULTIVER as (
                        select IFP.SERVICEPOINTID, IFP.METERID from IEE_FLATCONFIGPHYSICAL ifp
                  where channelnumber<>4101
                  and channelnumber<>4201
                  and channelnumber<>4301
                  and meterid is not NULL
                  and IFP.EFFECTIVEENDDATE<SYSDATE),
    NMI_DETAILS as (
                SELECT SS.SERVICEPOINT AS NMI
           FROM STATUS ST,
                SERVICEPOINT_STATUS SS,
                MultiMeter_MultiReg SMR,
                SERVICEPOINT_REF_ATTRIBUTE SRA,
                SERVICEPOINT_REF_ATTRIBUTE SRA2,
                IEE_FLATCONFIGPHYSICAL IFP, 
                  IEE_METERATTRIBUTES IM, MULTIVER MV
            WHERE     SS.STATUS_ID = ST.ID
                AND SMR.SERVICEPOINTID=SS.SERVICEPOINT
                AND ST.NAME = 'A'
                AND SS.END_DT > SYSDATE
                AND SRA.SERVICEPOINT = SS.SERVICEPOINT
                AND SS.SERVICEPOINT = IFP.SERVICEPOINTID
                AND SRA.VALUE like  '<Meter_Type>%'
                AND SRA.END_DT > SYSDATE
                AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
                AND SRA2.VALUE =  '<NMI_Size>'
                AND SRA2.END_DT > SYSDATE
               AND  IFP.meterid is not NULL
               and MV.SERVICEPOINTID=IFP.SERVICEPOINTID
               and IFP.METERID=MV.METERID
                and IFP.EFFECTIVEENDDATE>SYSDATE
                and IFP.METERID=IM.METERID
                and IM.METEREFFECTIVEENDDATE>SYSDATE
                and IM.STATUSID='RemoteDisc'
         MINUS
         SELECT NMI FROM RESERVED_NMIS),
         ROUTE_SEL as (
         select ND.NMI, IR.ROUTE, min(SR.READ_DATE) as NEXT_READ_DATE from IEE_ROUTEBYSP IR, NMI_DETAILS ND, scheduled_read SR
         where ND.NMI=IR.SERVICEPOINT
         and SR.ROUTE=IR.ROUTE
         and SR.READ_DATE>SYSDATE
         and IR.EFFECTIVEENDLINKDATE>SYSDATE
         group by ND.NMI, IR.ROUTE)
 SELECT SRA.VALUE AS NMI_SIZE,
       'MDP' AS Static_MDP,
       'RP' AS Static_RP,
       'MPB' AS Static_MPB,
       'LNSP' AS Static_LNSP,
       MPRP.PARTICIPANT_NAME AS ROLE_RP,
       MPMDP.PARTICIPANT_NAME AS ROLE_MDP,
       MPMPB.PARTICIPANT_NAME AS ROLE_MPB,
       MPMPC.PARTICIPANT_NAME AS ROLE_MPC,
       MPLNSP.PARTICIPANT_NAME AS ROLE_LNSP,
       MPFRMP.PARTICIPANT_NAME AS ROLE_FRMP,
       (SELECT MPAF.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPAF, MARKET_PARTICIPANT_ROLE MPRAF, NMI_TO_ROLE NR
         WHERE     MPAF.ID = MPRAF.MARKET_PARTICIPANT_ID
               AND MPRAF.ROLE_ID = 6                            /* 6 = FRMP */
               and MPAF.ID <> nr.mp_id
               AND nr.r_id = 6
               AND MPAF.ID <> SMRFRMP.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
          AS ROLE_ALTFRMP,
       (SELECT MPARP.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPARP,
               MARKET_PARTICIPANT_ROLE MPRARP,
               NMI_TO_ROLE nr
         WHERE     MPARP.ID = MPRARP.MARKET_PARTICIPANT_ID
               AND MPRARP.ROLE_ID = 2
               AND MPARP.id <> nr.mp_id
               AND nr.r_id = 2
               AND MPARP.ID <> SMRRP.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
          AS ROLE_ALTRP,                    /*2 = RP*/
       (SELECT MPAMD.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPAMD, MARKET_PARTICIPANT_ROLE MPRAMD
         WHERE     MPAMD.ID = MPRAMD.MARKET_PARTICIPANT_ID
               AND MPRAMD.ROLE_ID = 5                            /* 5 = MDP */
               AND MPAMD.ID <> SMRMDP.MARKET_PARTICIPANT_ID
               AND ROWNUM <= 1)
          AS ROLE_ALTMDP,
       (SELECT MPAMB.PARTICIPANT_NAME
          FROM MARKET_PARTICIPANT MPAMB,
               MARKET_PARTICIPANT_ROLE MPRAMB,
               NMI_TO_ROLE nr
         WHERE     MPAMB.ID = MPRAMB.MARKET_PARTICIPANT_ID
               AND MPRAMB.ROLE_ID = 3                             /* 3= MPB */
               AND MPAMB.ID <> SMRMPB.MARKET_PARTICIPANT_ID
               AND mpamb.id <> nr.mp_id
               AND nr.r_id = 3
               AND ROWNUM <= 1)
          AS ROLE_ALTMPB,
       S.NAME AS NMI_STATUS,
       NDF.NMI AS NMI,
        SRA2.VALUE as METER_TYPE,
        TO_CHAR(RS.NEXT_READ_DATE ,'YYYY-MM-DD') ,
        RS.ROUTE CORROUTE,
       (select ROUTE from IEE_ROUTEBYSP IR
       where  RS.ROUTE<> IR.ROUTE
       and ROWNUM<=1) as ROUTE,
        'RWD' as READTYPECODE, /*HARDCODED AS COMMS DO NOT HAVE READ TYPES*/
       'zCol' AS zCol
  FROM SERVICEPOINT_STATUS SS
       JOIN NMI_DETAILS NDF ON SS.SERVICEPOINT = NDF.NMI
       JOIN ROUTE_SEL RS ON RS.NMI=SS.SERVICEPOINT
       JOIN STATUS S ON S.ID = SS.STATUS_ID
       JOIN SERVICEPOINT_REF_ATTRIBUTE SRA
          ON SRA.SERVICEPOINT = SS.SERVICEPOINT
       JOIN SERVICEPOINT_REF_ATTRIBUTE SRA2
          ON SRA2.SERVICEPOINT = SS.SERVICEPOINT
       JOIN SERVICEPOINT_MP_ROLE SMRRP
          ON SMRRP.SERVICEPOINT = SS.SERVICEPOINT AND SMRRP.ROLE_ID = 2 /* 2 = RP   */
       JOIN MARKET_PARTICIPANT MPRP
          ON     MPRP.ID = SMRRP.MARKET_PARTICIPANT_ID
             AND NOT EXISTS
                    (SELECT 1
                       FROM ORGANISATION_PARTICIPANT
                      WHERE PARTICIPANT_ID = MPRP.ID)
       JOIN SERVICEPOINT_MP_ROLE SMRMDP
          ON SMRMDP.SERVICEPOINT = SS.SERVICEPOINT AND SMRMDP.ROLE_ID = 5 /* 5 = MDP  */
       JOIN MARKET_PARTICIPANT MPMDP
          ON     MPMDP.ID = SMRMDP.MARKET_PARTICIPANT_ID
             AND NOT EXISTS
                    (SELECT 1
                       FROM ORGANISATION_PARTICIPANT
                      WHERE PARTICIPANT_ID = MPMDP.ID)
       JOIN SERVICEPOINT_MP_ROLE SMRMPB
          ON SMRMPB.SERVICEPOINT = SS.SERVICEPOINT AND SMRMPB.ROLE_ID = 3 /* 3 = MPB  */
       JOIN MARKET_PARTICIPANT MPMPB
          ON     MPMPB.ID = SMRMPB.MARKET_PARTICIPANT_ID
             AND NOT EXISTS
                    (SELECT 1
                       FROM ORGANISATION_PARTICIPANT
                      WHERE PARTICIPANT_ID = MPMPB.ID)
       JOIN SERVICEPOINT_MP_ROLE SMRMPC
          ON SMRMPC.SERVICEPOINT = SS.SERVICEPOINT AND SMRMPC.ROLE_ID = 4 /* 4 = MPC  */
       JOIN MARKET_PARTICIPANT MPMPC
          ON MPMPC.ID = SMRMPC.MARKET_PARTICIPANT_ID
       JOIN SERVICEPOINT_MP_ROLE SMRLNSP
          ON SMRLNSP.SERVICEPOINT = SS.SERVICEPOINT AND SMRLNSP.ROLE_ID = 1 /* 1 = LNSP */
       JOIN MARKET_PARTICIPANT MPLNSP
          ON MPLNSP.ID = SMRLNSP.MARKET_PARTICIPANT_ID
       JOIN SERVICEPOINT_MP_ROLE SMRFRMP
          ON SMRFRMP.SERVICEPOINT = SS.SERVICEPOINT AND SMRFRMP.ROLE_ID = 6 /* 6 = FRMP */
       JOIN MARKET_PARTICIPANT MPFRMP
          ON MPFRMP.ID = SMRFRMP.MARKET_PARTICIPANT_ID
 WHERE     1 = 1
       AND S.NAME <> 'X'
       AND SRA.VALUE = '<NMI_Size>'
       AND SRA.END_DT > SYSDATE
       AND SRA2.VALUE like '<Meter_Type>%'
       AND SRA2.END_DT > SYSDATE
       AND SS.END_DT > SYSDATE
       AND SMRRP.END_DT > SYSDATE
       AND SMRMDP.END_DT > SYSDATE
       AND SMRMPB.END_DT > SYSDATE
       AND SMRMPC.END_DT > SYSDATE
       AND SMRLNSP.END_DT > SYSDATE
       AND SMRFRMP.END_DT > SYSDATE
	    AND SS.SERVICEPOINT NOT IN (SELECT DISTINCT (CCD.NMID || CCD.CHECKSUM) FROM CAT_CR_DATA CCD WHERE CCD.UPDATED_DT >= SYSDATE - 100)
AND ROWNUM <= 1
  AND 1 = 1