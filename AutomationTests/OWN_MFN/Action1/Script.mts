﻿Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

Dim intScratch
gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function
gstr_Field_Seperator = "^"
' Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true
Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch, strScratch1
Dim gFWcell_xferNMI 

BASE_XML_Template_DIR = cBASE_XML_Template_DIR

Dim strFolderNameWithSlashSuffix_Scratch
' strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR ' : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
BASE_GIT_DIR =   cBASE_GIT_DIR

Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir  = BASE_GIT_DIR


' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  

	Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = cTemp_Execution_Results_Location
	Dim strRunLog_Folder
	Dim str_CorrelationId
	dim qtTest, qtResultsOpt, qtApp
	
	
	Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
	If qtApp.launched <> True then
	    qtApp.Launch
	End If
	
	qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	' qtApp.Options.Run.RunMode = "Fast"
	qtApp.Options.Run.ViewResults = False
	
	
	Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
	Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539


	Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
	str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
	If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
		str_AnFw_FocusArea = ""
	else
		str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
	End If
	
	strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
	    gQtTest.Name                                 , _
	    Parameter.Item("Environment")                       , _
	    Parameter.Item("Company")                           , _
	    fnTimeStamp(gDt_Run_TS, "YYYYMMDD_HHmmss")))
	    ' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))



	Path_MultiLevel_CreateComplete strRunLog_Folder
	gFWstr_RunFolder = strRunLog_Folder
	
	qtResultsOpt.ResultsLocation = gFWstr_RunFolder
	gQtResultsOpt.ResultsLocation = strRunLog_Folder
	

	qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
	qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
	qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
	qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
	qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
	qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
	qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
	qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
	qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
	qtApp.Options.Run.ScreenRecorder.RecordSound = False
	qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True
	
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	
	gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
	gQtApp.Options.Run.ViewResults                = False

	strFolderNameWithSlashSuffix_Scratch = strRunLog_Folder


' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary

'Set gdict_scratch = CreateObject("Scripting.Dictionary")
'gdict_scratch.CompareMode = vbTextCompare
'
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  


 
	'  Description:-  Function to replace arguments in a string format
	'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 

	'						The template should have arguments within {} and the numbering should s
	
	If 1 = 1  Then		
					
					
					Environment.Value("COMPANY")=Parameter.Item("Company")
					Environment.Value("ENV")=Parameter.Item("Environment")
					    
					' Load the MasterWorkBook objWB_Master
					Dim strWB_noFolderContext_onlyFileName  
					strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
					
					Dim wbScratch
					Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
					dim  dictWSDataParameter_KeyName_ColNr
					
					fnExcel_CreateAppInstance  objXLapp, true
					
					' Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO"
					' LoadData_RunContext_V2  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
					' LoadData_RunContext_V3 objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
					LoadData_RunContext objXLapp, wbScratch, 0, Parameter.Item("Worksheet_Name"), strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, 0, tsOfRun, strRunLog_Folder

					Dim	strEnvCoy
					strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")

					' ####################################################################################################################################################################################
					' #####################################################                 LOGGING STEP 1 - SETUP      #################################################################################################
					' ####################################################################################################################################################################################
					fn_setup_Test_Execution_Log Parameter.Item("Company"), Parameter.Item("Environment"), "N", "OWN_PIN", GstrRunFileName, "OWN_PIN", strRunLog_Folder, gObjNet.ComputerName
					Printmessage "i", "Start of Execution", "Starting execution of OWN_PIN scripts"
					
					Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
					Set wbScratch = nothing
								
					objXLapp.Calculation = xlManual
					'objXLapp.screenUpdating=False
					
					Dim objWS_DataParameter 						 
					set objWS_DataParameter           					= objWB_Master.worksheets(Parameter.Item("Worksheet_Name"))
					objWS_DataParameter.activate
					
					Dim objWS_templateXML_TagDataSrc_Tables 	
					set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
					gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

					Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
					
				'	Get headers of table in a dictionary
					Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
					gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
					
					objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
					objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
					objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
					objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
					objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = now 

					gDt_Run_TS = now
					gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 

					Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE
					
					'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
					Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
					dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
					Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
					intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
					intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
					intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
					fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
					
					' =========
					' =========  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   =============
					' =========
					
					Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
					
					dim r
					Dim objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, objDBConn_TestData_A, objDBRcdSet_TestData_A
					Dim objDBConn_TestData_CIS, objDBRcdSet_TestData_CIS
					
'					On error resume next
		'	add more of these as-required, for querying databases
		'	add more of these as-required, for querying databases
					Set objDBConn_TestData_IEE    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_IEE  = CreateObject("ADODB.Recordset")
		'	add more of these as-required, for querying databases

					Set objDBConn_TestData_CIS    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_CIS  = CreateObject("ADODB.Recordset")


					Dim objDBConn_TestData_MTS, objDBRcdSet_TestData_MTS
					Set objDBConn_TestData_MTS    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_MTS  = CreateObject("ADODB.Recordset")

			Dim	DB_CONNECT_STR_MTS
					DB_CONNECT_STR_MTS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value("USERNAME") 	& 	";Password=" & _
						Environment.Value("PASSWORD") 	& ";"

			Dim	DB_CONNECT_STR_IEE
					DB_CONNECT_STR_IEE = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value(strEnvCoy & 	"_IEE_HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value(strEnvCoy & 	"_IEE_SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value(strEnvCoy & 	"_IEE_USERNAME") 	& 	";Password=" & _
						Environment.Value(strEnvCoy & 	"_IEE_PASSWORD") 	& ";"

			Dim	DB_CONNECT_STR_CIS
					DB_CONNECT_STR_CIS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
						Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
						Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
						Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
						Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"
'
'					Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
'					
					Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
					Dim strQueryTemplate, dtD, Hyph, strNMI
					
					Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
					Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
					' intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
					
					Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
					
					Dim intSQL_Pkg_ColNr, strSQL_RunPackage
					
					
					Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
					
						If Parameter.Item("RowNr_StartOn") = 0 and Parameter.Item("RowNr_FinishOn") = 0 Then
							intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
							intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
						Else
							intRowNr_LoopWS_RowNr_StartOn 	= Parameter.Item("RowNr_StartOn")
							intRowNr_LoopWS_RowNr_FinishOn	= Parameter.Item("RowNr_FinishOn")
						End If
						
						intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
					
								
							'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
						intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
						sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
					
						Dim strColName
						objWS_DataParameter.calculate	'	to ensure the filename is updated
									
									
									
					sb_File_WriteContents  _
						gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, "", objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
					
				    Dim intColNr_ScenarioStatus, intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
						  
						  intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
						  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
					
						Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
						If IsEmpty(intColNr_InScope) then	
							blnColumnSet =false
						ElseIf intColNr_InScope = -1 then
							blnColumnSet =false
						End if
						If not blnColumnSet then 
					'		qq log this	
					'		objXLapp.Calculation = xlManual
							objXLapp.screenUpdating=True
							exittest
						End If
					
						objXLapp.Calculation = xlManual 
						objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
						objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
						
						Printmessage "i", "Start row number", "Start row for the test is " & intRowNr_LoopWS_RowNr_StartOn
						Printmessage "i", "End row number", "End row for the test is " & intRowNr_LoopWS_RowNr_FinishOn
						
						objWS_DataParameter.calculate
						objxlapp.screenupdating = true
					
						' objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
					
					' =========
					' =========  FrameworkPhase 0c   - Setup the MultiStage Array                                           =============
					' =========
					
					Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
					Dim  intMasterZoomLevel 
					
					Dim rowStart, rowEnd, colStart, colEnd
					Dim RangeAr 
					
					
					'    ===>>>   Enable HotKeys
					' NOT NEEDED objWB_Master.Application.Run "HotKeys_Set"
					
					'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
					' NOT NEEDED objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
					
					
	End if

				objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = Parameter("InScope_Column_Name")
					
dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq


sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

	

'	determine the number of in-scope rows
Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = now()


'	qq     intNrOfRows_Inscope_and_Unfinished

sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , false, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf



dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow

'	now setup to do process the DriverSheet


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng

Dim str_ScenarioRow_FlowName 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false

Dim intColNr_Request_ID 
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
 intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
 Dim strRole, strInitiator_Role, var_ScheduledDate

Dim strInScope, strRowCompletionStatus, temp_all_Error_Messages
temp_all_Error_Messages = ""

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=true
objWB_Master.save


	
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
		' Get headers of table in a dictionary
	Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	

	
		' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue
	


' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
' Close MTS
fnMTS_WinClose
' Launch MTS
OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")
'
' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID
' Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role
'																	
Dim temp_XML_Tag_MeterDetails, temp_XML_Tag_MeterDetails_runtimeUpdated, temp_XML_Tag_MeterDetails_FinalTag, temp_XML_Tag_CSVIntervalData
Dim temp_XML_Tag_RegisterDetails, temp_XML_Tag_RegisterDetails_runtimeUpdated, temp_XML_Tag_RegisterDetails_FinalTag																	
Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role, temp_str_aqs, temp_bln_scratch


int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0
temp_bln_scratch = false

i = 1

Dim strAr_acrossA_Role, strAr_Stages
Dim intArSz_Role, intArSz_Stages, strPrefix_RoleIsRelevant, temp_CSV_Template 
Dim strCaseGroup, strCaseNr, strXML_TemplateName_Current, Date_TransactionDate
Dim str_DataParameter_PopulatedXML_FqFileName, str_MTS_Txn_Status
Dim strVerification_String, temp_range_rg_VerificationString_Suffix, varAr_Verification_String
Dim strSQL_MTS, strSQL_IEE, temp_XML_Tag_CSVIntervalData_FinalTag, temp_XML_Tag_ConcatenatedRegisters
Dim temp_strSqlCols_NamesList, temp_strSqlCols_ValuesList, temp_strSql_Suffix, temp_IEE_MeterNumber, temp_IEE_Meter_Counter, temp_IEE_Register_Counter, temp_No_of_Required_Meters


' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Do
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
									'	intRowNr_LoopWS_RowNr_StartOn   qq
	
		'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
		strInScope 							= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value

		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
			If   ucase(strInScope) = ucase(cY)  and str_ScenarioRow_FlowName <> "" and lcase(str_Next_Function) <> "fail" and lcase(str_Next_Function) <> "end" Then

				If i = 1 Then
					If ucase(Parameter.Item("RunMode")) = "N" Then ' Value can be N- New or R - restart
						objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start-SQL"
						str_Next_Function = "start-SQL"
					End If
					fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
					int_ctr_total_Eligible_rows = int_ctr_total_Eligible_rows + 1
					objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
				End If
			
				' Generate a filename for screenshot file
				gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, left(temp_Scenario_ID,10)))

				' ####################################################################################################################################################################################
				' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
				' ####################################################################################################################################################################################
				fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", "", "", "", "", "", ""
				
			Select Case lcase(str_ScenarioRow_FlowName)
				
				' ################################################################# START of first flow ####################################################################################################
				' ################################################################# START of first flow ####################################################################################################
				Case "flow1", "flow2", "flow3":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
								Select Case str_Next_Function
									Case "start-SQL" ' this is datamine
										' First function
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
											' capture MTS query
											strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Template_Name")).value 
											
											
											If trim(strSQL_MTS) <> ""  Then
											
												  PrintMessage "p", "MTS SQL template","Scenario row("& intRowNr_CurrentScenario &") - MTS SQL Template ("& strSQL_MTS &") "
											        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											        
											        
											        If lcase(strQueryTemplate) <> "fail" Then
											        	
														strSQL_MTS = strQueryTemplate
											            	
										            	PrintMessage "p", "SQL Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing MTS SQL ("& strSQL_MTS &") "
											            	
														' Execute MTS SQL
														fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "MTS Data Mine query", strSQL_MTS
														strScratch = Execute_SQL_Populate_DataSheet_Hatseperator_Multiple_Records (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_MTS, objDBRcdSet_TestData_MTS, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
														  
														If strScratch = "PASS" Then
															' Write the data in all respective columns for all roles
															' Reserve NMI
															str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
															strScratch = objDBRcdSet_TestData_MTS.RecordCount
															objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value = "'" & strScratch 

															' We need to reserve all the NMI's
															' ####################################################################################################################################################################################
															' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
															' ####################################################################################################################################################################################
															fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", str_NMI, "", "", "", "", ""


															For intctr = 1 To strScratch 
																str_NMI = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI"&intctr)
																PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as candidate"															
																fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "candidate"
															Next
															
														End If
													End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
											Else ' Commenting as the user may wish to use the same NMI in multiple rows
												fn_set_error_flags true, "Query for row (" & intRowNr_CurrentScenario & ") is missing"

											End If ' end of If strSQL_MTS <> ""  Then
											
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "Problem with SQL, moving to the next row", "Query did not return any data"
												' create a function to dump error in a file
												strScratch = gFWstr_RunFolder & "Error_Dump_" & fnTimeStamp(now, "YYYY`MM`DD_HH`mm`ss") & ".txt"
												sb_File_WriteContents  strScratch, gfsoForAppending, true, gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "MILESTONE - Problem with SQL, moving to the next row", "Error with SQL logged in file ("& strScratch &")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CSV_Creation"
												PrintMessage "p", "MILESTONE - Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
			            					
											strSQL_MTS = ""
											
									Case "CSV_Creation"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										
										temp_CSV_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CSVTemplate")).value
							
										If temp_CSV_Template  <> "" Then
							
											PrintMessage "p", "Starting CSV creation","Scenario row ("& intRowNr_CurrentScenario &") - starting CSV creation with template ("& temp_XML_Template &")"
											int_no_of_Records = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value
											temp_CSV_Template_Populated = ""
											temp_CSV_Template_RuntimeUpdates = ""
											
											' <NMI>,<DateIdentified>,<SupplyOn>,<SupplyOff>,<ReasonForNotice>,<Notes>,<MeterNumber>,<ParticipantID>,<RecipientID>
											
											for intctr = 1 to int_no_of_Records 
												temp_CSV_Template_RuntimeUpdates = temp_CSV_Template
												
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<NMI>",left(fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI"&intctr),10))
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<DateIdentified>",fnTimeStamp(now,"DD-MM-YYYY"))
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<DateIdentifiedminusOneDay>",fnTimeStamp(now-1,"DD-MM-YYYY"))
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<SupplyOn>",objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SupplyOn")).value)
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<SupplyOff>",objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SupplyOff")).value)
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<ReasonForNotice>",objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ReasonForNotice")).value)
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<Notes>",objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes")).value)
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<MeterNumber>",fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "MeterNumber"&intctr))
												' temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<ParticipantID>",objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ParticipantID_Initiator")).value)
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<ParticipantID>", fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "INITIATOR"&intctr))
												' temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<RecipientID>",objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("RecipientID")).value)
												temp_CSV_Template_RuntimeUpdates = replace(temp_CSV_Template_RuntimeUpdates,"<RecipientID>", fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "RECIPIENT"&intctr))
												
												If temp_CSV_Template_Populated = "" Then
													temp_CSV_Template_Populated = temp_CSV_Template_Populated & temp_CSV_Template_RuntimeUpdates	
												Else
													temp_CSV_Template_Populated = temp_CSV_Template_Populated & vbCrLf & temp_CSV_Template_RuntimeUpdates
												End If
											Next

											If temp_CSV_Template_Populated <> "" Then
												temp_filename = gFWstr_RunFolder & "MFN_CSV" & fnTimeStamp(now, "YYYYMMDDHHmmss") & ".CSV"
												sb_File_WriteContents  temp_filename, gfsoForAppending, true, temp_CSV_Template_Populated
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_CSV_Filename")).value = temp_filename
											End If
											
											
											
'											' ####################################################################################################################################################################################
'											' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
'											' ####################################################################################################################################################################################
'											fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", str_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
'											
			            				Else
			            					fn_set_error_flags true, "CSV Template for row (" & intRowNr_CurrentScenario & ") is missing. Please fill value in column CSVTemplate"
										End If 'end of If temp_CSV_Template  <> "" Then

											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at CSV creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "MILESTONE - CSV creation failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CSV creation complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Upload_CSV"
												 PrintMessage "p", "MILESTONE - CSV creation complete", "CSV creation complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												
			            					End If

									Case "MTS_Upload_CSV" ' PENDING fro UI
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										PrintMessage "i", "MTS_Upload_CSV step", "Starting to load CSV in MTS"
										
										fn_MTS_MenuNavigation "Transactions;One Way Notification;Meter Fault and Issue Notification;Search Meter Fault and Issue Notifications Sent"
										
										fn_wait(3)
										fn_MTS_Click_button_any_screen "Search_Meter_Fault_and_Issue_Notifications_Sent", "btn_Upload"
										
										' Select the file and upload - the function should capture error message and geenrate a failure
										strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_CSV_Filename")).value
										strScratch1 = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Popup_Notification")).value
										
										' We need to replace the NMI in message
										' Check for NMI
										If instr(1, strScratch1, "<NMI>") > 0  Then
											strScratch1 = replace(strScratch1, "<NMI>", left(fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI1"),10))
										End If

										' Check for Meter Number
										If instr(1, strScratch1, "<Meter Number>") > 0  Then
											strScratch1 = replace(strScratch1, "<Meter Number>", left(fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "METERNUMBER1"),10))
										End If

										' fn_MTS_MFN_Bulk_Request_File_Upload "", strScratch, "", "y", "MFN has been successfully saved", "n", "y", gFW_strRunLog_ScreenCapture_fqFileName
										fn_MTS_MFN_Bulk_Request_File_Upload "", strScratch, "", "y", strScratch1, "n", "y", gFW_strRunLog_ScreenCapture_fqFileName

										fn_MTS_Close_any_screen "Search_Meter_Fault_and_Issue_Notifications_Sent"
										
										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Upload_CSV because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - MTS_Upload_CSV creation failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else
											If lcase(str_ScenarioRow_FlowName) <> "flow2" Then
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Upload_CSV complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_FETCH_TRANSACTION_ID"
												 PrintMessage "p", "MILESTONE - MTS_Upload_CSV complete", "MTS_Upload_CSV complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_TwoMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											ElseIf lcase(str_ScenarioRow_FlowName) = "flow2" Then
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Upload_CSV complete"
												printmessage "p", "MILESTONE - MTS_Upload_CSV complete", "MILESTONE - MTS_Upload_CSV complete for row ("  & intRowNr_CurrentScenario &  ")" 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												Exit Do													
											End If										
		            					End If

									Case "MTS_FETCH_TRANSACTION_ID" ' PENDING fro UI
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										' PrintMessage "i", "MFN_OUT_XML_VERIFICATION_TRANS_ACK step", "Executing query in column MTS_SQL_MFN_FetchLatest_TransactionID to fetch the INIT Transaction ID for all records"

										strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_MFN_FetchLatest_TransactionID")).value 

										If strSQL_MTS <> ""  Then
										
									        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											
											If LCASE(strQueryTemplate) <> "fail" Then
												
												' Here we need to loop for the total number of records, run the query and do verifications
												int_no_of_Records = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value
												
												If int_no_of_Records > 0  Then
													PrintMessage "i", "OWN_MFN_MTS_DB_Validation step", "Starting to verify OWN MFN MTS DB Validation for total records - " & int_no_of_Records 
													
													for intctr = 1 to int_no_of_Records 
														strSQL_MTS  = strQueryTemplate
														
														' Perform replacements
										            	strSQL_MTS = replace(strSQL_MTS,"<NMI>",left(fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI"&intctr),10))
										            	fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "MTS_SQL_MFN_FetchLatest_TransactionID query("&intctr&") for MFN", strSQL_MTS
														strScratch = ""
														strScratch = Execute_SQL_Populate_DataSheet_Hatseperator_User_Defined_AttributeIndex (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_MTS, objDBRcdSet_TestData_MTS, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "", intctr)
														If strScratch = "FAIL" Then
															intctr = intctr + 1
															objWB_Master.save
														End If ' If strScratch = "PASS" Then
													Next 'end of for intctr = 1 to int_no_of_Records 
												Else
													fn_set_error_flags true, "MTS_FETCH_TRANSACTION_ID step - total records are 0, existing current row"
													PrintMessage "f", "MTS_FETCH_TRANSACTION_ID step", gFWstr_ExitIteration_Error_Reason
												End If ' If int_no_of_Records > 0  Then
											Else
												fn_set_error_flags true, "MTS_FETCH_TRANSACTION_ID step - Cannot locate query template ("&strSQL_MTS&")"
												PrintMessage "f", "MTS_FETCH_TRANSACTION_ID step", gFWstr_ExitIteration_Error_Reason
											End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
										Else
											fn_set_error_flags true, "MTS_FETCH_TRANSACTION_ID step - MTS_SQL_MFN_FetchLatest_TransactionID column is blank. Please supply the query template name for verification"
											PrintMessage "f", "MTS_FETCH_TRANSACTION_ID step", gFWstr_ExitIteration_Error_Reason
										End If ' end of check related to query

										
										
										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_FETCH_TRANSACTION_ID because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - MTS_FETCH_TRANSACTION_ID failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_FETCH_TRANSACTION_ID complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MFN_OUT_XML_VERIFICATION_TRANS_ACK"
											 PrintMessage "p", "MILESTONE - MTS_FETCH_TRANSACTION_ID complete", "MTS_FETCH_TRANSACTION_ID complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_OneMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		            					End If


									Case "MFN_OUT_XML_VERIFICATION_TRANS_ACK"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										PrintMessage "i", "MFN_OUT_XML_VERIFICATION_TRANS_ACK step", "Starting to verify MFN OUT XML (trans ack)"
										
										' capture MTS query to check trans ack
										strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_MFN_Trans_ack_Template_Name")).value 
										
										
										If strSQL_MTS <> ""  Then
										
									        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											
											If LCASE(strQueryTemplate) <> "fail" Then
												
												' Here we need to loop for the total number of records, run the query and do verifications
												int_no_of_Records = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value
												
												If int_no_of_Records > 0  Then
													
													for intctr = 1 to int_no_of_Records 
														strSQL_MTS  = strQueryTemplate
														
														
														' Perform replacements
										            	strSQL_MTS = replace ( strSQL_MTS , "<INIT_TRANSACTION_ID>", fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "INIT_TRANSACTION_ID"&intctr))
										            	fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "MTS_SQL_PIN_Trans_ack_Template_Name query "&intctr&" for PIN", strSQL_MTS
		
														' Execute MTS SQL
														strScratch = Execute_SQL_Populate_DataSheet_Hatseperator_User_Defined_AttributeIndex (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_MTS, objDBRcdSet_TestData_MTS, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "", intctr)
														
														objWB_Master.save
														
														' Verify output of query
														' Trans_Ack_NMI
														str_Expected = "" : str_Actual = ""
														str_Expected = left(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","NMI"&intctr)),10) 
														str_Actual = left(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","Trans_Ack_NMI"&intctr)),10)
														If str_Expected = str_Actual Then
															PrintMessage "p", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  "Actual value of NMI (" & str_Actual & ") is same as expected value ("& str_Expected &")"
														Else
															fn_set_error_flags true, "Actual value of NMI (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
															PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  gFWstr_ExitIteration_Error_Reason
															temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
														End If
														
														' Trans_Ack_DATEIDENTIFIED
														str_Expected = "" : str_Actual = ""
														str_Expected = fnTimeStamp(now,"DD-MM-YYYY")
														str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","Trans_Ack_DATEIDENTIFIED"&intctr))
														If str_Actual <> "" Then
															str_Actual = fnTimeStamp(str_Actual,"DD-MM-YYYY")
														End If
														If str_Expected = str_Actual Then
															PrintMessage "p", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  "Actual value of DATE IDENTIFIED (" & str_Actual & ") is same as expected value ("& str_Expected &")"
														Else
															fn_set_error_flags true, "Actual value of DATE IDENTIFIED (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
															PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  gFWstr_ExitIteration_Error_Reason
															temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
														End If
														
														' Trans_Ack_SUPPLYON
														str_Expected = "" : str_Actual = ""
														str_Expected = trim(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SupplyOn")).value)
														If str_Expected = "Y" Then str_Expected = "Yes"
														If str_Expected = "N" Then str_Expected = "No"
														str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","Trans_Ack_SUPPLYON"&intctr))
														If str_Expected = str_Actual Then
															PrintMessage "p", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  "Actual value of SUPPLY ON (" & str_Actual & ") is same as expected value ("& str_Expected &")"
														Else
															fn_set_error_flags true, "Actual value of SUPPLY ON (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
															PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  gFWstr_ExitIteration_Error_Reason
															temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
														End If
														
														' Trans_Ack_SUPPLYOFF
														str_Expected = "" : str_Actual = ""
														str_Expected = trim(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SupplyOff")).value)
														str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","Trans_Ack_SUPPLYOFF"&intctr))
														If str_Expected = str_Actual Then
															PrintMessage "p", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  "Actual value of SUPPLY OFF (" & str_Actual & ") is same as expected value ("& str_Expected &")"
														Else
															fn_set_error_flags true, "Actual value of SUPPLY OFF (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
															PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  gFWstr_ExitIteration_Error_Reason
															temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
														End If
														
														' Trans_Ack_METERSERIALNUMBER
														str_Expected = "" : str_Actual = ""
														str_Expected = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","METERNUMBER"&intctr))
														str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","TRANS_ACK_METERSERIALNUMBER"&intctr))
														If str_Expected = str_Actual Then
															PrintMessage "p", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  "Actual value of Meter Serial Number (" & str_Actual & ") is same as expected value ("& str_Expected &")"
														Else
															fn_set_error_flags true, "Actual value of Meter Serial Number (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
															PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  gFWstr_ExitIteration_Error_Reason
															temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
														End If
														
														' Trans_Ack_REASONFORNOTICE
														str_Expected = "" : str_Actual = ""
														str_Expected = trim(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ReasonForNotice")).value)
														str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","Trans_Ack_REASONFORNOTICE"&intctr))
														If str_Expected = str_Actual Then
															PrintMessage "p", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  "Actual value of REASON FOR NOTICE (" & str_Actual & ") is same as expected value ("& str_Expected &")"
														Else
															fn_set_error_flags true, "Actual value of REASON FOR NOTICE (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
															PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  gFWstr_ExitIteration_Error_Reason
															temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
														End If
														
														' Trans_Ack_NOTES
														str_Expected = "" : str_Actual = ""
														str_Expected = trim(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes")).value)
														str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","Trans_Ack_NOTES"&intctr))
														If str_Expected = str_Actual Then
															PrintMessage "p", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  "Actual value of NOTES (" & str_Actual & ") is same as expected value ("& str_Expected &")"
														Else
															fn_set_error_flags true, "Actual value of NOTES (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
															PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK Verification",  gFWstr_ExitIteration_Error_Reason
															temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
														End If
														
														If gFWbln_ExitIteration Then
															' Incase there is any error, just exit
															intctr = intctr + 1
															gFWstr_ExitIteration_Error_Reason = temp_all_Error_Messages
															temp_all_Error_Messages = ""
															
														End If ' end of If gFWbln_ExitIteration Then
													Next 'end of for intctr = 1 to int_no_of_Records 
												Else
													fn_set_error_flags true, "MFN_OUT_XML_VERIFICATION_TRANS_ACK step - total records are 0, existing current row"
													PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK step", gFWstr_ExitIteration_Error_Reason
												End If ' If int_no_of_Records > 0  Then
											Else
												fn_set_error_flags true, "MFN_OUT_XML_VERIFICATION_TRANS_ACK step - Cannot locate query template ("&strSQL_MTS&")"
												PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK step", gFWstr_ExitIteration_Error_Reason
											End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
										Else
											fn_set_error_flags true, "MFN_OUT_XML_VERIFICATION_TRANS_ACK step - MTS_SQL_MFN_Trans_ack_Template_Name column is blank. Please supply the query template name for verification"
											PrintMessage "f", "MFN_OUT_XML_VERIFICATION_TRANS_ACK step", gFWstr_ExitIteration_Error_Reason
										End If ' end of check related to query
										
										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MFN_OUT_XML_VERIFICATION_TRANS_ACK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - MFN_OUT_XML_VERIFICATION_TRANS_ACK failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MFN_OUT_XML_VERIFICATION_TRANS_ACK complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "OWN_MFN_MTS_DB_Validation"
											 PrintMessage "p", "MILESTONE - MFN_OUT_XML_VERIFICATION_TRANS_ACK complete", "MFN_OUT_XML_VERIFICATION_TRANS_ACK complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		            					End If

									Case "OWN_MFN_MTS_DB_Validation"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										PrintMessage "i", "OWN_MFN_MTS_DB_Validation step", "Starting to verify OWN MFN MTS DB Validation"
										
										' capture MTS query to check trans ack
										strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_MFN_DB_Validation_Template_Name")).value 

										If strSQL_MTS <> ""  Then
										
									        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											
											If LCASE(strQueryTemplate) <> "fail" Then
												
												' Here we need to loop for the total number of records, run the query and do verifications
												int_no_of_Records = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value
												
												If int_no_of_Records > 0  Then
													PrintMessage "i", "OWN_MFN_MTS_DB_Validation step", "Starting to verify OWN MFN MTS DB Validation for total records - " & int_no_of_Records 
													
													for intctr = 1 to int_no_of_Records 
														strSQL_MTS  = strQueryTemplate
														
														' Perform replacements
										            	strSQL_MTS = replace(strSQL_MTS,"<NMI>",left(fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI"&intctr),10))
										            	fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "MTS_SQL_MFN_DB_Validation query("&intctr&") for MFN", strSQL_MTS
														strScratch = ""
														 strScratch = Execute_SQL_Populate_DataSheet_Hatseperator_User_Defined_AttributeIndex (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_MTS, objDBRcdSet_TestData_MTS, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "", intctr)
														If strScratch = "PASS" Then
															objWB_Master.save
															
															' Verify output of query
															 ' Verify MFN_DB_NMI
															str_Expected = "" : str_Actual = ""
															str_Expected = left(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","NMI"&intctr)),10)
															str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","MFN_DB_NMI"&intctr))
															If str_Expected = str_Actual Then
																PrintMessage "p", "OWN_MFN_MTS_DB_Validation Verification",  "Actual value of NMI (" & str_Actual & ") is same as expected value ("& str_Expected &")"
															Else
																fn_set_error_flags true, "Actual value of NMI (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
																PrintMessage "f", "OWN_MFN_MTS_DB_Validation Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
																		
															' MFN_DB_DATE_IDENTIFIED
															str_Expected = "" : str_Actual = ""
															str_Expected = fnTimeStamp(now,"DD-MM-YYYY")
															str_Actual = fnTimeStamp(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","MFN_DB_DATE_IDENTIFIED"&intctr)),"DD-MM-YYYY")
															If str_Expected = str_Actual Then
																PrintMessage "p", "OWN_MFN_MTS_DB_Validation Verification",  "Actual value of DATE IDENTIFIED (" & str_Actual & ") is same as expected value ("& str_Expected &")"
															Else
																fn_set_error_flags true, "Actual value of DATE IDENTIFIED (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
																PrintMessage "f", "OWN_MFN_MTS_DB_Validation Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															
															' MFN_DB_SUPPLYON
															str_Expected = "" : str_Actual = ""
															str_Expected = trim(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SupplyOn")).value)
															str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","MFN_DB_SUPPLYON"&intctr))
															If str_Expected = str_Actual Then
																PrintMessage "p", "OWN_MFN_MTS_DB_Validation Verification",  "Actual value of SUPPLY ON (" & str_Actual & ") is same as expected value ("& str_Expected &")"
															Else
																fn_set_error_flags true, "Actual value of SUPPLY ON (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
																PrintMessage "f", "OWN_MFN_MTS_DB_Validation Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															
															' MFN_DB_SUPPLYOFF
															str_Expected = "" : str_Actual = ""
															str_Expected = trim(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SupplyOff")).value)
															str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","MFN_DB_SUPPLYOFF"&intctr))
															If str_Expected = str_Actual Then
																PrintMessage "p", "OWN_MFN_MTS_DB_Validation Verification",  "Actual value of SUPPLY OFF (" & str_Actual & ") is same as expected value ("& str_Expected &")"
															Else
																fn_set_error_flags true, "Actual value of SUPPLY OFF (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
																PrintMessage "f", "OWN_MFN_MTS_DB_Validation Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															
															' MFN_DB_METERNUMBER
															str_Expected = "" : str_Actual = ""
															str_Expected = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","MeterNumber"&intctr))
															str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","MFN_DB_METERNUMBER"&intctr))
															If str_Expected = str_Actual Then
																PrintMessage "p", "OWN_MFN_MTS_DB_Validation Verification",  "Actual value of Meter Number (" & str_Actual & ") is same as expected value ("& str_Expected &")"
															Else
																fn_set_error_flags true, "Actual value of Meter Number (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
																PrintMessage "f", "OWN_MFN_MTS_DB_Validation Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															
															' MFN_DB_REASONFORNOTICE
															str_Expected = "" : str_Actual = ""
															str_Expected = trim(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ReasonForNotice")).value)
															str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","MFN_DB_REASONFORNOTICE"&intctr))
															If str_Expected = str_Actual Then
																PrintMessage "p", "OWN_MFN_MTS_DB_Validation Verification",  "Actual value of REASON FOR NOTICE (" & str_Actual & ") is same as expected value ("& str_Expected &")"
															Else
																fn_set_error_flags true, "Actual value of REASON FOR NOTICE (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
																PrintMessage "f", "OWN_MFN_MTS_DB_Validation Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															
															' MFN_DB_NOTES - THIS IS MISSING - Check with Lloyd
															str_Expected = "" : str_Actual = ""
															str_Expected = trim(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes")).value)
															str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","MFN_DB_NOTES"&intctr))
															If str_Expected = str_Actual Then
																PrintMessage "p", "OWN_MFN_MTS_DB_Validation Verification",  "Actual value of NOTES (" & str_Actual & ") is same as expected value ("& str_Expected &")"
															Else
																fn_set_error_flags true, "Actual value of NOTES (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
																PrintMessage "f", "OWN_MFN_MTS_DB_Validation Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If

														End If ' If strScratch = "PASS" Then
														
														If gFWbln_ExitIteration Then
															' Incase there is any error, just exit
															intctr = intctr + 1
															gFWstr_ExitIteration_Error_Reason = temp_all_Error_Messages
															temp_all_Error_Messages = ""
															
														End If ' end of If gFWbln_ExitIteration Then
														
													Next 'end of for intctr = 1 to int_no_of_Records 
												Else
													fn_set_error_flags true, "OWN_MFN_MTS_DB_Validation step - total records are 0, existing current row"
													PrintMessage "f", "OWN_MFN_MTS_DB_Validation step", gFWstr_ExitIteration_Error_Reason
												End If ' If int_no_of_Records > 0  Then
											Else
												fn_set_error_flags true, "OWN_MFN_MTS_DB_Validation step - Cannot locate query template ("&strSQL_MTS&")"
												PrintMessage "f", "OWN_MFN_MTS_DB_Validation step", gFWstr_ExitIteration_Error_Reason
											End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
										Else
											fn_set_error_flags true, "OWN_MFN_MTS_DB_Validation step - MTS_SQL_MFN_DB_Validation_Template_Name column is blank. Please supply the query template name for verification"
											PrintMessage "f", "OWN_MFN_MTS_DB_Validation step", gFWstr_ExitIteration_Error_Reason
										End If ' end of check related to query
													
										
										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at OWN_MFN_MTS_DB_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - OWN_MFN_MTS_DB_Validation failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "OWN_MFN_MTS_DB_Validation complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML"
											 PrintMessage "p", "MILESTONE - OWN_MFN_MTS_DB_Validation complete", "OWN_MFN_MTS_DB_Validation complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		            					End If
		            					
									Case "GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										PrintMessage "i", "GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML step", "Starting to generate a business accept / reject XML"
										
										temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
							
										If temp_XML_Template  <> "" Then
										
												' Here we need to loop for the total number of records, run the query and do verifications
												int_no_of_Records = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value
												
												If int_no_of_Records > 0  Then
													PrintMessage "i", "GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML step", "Starting to generate  ACK BUSINESS ACCEPT REJECT XML for total records - " & int_no_of_Records 
													
													for intctr = 1 to int_no_of_Records 
													
														PrintMessage "p", "Starting XML (Accept/Reject) creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML (Accept/Reject) creation with template ("& temp_XML_Template &")"
														fn_wait(1) ' Intentional wait so that we don't generate same unique reference numbers in XML
														int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") 
														' Append transactionID to SQL Populated columns
														fn_append_To_AnyColumn objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "INIT_TRANSACTION_ID", int_UniqueReferenceNumberforXML, "^"
														
														
														' objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")).value = int_UniqueReferenceNumberforXML
														
														temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
														str_NMI = left(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","NMI"&intctr)),10)
														
														str_ChangeStatusCode = "MFN"
														objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Business_Receipt_XML_ID")).value = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","TRANSACTIONID"&intctr))
														objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("ParticipantID_Initiator")).value = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","INITIATOR"&intctr))
														objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RecipientID")).value = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","RECIPIENT"&intctr))
														objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("To_Role")).value = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","INITIATOR_Role"&intctr))
														temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("To_Role")).value ' Role for gateway
														
														' ####################################################################################################################################################################################
														' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
														' ####################################################################################################################################################################################
														fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", str_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
														PrintMessage "p", "Starting XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"
														
														str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
														temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
														intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now,  int_UniqueReferenceNumberforXML, _
														temp_Role, str_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
														
														' If there were no errors during XML creation, drop in gateway else exit the current row
											            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											            	PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
											            Else
														
															strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")	
															PrintMessage "p", "XML File","Scenario row("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
														
												            ' ZIP XML File
												            ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
												            PrintMessage "p", "Zippng XML File","Scenario row("& intRowNr_CurrentScenario &") - Zipping XML file for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &")"
												                    
												            GetDestinationFolder Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B",  temp_Role
						
															If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
																PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																gFWbln_ExitIteration = "y"
															      gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
															Else
																copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder")
																PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& Str_Role &") - Application (OWN_PIN - same location as CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder" )) = Environment.Value("CATSDestinationFolder") 
															End If
											          	  End If
																									
													Next 'end of for intctr = 1 to int_no_of_Records 
												Else
													fn_set_error_flags true, "OWN_MFN_MTS_DB_Validation step - total records are 0, existing current row"
													PrintMessage "f", "OWN_MFN_MTS_DB_Validation step", gFWstr_ExitIteration_Error_Reason
												End If ' If int_no_of_Records > 0  Then
											
											Else
												PrintMessage "i", "Skipping XML creation", "Skipping XML creation as no template specified in xmlTemplate_Name column"
											End If ' end of If temp_XML_Template  <> "" Then

										
										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Verify_MFN_UI"
											 PrintMessage "p", "MILESTONE - GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML complete", "GENERATE_ACK_BUSINESS_ACCEPT_REJECT_XML complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		            					End If
		            					
		            				Case "MTS_Verify_MFN_UI"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										PrintMessage "i", "MTS_Verify_MFN_UI step", "Starting to verify MFN on MTS"
										
										' Here we need to loop for the total number of records, run the query and do verifications
										int_no_of_Records = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value
										
										If int_no_of_Records > 0  Then
											PrintMessage "i", "OWN_MFN_MTS_DB_Validation step", "Starting to verify OWN MFN MTS DB Validation for total records - " & int_no_of_Records 
											
											for intctr = 1 to int_no_of_Records 
												str_NMI = left(fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI"&intctr),10)
												strScratch = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "INIT_TRANSACTION_ID"&intctr)
												
												fn_MTS_MenuNavigation "Transactions;One Way Notification;Meter Fault and Issue Notification;Search Meter Fault and Issue Notifications Sent"
												
												fn_MTS_Search_Meter_Fault_And_Issue_Notification_Sent fnTimeStamp(now,"DD/MM/YYYY"), fnTimeStamp(now,"DD/MM/YYYY"), str_NMI,  strScratch, "", "", "", "y", "n", ""
		
												fn_MTS_Search_Meter_Fault_And_Issue_Notification_Sent_Select_And_Verify_Record "", "", strScratch, str_NMI, "",   "", "", "", "",  objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Notification_Status")).value , "", "", "", "y", gFW_strRunLog_ScreenCapture_fqFileName
												'fn_MTS_Search_Meter_Fault_And_Issue_Notification_Sent_Select_And_Verify_Record "", "16/08/2017 10:46:20", "MFN_56D43BA97100875EE053180D200A4C96", "6102409350", "CORP\UANAND", "UMPLP", "N", "Remove Fuse", "Malfunction", "SENT", "OFF", "", "", "y", gFW_strRunLog_ScreenCapture_fqFileName
		
												fn_MTS_Close_any_screen "Search_Meter_Fault_and_Issue_Notifications_Sent"
												
												If gFWbln_ExitIteration Then
													intctr = intctr + 1
													gFWstr_ExitIteration_Error_Reason = temp_all_Error_Messages
													temp_all_Error_Messages = ""
												End If
											Next 'end of for intctr = 1 to int_no_of_Records 
										Else
											fn_set_error_flags true, "MTS_Verify_MFN_UI step - total records are 0, existing current row"
											PrintMessage "f", "MTS_Verify_MFN_UI step", gFWstr_ExitIteration_Error_Reason
										End If ' If int_no_of_Records > 0  Then
										
										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Verify_MFN_UI because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - MTS_Verify_MFN_UI failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else


											If lcase(str_ScenarioRow_FlowName) <> "flow3" Then

												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Verify_MFN_UI complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_DB_VALIDATION_Note_Creation"
												 PrintMessage "p", "MILESTONE - MTS_Verify_MFN_UI complete", "MTS_Verify_MFN_UI complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")

											ElseIf lcase(str_ScenarioRow_FlowName) = "flow3" Then
											
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Verify_MFN_UI complete"
												 PrintMessage "p", "MILESTONE - MTS_Verify_MFN_UI complete", "MTS_Verify_MFN_UI complete for row ("  & intRowNr_CurrentScenario &  ")" 
												 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												Exit Do													
											End If										

		            					End If
		            					
		            				Case "CIS_DB_VALIDATION_Note_Creation"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										PrintMessage "i", "CIS_DB_VALIDATION_Note_Creation step", "Starting to validate CIS usign SQL"

										' capture CIS query to check trans ack
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SQL_Note_Creation")).value 

										If strSQL_CIS <> ""  Then
										
									        strQueryTemplate = fnRetrieve_SQL(strSQL_CIS)
											
											If LCASE(strQueryTemplate) <> "fail" Then
												
												' Here we need to loop for the total number of records, run the query and do verifications
												int_no_of_Records = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value
												
												If int_no_of_Records > 0  Then
													PrintMessage "i", "CIS_DB_VALIDATION_Note_Creation step", "Starting to verify record in CIS for total records - " & int_no_of_Records 
													
													for intctr = 1 to int_no_of_Records 
														strSQL_CIS  = strQueryTemplate
														
												        ' Perform replacements
														strSQL_CIS = replace ( strSQL_CIS , "<NMI>", fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","NMI"&intctr) , 1, -1, vbTextCompare)
										            	fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "CIS_SQL_MFN_Note_Creation query for MFN" & intctr, strSQL_CIS
										            	
														strScratch = ""
														strScratch = Execute_SQL_Populate_DataSheet_Hatseperator ( objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_CIS, objDBRcdSet_TestData_CIS, strSQL_CIS, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  "")
														objWB_Master.save
														
														If lcase(strScratch) <> "fail" Then
															PrintMessage "P", "CIS_DB_VALIDATION_Note_Creation step", "Query has returned ("& objDBRcdSet_TestData_CIS.RecordCount &") row(s). There are no specific verification other than checking for a record"
														End If 'end of If lcase(strScratch) <> "fail" Then
														
														If gFWbln_ExitIteration Then
															' Incase there is any error, just exit
															intctr = intctr + 1
														End If ' end of If gFWbln_ExitIteration Then
													Next 'end of for intctr = 1 to int_no_of_Records 
												Else
													fn_set_error_flags true, "CIS_DB_VALIDATION_Note_Creation step - total records are 0, existing current row"
													PrintMessage "f", "CIS_DB_VALIDATION_Note_Creation step", gFWstr_ExitIteration_Error_Reason
												End If ' If int_no_of_Records > 0  Then
											Else
												fn_set_error_flags true, "CIS_DB_VALIDATION_Note_Creation step - Cannot locate query template ("&strSQL_CIS&")"
												PrintMessage "f", "CIS_DB_VALIDATION_Note_Creation step", gFWstr_ExitIteration_Error_Reason
											End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
										Else
											fn_set_error_flags true, "CIS_DB_VALIDATION_Note_Creation step - CIS_SQL_Note_Creation column is blank. Please supply the query template name for verification"
											PrintMessage "f", "CIS_DB_VALIDATION_Note_Creation step", gFWstr_ExitIteration_Error_Reason
										End If ' end of check related to query
													
										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at CIS_DB_VALIDATION_Note_Creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - CIS_DB_VALIDATION_Note_Creation failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_DB_VALIDATION_Note_Creation complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_RESPONSE_FROM_CIS_DB_VALIDATION"
											 PrintMessage "p", "MILESTONE - CIS_DB_VALIDATION_Note_Creation complete", "CIS_DB_VALIDATION_Note_Creation complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		            					End If
		            				

									Case "MTS_RESPONSE_FROM_CIS_DB_VALIDATION"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										PrintMessage "i", "MTS_RESPONSE_FROM_CIS_DB_VALIDATION step", "Starting to verfication of response from CIS in MTS DB"
										

										' capture MTS query to verify response from USB to MTS
										strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("USB_to_MTS_Response_Query")).value 

										If strSQL_MTS <> ""  Then
										
									        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											
											If LCASE(strQueryTemplate) <> "fail" Then
												
												' Here we need to loop for the total number of records, run the query and do verifications
												int_no_of_Records = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataMine_MTS_No_Of_Records")).value
												
												If int_no_of_Records > 0  Then
													PrintMessage "i", "MTS_RESPONSE_FROM_CIS_DB_VALIDATION step", "Starting to verify response from USB to MTS for total records - " & int_no_of_Records 
													
													for intctr = 1 to int_no_of_Records 
														strSQL_MTS  = strQueryTemplate
														
														' Perform replacements
										            	strSQL_MTS = replace(strSQL_MTS,"<NMI>",left(fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI"&intctr),10))
										            	strSQL_MTS = replace ( strSQL_MTS , "<INIT_TRANSACTION_ID>", fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "INIT_TRANSACTION_ID"&intctr))
										            	fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "MTS_SQL_MFN_DB_Validation query("&intctr&") for MFN", strSQL_MTS
														strScratch = ""
														 strScratch = Execute_SQL_Populate_DataSheet_Hatseperator_User_Defined_AttributeIndex (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_MTS, objDBRcdSet_TestData_MTS, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "", intctr)
														If strScratch = "PASS" Then
															objWB_Master.save
															
															' Verify output of query
															 ' Verify MFN_DB_NMI
															str_Expected = "" : str_Actual = ""
															str_Expected = "Success"
															str_Actual = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","USB_TO_MTS_Status"&intctr))
															If str_Expected = str_Actual Then
																PrintMessage "p", "OWN_MFN_MTS_DB_Validation Verification",  "Actual STATUS from USB (" & str_Actual & ") is same as expected value ("& str_Expected &")"
															Else
																fn_set_error_flags true, "Actual STATUS from USB (" & str_Actual & ") is DIFFERENT TO expected value ("& str_Expected &")"
																PrintMessage "f", "OWN_MFN_MTS_DB_Validation Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															
														End If ' If strScratch = "PASS" Then
														
														If gFWbln_ExitIteration Then
															' Incase there is any error, just exit
															intctr = intctr + 1
															gFWstr_ExitIteration_Error_Reason = temp_all_Error_Messages
															temp_all_Error_Messages = ""
															
														End If ' end of If gFWbln_ExitIteration Then
														
													Next 'end of for intctr = 1 to int_no_of_Records 
												Else
													fn_set_error_flags true, "OWN_MFN_MTS_DB_Validation step - total records are 0, existing current row"
													PrintMessage "f", "OWN_MFN_MTS_DB_Validation step", gFWstr_ExitIteration_Error_Reason
												End If ' If int_no_of_Records > 0  Then
											Else
												fn_set_error_flags true, "OWN_MFN_MTS_DB_Validation step - Cannot locate query template ("&strSQL_MTS&")"
												PrintMessage "f", "OWN_MFN_MTS_DB_Validation step", gFWstr_ExitIteration_Error_Reason
											End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
										Else
											fn_set_error_flags true, "OWN_MFN_MTS_DB_Validation step - MTS_SQL_MFN_DB_Validation_Template_Name column is blank. Please supply the query template name for verification"
											PrintMessage "f", "OWN_MFN_MTS_DB_Validation step", gFWstr_ExitIteration_Error_Reason
										End If ' end of check related to query
													

										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_RESPONSE_FROM_CIS_DB_VALIDATION because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - MTS_RESPONSE_FROM_CIS_DB_VALIDATION failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_RESPONSE_FROM_CIS_DB_VALIDATION complete"
											printmessage "p", "MILESTONE - MTS_Verify_MFN complete", "MILESTONE - MTS_Verify_MFN complete for row ("  & intRowNr_CurrentScenario &  ")" 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
											Exit Do
		            					End If
			            					
										case "FAIL"
											Exit Do
										case else ' incase there is no function (which would NEVER happen) name
											Exit Do
								End Select
					Loop While  (lcase(gFWbln_ExitIteration) = "n" or gFWbln_ExitIteration = false)

				' ################################################################# END of first flow ####################################################################################################
				' ################################################################# END of first flow ####################################################################################################


				Case else ' Incase there is no elaboration for a flow, we need to report in datasheet and move on to next row
				    gFWbln_ExitIteration = "Y"
				    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
					PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = gFWstr_ExitIteration_Error_Reason
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
			End Select ' end of 	Select Case str_ScenarioRow_FlowName
		
		End If ' end of If  (	( strInScope = cY)  and str_ScenarioRow_FlowName <> "" and (trim(str_Next_Function) = "" or lcase(str_Next_Function) <> "fail" or lcase(str_Next_Function) <> "end") ) Then

		' ####################################################################################################################################################################################
		' #####################################################                 LOGGING STEP 3 - Clear data before moving to next row #########################################################################
		' ####################################################################################################################################################################################
		fnTestExeLog_ClearRuntimeValues
		
		' Reset error flags
		fn_reset_error_flags
		
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch

	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	

	' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows

	' Reset error flags
	fn_reset_error_flags

	i = 2
	objWB_Master.save ' Save the workbook 
Loop While int_ctr_no_of_Eligible_rows > 0

PrintMessage "i", "MILESTONE - Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & ") - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= now

objWB_Master.save ' Save the workbook 
Printmessage "i", "MILESTONE - End of Execution", "Finished execution of OWN_PIN"

' Close MTS
fnMTS_WinClose

' Once the test is complete, move the results to network drive
Printmessage "i", "MILESTONE - Copy to network drive", "Copying results to network drive ("& Cstr_OWN_Final_Result_Location &")"
copyfolder  gFWstr_RunFolder , Cstr_OWN_Final_Result_Location

ExitAction