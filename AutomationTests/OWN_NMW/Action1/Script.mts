﻿
Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

Dim intScratch


gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

' Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScopestr_ScenarioRow_FlowName 

intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true

'Neeraja -Need to define below as per the scenario

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI 
Dim strBarText, strBarText_Save, strMsg
Dim cStr_BarText_Format
Dim temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason , temp_DataPrep_CIS_TaskType, str_Required_SP_Status_MTS, temp_NMI_List
Dim BASE_XML_Template_DIR

' BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from GIT
BASE_XML_Template_DIR = cBASE_XML_Template_DIR


cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^RequestID`{4}^NMI`{5} ."

Dim strFolderNameWithSlashSuffix_Scratch
strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"


' push the RunStats to the DriverWS report-area


Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR 
BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"

Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir  = BASE_GIT_DIR



'###############################################################################  UFT configurations at the start of test  - START  ####################################################################################################################


Dim Temp_Execution_Results_Location  
    Temp_Execution_Results_Location = "C:\_data\Test_Execution_Results\"        'Share drive for AMI Energisation End to End project
	
	Dim strRunLog_Folder
	
	dim qtTest, qtResultsOpt, qtApp
	
	
	Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
	If qtApp.launched <> True then
	    qtApp.Launch
	End If
	
	qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	qtApp.Options.Run.RunMode = "Fast"
	qtApp.Options.Run.ViewResults = False
	
	
	Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
	Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539


	Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
	str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
	If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
		str_AnFw_FocusArea = ""
	else
		str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
	End If
	    
	  
	   
	    
strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", array(fnAttachStr(cTemp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
	    gQtTest.Name                                 , _
	    Parameter.Item("Environment")                       , _
	    Parameter.Item("Company")                           , _
	    fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
	    ' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))



'strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\"					, _
'		fnAttachStr(cTemp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)	, _
'		qtTest.Name																	, _
'		Parameter.Item("Environment")												, _
'		Parameter.Item("Company") 													, _
'		fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`ddDdD_HH`mm")	).toString
'
        
    
	Path_MultiLevel_CreateComplete strRunLog_Folder
	gFWstr_RunFolder = strRunLog_Folder
	
	qtResultsOpt.ResultsLocation = gFWstr_RunFolder
	gQtResultsOpt.ResultsLocation = strRunLog_Folder
	

	qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
	qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
	qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
	qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
	qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
	qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
	qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
	qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
	qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
	qtApp.Options.Run.ScreenRecorder.RecordSound = False
	qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True
	
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	
	gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
	gQtApp.Options.Run.ViewResults                = False

	strFolderNameWithSlashSuffix_Scratch = strRunLog_Folder


' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary

'Set gdict_scratch = CreateObject("Scripting.Dictionary")
'gdict_scratch.CompareMode = vbTextCompare
'
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  



 
	'  Description:-  Function to replace arguments in a string format
	'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 
	'						cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."
	'						The template should have arguments within {} and the numbering should s
	
	

	If 1 = 1  Then		
					
					
					Environment.Value("COMPANY")=Parameter.Item("Company")
					Environment.Value("ENV")=Parameter.Item("Environment")
					    
					   
				  
					' Load the MasterWorkBook objWB_Master
					Dim strWB_noFolderContext_onlyFileName  
					strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
					
					Dim wbScratch
					Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
					dim  dictWSDataParameter_KeyName_ColNr
					
					fnExcel_CreateAppInstance  objXLapp, true  
					
					'Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO"
					
					'call function LoadData_RunContext_V2  below
					
					' LoadData_RunContext_V2  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
					LoadData_RunContext_V3 objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
					
					
					Dim objWB_Master
					Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
					Set wbScratch = nothing
								
					objXLapp.Calculation = xlManual
					'objXLapp.screenUpdating=False
					
					Dim objWS_DataParameter 						 
					set objWS_DataParameter           					= objWB_Master.worksheets("NMW")  ' qq this must become a parm
					objWS_DataParameter.activate
					
					Dim objWS_templateXML_TagDataSrc_Tables 	
					set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
					set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
					
					Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
					' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
					gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

				'	Get headers of table in a dictionary
					Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
					gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
					
					
					gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
					Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
					gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
					' Load the first/title row in a dictionary
					int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
					fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
					int_MaxColumns = UBound(gvntAr_RuleTable,2)

					objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
					objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
					objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
					objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
					' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = now 

					gDt_Run_TS = now
					gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 

					Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE
					
					'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
					Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
					dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
					Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
					intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
					intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
					intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
					fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
					
					rowTL = 1 : colTL = 1
					
					
                
	
					' =============================================================================================================
'=================================  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   ==========================================
					' =============================================================================================================
					
					Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
					
					dim r
					Dim objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE
					Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
					
					
					Set objDBConn_TestData_IEE    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_IEE  = CreateObject("ADODB.Recordset")
					
'					On error resume next
				'	add more of these as-required, for querying databases
					Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
		'	add more of these as-required, for querying databases
		
		
					Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
					Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset")

					strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")


			Dim	DB_CONNECT_STR_MTS
					DB_CONNECT_STR_MTS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value("USERNAME") 	& 	";Password=" & _
						Environment.Value("PASSWORD") 	& ";"

			Dim	DB_CONNECT_STR_IEE
					DB_CONNECT_STR_IEE = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value(strEnvCoy & 	"_IEE_HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value(strEnvCoy & 	"_IEE_SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value(strEnvCoy & 	"_IEE_USERNAME") 	& 	";Password=" & _
						Environment.Value(strEnvCoy & 	"_IEE_PASSWORD") 	& ";"
						
					
					
	'		Dim	DB_CONNECT_STR_CIS
		'			DB_CONNECT_STR_CIS = _
	'						"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
		'				Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
		'				Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
		'				Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
		'				Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"

					' dim strTestName, uftapp : Set uftapp = CreateObject("QuickTest.Application")
					' strTestName = uftapp.Test.Name
					' Set uftapp = nothing
					
'					Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
					
					Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
					Dim strQueryTemplate, dtD, Hyph, strNMI
					
					Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
					Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
					intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
'
					Dim int_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
					Dim int_NMICHECKSUM
					
					Dim intSQL_Pkg_ColNr, strSQL_RunPackage
					
					
					Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
					
					
						intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
						intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
						intColNR_ScenarioID 			= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
					
								
							'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
						intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
						 sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
					
						Dim strColName
						objWS_DataParameter.calculate	'	to ensure the filename is updated
									
									
					'Dim  str_xlBitNess : str_xlBitNess = objWS_DataParameter.range("rgWS_XL_BitNess_x32x64").value	
					
					
					Dim  str_xlBitNess : str_xlBitNess = ""
									
					sb_File_WriteContents  _
						gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
					
					
					Dim intColNr_ScenarioStatus, intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
						   intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
						  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")			
					
											
						Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
						If IsEmpty(intColNr_InScope) then	
							blnColumnSet =false
						ElseIf intColNr_InScope = -1 then
							blnColumnSet =false
						End if
						If not blnColumnSet then 
					'		qq log this	
					'		objXLapp.Calculation = xlManual
							objXLapp.screenUpdating=True
							exittest
						End If
					
						objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
						objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
						objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
						objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
						
						Printmessage "i", "Start row number", "Start row for the test is " & intRowNr_LoopWS_RowNr_StartOn
						Printmessage "i", "End row number", "End row for the test is " & intRowNr_LoopWS_RowNr_FinishOn
						
						objWS_DataParameter.calculate
						objxlapp.screenupdating = true
					
						' objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
						
				
				
					      ' ==========================================================================
'=======================================  FrameworkPhase 0c   - Setup the MultiStage Array ===========================================================
					      ' ==========================================================================
					
					Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
					Dim  intMasterZoomLevel 
					
					Dim rowStart, rowEnd, colStart, colEnd
					Dim RangeAr 
					
					
					'    ===>>>   Enable HotKeys
					' NOT NEEDED objWB_Master.Application.Run "HotKeys_Set"
					
					'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
					' NOT NEEDED objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
				
	 End If


objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = Parameter("InScope_Column_Name")
					
dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq


sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

	

'	determine the number of in-scope rows
Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = now()


'	qq     intNrOfRows_Inscope_and_Unfinished

sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , false, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf



dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow

'	now setup to do process the DriverSheet


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_CurrentScenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = now()-7
cInt_MinutesRequired_toComplete_eachScenario = cInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = cdbl(1.2)

	                
Dim str_ScenarioRow_FlowName 
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false
Dim temp_XML_Template
Dim intColNr_Request_ID 
Dim intColNr_RowCompletionStatus 
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
intColNr_ScenarioStatus 		= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")

Dim strRole, strInitiator_Role, var_ScheduledDate
Dim strInScope, strRowCompletionStatus

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=true
objWB_Master.save


	
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
		' Get headers of table in a dictionary
	Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	

	
		' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue
	



' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
' Launch MTS
fnMTS_WinClose
OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")
''
' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID
Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role
Dim temp_strSQL_MTS ' This is to store SQL template incase of multiple row XML (having different NMI's)

int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0

i = 1

' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PREP - Update company related information in driver sheet %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

' set the state to run the SingleScenarios over, based on the company being tested
  Select Case ucase(Parameter.Item("Company"))
    Case "CITI", "PCOR"
      str_Company_Name = "VIC"
      PrintMessage "p","Company Name for Test", "Company name passed as parameter ("& Parameter.Item("Company") &"). Company name used for the test is ("& str_Company_Name  &")"
    Case "SAPN"
      str_Company_Name = "SA"
      PrintMessage "p","Company Name for Test", "Company name passed as parameter ("& Parameter.Item("Company") &"). Company name used for the test is ("& str_Company_Name  &")"
    Case else
    	PrintMessage "f","Invalid company name passed as parameter", "Company name passed as parameter ("& Parameter.Item("Company") &") is not handled in script...., exiting test"
      ExitAction ' exit the test case
  End Select




Dim strAr_acrossA_Role, strAr_Stages
Dim intArSz_Role, intArSz_Stages, strPrefix_RoleIsRelevant
Dim strCaseGroup, strCaseNr, strXML_TemplateName_Current, Date_TransactionDate
Dim str_DataParameter_PopulatedXML_FqFileName, str_MTS_Txn_Status
Dim strVerification_String, temp_range_rg_VerificationString_Suffix, varAr_Verification_String




' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Do
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
									'	intRowNr_LoopWS_RowNr_StartOn   qq
	
		'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
		strInScope 							= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
		
		
		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
			If   ucase(strInScope) = ucase(cY)  and str_ScenarioRow_FlowName <> "" and lcase(str_Next_Function) <> "fail" and lcase(str_Next_Function) <> "end" Then
			
				If i = 1 Then
					If ucase(Parameter.Item("RunMode")) = "N" Then ' Value can be N- New or R - restart
						objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start-SQL"
						str_Next_Function = "start-SQL"
					End If
					fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
					int_ctr_total_Eligible_rows = int_ctr_no_of_Eligible_rows
				End If
			
				' Generate a filename for screenshot file
				gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, left(temp_Scenario_ID,10)))
				
				
			Select Case lcase(str_ScenarioRow_FlowName)
				
				' ################################################################# START of first flow ####################################################################################################
				' ################################################################# START of first flow ####################################################################################################
				Case "flow1", "flow2", "flow3", "flow4", "flow5", "flow6", "flow7":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
								Select Case str_Next_Function
									Case "start-SQL" ' this is datamine
										' First function
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											
												' Query would be executed once for every row. It would not be executed for ALL ROLES as there are no roles related replacements
												
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%

											PrintMessage "P", "MTS SQL Query", "Executing SQL ("& strSQL_MTS &")"
											
											' capture MTS query
											strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Template_Name")).value 

											If strSQL_MTS <> ""  Then
											
											       
											         PrintMessage "p", "MTS SQL template","Scenario row("& intRowNr_CurrentScenario &") - MTS SQL Template ("& strSQL_MTS &") "
											        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											        
											        If lcase(strQueryTemplate) <> "fail" Then
														strSQL_MTS = strQueryTemplate
														
														
															' Perform replacements
											            	strSQL_MTS = replace ( strSQL_MTS , "<From_Role>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("From_Role")).value, 1, -1, vbTextCompare)
											            	strSQL_MTS = replace ( strSQL_MTS , "<To_Role>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("To_Role")).value, 1, -1, vbTextCompare)
											            	strSQL_MTS = replace ( strSQL_MTS , "<Meter_Type_MTS>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Type_MTS")).value, 1, -1, vbTextCompare)
														' Execute MTS SQL
														strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
	
	                                                    'NK added on 26/06/2017	
															
														If lcase(str_ScenarioRow_FlowName) = "flow4" Then ' we need a wrong NMI in case of Flow 4
														
                                                         int_NMI = 64020012689
                                                         objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = int_NMI 
												        'int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
													
'													     Else 
'                                                        If lcase(str_ScenarioRow_FlowName) = "flow7" Then
'                                                        int_NMICHECKSUM = 5
'                                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMICHECKSUM" )) = int_NMICHECKSUM 	
                                                         
'                                                         Working here - Neeraja
                                                        Else 
                                                        If lcase(str_ScenarioRow_FlowName) = "flow5" Then
                                                        int_NMI = ""
                                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = int_NMI 	
                                                        
'                                                        Else 
'                                                        If lcase(str_ScenarioRow_FlowName) = "flow6" Then
'                                                        int_NMI = left(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value,10)
'                                                       													
														else
'														int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
	
														If strScratch = "PASS" Then
															' Write the data in all respective columns for all roles
															' Reserve NMI in test_data_key_register table in DB
															
														int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
															
														End If
														PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR "	
														fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
														End If 'flow5
														End if 'flow4
														'End if 'flow7
														
														'End IF 
													End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
											End If ' end of If strSQL_MTS <> ""  Then



' COMMENTING THE IEE QUERY PART AS FOR CURRENT ITERATION EVERYTHING IS DRIVEN FROM EXCEL
' THIS WOULD BE NEEDED IN THE FUTURE HENCE DO NOT DELETE THE CODE


'''''											' If (lcase(str_ScenarioRow_FlowName) = "flow2" or lcase(str_ScenarioRow_FlowName) = "flow3" or lcase(str_ScenarioRow_FlowName) = "flow4" or lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow5")   and lcase(gFWbln_ExitIteration) <> "y" and gFWbln_ExitIteration <> true Then ' IEE Meter related information is minned only for flow 2
'''''												
'''''													' capture IEE query to fetch Meter Data
'''''													strSQL_IEE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_Meter_Details_SQL_Template_Name")).value 
'''''		
'''''													If trim(strSQL_IEE) <> ""  Then
'''''														
'''''														If int_NMI <> "" Then
'''''														  PrintMessage "p", "IEE SQL template","Scenario row("& intRowNr_CurrentScenario &") - IEE SQL Template ("& strSQL_IEE &") "
'''''													        strQueryTemplate = fnRetrieve_SQL(strSQL_IEE)
'''''													        
'''''													        If lcase(strQueryTemplate) <> "fail" Then
'''''																strSQL_IEE = strQueryTemplate
'''''																' Perform replacements
'''''													            	strSQL_IEE = replace ( strSQL_IEE , "<NMI>", int_NMI, 1, -1, vbTextCompare)
'''''													            	
'''''													            	PrintMessage "p", "SQL Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing IEE SQL ("& strSQL_IEE &") "
'''''													            	
'''''													            	' Append the query in SQL Populated Coulmn
'''''									            			        	set cellScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated"))
'''''									            			        	strScratch = cellScratch.value
'''''														        	cellScratch.formula = strScratch & vbCrLf & " IEE QUERY : - " & vbCrLf & strSQL_IEE
'''''														        	cellScratch.WrapText = False
'''''														        	
'''''																' Execute IEE SQL
'''''																strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", "", "", "")
'''''																
'''''																' strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", "", "", "")
'''''																
'''''																' PENDING, merge the ourput of IEE and MTS in a single CELL
'''''																If strScratch = "PASS" Then
'''''
'''''																' On error resume next
'''''																	' $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
'''''																	' The below code would cater to a requirement where there is more than 1 meter and 1 or many registers returned against a NMI. We need to update the XML on the basis of what the query returns
'''''																	' $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
'''''																	
''''''																	objDBRcdSet_TestData_IEE.Filter = "STATUSID='Active'"
''''''																	msgbox objDBRcdSet_TestData_IEE.RecordCount
''''''																	
''''''																	objDBRcdSet_TestData_IEE.Filter = "STATUSID='Inactive'"
''''''																	msgbox objDBRcdSet_TestData_IEE.RecordCount
''''''																	
''''''																	objDBRcdSet_TestData_IEE.Filter = "STATUSID='active'"
''''''																	msgbox objDBRcdSet_TestData_IEE.RecordCount
''''''																	
''''''																	objDBRcdSet_TestData_IEE.Filter = "STATUSID='inactive'"
''''''																	msgbox objDBRcdSet_TestData_IEE.RecordCount
''''''
''''''																	objDBRcdSet_TestData_IEE.Filter = "statusid='inactive'"
''''''																	msgbox objDBRcdSet_TestData_IEE.RecordCount
'''''
'''''																	
'''''																	temp_strSqlCols_NamesList 	= "" 
'''''																	temp_strSqlCols_ValuesList = ""
'''''																      temp_strSql_Suffix = ","  ' the list is 1-based
'''''																	temp_IEE_MeterNumber = ""
'''''																	temp_IEE_Meter_Counter = 0 
'''''																	temp_IEE_Register_Counter = 0 
'''''																	temp_XML_Tag_CSVIntervalData_runtimeUpdated = ""
'''''																	temp_XML_Tag_CSVIntervalData_FinalTag = "" 
'''''																	temp_XML_Tag_ConcatenatedRegisters = ""
'''''																	temp_bln_scratch = true
'''''																	
'''''																	
'''''																	' ###########################################################################################################################
'''''																	' 													Total Installed Meters - Start
'''''																	' ###########################################################################################################################
'''''																	
'''''																	temp_TotalInstalledMeters = 0
'''''																	temp_TotalInstalledMeters = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("TotalInstalledMeters"))
'''''																	temp_XML_Tag_MeterDetails = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_MeterDetails"))
'''''																	temp_XML_Tag_MasterData_SerialNumber = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_MasterData_SerialNumber"))
'''''																	temp_XML_Tag_Meter_Transformer = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_Meter_Transformer"))
'''''																	temp_XML_Tag_MeterDetails_Updated = ""
'''''																	temp_XML_Tag_MeterDetails_Final = ""
'''''																	temp_XML_Tag_MasterData_SerialNumber_Updated = ""
'''''																	temp_XML_Tag_MasterData_SerialNumber_Final = ""
'''''																	temp_XML_Tag_Meter_Transformer_Updated = ""
'''''																	temp_XML_Tag_Meter_Transformer_Final = ""
'''''																	
'''''																	If temp_TotalInstalledMeters  > 0  Then
'''''																		objDBRcdSet_TestData_IEE.MoveFirst
'''''																		objDBRcdSet_TestData_IEE.Filter = "STATUSID='active'"
'''''																		
'''''																		If objDBRcdSet_TestData_IEE.RecordCount > 0  Then
'''''																			For i = 1  To temp_TotalInstalledMeters
'''''																				temp_XML_Tag_MeterDetails_Updated = temp_XML_Tag_MeterDetails
'''''																				temp_XML_Tag_MasterData_SerialNumber_Updated = temp_XML_Tag_MasterData_SerialNumber
'''''																				temp_XML_Tag_Meter_Transformer_Updated = temp_XML_Tag_Meter_Transformer
'''''																				
'''''																				If instr(1,temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_SerialNumber^") > 0  Then
'''''																					temp_XML_Tag_MeterDetails_Updated = replace(temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_SerialNumber^", objDBRcdSet_TestData_IEE.Fields("METERNUMBER").value)
'''''																				End If
'''''																				
'''''																				If instr(1,temp_XML_Tag_MasterData_SerialNumber_Updated, "^data_SrcSQL_SerialNumber^") > 0  Then
'''''																					temp_XML_Tag_MasterData_SerialNumber_Updated = replace(temp_XML_Tag_MasterData_SerialNumber_Updated, "^data_SrcSQL_SerialNumber^", objDBRcdSet_TestData_IEE.Fields("METERNUMBER").value)
'''''																				End If
'''''																				
'''''																				If instr(1,temp_XML_Tag_Meter_Transformer_Updated, "^data_SrcSQL_SerialNumber^") > 0  Then
'''''																					temp_XML_Tag_Meter_Transformer_Updated = replace(temp_XML_Tag_Meter_Transformer_Updated, "^data_SrcSQL_SerialNumber^", objDBRcdSet_TestData_IEE.Fields("METERNUMBER").value)
'''''																				End If
'''''																				
'''''																				If instr(1,temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_SupplyPhase^") > 0  Then
'''''																					temp_XML_Tag_MeterDetails_Updated = replace(temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_SupplyPhase^", "RAMYA-TO-PROVIDE")
'''''																				End If
'''''																				
'''''																				If instr(1,temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_GenerationType^") > 0  Then
'''''																					temp_XML_Tag_MeterDetails_Updated = replace(temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_GenerationType^", "RAMYA-TO-PROVIDE")
'''''																				End if
'''''																				
'''''																				If instr(1,temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_GeneralSupply^") > 0  Then
'''''																					temp_XML_Tag_MeterDetails_Updated = replace(temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_GeneralSupply^", "RAMYA-TO-PROVIDE")
'''''																				End If
'''''																				
'''''																				If instr(1,temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_ControlledLoad^") > 0  Then
'''''																					temp_XML_Tag_MeterDetails_Updated = replace(temp_XML_Tag_MeterDetails_Updated, "^data_SrcSQL_ControlledLoad^", objDBRcdSet_TestData_IEE.Fields("ControlledLoad").value)
'''''																				End if
'''''																				
'''''																				objDBRcdSet_TestData_IEE.MoveNext
'''''																				
'''''																				If objDBRcdSet_TestData_IEE.EOF and i < temp_TotalInstalledMeters Then
'''''																					fn_set_error_flags true, "Total Installed Meters tag population - FAILED - IEE Query has returned (" & objDBRcdSet_TestData_IEE.RecordCount &") Active Meter whereas the scenario row expectes (" & temp_TotalInstalledMeters &")"
'''''																					PrintMessage "f", "Total Installed Meters tag population - FAILED", gFWstr_ExitIteration_Error_Reason
'''''																					temp_XML_Tag_MeterDetails_Final = temp_XML_Tag_MeterDetails_Updated
'''''																					temp_XML_Tag_MeterDetails_Updated = ""
'''''																					temp_XML_Tag_MasterData_SerialNumber_Final = temp_XML_Tag_MasterData_SerialNumber_Updated
'''''																					temp_XML_Tag_MasterData_SerialNumber_Updated = ""
'''''																					temp_XML_Tag_Meter_Transformer_Final = temp_XML_Tag_Meter_Transformer_Updated
'''''																					i = temp_TotalInstalledMeters + 1
'''''																				Else
'''''																					If i = 1  Then
'''''																						temp_XML_Tag_MeterDetails_Final = temp_XML_Tag_MeterDetails_Updated
'''''																						temp_XML_Tag_MeterDetails_Updated = ""
'''''																						temp_XML_Tag_MasterData_SerialNumber_Final = temp_XML_Tag_MasterData_SerialNumber_Updated
'''''																						temp_XML_Tag_MasterData_SerialNumber_Updated = ""
'''''																						temp_XML_Tag_Meter_Transformer_Final = temp_XML_Tag_Meter_Transformer_Updated
'''''																						temp_XML_Tag_Meter_Transformer_Updated = ""
'''''																					Else
'''''																						temp_XML_Tag_MeterDetails_Final = temp_XML_Tag_MeterDetails_Final & vbCrLf & temp_XML_Tag_MeterDetails_Updated
'''''																						temp_XML_Tag_MeterDetails_Updated = ""
'''''																						temp_XML_Tag_MasterData_SerialNumber_Final = temp_XML_Tag_MasterData_SerialNumber_Final & vbCrLf & temp_XML_Tag_MasterData_SerialNumber_Updated
'''''																						temp_XML_Tag_MasterData_SerialNumber_Updated = ""
'''''																						temp_XML_Tag_Meter_Transformer_Final = temp_XML_Tag_Meter_Transformer_Final & vbCrLf & temp_XML_Tag_Meter_Transformer_Updated
'''''																						temp_XML_Tag_Meter_Transformer_Updated = ""
'''''																					End If
'''''																				End If
'''''																			Next
'''''																			
'''''																			objDBRcdSet_TestData_IEE.MoveFirst
'''''																			' msgbox temp_XML_Tag_MeterDetails_Final
'''''																			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_MeterDetails")).value = temp_XML_Tag_MeterDetails_Final
'''''																			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_MasterData_SerialNumber")).value = temp_XML_Tag_MasterData_SerialNumber_Final
'''''																			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_Meter_Transformer")).value = temp_XML_Tag_Meter_Transformer_Final
'''''																		Else
'''''																			fn_set_error_flags true, "Total Installed Meters tag population - FAILED - IEE Query has not returned any active meter for NMI ("& int_NMI &")"
'''''																			PrintMessage "f", "Total Installed Meters tag population - FAILED", gFWstr_ExitIteration_Error_Reason
'''''																		End If
'''''																		
'''''																	End If
'''''																	' ###########################################################################################################################
'''''																	' 													Total Installed Meters - End
'''''																	' ###########################################################################################################################
'''''																	
'''''																	
'''''																	' ###########################################################################################################################
'''''																	' 													Total Removed Meters - Start
'''''																	' ###########################################################################################################################
'''''																	
'''''																	temp_TotalRemovedMeters = 0
'''''																	temp_TotalRemovedMeters = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("TotalRemovedMeters"))
'''''																	
'''''																	temp_XML_Tag_RemovedEquipmentType = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_RemovedEquipmentType"))
'''''																	temp_XML_Tag_RemovedEquipmentType_Updated = ""
'''''																	temp_XML_Tag_RemovedEquipmentType_Final = ""
'''''																	
'''''																	If temp_TotalRemovedMeters  > 0  Then
'''''																		objDBRcdSet_TestData_IEE.MoveFirst
'''''																		objDBRcdSet_TestData_IEE.Filter = "STATUSID='Disconnected'"
'''''																		
'''''																		If objDBRcdSet_TestData_IEE.RecordCount > 0  Then
'''''																			For i = 1  To temp_TotalRemovedMeters
'''''																				temp_XML_Tag_RemovedEquipmentType_Updated = temp_XML_Tag_RemovedEquipmentType
'''''																				
'''''																				If instr(1,temp_XML_Tag_RemovedEquipmentType_Updated, "^data_SrcSQL_SerialNumber^") > 0  Then
'''''																					temp_XML_Tag_RemovedEquipmentType_Updated = replace(temp_XML_Tag_RemovedEquipmentType_Updated, "^data_SrcSQL_SerialNumber^", objDBRcdSet_TestData_IEE.Fields("METERNUMBER").value)
'''''																				End If
'''''																				
'''''																				If instr(1,temp_XML_Tag_RemovedEquipmentType_Updated, "^data_SRCSQL_RemovedEquipmentType^") > 0  Then
'''''																					temp_XML_Tag_RemovedEquipmentType_Updated = replace(temp_XML_Tag_RemovedEquipmentType_Updated, "^data_SRCSQL_RemovedEquipmentType^", "RAMYA-TO-Provide")
'''''																				End If
'''''																				
'''''																				objDBRcdSet_TestData_IEE.MoveNext
'''''																				
'''''																				If objDBRcdSet_TestData_IEE.EOF and i < temp_TotalRemovedMeters Then
'''''																					fn_set_error_flags true, "Total Removed Meters tag population - FAILED - IEE Query has returned (" & objDBRcdSet_TestData_IEE.RecordCount &") Inactive Meter whereas the scenario row expectes (" & temp_TotalRemovedMeters &")"
'''''																					PrintMessage "f", "Total Removed Meters tag population - FAILED", gFWstr_ExitIteration_Error_Reason
'''''																					temp_XML_Tag_RemovedEquipmentType_Final = temp_XML_Tag_RemovedEquipmentType_Updated
'''''																					temp_XML_Tag_RemovedEquipmentType_Updated = ""
'''''																					i = temp_TotalRemovedMeters + 1
'''''																				Else
'''''																					If i = 1  Then
'''''																						temp_XML_Tag_RemovedEquipmentType_Final = temp_XML_Tag_RemovedEquipmentType_Updated
'''''																						temp_XML_Tag_RemovedEquipmentType_Updated = ""
'''''																					Else
'''''																						temp_XML_Tag_RemovedEquipmentType_Final = temp_XML_Tag_RemovedEquipmentType_Final & vbCrLf & temp_XML_Tag_RemovedEquipmentType_Updated
'''''																						temp_XML_Tag_RemovedEquipmentType_Updated = ""
'''''																					End If ' end of If i = 1  Then
'''''																				End If ' end of If objDBRcdSet_TestData_IEE.EOF and i < temp_TotalRemovedMeters Then
'''''																			Next
'''''																			
'''''																			objDBRcdSet_TestData_IEE.MoveFirst
'''''																			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_RemovedEquipmentType")).value = temp_XML_Tag_RemovedEquipmentType_Final
'''''																		Else
'''''																			fn_set_error_flags true, "Total Removed Meters tag population - FAILED - IEE Query has not returned any Inactive meter for NMI ("& int_NMI &")"
'''''																			PrintMessage "f", "Total Removed Meters tag population - FAILED", gFWstr_ExitIteration_Error_Reason
'''''																		End If ' end of If objDBRcdSet_TestData_IEE.RecordCount > 0  Then
'''''																		
'''''																	End If	' end of If temp_TotalRemovedMeters  > 0  Then																
'''''
'''''																	' ###########################################################################################################################
'''''																	' 													Total Removed Meters - End
'''''																	' ###########################################################################################################################
'''''
'''''
'''''																	' ###########################################################################################################################
'''''																	' 													Network Device - Start
'''''																	' ###########################################################################################################################
'''''																	
'''''																	temp_list_NetworkDeviceNumber = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NetworkDeviceNumber"))
'''''																	temp_list_NetworkDeviceNumber  = replace(temp_list_NetworkDeviceNumber, "<blank>", "")
'''''																	temp_list_NetworkDeviceLocation = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NetworkDeviceLocation"))
'''''																	temp_list_NetworkDeviceLocation  = replace(temp_list_NetworkDeviceLocation, "<blank>", "")
'''''																	temp_XML_Tag_NetworkDevice = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_NetworkDevice"))
'''''																	temp_XML_Tag_NetworkDevice_upadted = ""
'''''																	temp_XML_Tag_NetworkDevice_final = ""
'''''																	
'''''																	If temp_XML_Tag_NetworkDevice <> "" Then
'''''																		If trim(temp_list_NetworkDeviceNumber) <> "" and  trim(temp_list_NetworkDeviceLocation) <> "" Then
'''''																			temp_array_NetworkDeviceNumber = split(temp_list_NetworkDeviceNumber,",")
'''''																			temp_array_NetworkDeviceLocation = split(temp_list_NetworkDeviceLocation,",")
'''''																			
'''''																			If ubound(temp_array_NetworkDeviceNumber) <> ubound(temp_array_NetworkDeviceLocation) Then
'''''																				fn_set_error_flags true, "The number of items in Network Device Number is () which does not match the number of items in Network Device Location (). Please correct the driver sheet"
'''''																				PrintMessage "f", "XML_Tag_NetworkDevice tag replacement failure", gFWstr_ExitIteration_Error_Reason
'''''																			Else
'''''																				' Start the replacement
'''''																				
'''''																				For i  = 1 To  ubound(temp_array_NetworkDeviceNumber)
'''''																					temp_XML_Tag_NetworkDevice_upadted = temp_XML_Tag_NetworkDevice
'''''																					temp_XML_Tag_NetworkDevice_upadted = replace(temp_XML_Tag_NetworkDevice_upadted, "^data_SRCWS_list_NetworkDeviceNumber^", temp_array_NetworkDeviceNumber(i))
'''''																					temp_XML_Tag_NetworkDevice_upadted = replace(temp_XML_Tag_NetworkDevice_upadted, "^data_SRCWS_list_NetworkDeviceLocation^", temp_array_NetworkDeviceLocation(i))
'''''																					
'''''																					If i =1  Then
'''''																						temp_XML_Tag_NetworkDevice_final = temp_XML_Tag_NetworkDevice_upadted	
'''''																					Else
'''''																						temp_XML_Tag_NetworkDevice_final = temp_XML_Tag_NetworkDevice_final  & vbCrLf & temp_XML_Tag_NetworkDevice_upadted	
'''''																					End If
'''''																					
'''''																				Next
'''''																				objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_NetworkDevice")) = temp_XML_Tag_NetworkDevice_final
'''''																				temp_XML_Tag_NetworkDevice_upadted = ""
'''''																				temp_XML_Tag_NetworkDevice_final = ""
'''''																			End If
'''''																		Else
'''''																			fn_set_error_flags "XML_Tag_NetworkDevice tag replacement failure", "Either of NetworkDeviceNumber or NetworkDeviceLocation is empty, please correct the configuration file"
'''''																			PrintMessage "f", "XML_Tag_NetworkDevice tag replacement failure", gFWstr_ExitIteration_Error_Reason
'''''																		End If ' end of If trim(temp_list_NetworkDeviceNumber) <> "" and  trim(temp_list_NetworkDeviceLocation) <> "" Then
'''''																	Else
'''''																		PrintMessage "i", "XML_Tag_NetworkDevice tag replacement", "Not generating the Network Device tag as tag cell is empty.. moving on.."
'''''																	End If ' end of If temp_XML_Tag_NetworkDevice <> "" Then
'''''
'''''
'''''																	' ###########################################################################################################################
'''''																	' 													Network Device - End
'''''																	' ###########################################################################################################################
'''''
'''''
'''''																End If ' end of If strScratch = "PASS" Then
'''''																
'''''															End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
'''''														
'''''														Else
'''''															PrintMessage "f", "IEE Query Execution", "Skipping execution of IEE query as there is no NMI found for record ("& intRowNr_CurrentScenario &"). Marking the row as fail and moving to next one"
'''''															fn_set_error_flags true, "IEE query is provided however there is no NMI available for replacement. Skipping this row"
'''''														End If ' end of If int_NMI <> "" Then
'''''
'''''
'''''
'''''
'''''
'''''													End If ' end of If strSQL_IEE <> ""  Then
'''''													
'''''											' End If	' end of If (lcase(str_ScenarioRow_FlowName) = "flow2" or lcase(str_ScenarioRow_FlowName) = "flow3")  and (lcase(gFWbln_ExitIteration) <> "y" or gFWbln_ExitIteration <> true) Then ' IEE Meter related information is minned only for flow 2
											
											

											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												'Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												printmessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Creation"
												'Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												printmessage "p", "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
			            					
											strSQL_MTS = ""
											
										
									Case "XML_Creation"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										
										If tsNow >= tsNext_ScenarioFunction_canStart_atOrAfter Then
			
											temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
											
									        If temp_XML_Template  <> "" Then
									            
									            If trim(lcase(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Request_ID_YN")).value)) <> "n" Then
									            	int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution	
									            	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID") ).value = int_UniqueReferenceNumberforXML
									            Else
									            	int_UniqueReferenceNumberforXML = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value
									            End If
									            
'									            'Working - Neeraja 26/06/17
'									            
'									           	If lcase(str_ScenarioRow_FlowName) = "flow3" Then ' we need a wrong NMICHECKSUM in case of Flow 3	
'                                               					           	 
''												int_NMICHECKSUM = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMICHECKSUM")).value,",","", "WRONG_NMICHECKSUM")
''												else											
'												int_NMICHECKSUM = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMICHECKSUM" )).value
'												End If	' If lcase(str_ScenarioRow_FlowName) = "flow3" Then ' we need a wrong NMICHECKSUM in case of Flow 3
'									            
''									            
'									            If lcase(str_ScenarioRow_FlowName) = "flow4" Then ' we need a wrong NMI in case of Flow 4
''												int_NMI = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "WRONG_NMI")
''												else											
'												int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
'												End If	' If lcase(str_ScenarioRow_FlowName) = "flow4" Then ' we need a wrong NMI in case of Flow 4
									            
									            
									            temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
									            temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("To_Role")).value
									            int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
									           
'									            If lcase(str_ScenarioRow_FlowName) = "flow3" Then
'									            
'                                            		int_NMICHECKSUM =objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMICHECKSUM" )).value									            
'									            
'									            Else
'									                int_NMICHECKSUM=right(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value,1)
'									                
'									            	'int_NMICHECKSUM = ""
'									            End If
									           
									            str_ChangeStatusCode = "NMW"
									            
									            str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
									                                                        temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
									                                                        intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, now,  int_UniqueReferenceNumberforXML, _
									                                                        temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
									
									            strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
									            
									            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
									            	'Reporter.ReportEvent micFail,"Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
									            	printmessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
									            Else
									            
										            ' copy the PopulatedXML file from the temp folder to the input folder
										            ' copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
										            
										            ' Store from tag in datasheet
													objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_Value_From_Role")).value = gstr_XML_TAG_FromSrcSQL 
													
										            ' copy the PopulatedXML file from the temp folder to the results folder
										            copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, gFWstr_RunFolder  ' Environment.Value("CATSDestinationFolder")
										            
										            ' ZIP XML File
										            ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
										                    
										            GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
										          

												If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
													printmessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													gFWbln_ExitIteration = "y"
												    gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												Else
                                                    'copy xml file to gateway destination folder												
		  											copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
													printmessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
												End If
									            End If
									            End IF
									            	
									            End If
									            
										

											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												'Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
												If lcase(str_ScenarioRow_FlowName) = "flow3" Then
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Rejection_Code_SchemaValidation"
												Else
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_OWN_DB_Validation"
												End If
													
												'Reporter.ReportEvent micPass, "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												printmessage "p", "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If


  'Working here -  Neeraja --- NMW Scehema Validation


                                     case "Rejection_Code_SchemaValidation"
                                            tsNow = now() 
                                            tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
                                            IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
                                                Exit do
                                            End if
                                            
                                            ' capture MTS query to check trans ack
                                            strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_NMW_Schema_Validation")).value 

                                            If strSQL_MTS <> ""  Then
                                            
                                                strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
                                                
                                                If LCASE(strQueryTemplate) <> "fail" Then
                                                    
                                                    strSQL_MTS = strQueryTemplate
                                                    
                                                  ' Perform replacements
                                                  strSQL_MTS = replace ( strSQL_MTS , "<INIT_TRANSACTION_ID>", "TxnID_" & objWS_DataParameter.cells ( intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value)

                                                  'strSQL_MTS = replace ( strSQL_MTS , "<INIT_TRANSACTION_ID>", fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario,     dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","XML_RUNTIME_REQUEST_ID") , 1, -1, vbTextCompare)
                                                        
                                                        ' Update query in SQL Query cell
                                                strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
                                                strScratch = strScratch & vbCrLf & " Rejection_Code_SchemaValidation Query:" & vbCrLf & strSQL_MTS
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
                                                
                                                    If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Code_SchemaValidation") ).value))  <> "" Then
                                                        ' Execute MTS SQL
                                                        strScratch = Execute_SQL_Populate_DataSheet_handlecommainSQLvalue(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "")
                                                    
                                                        If strScratch = "PASS" Then
                                                        
                                                        	objWB_Master.save
                                                            If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","CODE")) = _
                                                            lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Code_SchemaValidation") ).value)) Then                                                        
                                                                PrintMessage "p", "Rejection_Code_SchemaValidation step", " CODE ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","CODE"))  &") is same as Rejection_Code_SchemaValidation ("& (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Rejection_Code_SchemaValidation")).value) &")"
                                                            Else
                                                                fn_set_error_flags true, " CODE ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","CODE"))  &") is DIFFERENT FROM Rejection_Code_SchemaValidation ("& (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Rejection_Code_SchemaValidation")).value) &")"
                                                                PrintMessage "f", "Rejection_Code_SchemaValidation step", gFWstr_ExitIteration_Error_Reason
                                                            End If
                                                        	
                                                        
                                                        End If 'end of If strScratch = "PASS" Then
                                                     End if ' end of If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Code_SchemaValidation") ).value))  <> "" Then
                                                            
'                                                            
'    
'                                                                    ' No file should be sent
'                                                                     If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Code_SchemaValidation") ).value))  = "2" Then
'                                                                    If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FILENAME")) <> "" Then
'                                                                        fn_set_error_flags true, " An output file with the name ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FILENAME"))  &") is generated for SAR OUT scenario. This is not the expected value as no file should be generated"
'                                                                        PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
'                                                                    End If
'                                                                Else
'                                                                    ' file should be sent
'                                                                    If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FILENAME")) = "" Then
'                                                                        fn_set_error_flags true, " An output file (SAN XML) is not generated. This is not the expected value as no file should be generated"
'                                                                        PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
'                                                                    End If
'                                                                    ' Verify TO_MARKET_PART
'                                                                    If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_MARKET_PART")) = _
'                                                                    trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))  Then
'                                                                        PrintMessage "p", "SAN_OUT_XML_CHECK step", " TO_MARKET_PART ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_MARKET_PART"))  &") is same as FROM_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))   &")"
'                                                                    Else
'                                                                        fn_set_error_flags true, " TO_MARKET_PART ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_MARKET_PART"))  &") is DIFFERENT FROM FROM_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))   &")"
'                                                                        PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
'                                                                    End If
'                                                                    ' Verify FROM_MARKET_PART
'                                                                    If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_MARKET_PART")) = _
'                                                                    trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))  Then                                                        
'                                                                        PrintMessage "p", "SAN_OUT_XML_CHECK step", " FROM_MARKET_PART ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_MARKET_PART"))  &") is same as TO_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))   &")"
'                                                                    Else
'                                                                        fn_set_error_flags true, " FROM_MARKET_PART ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_MARKET_PART"))  &") is DIFFERENT FROM TO_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))   &")"
'                                                                        PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
'                                                                    End If
											
														End If 'end of   If LCASE(strQueryTemplate) <> "fail" Then
													
													End If 'end of If strSQL_MTS <> ""  Then
													
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Rejection_Code_SchemaValidation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												printmessage "f", "Rejection_Code_SchemaValidation failed", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												'Reporter.ReportEvent micPass, "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												printmessage "p", "Rejection_Code_SchemaValidation complete", "Rejection_Code_SchemaValidation complete for row ("  & intRowNr_CurrentScenario &  ")" 
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Rejection_Code_SchemaValidation complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
												  fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												  int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												  int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												  Exit Do													
			            					End If




									case "MTS_OWN_DB_Validation"
										
											' fn_compare_Current_and_expected_execution_time
											
											tsNow = now() 
											tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
											IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
											End if
			            					
			            					
			            					
			            					
			            					'+++++++++++++++++ Execute SQL to validate data in the DB .......'+++ 
			            					
			            					'' capture MTS query
											strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DB_SQL_Verification_Template_Name")).value 
											
										
										If strSQL_MTS <> ""  Then
											
											        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											        
											        If lcase(strQueryTemplate) <> "fail" Then
											        	
														strSQL_MTS = strQueryTemplate
														temp_strSQL_MTS = strSQL_MTS 
												
'												
'														
										            	' 'Perform replacements
										            	
										            	'strSQL_MTS = replace ( strSQL_MTS , "<SQL_NMI>", left(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value,10), 1, -1, vbTextCompare)
										            	'strSQL_MTS = replace ( strSQL_MTS , "<SQL_NMI_CK>", right(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value,1), 1, -1, vbTextCompare)
										            	' strSQL_MTS = replace ( strSQL_MTS , "<Runtime_TxnID>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value, 1, -1, vbTextCompare)
										            	'strSQL_MTS = replace ( strSQL_MTS , "<Meter_Type_MTS>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Type_MTS")).value, 1, -1, vbTextCompare)
										            	'strSQL_MTS = replace ( strSQL_MTS , "<Meter_Type_MTS>", (fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "METERNUMBER")), 1, -1, vbTextCompare)
										            	strSQL_MTS = replace ( strSQL_MTS , "<Runtime_RequestID>", "TxnID_" & objWS_DataParameter.cells ( intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value, 1, -1, vbTextCompare)
								                       'strSQL_MTS = replace ( strSQL_MTS , "<Runtime_RequestID>", left(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr("TxnID_" & "Runtime_RequestID"),10).value))
										            	
														' 'Execute MTS SQL
														'strScratch = Execute_SQL_Populate_DataSheet ((objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
														
														strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "3",  "", "", "", "")
													
                                                    
															
														'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Expected_Value")).value
														
														
														
														If lcase(strScratch) = "pass" Then
														
															If objDBRcdSet_TestData_B.recordcount > 0  Then
																
												
														' temp_actual_value = dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values").value
														temp_actual_value = left(objDBRcdSet_TestData_B.Fields("NMI").value,10) & "," & objDBRcdSet_TestData_B.Fields("CHECKSUM").value _
														& "," & objDBRcdSet_TestData_B.Fields("TRANSACTION_ID").value _ 
														& "," & objDBRcdSet_TestData_B.Fields("WORK_TYPE").value _
														& "," & objDBRcdSet_TestData_B.Fields("NOMWID").value 
														'& "," & objDBRcdSet_TestData_B.Fields(".GW_FILE_TIMESTAMP").value _
														'& "," & objDBRcdSet_TestData_B.Fields("TO_ROLE").value _
														 '& "," & objDBRcdSet_TestData_B.Fields("FROM_ROLE").value 
														
														'temp_expected_value = left(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "NMI"),10) _ 
														temp_expected_value = left(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI")).value,10) _
														& "," & right(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI")).value,1) _ 
														& "," & "TxnID_" & objWS_DataParameter.cells ( intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value _
														& "," & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("WorkType")).value _
													    & "," & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NOMWID")).value
													    '& "," & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_Value_From_Role")).value _
														
														
'														temp_actual_value = left(objDBRcdSet_TestData_B.Fields("NMI").value,10) & "," & objDBRcdSet_TestData_B.Fields("METERSERIALNUMBER").value 
'														temp_expected_value = left(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "NMI"),10) _
'														& "," & fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "METERNUMBER")
																
																If ucase(trim(temp_actual_value)) =  ucase(trim(temp_expected_value)) Then
																	PrintMessage "P", "MTS_OWN_DB_Validation pass", "Expected value ("&  temp_expected_value & ") is SAME as Actual value("&  temp_actual_value & ")"
																Else
																	PrintMessage "f", "MTS_OWN_DB_Validation fail", "Expected value ("&  temp_expected_value & ") is DIFFERENT than Actual value("&  temp_actual_value & ")"
																End If
															End If 'end of If objDBRcdSet_TestData_B.recordcount > 0  Then
														
														Else
															fn_set_error_flags true, "value cannot be found in B2B_OWN_NMW table"
														End If ' end of If lcase(strScratch) = "pass" Then
													
											
											End If '' end of  If lcase(strQueryTemplate) <> "fail" Then
																											
											End If '' end of If strSQL_MTS <> ""  Then
																								
'												
													'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$	
													
												
                                              
'											
											
											
																					
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data validation failed in DB because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												''Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												printmessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "END"
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DB Data validation complete"
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												''Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												'printmessage "p", "DB Data validation complete", "Data validate complete for row ("  & intRowNr_CurrentScenario &  ")" 
											'	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
											'	fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												

												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DB Data validation complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_OWN_Screen_Verification"
												'Reporter.ReportEvent micPass, "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												printmessage "p", "DB Data validation complete", "Data validate complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												
												
			            					End If
			            					
											strSQL_MTS = ""
											
		'************************************************************************************************************************************************************************									 
											 
											 'MTS_Screen_Verification - code written on 06/06/2017
        '**************************************************************************************************************************************************************************
        
									    case "MTS_OWN_Screen_Verification"
																					
												' fn_compare_Current_and_expected_execution_time
																						
												tsNow = now() 
												tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
												IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
												End if
												
												'int_NMI = left(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value,10)
												
												
											    fn_MTS_MenuNavigation "Transactions;One Way Notification;Notice of Metering Works;Search Notices of Metering Works Received"
											    
											   								
												tmp_NMI = left(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")),10)
												'str_From_Participant = ""
                                                'str_From_Participant = 	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_Value_From_Role"))
                                                'str_NMW_Worktype = ""
                                                'str_Notification_Status = ""
											    'To verify the fields in the panel   
											    'var_Date_Received_From, var_Date_Received_To, str_From_Participant, int_NMI, str_NMW_Worktype, str_Notification_Status, WhetherClickDisplay_YN, WhetherClickResetFilters_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation
											    fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received  fnTimeStamp(now-7, "DD/MM/YYYY"),  fnTimeStamp(now, "DD/MM/YYYY"),  str_From_Participant, tmp_NMI, str_NMW_Worktype, str_Notification_Status, "y", "n", "y", gFW_strRunLog_ScreenCapture_fqFileName
											
											
											
											
											   Dim tmp_NMW_ID, tmp_Notification_Received_Date, tmp_NMI, tmp_NMW_Work_Type, tmp_From_Participant, tmp_Notification_Status, tmp_Rejection_Code, tmp_Rejection_Description
											   
											   tmp_NMW_ID = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOMWID")).value
											                                              
						 										   
'								               *** tmp_Notification_Received_Date validation **
'								          
''											 
                                                strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Notification_Received_Date")).value 
											
										
										     	If strSQL_MTS <> ""  Then
											
											        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											        
											        If lcase(strQueryTemplate) <> "fail" Then
											        	
														strSQL_MTS = strQueryTemplate
														'temp_strSQL_MTS = strSQL_MTS 
																							
'																									
'											  strSQL_MTS = replace ( strSQL_MTS , "<SQL_REQ_ID>", "TxnID_" & objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID").value), 1, -1, vbTextCompare)
'                                              											
'                                             strSQL_MTS = replace ( strSQL_MTS , "<SQL_REQ_ID>", left(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr("TxnID_" & Runtime_RequestID"),10).value)
                                             'strSQL_MTS = replace ( strSQL_MTS , "<SQL_REQ_ID>", left(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr("TxnID_" & "Runtime_RequestID"),10).value))
                                             strSQL_MTS = replace ( strSQL_MTS , "<SQL_REQ_ID>", "TxnID_" & objWS_DataParameter.cells ( intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value)
                                             
'working here

'                                               ' 'Execute MTS SQL
												strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "3", "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "")
'
'                                                 'Execute MTS SQL
'												strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("Notification_Received_Date"))
'														
'                                               ' ++++concatenate names++++  
                                              'strScratch = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", fnTimeStamp(GW_FILE_TIMESTAMP, "DD/MM/YYYY HH:mm:ss"))
'                                               
                                              strScratch = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "GW_FILE_TIMESTAMP")
											   'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = strScratch
'											   strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
'                                              objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
                                               'tmp_Notification_Received_Date = strScratch
                                               tmp_Notification_Received_Date = fnTimeStamp(strscratch, "DD/MM/YYYY HH:mm")
                                               
                                               'tmp_Notification_Received_Date = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
'											   
											   End if 'case(strQueryTemplate)
                                               End If  'strSQL_MTS <> 
'
'                                                                                                                              
                                      										   
											   tmp_NMI = left(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")),10)
											   tmp_NMW_Work_Type = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WorkType"))
										       tmp_From_Participant = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_Value_From_Role"))
											   tmp_Notification_Status = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Notification_Status"))
											   tmp_Rejection_Code = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Code"))
											   tmp_Rejection_Description = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Description"))
											   
											
											    ' Verify on grid
                                                'fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results (var_record_no, str_NMW_ID, var_Notification_Received_date, int_NMI, str_NMW_Work_Type, str_From_Participant, str_Notification_Status, str_Rejection_Code, str_Rejection_Description, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)											    
												fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results  "", tmp_NMW_ID,  tmp_Notification_Received_Date, tmp_NMI,  tmp_NMW_Work_Type, tmp_From_Participant,  tmp_Notification_Status,  tmp_Rejection_Code,  tmp_Rejection_Description,  "Y",  gFW_strRunLog_ScreenCapture_fqFileName
											     
											    'fn_MTS_Search_Meter_Exchange_Notifications_Received_Details   "", "TxnID_" & temp_Notification_ID,  "", temp_No_of_NMIs,  tmp_From_Participant,  tmp_Notification_Status,  tmp_Rejection_Code,  tmp_Rejection_Description,  "Y",  gFW_strRunLog_ScreenCapture_fqFileName
											 
											     
												' Click Deatils
												'fn_MTS_Click_button_any_screen "Search_Meter_Exchange_Notifications_Received", "btn_Details"
												
												' Verify on screen
												
'												Dim temp_transaction_id, temp_retailer, temp_date_sent, temp_status, temp_bus_acc_rej_dt, temp_bus_acc_rej_status, temp_bus_rej_event_cd, temp_bus_rej_explanation
'												
'												temp_transaction_id = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_RequestID" )).value
'												temp_retailer = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_Value_From_Role"))
'												'temp_retailer = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("From_Role"))
'												'temp_date_sent = 
'												temp_status = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Notification_Status"))
'												'temp_bus_acc_rej_dt = 
'												temp_bus_acc_rej_status = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Ack_File_Status"))
'												temp_bus_rej_event_cd = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Code"))
'												temp_bus_rej_explanation = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Explanation"))  
'																												
'												
'												'fn_MTS_Meter_Exchange_Notifications_Details "", "TxnID_" & temp_transaction_id,  temp_retailer, "temp_date_sent", temp_status,  "temp_bus_acc_rej_dt", temp_bus_acc_rej_status,  temp_bus_rej_event_cd,  temp_bus_rej_explanation, "Y",  gFW_strRunLog_ScreenCapture_fqFileName												
' 												fn_MTS_Meter_Exchange_Notifications_Details "TxnID_" & temp_transaction_id,  temp_retailer, "", temp_status,  "", temp_bus_acc_rej_status,  temp_bus_rej_event_cd,  temp_bus_rej_explanation, "Y",  gFW_strRunLog_ScreenCapture_fqFileName
' 												
'												
'												' Close Detail screen
'												fn_MTS_Close_any_screen "Meter_Exchange_Notification_Received_Details"
												
											   'Close the MTS screen
											   fn_MTS_Close_any_screen "Search_Notices_of_Metering_Works_Received"
											
											   tmp_NMW_ID = ""
											   tmp_Notification_Received_Date = ""
											   tmp_NMI = ""
											   tmp_NMW_Work_Type = ""
											   tmp_From_Participant = ""
											   tmp_Notification_Status = ""
											   tmp_Rejection_Code = ""
											   tmp_Rejection_Description = ""
											
											   ' At the end, check if any error was there and write in corresponding column
											   
											   If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											   objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											   objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Check_status_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											   objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											   Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
											   objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											   int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											   Exit do
											  Else
											  objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
											  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
											  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_OWN_Screen_Verification complete"
											  'Reporter.ReportEvent micPass, "CIS_Check_SOstatus_isRaised complete", "CIS_Check_status_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
											  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
											' Function to write timestamp for next process
											  fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											  int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											  int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
											  Exit Do
											  End If											
																						            			
			            					' End if
											case "FAIL"
											Exit Do
										case else ' incase there is no function (which would NEVER happen) name
											Exit Do
								End Select
					Loop While  (lcase(gFWbln_ExitIteration) = "n" or gFWbln_ExitIteration = false)
					
 
 '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 

				' ################################################################# END of first flow ####################################################################################################
				' ################################################################# END of first flow ####################################################################################################


	Case else ' Incase there is no elaboration for a flow, we need to report in datasheet and move on to next row
				    gFWbln_ExitIteration = "Y"
				    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
					'Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
	    End Select ' end of 	Select Case str_ScenarioRow_FlowName
		
		End If ' end of If  (	( strInScope = cY)  and str_ScenarioRow_FlowName <> "" and (trim(str_Next_Function) = "" or lcase(str_Next_Function) <> "fail" or lcase(str_Next_Function) <> "end") ) Then

'		' Before resetting the flags, write the last error in comments column
		If gFWstr_ExitIteration_Error_Reason <> "" Then
			strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value 
			strScratch = strScratch & " : " & gFWstr_ExitIteration_Error_Reason
			objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value = strScratch 
		End If
'		
		' Reset error flags
		fn_reset_error_flags
		
		' first write the data of scratch dictionary in excel
		
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch

	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	

	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows

	' Reset error flags
	fn_reset_error_flags

	i = 2
	objWB_Master.save ' Save the workbook 
Loop While int_ctr_no_of_Eligible_rows > 0

'Reporter.ReportEvent micDone, "Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
printmessage "i", "Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
'objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= now

objWB_Master.save ' Save the workbook 

'fnMTS_WinClose

' Once the test is complete, move the results to network drive
copyfolder  gFWstr_RunFolder , Cstr_NMW_Final_Result_Location


'ExitAction

'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%End of First Flow%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  

'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



	

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------


'------------------------------------------------------------



'
''Define Global variables
'
'Dim g_strObjTransactionID      
'Dim g_strMTSObjFromParticipant 
'Dim g_strMTSObjDateReceived
'Dim g_strMTSObjStatus
'Dim g_strMTSObjDate
'Dim g_strMTSObjBusinesStatus
'Dim g_strMTSObjEventCode
'Dim g_trMTSObjExplanation
'Dim gDict_FieldName_ValueActual_allForSingleScenarioStageRole
'    
'    
'    Dim temp_cntr_row, temp_total_rows, temp_row_to_be_Selected, strRowFoundSoContinueAndGetLowerLevelDetails_fromMTS_yN
'    Dim local_strNotificationId, local_strNotifcationRecdDate, local_strNumberofNMI, local_strFromParticipants, local_strNotificationStatus, local_strRejectionCode_local_strRejectionDescrip
'    
'   
'      ' Clear the dictionary at the start of function
'    gDict_FieldName_ValueActual_allForSingleScenarioStageRole.RemoveAll  
'
'    temp_total_rows = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbDataWindow("Grid_Result").RowCount
'    strRowFoundSoContinueAndGetLowerLevelDetails_fromMTS_yN= cN
'
'' First we need to select the right row in the grid
'    For temp_cntr_row = 1 To temp_total_rows
'
'
'        local_strNotificationId            = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbDataWindow("Grid_Result").GetCellData ("#"&temp_cntr_row,"Notification_ID")
'        local_strNotifcationRecdDate       = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbDataWindow("Grid_Result").GetCellData ("#"&temp_cntr_row,"Notification_Received_date")
'        local_strNumberofNMI               = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbDataWindow("Grid_Result").GetCellData ("#"&temp_cntr_row,"Number_of_NMIs")
'        local_strFromParticipants          = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbDataWindow("Grid_Result").GetCellData ("#"&temp_cntr_row,"from_participant")
'        local_strNotificationStatus        = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbDataWindow("Grid_Result").GetCellData ("#"&temp_cntr_row,"Notification_status")
'        local_strRejection                 =  PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbDataWindow("Grid_Result").GetCellData ("#"&temp_cntr_row,"Rejection_Code")
'        local_strRejectionDescrip          = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbDataWindow("Grid_Result").GetCellData ("#"&temp_cntr_row,"Rejection_Description")
'        
'        
'        ' Add all the values found on for this record in a dictionary
'         
'         fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Notification_ID", local_strNotificationId
'         fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Notification_Received_date", local_strNotifcationRecdDate 
'         fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Number_of_NMIs", local_strNumberofNMI
'         fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^from_participant", local_strFromParticipants
'         fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Notification_status", local_strNotificationStatus   
'         fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Rejection_Code", local_strRejection
'         fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Rejection_Description", local_strRejectionDescrip
'               
'         
'         'fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^ID", local_strMTSID
'        
'        
'                
'                PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbButton("Details").Click
'                ' strscratch = 
'                g_strObjTransactionID      = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","Transaction_ID")
'                fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Transaction_ID", g_strObjTransactionID
'
'                g_strMTSObjFromParticipant = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","From_Participant")
'                fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^From_Participant", g_strMTSObjFromParticipant
'                
'                g_strMTSObjDateReceived    = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","Date_Received")
'                fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Date_Received", g_strMTSObjDateReceived
'                
'                g_strMTSObjStatus      = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","Status")
'                fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Status", g_strMTSObjStatus
'                
'                g_strMTSObjDate            = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","Date")
'                fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Date", g_strMTSObjDate
'                
'                g_strMTSObjBusinesStatus   = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","Business_Status")
'                fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Business_Status", g_strMTSObjBusinesStatus
'                
'                g_strMTSObjEventCode       = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","Event_Code")
'                fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Event_Code", g_strMTSObjEventCode
'                
'                g-StrMTSObjExplanation     = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","Explanations")
'                fnLoadKey_toDictionary gDict_FieldName_ValueActual_allForSingleScenarioStageRole, "gMTS^Explanations", g_strMTSObjExplanation
'                
'               
'
'                call PbWindow("MTS").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName ,True )'p2 is overwrite=true/false
'                call reporter.ReportEvent  (micDone,  strRunLog_ScreenCapture_fqFileName , "",  strRunLog_ScreenCapture_fqFileName )
'                
'                PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbButton("Close").Click
'                PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received_Details").PbButton("Close").Click
'        
'        
        
'        Else
'                  Reporter.ReportEvent micFail,"Step - fn_MTS_Search_Meter_Exchange_Notifications_Received_Details Function", "Unable to find row with stage - " & piStr_Stage & ", Request ID [" & piStr_RequestID & "] and NMI [" & piint_NMI & "]"
'    End If
'
'    ' Return the local_strMTSTxnStatus (txn status) to the function. Incase its Processed, then only move to next stage
'                fnCheckCatsNotificationReceived_MTS = local_strMTSTxnStatus





'Function fn_MTS_Meter_Exchange_Notifications_Details (str_Transaction_ID,  str_From_Participant, var_Date_Received, str_Notification_Status, var_BusinessDate, str_Business_Status, str_Event_Code, str_Explanation, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_MTS_Meter_Exchange_Notifications_Details
'    '  Description:-  Function is used to check Meter Exchange Notification Details screen
'    '  Parameters:-    
'    '  Return Value:-
'    '  Creator:- 
'    '################################################################################
'
'
'    
'    'Compare Expeted_AllValues, _Actual_AllValues
'    Dim dt_Compare_Date, strCommonResult
'    Dim strTransactionID, strFromParticipant, strDateReceived, strStatus , strDate, strBusinesStatus, strEventCode, strExplanation
' 
' 
' 
' Dim strscratch, int_total_recs, int_datagridcount
'
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" or gFWbln_ExitIteration = true Then
'    	printmessage "i", "fn_MTS_Meter_Exchange_Notifications_Details function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
'    ' On error resume next
'
'    ' PbWindow("w_frame").Activate
'    wait 0,int_mini_wait_time
'        On error resume next
'      PbWindow("MTS").Activate
'    On error goto 0
'
'    PbWindow("MTS").RefreshObject
'   
'    
''  
''   Dim intTotalWait : intTotalWait = 0  
''   do while( PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbButton("Display").exist(0) = false )
''        
''        intTotalWait = intTotalWait + 1
''        wait 0, 200
''        If intTotalWait >= 100 Then
''            ExitAction
''        End if
''    loop
''          
''    pbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbButton("Display").Click
''    
''    wait(1)
''    
''    If PbWindow("MTS").Dialog("DialogMTS").Static("NoRecordsWereFoundForTheSpecifiedSearchCriteria").exist(2) then
''
''        call PbWindow("MTS").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName ,True) 
''        
''        
''      call reporter.ReportEvent  ( micWarning, "Notification_Id `" & str_Transaction_ID & "` not found.", "", strRunLog_ScreenCapture_fqFileName )
''        gFwInt_DataParameterRow_ErrorsTotal = gFwInt_DataParameterRow_ErrorsTotal + 1
''        gFwInt_AbandonIteration_ReasonCount = gFwInt_AbandonIteration_ReasonCount + 1    
''        
''        
''        
''        fnAFW_wIncrementGlobalCounter gFwInt_DataParameterRow_ErrorsTotal
''        fnAFW_wIncrementGlobalCounter gFwInt_AbandonIteration_ReasonCount
''
''        PbWindow("MTS").Dialog("DialogMTS").WinButton("OK").Click ' return the app to it's post-search usual-stat
''        
''        
''  
''   Dim temp_cntr_row, temp_total_rows, temp_row_to_be_Selected, strRowFoundSoContinueAndGetLowerLevelDetails_fromMTS_yN
''   Dim local_strMTSRecdNotificationID, local_strMTSNumberofNMIs
''  
''    temp_total_rows = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbDataWindow("dw_slct").RowCount
''    strRowFoundSoContinueAndGetLowerLevelDetails_fromMTS_yN= cN
''
''   
''    'select the right row in the grid
''    For temp_cntr_row = 1 To temp_total_rows
''
''     
''    local_strMTSRecdNotificationID               = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbDataWindow("dw_slct").GetCellData ("#"&temp_cntr_row,"Notification_ID")
''    local_strMTSNumberofNMIs    = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbDataWindow("dw_slct").GetCellData ("#"&temp_cntr_row,"Number_of_NMIs")
''
''
''        PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbButton("Details").Click
''      
' @@ hightlight id_;_853714_;_script infofile_;_ZIP::ssf49.xml_;_
' 
'
' 
' 
' 
'
'  ' Record verification
'    If trim(lcase(str_Transaction_ID)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","transaction_id")
'
'        If lcase(trim(str_Transaction_ID)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Notification Id same as Expected value (" & str_Transaction_ID &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Investigation Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Investigation_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If 
'        
'        If trim(lcase(str_From_Participant)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","retailer")
'
'        If lcase(trim(str_From_Participant)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "From Participant same as Expected value (" & str_From_Participant &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Investigation Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Investigation_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'        
'               
'         If trim(lcase(var_Date_Received)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","date_sent")
'
'        If lcase(trim(var_Date_Received)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Transaction_ID &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Investigation Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Investigation_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'        
'         If trim(lcase(str_Notification_Status)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","status")
'
'        If lcase(trim(str_Notification_Status)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Transaction_ID &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Investigation Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Investigation_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'        
'        
'         If trim(lcase(var_BusinessDate)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","bus_acc_rej_dt")
'
'        If lcase(trim(var_BusinessDate)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Transaction_ID &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Investigation Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Investigation_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End IF
'        
'        
'         If trim(lcase(str_Business_Status)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","bus_acc_rej_status")
'
'        If lcase(trim(str_Business_Status)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Transaction_ID &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Investigation Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Investigation_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'                
'         If trim(lcase(str_Event_Code)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","bus_rej_event_cd")
'
'        If lcase(trim(str_Event_Code)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Transaction_ID &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Investigation Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Investigation_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If 
'        
'         If trim(lcase(str_Explanation)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Verify_Meter_Exchange_Notifications_Details").PbDataWindow("Results_Verify_Meter_Exchange_Notifications_Details").GetCellData("#1","bus_rej_explanation")
'
'        If lcase(trim(str_Explanation)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Transaction_ID &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Investigation Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Investigation_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'              
'               
'    End Function 'fn_MTS_Meter_Exchange_Notifications_Details   	
'
'''    

'&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                                 'NMW
                                 
  '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&                                


'fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received "29/06/2017", "06/07/2017", "","","","", "","", "",""


Function fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received (var_Date_Received_From, var_Date_Received_To, str_From_Participant, int_NMI, str_NMW_Worktype, str_Notification_Status, WhetherClickDisplay_YN, WhetherClickResetFilters_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
 

    '################################################################################
    '
    '  Function Name:- fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received
    '  Description:-  This function is to search Notices of Metering Works screen
    '  Parameters:-    1)
    '  Return Value:-
    '  Creator:-  
    '################################################################################

    ' Check if there was any error and whether the function should continue
    If lcase(trim(gFWbln_ExitIteration)) = "y" or gFWbln_ExitIteration = true Then
    	printmessage "i", "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function", "Exiting function as there was some error in previous step"
        Exit function
    End If

    On error resume next

    wait 0,int_mini_wait_time
        On error resume next
          PbWindow("MTS").Activate
    	On error goto 0


   ' Window("MTS_Window").WinMenu("Menu").Select "Transactions;One Way Notification;Notice of Metering Works;Meter Exchange Notification Received Search"

   PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").RefreshObject
   PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").RefreshObject

  
  
    if var_Date_Received_From <> "" Then
    	
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SelectCell "#1","created_dt_from"
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SetCellData  "#1","created_dt_from", var_Date_Received_From
        printmessage "p", "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function", "Entring Txn received date - from ("& var_Date_Received_From &")"
    End If

    if var_Date_Received_To <> "" Then
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SelectCell "#1","created_dt_to"
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SetCellData  "#1","created_dt_to", var_Date_Received_To
        printmessage "p", "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function", "Entring Txn received date - To ("& varDate_Received_To &")"
    End If
    
           
    
    if str_From_Participant <> "" Then
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SelectCell "#1","retailer"
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SetCellData  "#1","retailer", str_From_Participant
        printmessage "p", "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function", "Entring Txn From Participant ("& str_From_Participant &")"
    End If
    

      if int_NMI <> "" Then
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SelectCell "#1","nmi"
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SetCellData  "#1","nmi", int_NMI
        printmessage "p", "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function", "Entring Txn From Participant ("& int_NMI &")"
     End If
    
    
       if str_NMW_Worktype <> "" Then
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SelectCell "#1","nmw_worktype"
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SetCellData  "#1","nmw_worktype", str_NMW_Worktype
        printmessage "p", "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function", "Entring Txn From Participant ("& str_NMW_Worktype &")"
     End If   
     
      
      
    if str_Notification_Status <> "" Then
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SelectCell "#1","status"
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Panel_Search").SetCellData  "#1","status", str_Notification_Status
        printmessage "p", "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function", "Entring Txn Notification Status ("& str_Notification_Status &")"
    End If
    

  If lcase(WhetherClickDisplay_YN) = "y" Then
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbButton("btn_Display").Click  
    End If 

  If lcase(WhetherClickResetFilters_YN) = "y" Then
        PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbButton("btn_Reset_Filters").Click  
    End If   

   
'	On error resume next
	    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
	        ' First add timestamp to filename
	        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
	        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
	        call PbWindow("MTS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
	    End If
	    err.clear
    On error goto 0


    ' Check if no record found
    If Dialog("MTS_No_Record_Found").exist(2) Then
        Dialog("MTS_No_Record_Found").WinButton("btn_OK").Click
        fn_set_error_flags true, "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function - No Record found on Search_Site_Access_Request_Received Screen"
        fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received = "NO_RECORD_FOUND"
        on error goto 0
        err.clear
        Exit Function
    End If


    ' At the end of the function, check if there was any error during any operation and if error needs to be generated


    ' Write information to Run Log (database)
    
    If err.number > 0  Then
    	' capture error reason 
    	fn_set_error_flags true, "fn_MTS_Search_of_Notices_of_Metering_Works_Notifications_Received function - Error (" & err.description  & "). Exiting row"
    	
    End If

	err.clear
	on error goto 0

End Function ' end of function fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received

'
Function fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results (var_record_no, str_NMW_ID, var_Notification_Received_date, int_NMI, str_NMW_Work_Type, str_From_Participant, str_Notification_Status, str_Rejection_Code, str_Rejection_Description, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)

    '################################################################################
   
    '  Function Name:- fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results
    '  Description:-  Function used to check Notice of Meterting Works record Details
    '  Parameters:-    
    '  Return Value:-
    '  Creator:- 
    '################################################################################
    Dim strscratch
    ' int_total_recs, var_record_no, int_datagridcount

    ' Check if there was any error and whether the function should continue
    If lcase(trim(gFWbln_ExitIteration)) = "y" or gFWbln_ExitIteration = true Then
    	printmessage "i", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Exiting function as there was some error in previous step"
        Exit function
    End If

    ' On error resume next

    ' PbWindow("w_frame").Activate
    wait 2,int_mini_wait_time
        On error resume next
      PbWindow("MTS").Activate
    On error goto 0

    PbWindow("MTS").RefreshObject

    PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").RefreshObject
    PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").RefreshObject

  
    if PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").RowCount > 0 Then

        Select case lcase(trim(var_record_no))
	        Case "first"
	            var_record_no = 1
	        Case "second"
	            var_record_no = 2
	        Case "last"
	            var_record_no = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").RowCount
	        Case else
	        	var_record_no = 0
        End Select


        If var_record_no > 0  Then
        	PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").SelectCell "#" & var_record_no,"nmw_id"
        Else

	          var_record_no = 0
	            ' code to traverse through the table and select the correct value
	          int_total_recs = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").RowCount
	          
	            
	          If str_NMW_ID <> "" Then
		      	    If lcase(str_NMW_ID) = "<blank>"Then
				    	str_NMW_ID = ""
				    End If

	            For int_datagridcount = 1 To int_total_recs
	              If trim(lcase(PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& int_datagridcount,"nmw_id"))) = trim(lcase(str_NMW_ID)) Then
	                      var_record_no = int_datagridcount
	                      Exit for 
		          End If
	            Next
	          End If
	
	          If var_record_no = 0 Then
	            gFWbln_ExitIteration = "Y"
	            gFWstr_ExitIteration_Error_Reason = "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function - No record found on results grid for NMW_ID(" & str_NMW_ID & ")"
	            ' Reporter.ReportEvent micFail, "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
	            printmessage "f", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
	            fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results = "FAIL"
	            Exit function
	          Else
	           wait 5
	            PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").SelectCell "#" & var_record_no,"nmw_id"
	            PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").ActivateCell "#" & var_record_no,"nmw_id"
	          End If

         End If
    Else
        gFWbln_ExitIteration = "Y"
        gFWstr_ExitIteration_Error_Reason = "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function - No record found on grid"
        'Reporter.ReportEvent micFail, "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "NO RECORD FOUND...exiting"
        printmessage "f", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "NO RECORD FOUND...exiting"
        fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results = "FAIL"
        Exit function
    End if

   
    ' -----Verify records---

    ' NMW_ID 
    If trim(lcase(str_NMW_ID)) <> "" Then
    strscratch = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& var_record_no,"nmw_id")
    
	    If lcase(str_NMW_ID) = "<blank>"Then
	    	str_NMW_ID = ""
	    End If
    
        If trim(lcase(str_NMW_ID)) = trim(lcase(strscratch))  Then
             printmessage "p", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "NMW_ID SAME on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & str_NMW_ID &") match with actual value ("& strscratch &")"
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "NMW_ID different on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & str_NMW_ID &") doesn't match with actual value ("& strscratch &")"
            printmessage "f", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason        
        End If
    End If

	' Notification received date
    If trim(lcase(var_Notification_Received_date)) <> "" Then
        strscratch = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& var_record_no,"notif_recvd_dt")
       
       'this is to validate specific time stamp 
        If strscratch <> "" Then
			PrintMessage "i", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results Function", "Converting format of actual var_Notification_Received_date ("& strscratch &") to DD/MM/YYYY HH:mm as we need to compare date part only"
        	strscratch = fnTimeStamp(strscratch,"DD/MM/YYYY HH:mm")
			PrintMessage "i", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results Function", "Converting format of expected var_Notification_Received_date ("& var_Notification_Received_date &") to DD/MM/YYYY HH:mm as we need to compare date part only"
        	var_Notification_Received_date = fnTimeStamp(var_Notification_Received_date,"DD/MM/YYYY HH:mm")
        End If

        If trim(lcase(var_Notification_Received_date)) = trim(lcase(strscratch))  Then
            'Reporter.ReportEvent micPass,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Request Received Date same. Expected value (" & var_Request_Received_Date &") matched actual value ("& strscratch &")"
            printmessage "p", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Notification_Received_date same on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value ("& var_Notification_Received_date &")"  
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "Notification Received Date different on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & var_Notification_Received_date &") doesn't match with actual value ("& strscratch &")"
            'Reporter.ReportEvent micFail,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
            printmessage "f", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason  
        End If
    End If
    
	' NMI
    If trim(lcase(int_NMI)) <> "" Then
        strscratch = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& var_record_no,"nmi")

        If trim(lcase(int_NMI)) = trim(lcase(strscratch))  Then
            'Reporter.ReportEvent micPass,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "NMI same. Expected value (" & int_NMI &") matched actual value ("& strscratch &")"
            printmessage "p","fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "NMI same on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid.Expected value (" & int_NMI &") matched actual value ("& strscratch &")"
            
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "NMI different on Search_Site_Access_Request_Received results grid. Expected value (" & int_NMI &") doesn't match with actual value ("& strscratch &")"
            'Reporter.ReportEvent micFail,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
            printmessage "f", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
        End If
    End If

 'NMW_Work_Type
    If trim(lcase(str_NMW_Work_Type)) <> "" Then
        strscratch = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& var_record_no,"nmw_work_type")

        If trim(lcase(str_NMW_Work_Type)) = trim(lcase(strscratch))  Then
            'Reporter.ReportEvent micPass,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "NMW_Work_Type same. Expected value (" & str_NMW_Work_Type &") matched actual value ("& strscratch &")"
            printmessage "p","fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "NMW_Work_Type same on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid.Expected value (" & str_NMW_Work_Type &") matched actual value ("& strscratch &")"
            
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "NMW_Work_Type different on Search_Site_Access_Request_Received results grid. Expected value (" & str_NMW_Work_Type &") doesn't match with actual value ("& strscratch &")"
            'Reporter.ReportEvent micFail,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
            printmessage "f", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
        End If
    End If




	' FROM PARTICIPANT
    If trim(lcase(str_From_Participant)) <> "" Then
        strscratch = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& var_record_no,"from_participant")
        
        If trim(lcase(str_From_Participant)) = trim(lcase(strscratch))  Then
            'Reporter.ReportEvent micPass,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "From participant same. Expected value (" & str_From_Participant &") matched actual value ("& strscratch &")"
            printmessage "p","fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "From participant same on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & str_From_Participant &") matched actual value ("& strscratch &")"
            
            
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "From participant different on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & str_From_Participant &") doesn't match with actual value ("& strscratch &")"
            'Reporter.ReportEvent micFail,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
            printmessage "f", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
        End If
    End If



	' Notification status
    If trim(lcase(str_Notification_Status)) <> "" Then
        strscratch = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& var_record_no,"notification_status")

        If trim(lcase(str_Notification_Status)) = trim(lcase(strscratch))  Then
            'Reporter.ReportEvent micPass,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Notification Status same. Expected value (" & str_Notification_Status &") matched actual value ("& strscratch &")"
             printmessage "p", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Notification Status same on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid.. Expected value (" & str_Notification_Status &") matched actual value ("& strscratch &")"
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "Notification Status different on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & str_Notification_Status &") doesn't match with actual value ("& strscratch &")"
            'Reporter.ReportEvent micFail,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
             printmessage "f","fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
        End If
    End If



	' Rejection_Code
    If trim(lcase(str_Rejection_Code)) <> "" Then
        strscratch = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& var_record_no,"rejection_code")

        If trim(lcase(str_Rejection_Code)) = trim(lcase(strscratch))  Then
            'Reporter.ReportEvent micPass,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Rejection Code same. Expected value (" & str_Rejection_Code &") matched actual value ("& strscratch &")"
             printmessage "p","fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Rejection Code same on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & str_Rejection_Code &") matched actual value ("& strscratch &")"
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "Rejection Code different on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & str_Rejection_Code &") doesn't match with actual value ("& strscratch &")"
           ' Reporter.ReportEvent micFail,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
             printmessage "f", "fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
             End If
    End If
    
	' Rejection_Description
    If trim(lcase(str_Rejection_Description)) <> "" Then
        strscratch = PbWindow("MTS").PbWindow("Search_Notices_of_Metering_Works_Received").PbDataWindow("Grid_Results").GetCellData("#"& var_record_no,"rejection_description")

        If trim(lcase(str_Rejection_Description)) = trim(lcase(strscratch))  Then
            'Reporter.ReportEvent micPass,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Notification Status same. Expected value (" & str_Rejection_Description &") matched actual value ("& strscratch &")"
            printmessage "p","fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", "Rejection Description same on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid.. Expected value (" & str_Rejection_Description &") matched actual value ("& strscratch &")"
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "Rejection Description different on Search_of_Notice_of_Metering_Works_Notifications_Received_Details results grid. Expected value (" & str_Rejection_Description &") doesn't match with actual value ("& strscratch &")"
           ' Reporter.ReportEvent micFail,"fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
           printmessage "f","fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results function", gFWstr_ExitIteration_Error_Reason
        End If
    End If

	On error resume next
    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
        ' First add timestamp to filename
        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
        ' call PbWindow("w_frame").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
        call PbWindow("MTS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
    End If
    err.clear
    On error goto 0 



    ' At the end of the function, check if there was any error during any operation and if error needs to be generated


    ' Write information to Run Log (database)

' on error goto 0

End Function ' end of function fn_MTS_Search_of_Notice_of_Metering_Works_Notifications_Received_Select_Record_Results

 
 'Once the test is complete, move the results to network drive

copyfolder  gFWstr_RunFolder , Cstr_NMW_Final_Result_Location


'Function fn_MTS_Notice_of_Metering_Works_Received_Detail (str_Transaction_ID,  str_From_Participant, var_Date_Received, str_Notification_Status, var_BusinessDate, str_Business_Status, str_Event_Code, str_Explanation, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
'  str_NMW_ID,  str_From_Participant, var_Date_Received, str_Notification_Status, var_BusinessDate, str_Business_Status, str_Event_Code, str_Explanation, int_NMI, int_NMICHECKSUM, var_DATE, srt_Work_Type, var_FieldWorkDate_Time, str_Customer_Type, str_Energisation_Status, str_Primary_Voltage, _
'  str_Longitude, int_TotalInstalledMeters, int_MeterSerialNumber, str_SupplyPhases, str_GeneralSupply, str_ControlledLoad, str_GenerationType, int_TotalInstalledNetworkDevices, int_NetworkDeviceNumber, int_ )
'
'    '################################################################################
'    '
'    '  Function Name:- fn_MTS_Meter_Exchange_Notifications_Details
'    '  Description:-  Function is used to check Meter Exchange Notification Details screen
'    '  Parameters:-    
'    '  Return Value:-
'    '  Creator:- 
'    '################################################################################
'
'
'    
'    'Compare Expeted_AllValues, _Actual_AllValues
'    Dim dt_Compare_Date, strCommonResult
'    Dim strTransactionID, strFromParticipant, strDateReceived, strStatus , strDate, strBusinesStatus, strEventCode, strExplanation
' 
' 
' 
' Dim strscratch, int_total_recs, int_datagridcount
'
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" or gFWbln_ExitIteration = true Then
'    	printmessage "i", "fn_MTS_Meter_Exchange_Notifications_Details function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
'    ' On error resume next
'
'    ' PbWindow("w_frame").Activate
'    wait 0,int_mini_wait_time
'        On error resume next
'      PbWindow("MTS").Activate
'    On error goto 0
'
'    PbWindow("MTS").RefreshObject
'   
'    
''  
''   Dim intTotalWait : intTotalWait = 0  
''   do while( PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbButton("Display").exist(0) = false )
''        
''        intTotalWait = intTotalWait + 1
''        wait 0, 200
''        If intTotalWait >= 100 Then
''            ExitAction
''        End if
''    loop
''          
''    pbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbButton("Display").Click
''    
''    wait(1)
''    
''    If PbWindow("MTS").Dialog("DialogMTS").Static("NoRecordsWereFoundForTheSpecifiedSearchCriteria").exist(2) then
''
''        call PbWindow("MTS").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName ,True) 
''        
''        
''      call reporter.ReportEvent  ( micWarning, "Notification_Id `" & str_Transaction_ID & "` not found.", "", strRunLog_ScreenCapture_fqFileName )
''        gFwInt_DataParameterRow_ErrorsTotal = gFwInt_DataParameterRow_ErrorsTotal + 1
''        gFwInt_AbandonIteration_ReasonCount = gFwInt_AbandonIteration_ReasonCount + 1    
''        
''        
''        
''        fnAFW_wIncrementGlobalCounter gFwInt_DataParameterRow_ErrorsTotal
''        fnAFW_wIncrementGlobalCounter gFwInt_AbandonIteration_ReasonCount
''
''        PbWindow("MTS").Dialog("DialogMTS").WinButton("OK").Click ' return the app to it's post-search usual-stat
''        
''        
''  
''   Dim temp_cntr_row, temp_total_rows, temp_row_to_be_Selected, strRowFoundSoContinueAndGetLowerLevelDetails_fromMTS_yN
''   Dim local_strMTSRecdNotificationID, local_strMTSNumberofNMIs
''  
''    temp_total_rows = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbDataWindow("dw_slct").RowCount
''    strRowFoundSoContinueAndGetLowerLevelDetails_fromMTS_yN= cN
''
''   
''    'select the right row in the grid
''    For temp_cntr_row = 1 To temp_total_rows
''
''     
''    local_strMTSRecdNotificationID               = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbDataWindow("dw_slct").GetCellData ("#"&temp_cntr_row,"Notification_ID")
''    local_strMTSNumberofNMIs    = PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbDataWindow("dw_slct").GetCellData ("#"&temp_cntr_row,"Number_of_NMIs")
''
''
''        PbWindow("MTS").PbWindow("Search_Meter_Exchange_Notifications_Received").PbButton("Details").Click
''      
'
'
'
'  ' Record verification
'    If trim(lcase(str_Transaction_ID)) <> "" Then
'    	'strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_notification").highlight
'    	'strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_notification").GetCellData("#1","transaction_id")
'        strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_notification").GetCellData("#1","transaction_id")
'
'        If lcase(trim(str_Transaction_ID)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Transaction Id same as Expected value (" & str_Transaction_ID &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Transaction Id different on Meter_Exchange_Notifications_Received_Details. Expected value (" & str_Transaction_ID &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If 
'        
'        If trim(lcase(str_From_Participant)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_notification").GetCellData("#1","retailer")
'
'        If lcase(trim(str_From_Participant)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "From Participant same as Expected value (" & str_From_Participant &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "From Participant different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_From_Participant &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'        
'               
'         If trim(lcase(var_Date_Received)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_notification").GetCellData("#1","date_sent")
'
'        If lcase(trim(var_Date_Received)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & var_Date_Received &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Date Received different on Verify Meter Exchange Notifications Details Screen. Expected value (" & var_Date_Received &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'        
'         If trim(lcase(str_Notification_Status)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_notification").GetCellData("#1","status")
'
'        If lcase(trim(str_Notification_Status)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Notification_Status &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Notification Status different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Notification_Status &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'        
'        
'         If trim(lcase(var_BusinessDate)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_acc_rej_dt")
'
'        If lcase(trim(var_BusinessDate)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & var_BusinessDate &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Business Date different on Verify Meter Exchange Notifications Details Screen. Expected value (" & var_BusinessDate &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End IF
'        
'        
'         If trim(lcase(str_Business_Status)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_acc_rej_status")
'
'        If lcase(trim(str_Business_Status)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Business_Status &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Business Status different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Business_Status &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'                
'         If trim(lcase(str_Event_Code)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_rej_event_cd")
'
'        If lcase(trim(str_Event_Code)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Event_Code &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Event Code different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Event_Code &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If 
'        
'         If trim(lcase(str_Explanation)) <> "" Then
'        strscratch = PbWindow("MTS").PbWindow("Meter_Exchange_Notification_Received_Details").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_rej_explanation")
'
'        If lcase(trim(str_Explanation)) = lcase(trim(strscratch)) Then
'            Reporter.ReportEvent micPass,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", "Date Received same as Expected value (" & str_Explanation &") matched actual value ("& strscratch &")"
'        Else
'            gFWbln_ExitIteration = "Y"
'            gFWstr_ExitIteration_Error_Reason = "Explanation different on Verify Meter Exchange Notifications Details Screen. Expected value (" & str_Explanation &") doesn't match with actual value ("& strscratch &")"
'            Reporter.ReportEvent micFail,"fn_MTS_Search_Meter_Exchange_Notifications_Received_Details function", gFWstr_ExitIteration_Error_Reason
'        End If
'        
'        End If
'              
'        On error resume next
'	    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'	        ' First add timestamp to filename
'	    Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'	        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'	        ' call PbWindow("w_frame").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'	    call PbWindow("MTS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'	    End If
'	    err.clear
'	    On error goto 0       
'               
'    End Function 'fn_MTS_Meter_Exchange_Notifications_Details  
'
