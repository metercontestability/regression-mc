﻿
' Script used to verify on MTS GUI all changes for RP to MC change

Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong


Set hFWobj_SB = DotNetFactory.CreateInstance( "System.Text.StringBuilder" ) 
Set gFWobj_SystemDate = DotNetFactory.CreateInstance( "System.DateTime" )
' hFWobj_SB.AppendFormat "My name is {0} {1}, I own a {2} and {3} name is {4}", fName, lName,  "dog", "his", "Star"
' Print gFWobj_SB.ToString


' push the RunStats to the DriverWS report-area
Dim cellTopLeft, iTS, iTS_Max, rowTL, colTL

Dim strRunLog_Folder

' -----

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq



dim qtTest, qtResultsOpt, qtApp
Dim i_timetowait : i_timetowait = 180

Set qtApp = CreateObject("QuickTest.Application")	' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
If qtApp.launched <> True then
	qtApp.Launch
End If

qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
qtApp.Options.Run.RunMode = "Fast"
qtApp.Options.Run.ViewResults = False


Set qtTest = qtApp.Test											' http://www.geekinterview.com/question_details/64501
Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions") 	' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539

Dim strRunLog_ScreenCapture_fqFileName


gDt_Run_TS = tsOfRun
Dim scratch
scratch = fnAttachStr(BASE_AUTOMATION_DIR,"\","R", true)

'strRunLog_Folder = hFWobj_SB.AppendFormat ("{0}DataSheets\runs\{1}_{2}_{3}_{4}\"					, _
	'	fnAttachStr(BASE_AUTOMATION_DIR,"\","R", gcLibBln_OnlyIfNotAlreadyThere)	, _
	'	qtTest.Name																	, _
	'	Parameter.Item("Environment")												, _
	'	Parameter.Item("Company") 													, _
	'	fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`ddDdD_HH`mm")	).toString
		
		
strRunLog_Folder = hFWobj_SB.AppendFormat ("{0}{1}_{2}_{3}_{4}\"					, _
		fnAttachStr(cTemp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)	, _
		qtTest.Name																	, _
		Parameter.Item("Environment")												, _
		Parameter.Item("Company") 													, _
		fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`ddDdD_HH`mm")	).toString


Path_MultiLevel_CreateComplete strRunLog_Folder 
gFWstr_RunFolder = strRunLog_Folder 

' ----------------------------------

qtResultsOpt.ResultsLocation = strRunLog_Folder

'										
'										qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
'										qtApp.Options.Run.MovieCaptureForTestResults = "Never"
'										qtApp.Options.Run.MovieSegmentSize = 2048
'										qtApp.Options.Run.RunMode = "Fast"
'										qtApp.Options.Run.SaveMovieOfEntireRun = False
'										qtApp.Options.Run.StepExecutionDelay = 0
'										qtApp.Options.Run.ViewResults = False
qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
qtApp.Options.Run.AutoExportReportConfig.ExportLocation = strRunLog_Folder
qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
qtApp.Options.Run.ScreenRecorder.RecordSound = False
qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True

		gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
		gFWbln_ExitIteration = false
    	
		Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")
		
    	dim MethodName 
    	On error resume next
    '	MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
    	MethodName = oDnf_SysDiags.GetMethod().Name
    	On error goto 0
    	
    	
    	


' =========  
' =========  Stage 00   - Setup the Stage-reporting elements =============
' =========  

Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
Dim ctSucc, intStageSuccessAr : intStageSuccessAr = split(",0,0,0,0", ",") : intWantedSuccessCount = uBound(intStageSuccessAr)
Dim BASE_XML_Template_DIR

' =========  
' =========  Stage 0a   - Expand the Template Rows into DataPermutation Rows  =============
' =========  

					'''''Dim tsAr_EventStartedAtTS : tsAr_EventStartedAtTS = split(",,,,,,,,,,", ",")
					'''''Dim tsAr_EventEndedAtTS : tsAr_EventEndedAtTS = split(",,,,,,,,,,", ",")
Dim tsAr_EventPermutationsCount : tsAr_EventPermutationsCount = split("0,0,0,0,0,0,0,0,0,0,0", ",")
const iTS_Stage_0 = 0
const iTS_Stage_I = 1
const iTS_Stage_II = 2
const iTS_Stage_III = 3

Dim str_CatsInboxFolder, str_ackfile_all

					'''''tsAr_EventStartedAtTS(iTS_Stage_0) = now()

Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
' Environment.Value("Verify_RPorMC")=Parameter.Item("Verify_RPorMC")


Dim objNet : Set objNet = CreateObject("WScript.NetWork") '  objNet.UserName objNet.ComputerName 

' Environment.Value("RUNCOUNT")=Parameter.Item("RunCount") ' not required

Dim intColNames_RowNr, intColNames_ColNr_Start, intColNames_ColNr_End
Dim r_rtTemplate, r_rtDataPermutation, intColNr_RowType, intColNr_DataCol_First, intColNr_DataCol_Last, intColNr_InScope, intColNr_ScenarioStatus


Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"

' Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "MeterContestablity_Objections.v12.xls"
' Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "MC_Objections-Preeti.v32.xlsm"
Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "Automation_Plan_Book.xlsm"



Dim BASE_GIT_DIR : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"


fnExcel_CreateAppInstance  objXLapp, true
LoadData_RunContext objXLapp, objWB_Master, objWS_TestRunContext, 1,  BASE_GIT_DIR & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder

' load the DataParameter worksheet objWS_DataParameter - this contains the DataScenarios to test
'
' set objWS_DataParameter    = objWB_Master.worksheets("wsDT_MeterCntestblty_Objections")
'Umesh -  set objWS_DataParameter    = objWB_Master.worksheets("wsSsDT_mrCntestbty_Objectns") 

set objWS_templateXML_TagDataSrc_Tables = objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' store XML Template WS name
set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference

' set objWS_SAN    = objWB_Master.worksheets("wsVefiry_MTS_GUI_Changes")

set objWS_SAN    = objWB_Master.worksheets("wsMs_SAN")

objWS_SAN.activate


'MethodName = gFWoDnf_SysDiags.GetMethod().Name

objWS_SAN.range("rgWS_RunConfig_ENV"             ).formula = "'" & Parameter.Item("Environment")
objWS_SAN.range("rgWS_runConfig_COY"             ).formula = "'" & Parameter.Item("Company")
' objWS_SAN.range("rgWS_Verify_RPorMC"             ).formula = "'" & Parameter.Item("Verify_RPorMC")


'Umesh -  int_NrOf_InScope_Rows      = objWS_DataParameter.range("rgWS_Count_NrOf_needed_NMIs").value
'Umesh -  int_NrOf_InScope_Rows      = cInt(int_NrOf_InScope_Rows * 1.5) + 10 ' get some extra rows in case some of those identified are reserved for other cases
'Umesh -  int_NrOf_NMI_rows_required = int_NrOf_InScope_Rows * (intSize_listOldNew + 1)

Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE, cellReportStatus_SingleScenario

'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr

Set gdictws_SAN_KeyName_ColNr = CreateObject("Scripting.Dictionary")
gdictws_SAN_KeyName_ColNr.CompareMode = vbTextCompare


Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd

intWsRowNr_ColumNames = objWS_SAN.range("rgWS_ColumName_Row").row
intWsColNr_ParmsStart = objWS_SAN.range("rgWS_DataCol_First").column
intWsColNr_ParmsEnd   = objWS_SAN.range("rgWS_DataCol_Last").column


gdictws_SAN_KeyName_ColNr.RemoveAll
'Umesh -  fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
fnWS_LoadKey_ColOffsets_toDictionary objWS_SAN, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", gdictws_SAN_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"


Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
Dim tempInt_RoleIdentificationCounter

gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
' Load the first/title row in a dictionary
int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue


'Umesh -  set cellTopLeft = objWS_DataParameter.range("rgWS_RunReport_TopLeftCell_Stage0_StartTS")
rowTL = 1 : colTL = 1


' =========  
' =========  Stage 0b   - Technical Setup for Running the DataPermutation Rows   =============
' =========  


Dim objDBConn_TestData_A, objDBRcdSet_TestData_A 
dim r

On error resume next
Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
'Umesh -  r = 0
set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")


Dim strAr_listOldNew, intSize_listOldNew, str_listOldNew


Dim objXLapp, objWB_Master, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
dim objWS_DataParameter, int_NrOf_InScope_Rows, int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr

Dim DB_CONNECT_STR_MTS 
DB_CONNECT_STR_MTS = "Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & Environment.Value("HOSTNAME") & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & Environment.Value("SERVICENAME") & "))); User ID=" & Environment.Value("USERNAME") & ";Password=" & Environment.Value("PASSWORD") & ";"

' Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL")&"SORD\"

Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize 
Dim strQueryTemplate, dtD, Hyph, strNMI

Dim strList_NMIsizes, strAr_NMIsizes, nmiSizes
strList_NMIsizes = ",SMALL,LARGE"
strAr_NMIsizes = split(strList_NMIsizes, ",")

Dim oldnew, oldnewMax, n
oldnewMax = intSize_listOldNew

Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI

Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0

Dim intSQL_Pkg_ColNr, strSQL_RunPackage


'Umesh -  Dim intRowNr_DPs_SmallLarge : intRowNr_DPs_SmallLarge = objWS_DataParameter.range("rgWS_RowNr_DPs_SmallLarge").row

Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn


intRowNr_LoopWS_RowNr_StartOn = objWS_SAN.Range("rgWS_DataRow_First_MinusOne").Row + 2
'intRowNr_LoopWS_RowNr_StartOn = 1003

intRowNr_LoopWS_RowNr_FinishOn = objWS_SAN.range("rgWS_DataRow_Last_PlusOne").row - 1
'intRowNr_LoopWS_RowNr_FinishOn = 1003


' intRowNr_LoopWS_RowNr_FinishOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "uanand", 1231, intRowNr_LoopWS_RowNr_FinishOn)

'intRowNr_LoopWS_RowNr_FinishOn = 22

					'''''tsAr_EventEndedAtTS(iTS_Stage_0) = now()

' =========  
' =========  Stage I   - Execute SQL and gather data =============
' =========  

   
'   qq load the dictionary from the rgWS_ColumName_Row
    intColNr_RowType = gdictws_SAN_KeyName_ColNr.Item("RowType")
' qq - FrameWork.Rule - retain this single evaluation here so that work can be done concurrently by different test-engineers
 
 
    
    Dim vntAr_RowTypes, vntAR_ColumnTypes
    
'   Create new DataPermutation rows from the DataTemplate rows
    
    Dim intArSz_Role, intArSz_State, intArSz_NorC, intArSz_sizeNMI
    Dim iRole, iState, iNorC, isizeNMI
    Dim colNr_Role, colNr_State, colNr_NorC, colNr_sizeNMI
    Dim strAr_acrossA_Role, strAr_downA_State, strAr_downB_NorC, strAr_downC_sizeNMI
    Dim strRow_DataPermutation_Template, strRow_DataPermutation: strRow_DataPermutation_Template = "tr<TemplateRow>rl<RoleNr>st<State>nc<NorC>nmi<NMI>"
    Dim objCell_NewRowCell
    Dim strRole_NorC_Negative, cStr_OmitThisCombination: cStr_OmitThisCombination = "-"
    Dim intNrOfNewRows: intNrOfNewRows = 0
    Dim intDisplayChanges_Interval: intDisplayChanges_Interval = 0
    Dim intColNr_SQL_DataParameter, strMasterWS_ColumnName
    Dim intColNr_XML_DataTemplate_FileNameQQ
    Dim strSQL_TemplateName_Current, strSQL_TemplateName_Previous, strScratch, intScratch, cellScratch
    Dim strXML_TemplateName_Current, strXML_TemplateName_Previous
    Dim intColNr_NMI, intColNr_TestResult
    Dim strSuffix_Expected, strSuffix_Actual, strSuffix_NMI
    Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true
    Dim qintColNr, qintColsCount, strSqlCols_NamesList, strSqlCols_ValuesList, strSql_Suffix, qintFieldFirst_Nr : qintFieldFirst_Nr = 0
    qintColNr = 0
    Dim strAr_SqlCol_NamesAr, strAr_SqlCol_ValuesAr, dictSqlData_forThisRow_ColValues
    Dim colNr_SqlCol_Names, colNr_SqlCol_Values
    Dim strInScope_SmallLarge_Ar, intCt_AdditionalCriteria_All, cellNMI, cellExpectedValue, cellActualValue
    Dim strArr_SQL_RowData_columnNames, strArr_SQL_RowData_columnValues
    
    
    'strSuffix_Expected = objWS_DataParameter.range("rgWS_Suffix_Actual").value
    'strSuffix_Actual   = objWS_DataParameter.range("rgWS_Suffix_Expected").value
    
    ' strSuffix_NMI      = objWS_DataParameter.range("rgWS_nameNMI").Value


Dim dictWSDataParameter_KeyColName_ItemColValue
Set dictWSDataParameter_KeyColName_ItemColValue = CreateObject("Scripting.Dictionary")
dictWSDataParameter_KeyColName_ItemColValue.CompareMode = vbTextCompare

Dim StrXMLTemplateLocation, Str_TransactionID, Date_TransactionDate, Str_Role, Str_RoleStatus, Str_Parcipant, Str_ChangeReasonCode
Dim date_ChgDt_Type_A_P, Int_NMI, Str_list_tState, Str_list_tNMI, Int_DayOffset, strJurisdiction
Dim str_DataParameter_PopulatedXML_FqFileName, int_UniqueReferenceNumberforXML

' load the SQL{ColNames and ColValues) into splitAr's
Dim strAr_ColumnNames, int_ArSize_ColNames, strAr_ColumnValue, int_ArSize_ColValues, intArSQL_elNr
Dim vntSqlCell, strSqlCellContents

' Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage

gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
' Get headers of table in a dictionary
Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare


int_MaxColumns = ubound(gvntAr_RuleTable,2)

' Load the first/title row in a dictionary
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_RuleTable, 1, int_MaxColumns, gdictWStemplateXML_TagDataSrc_Tables_ColValue
					

Dim strScratch_MatchStr

'BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from framework
BASE_XML_Template_DIR = cBASE_XML_Template_DIR

Dim strCaseGroup, strCaseNr, strCsList1_TagsNeedingFlexibleManagement_Current, strCsList1_TagsNeedingFlexibleManagement_Previous
Dim intSeconds 

Dim str_MTS_Objection_Reason_Code
Dim intColNr_CatsCrCode_Actual 


' Close any open instance of the application
fnMTS_WinClose ' qq cleanup RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration

' Open MTS aplication
fnMTS_Open_wEnvVars ' qq remove following :   RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration

	' We need Environment.Value("COMPANY")
	' We need Environment.Value("ENV")
	' strSource = "C:\_data\GUILoader\MTS\" ' this source folder needs to be there
	' "C:\_data\GUILoader\MTS\" needs to have ini files
	
' To call the function please use the next line:
' str_MTS_Objection_Reason_Code =  fnCheckCats_MTS(strRequestId, strNMI, "", "", "")


					
Dim temp_InScopeYN, temp_Navigation_Path, temp_fnVerify_ScreenName, temp_from_Role, temp_to_Role, temp_Meter_Type, temp_NMI_SIZE
Dim temp_Object_Type, temp_NMI, temp_Object_Name, temp_Scenario_Name
Dim temp_Object_Key_Value, temp_PassORFail, temp_SQL_Query, temp_XML_Template
Dim temp_csListSqlColumn_Names, temp_csListSqlColumn_Values, temp_Investigation_Code, temp_Parent_Scneario_ID
Dim temp_AQ_Error_Reason, temp_AQ_Status
Dim temp_AccessDetail, temp_Hazard_Description
Dim int_ctr_currentRow, temp_TRANSACTION_GRP
Dim dt_from, dt_to, int_datagridcount
Dim int_temp_exit_row
Dim str_WhetherFailure , str_Scratch_Error_Comment

Dim strFolderNameWithSlashSuffix_Scratch
Dim ack_objFSO, objFile_ack

Dim intTotalWait


strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"


' STAGE I - harvest data and generate XML

	For int_ctr_currentRow = intRowNr_LoopWS_RowNr_StartOn To intRowNr_LoopWS_RowNr_FinishOn
	
		' Fetch data for row
		temp_InScopeYN = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("InScope_YN")).value
		
		int_temp_exit_row = "n"
		str_WhetherFailure = "n"
		str_Scratch_Error_Comment = ""
		intTotalWait = 0
		
		strSqlCols_NamesList  = ""
		strSqlCols_ValuesList = ""
		strSql_Suffix = ","
						
		  ' strSqlCols_NamesList = "" : strSql_Suffix = "," ' we'll make the list 1-based
		  ' strSqlCols_ValuesList = "" : strSqlCols_ValuesList = "," ' we'll make the list 1-based
	
		If trim(lcase(temp_InScopeYN)) = "y" Then
		
			temp_Scenario_Name = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Unique_Scenario_ID")).value
			temp_ExitTestIteration_NY = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("ExitTestIteration_NY")).value
			temp_ChangeSet_or_WorkItem = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("ChangeSet_or_WorkItem")).value
			temp_RowType = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("RowType")).value
			temp_DayOffset= objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("DayOffset")).value
			temp_SQL_Query = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("SQL_Template_Name")).value
			temp_XML_Template = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("xmlTemplate_Name")).value
			temp_NMI_SIZE = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("NMI_SIZE")).value
			temp_Meter_Type= objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Meter_Type")).value
			temp_row_MasterConfig = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("row_MasterConfig")).value
			temp_SQL_Populated = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("SQL_Populated")).value
			temp_csListSqlColumn_Names = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("csListSqlColumn_Names")).value
			temp_csListSqlColumn_Values = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("csListSqlColumn_Values")).value
			temp_NMI = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("NMI")).value
			temp_XML_TAG_Evaluation_Key = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("XML_TAG_Evaluation_Key")).value
			temp_from_Role = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("From_Role")).value
			temp_to_Role = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("To_Role")).value
			temp_Notification_Status = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Notification_Status")).value
			temp_AQ_Error_Reason = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("AQ Name")).value
			temp_AQ_Status = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("AQ Status")).value
			temp_AccessDetail = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("AccessDetail")).value
			temp_Hazard_Description = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Hazard_Description")).value
			temp_Parent_Scneario_ID = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Parent_Scenario_ID")).value
			
			Wait(7)
			' Execute the SQL and fetch data
			
			'Updated strQueryTemplate location with Kasun's fnRetrieve_SQL function - Neeraja
			
			If temp_SQL_Query <> ""  Then		
				' Capture MTS SQL
				' intSQL_ColNr           				= dictWSTestRunContext_KeyName_ColNr( temp_SQL_Query )  '"MTS_FIND_NMI_withNo_METER"
										 				 	
			' Read query - Select Active_YN from B2B_MD_INVESTIGATION_CODE where INVESTIGATION_CD in  ('Require Latest Version' ,'Recipient Not Responsible For The NMI')
			'intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr(temp_SQL_Query)  '   "MTS_FIND_NMI_withNo_METER"
			 'strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 	
			   strQueryTemplate = fnRetrieve_SQL(temp_SQL_Query)
			   		       	
				      If LCASE(strQueryTemplate) <> "fail" Then
		 
			              	temp_SQL_Query = strQueryTemplate
			
						
						' Update query to replace with runtime parameters
						
						
						On error resume next
							strQueryTemplate = replace ( strQueryTemplate , "<NMI_Size>", temp_NMI_SIZE, 1, -1, vbTextCompare)
							strQueryTemplate = replace ( strQueryTemplate , "<Meter_Type>", temp_Meter_Type, 1, -1, vbTextCompare)
							strQueryTemplate = replace ( strQueryTemplate , "<To_Role>", temp_to_Role, 1, -1, vbTextCompare)
							strQueryTemplate = replace ( strQueryTemplate , "<From_Role>", temp_from_Role, 1, -1, vbTextCompare)
							strQueryTemplate = replace ( strQueryTemplate , "<AccessDetail>", temp_AccessDetail, 1, -1, vbTextCompare)
							strQueryTemplate = replace ( strQueryTemplate , "<Hazard_Description>", temp_Hazard_Description, 1, -1, vbTextCompare)
							err.clear
						On error goto 0
			
			            	If temp_Parent_Scneario_ID <> "" Then
			            		strQueryTemplate = replace ( strQueryTemplate , "<Parent_Scenario_ID>", temp_Parent_Scneario_ID, 1, -1, vbTextCompare)
			            	End If
			
						
						' Write finally formed SQL in XL
						objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("SQL_Populated")).formula = "'" & strQueryTemplate
						objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("SQL_Populated")).WrapText = False
						
						' Execute query
						fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strQueryTemplate, "10"
					
							
					End If
												
			End If ' end of If LCASE(strQueryTemplate) <> "fail" Then

		
			
			' traverse through recordset and check for Y
			If objDBRcdSet_TestData_A.State  > 0 Then
				
					If objDBRcdSet_TestData_A.RecordCount > 0 Then
						objDBRcdSet_TestData_A.MoveFirst
						
						'while not objDBRcdSet_TestData_A.EOF 
							objDBRcdSet_TestData_A.MoveFirst
							For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count -1 
								strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
								strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
							Next	
		
							' Reserve NMI
							Int_NMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
							fnKDR_Insert_Data_V1 "n", "n",Int_NMI, temp_Scenario_Name, "candidate"
		
							objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("csListSqlColumn_Names")).formula = "'" & strSqlCols_NamesList
							objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("csListSqlColumn_Names")).WrapText = False
							objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("csListSqlColumn_Values")).formula = "'" & strSqlCols_ValuesList
							objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("csListSqlColumn_Values")).WrapText = False
					Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
					End If
			Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
			End If			
			
			If str_WhetherFailure = "y" Then
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value = "FAIL"
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Comments")).value = str_Scratch_Error_Comment
			Else
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value = ""
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Comments")).value = "Data-Harvest-Complete"
			End If

		End If
	
	err.clear
	
		str_WhetherFailure = "n"
		
		If  trim(lcase(temp_InScopeYN)) = "y" AND objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value <> "FAIL" Then		
			
			' Create an XML
			int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
			' "CCYYMMDDHHmmss99hhmmss"
			' Str_TransactionID =  int_UniqueReferenceNumberforXML ' "txnID_" &
			
			' Some queries returns
			Int_NMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
			
			If Int_NMI = ""  Then
				str_WhetherFailure = "y"
				str_Scratch_Error_Comment = "Unable to fetch NMI. May be some problem during datamine. Please check the query"
			Else 
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("NMI")).value = Int_NMI
				
				
				str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_row_MasterConfig,"SAHN" , BASE_XML_Template_DIR,temp_XML_Template, strFolderNameWithSlashSuffix_Scratch ,  objWS_SAN, objWS_SAN,  gdictws_SAN_KeyName_ColNr,  int_ctr_currentRow, gvntAr_RuleTable, gdictWStemplateXML_TagDataSrc_Tables_ColValue, now, int_UniqueReferenceNumberforXML, temp_to_Role, Int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
				
				If str_DataParameter_PopulatedXML_FqFileName = "" Then
					str_WhetherFailure = "y"
					str_Scratch_Error_Comment = "Problem generating XML file. Error ("& gFWstr_ExitIteration_Error_Reason &")"
				Else
					
					strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
					
					'					' Drop the XML in 
					'					GetCATSDestinationFolder_V1 Environment.Value("COMPANY"),Environment.Value("ENV"),"MDP", gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
					'								
					'---------------------------- START OF - Code to copy XML's to run specific folder -------------------------------
					
					' copy the PopulatedXML file from the temp folder to the input folder
					' copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
					
					' copy the PopulatedXML file from the temp folder to the results folder
					copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, gFWstr_RunFolder  ' Environment.Value("CATSDestinationFolder")
					
					' ZIP XML File
					ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
					
		
					GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_to_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
					
					If Environment.Value("CATSDestinationFolder") = "<notfound>" Then
						str_WhetherFailure = "y"
						str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Cannot find outbox folder for Company("& Environment.Value("COMPANY") & ")-Environment("& Environment.Value("ENV") & ")-Application(B2B)-Role(" & temp_to_Role & "). Exiting row..."
					Else
						copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
						objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder") 
					End If
					
				End If				
								
				
			End If

			If str_WhetherFailure = "y" Then
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value = "FAIL"
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Comments")).value = str_Scratch_Error_Comment
			Else
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value = ""
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Comments")).value = "XML-Creation-Complete"
			End If

		End If
		
	Next
	
	Set objDBConn_TestData_A    =Nothing
	set objDBRcdSet_TestData_A  =Nothing
	objWB_Master.save

	For int_ctr_currentRow = intRowNr_LoopWS_RowNr_StartOn To intRowNr_LoopWS_RowNr_FinishOn
	
		' Fetch data for row
		temp_InScopeYN = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("InScope_YN")).value
		
		int_temp_exit_row = "n"
		str_WhetherFailure = "n"
		str_Scratch_Error_Comment = ""
		intTotalWait = 0
		
		'strSqlCols_NamesList  = ""
		'strSqlCols_ValuesList = ""
						
		 ' strSqlCols_NamesList = "" : strSql_Suffix = "," ' we'll make the list 1-based
		 ' strSqlCols_ValuesList = "" : strSqlCols_ValuesList = "," ' we'll make the list 1-based
	
		If  trim(lcase(temp_InScopeYN)) = "y" AND objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value <> "FAIL" Then		
		
			temp_Scenario_Name = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Unique_Scenario_ID")).value
			temp_ExitTestIteration_NY = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("ExitTestIteration_NY")).value
			temp_ChangeSet_or_WorkItem = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("ChangeSet_or_WorkItem")).value
			temp_RowType = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("RowType")).value
			temp_DayOffset= objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("DayOffset")).value
			temp_SQL_Query = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("SQL_Template_Name")).value
			temp_XML_Template = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("xmlTemplate_Name")).value
			temp_NMI_SIZE = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("NMI_SIZE")).value
			temp_Meter_Type= objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Meter_Type")).value
			temp_row_MasterConfig = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("row_MasterConfig")).value
			temp_SQL_Populated = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("SQL_Populated")).value
			temp_csListSqlColumn_Names = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("csListSqlColumn_Names")).value
			temp_csListSqlColumn_Values = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("csListSqlColumn_Values")).value
			temp_NMI = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("NMI")).value
			temp_XML_TAG_Evaluation_Key = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("XML_TAG_Evaluation_Key")).value
			temp_from_Role = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("From_Role")).value
			temp_to_Role = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("To_Role")).value
			temp_Notification_Status = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Notification_Status")).value
			temp_AQ_Error_Reason = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("AQ Name")).value
			temp_AQ_Status = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("AQ Status")).value
			temp_AccessDetail = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("AccessDetail")).value
			temp_Hazard_Description = objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Hazard_Description")).value			
			' MTS screen verification
			
					
			fn_MTS_MenuNavigation "transactions;customer / site details;search site access notifications received" 
			
			fn_MTS_Search_Site_Access_Notifications_Received temp_NMI, fnTimeStamp((now), "DD/MM/YYYY"), fnTimeStamp((now), "DD/MM/YYYY"),"","","","","y"
			
			fn_MTS_Search_SAN_Received_Select_Record_Results_Verification "last","","","","",temp_Notification_Status,"","","","y"
			
			If lcase(trim(gFWbln_ExitIteration)) = "y" Then
					gFWbln_ExitIteration = "Y"
					str_WhetherFailure = "y"
					str_Scratch_Error_Comment = str_Scratch_Error_Comment  & gFWstr_ExitIteration_Error_Reason
			End If
			
			If str_WhetherFailure = "y" Then
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value = "FAIL"
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Comments")).value = str_Scratch_Error_Comment
				
			Else
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value = "PASS"
				objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Comments")).value = "Notification Status Match"
				
				If temp_AQ_Error_Reason <> "" Then
					
						PbWindow("MTS").PbWindow("Search_Site_Access_Notifications_Received").Activate
						PbWindow("MTS").PbWindow("Search_Site_Access_Notifications_Received").PbButton("btn_Details").Click
						wait(5)
						PbWindow("MTS").PbWindow("Site_Access_Notif_Received").PbButton("btn_AQ Details").Click
					
						fn_MTS_SAN_Received_Verification "",temp_AQ_Error_Reason,"","","","","","","","","","",temp_AQ_Status,"","","","","y"
					
						If lcase(trim(gFWbln_ExitIteration)) = "y" Then
								gFWbln_ExitIteration = "Y"
								str_WhetherFailure = "y"
								str_Scratch_Error_Comment = str_Scratch_Error_Comment  & gFWstr_ExitIteration_Error_Reason
						End If
						
						If str_WhetherFailure = "y" Then
							objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value = "FAIL"
							objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Comments")).value = str_Scratch_Error_Comment
						Else
							objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("PassORFail")).value = "PASS"
							objWS_SAN.Cells(int_ctr_currentRow, gdictws_SAN_KeyName_ColNr("Comments")).value = "AQ Match"
						End If
						
						PbWindow("MTS").PbWindow("Site_Access_Notif_Received").Activate
						PbWindow("MTS").PbWindow("Site_Access_Notif_Received").PbButton("btn_Close").Click
					
				End If
				
				PbWindow("MTS").PbWindow("Search_Site_Access_Notifications_Received").Activate	
				PbWindow("MTS").PbWindow("Search_Site_Access_Notifications_Received").PbButton("btn_Close").Click	
				
			End If
			
			on error resume next ' The script is ot able to handle the below 2 lines of code as the window is already closed. Handing it temporatrioly to let execution go through
				PbWindow("MTS").PbWindow("Search_Site_Access_Notifications_Received").Activate	
				PbWindow("MTS").PbWindow("Search_Site_Access_Notifications_Received").PbButton("btn_Close").Click	
				error.clear
			On error goto 0
				
		End If
		
			temp_Scenario_Name= ""
			temp_ExitTestIteration_NY =  ""
			temp_SQL_Query = ""
			temp_XML_Templat = ""
			temp_NMI_SIZE = ""
			temp_Meter_Type = ""
			temp_row_MasterConfig = ""
			temp_SQL_Populated = ""
			temp_csListSqlColumn_Names = ""
			temp_csListSqlColumn_Values = ""
			temp_NMI = ""
			temp_XML_TAG_Evaluation_Key  = ""
			temp_from_Role  = ""
			temp_to_Role = ""
			temp_Notification_Statu = ""
			temp_AQ_Error_Reason = ""
			temp_AQ_Status = ""
			str_WhetherFailure = "n"
			gFWbln_ExitIteration = ""
			str_Scratch_Error_Comment = ""
			objWB_Master.save
			
	Next

fnMTS_WinClose

	objWB_Master.save
'	objWB_Master.close

'objDBRcdSet_TestData_A.Close
'objDBConn_TestData_A.Close

Set objDBRcdSet_TestData_A = nothing
set objDBConn_TestData_A = nothing

Set cellTopLeft = nothing

Set qtResultsOpt = Nothing ' Release the Run Results Options object
set qtApp   = nothing
Set qtTest = Nothing ' Release the Test object 


' Once the test is complete, move the results to network drive
copyfolder  gFWstr_RunFolder , Cstr_SAN_Final_Result_Location
		
ExitAction @@ hightlight id_;_1770748_;_script infofile_;_ZIP::ssf270.xml_;_