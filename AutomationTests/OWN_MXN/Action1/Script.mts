﻿

Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

Dim intScratch


gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

' Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScope

intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true

'Neeraja -Need to define below as per the scenario

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI 
Dim strBarText, strBarText_Save, strMsg
Dim cStr_BarText_Format
Dim temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason , temp_DataPrep_CIS_TaskType, str_Required_SP_Status_MTS, temp_NMI_List
Dim BASE_XML_Template_DIR

' BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from GIT
BASE_XML_Template_DIR = cBASE_XML_Template_DIR


cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^RequestID`{4}^NMI`{5} ."

Dim strFolderNameWithSlashSuffix_Scratch
strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"


' push the RunStats to the DriverWS report-area


Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR 
BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"

Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir  = BASE_GIT_DIR



'###############################################################################  UFT configurations at the start of test  - START  ####################################################################################################################


Dim Temp_Execution_Results_Location  
    Temp_Execution_Results_Location = "C:\_data\Test_Execution_Results\"        'Share drive for AMI Energisation End to End project
	
	Dim strRunLog_Folder
	
	dim qtTest, qtResultsOpt, qtApp
	
	
	Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
	If qtApp.launched <> True then
	    qtApp.Launch
	End If
	
	qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	qtApp.Options.Run.RunMode = "Fast"
	qtApp.Options.Run.ViewResults = False
	
	
	Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
	Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539


	Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
	str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
	If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
		str_AnFw_FocusArea = ""
	else
		str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
	End If
	    
	  
	   
	    
strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", array(fnAttachStr(cTemp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
	    gQtTest.Name                                 , _
	    Parameter.Item("Environment")                       , _
	    Parameter.Item("Company")                           , _
	    fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
	    ' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))



'strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\"					, _
'		fnAttachStr(cTemp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)	, _
'		qtTest.Name																	, _
'		Parameter.Item("Environment")												, _
'		Parameter.Item("Company") 													, _
'		fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`ddDdD_HH`mm")	).toString
'
        
    
	Path_MultiLevel_CreateComplete strRunLog_Folder
	gFWstr_RunFolder = strRunLog_Folder
	
	qtResultsOpt.ResultsLocation = gFWstr_RunFolder
	gQtResultsOpt.ResultsLocation = strRunLog_Folder
	

	qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
	qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
	qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
	qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
	qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
	qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
	qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
	qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
	qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
	qtApp.Options.Run.ScreenRecorder.RecordSound = False
	qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True
	
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	
	gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
	gQtApp.Options.Run.ViewResults                = False

	strFolderNameWithSlashSuffix_Scratch = strRunLog_Folder


' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary

'Set gdict_scratch = CreateObject("Scripting.Dictionary")
'gdict_scratch.CompareMode = vbTextCompare
'
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  



 
	'  Description:-  Function to replace arguments in a string format
	'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 
	'						cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."
	'						The template should have arguments within {} and the numbering should s
	
	

	If 1 = 1  Then		
					
					
					Environment.Value("COMPANY")=Parameter.Item("Company")
					Environment.Value("ENV")=Parameter.Item("Environment")
					    
					   
				  
					' Load the MasterWorkBook objWB_Master
					Dim strWB_noFolderContext_onlyFileName  
					strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
					
					Dim wbScratch
					Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
					dim  dictWSDataParameter_KeyName_ColNr
					
					fnExcel_CreateAppInstance  objXLapp, true  
					
					'Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO"
					
					'call function LoadData_RunContext_V2  below
					
					LoadData_RunContext_V2  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
					
					
					
					Dim objWB_Master
					Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
					Set wbScratch = nothing
								
					objXLapp.Calculation = xlManual
					'objXLapp.screenUpdating=False
					
					Dim objWS_DataParameter 						 
					set objWS_DataParameter           					= objWB_Master.worksheets("OWN")  ' qq this must become a parm
					objWS_DataParameter.activate
					
					Dim objWS_templateXML_TagDataSrc_Tables 	
					set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
					set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
					
					Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
					' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
					gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

				'	Get headers of table in a dictionary
					Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
					gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
					
					
					gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
					Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
					gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
					' Load the first/title row in a dictionary
					int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
					fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
					int_MaxColumns = UBound(gvntAr_RuleTable,2)

					objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
					objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
					objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
					objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
					' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = now 

					gDt_Run_TS = now
					gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 

					Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE
					
					'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
					Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
					dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
					Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
					intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
					intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
					intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
					fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
					
					rowTL = 1 : colTL = 1
					
					
                
	
					' =============================================================================================================
'=================================  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   ==========================================
					' =============================================================================================================
					
					Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
					
					dim r
					Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
					
'					On error resume next
				'	add more of these as-required, for querying databases
					Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
		'	add more of these as-required, for querying databases
		
		
					Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
					Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset")



			Dim	DB_CONNECT_STR_MTS
					DB_CONNECT_STR_MTS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value("USERNAME") 	& 	";Password=" & _
						Environment.Value("PASSWORD") 	& ";"

			Dim	strEnvCoy
					strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
					
					
	'		Dim	DB_CONNECT_STR_CIS
		'			DB_CONNECT_STR_CIS = _
	'						"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
		'				Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
		'				Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
		'				Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
		'				Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"

					' dim strTestName, uftapp : Set uftapp = CreateObject("QuickTest.Application")
					' strTestName = uftapp.Test.Name
					' Set uftapp = nothing
					
'					Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
					
					Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
					Dim strQueryTemplate, dtD, Hyph, strNMI
					
					Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
					Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
					intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
'
					Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
					
					Dim intSQL_Pkg_ColNr, strSQL_RunPackage
					
					
					Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
					
					
						intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
						intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
						intColNR_ScenarioID 			= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
					
								
							'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
						intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
						 sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
					
						Dim strColName
						objWS_DataParameter.calculate	'	to ensure the filename is updated
									
									
					'Dim  str_xlBitNess : str_xlBitNess = objWS_DataParameter.range("rgWS_XL_BitNess_x32x64").value	
					
					Dim  str_xlBitNess : str_xlBitNess = ""
									
					sb_File_WriteContents  _
						gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
					
					
					Dim intColNr_ScenarioStatus, intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
						   intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
						  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")			
					
											
						Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
						If IsEmpty(intColNr_InScope) then	
							blnColumnSet =false
						ElseIf intColNr_InScope = -1 then
							blnColumnSet =false
						End if
						If not blnColumnSet then 
					'		qq log this	
					'		objXLapp.Calculation = xlManual
							objXLapp.screenUpdating=True
							exittest
						End If
					
						objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
						objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
						objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
						objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
						objWS_DataParameter.calculate
						objxlapp.screenupdating = true
					
						' objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
						
				
				
					      ' ==========================================================================
'=======================================  FrameworkPhase 0c   - Setup the MultiStage Array ===========================================================
					      ' ==========================================================================
					
					Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
					Dim  intMasterZoomLevel 
					
					Dim rowStart, rowEnd, colStart, colEnd
					Dim RangeAr 
					
					
					'    ===>>>   Enable HotKeys
					' NOT NEEDED objWB_Master.Application.Run "HotKeys_Set"
					
					'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
					' NOT NEEDED objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
				
	 End If


objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = Parameter("InScope_Column_Name")
					
dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq


sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

	

'	determine the number of in-scope rows
Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = now()


'	qq     intNrOfRows_Inscope_and_Unfinished

sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , false, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf



dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow

'	now setup to do process the DriverSheet


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_CurrentScenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = now()-7
cInt_MinutesRequired_toComplete_eachScenario = cInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = cdbl(1.2)

	                
Dim str_ScenarioRow_FlowName 
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false
Dim temp_XML_Template
Dim intColNr_Request_ID 
Dim intColNr_RowCompletionStatus 
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
intColNr_ScenarioStatus 		= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")

Dim strRole, strInitiator_Role, var_ScheduledDate
Dim strInScope, strRowCompletionStatus

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=true
objWB_Master.save


	
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
		' Get headers of table in a dictionary
	Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	

	
		' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue
	



' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
' Launch MTS
fnMTS_WinClose
OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")
'
' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID
Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role
Dim temp_strSQL_MTS ' This is to store SQL template incase of multiple row XML (having different NMI's)

int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0

i = 1

' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PREP - Update company related information in driver sheet %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Dim strAr_acrossA_Role, strAr_Stages
Dim intArSz_Role, intArSz_Stages, strPrefix_RoleIsRelevant
Dim strCaseGroup, strCaseNr, strXML_TemplateName_Current, Date_TransactionDate
Dim str_DataParameter_PopulatedXML_FqFileName, str_MTS_Txn_Status
Dim strVerification_String, temp_range_rg_VerificationString_Suffix, varAr_Verification_String



' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Do
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
									'	intRowNr_LoopWS_RowNr_StartOn   qq
	
		'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
		strInScope 							= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
		
		
		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
			If   ucase(strInScope) = ucase(cY)  and str_ScenarioRow_FlowName <> "" and lcase(str_Next_Function) <> "fail" and lcase(str_Next_Function) <> "end" Then
			
				If i = 1 Then
					If ucase(Parameter.Item("RunMode")) = "N" Then ' Value can be N- New or R - restart
						objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start-SQL"
						str_Next_Function = "start-SQL"
					End If
					fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
					int_ctr_total_Eligible_rows = int_ctr_no_of_Eligible_rows
				End If
			
				' Generate a filename for screenshot file
				gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, left(temp_Scenario_ID,10)))
				
				
			Select Case lcase(str_ScenarioRow_FlowName)
				
				' ################################################################# START of first flow ####################################################################################################
				' ################################################################# START of first flow ####################################################################################################
				Case "flow1", "flow2":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
								Select Case str_Next_Function
									Case "start-SQL" ' this is datamine
										' First function
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											
												' Query would be executed once for every row. It would not be executed for ALL ROLES as there are no roles related replacements
												
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%

											PrintMessage "P", "MTS SQL Query", "Executing SQL ("& strSQL_MTS &")"
											
											' capture MTS query
											strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Template_Name")).value 

											If strSQL_MTS <> ""  Then
											
											        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											        
											        If lcase(strQueryTemplate) <> "fail" Then
											        	
														strSQL_MTS = strQueryTemplate
														
														' Perform replacements
										            	strSQL_MTS = replace ( strSQL_MTS , "<From_Role>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("From_Role")).value, 1, -1, vbTextCompare)
										            	strSQL_MTS = replace ( strSQL_MTS , "<To_Role>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("To_Role")).value, 1, -1, vbTextCompare)
                                                  										            


														' Execute MTS SQL
														strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
														
'													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NOTBEFOREDATE")).value = fnTimeStamp(now,"YYYY/MM/DD")
'														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NOTAFTERDATE")).value = fnTimeStamp(now+5,"YYYY/MM/DD")
'														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NOTICEDATE")).value = fnTimeStamp(now+10,"YYYY/MM/DD")

														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NOTBEFOREDATE")).value = "'" & fnTimeStamp(now+5,"YYYYMMDD")
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NOTAFTERDATE")).value = "'" & fnTimeStamp(now+10,"YYYYMMDD")
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NOTICEDATE")).value = "'" & fnTimeStamp(now,"YYYYMMDD")

														If strScratch = "PASS" Then
															' Write the data in all respective columns for all roles
															' Reserve NMI in test_data_key_register table in DB
															int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
															PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR "	
															fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
														End If
														
														
														' For a particular XML, we may need multiple data in the same XML . One row with multiple records with different NMIs
														' Execute MTS SQL
														strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CSVNotificationDetail_BodyeRecord2")).value
														
														If strScratch <> "<Intentional_Blank_required>" Then
														
															strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", "", "", "")
															
															If strScratch = "PASS" Then
																' Write the data in all respective columns for all roles
																strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value
																strScratch = strScratch & ",NMI2,METERNUMBER2" 
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = strScratch
																strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
																strScratch = strScratch & "," & objDBRcdSet_TestData_B.Fields("NMI").value & "," & objDBRcdSet_TestData_B.Fields("METERNUMBER").value
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
																
																' Reserve NMI in test_data_key_register table in DB
																int_NMI = objDBRcdSet_TestData_B.Fields("NMI").value
																PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR "	
																fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
															End If ' end of If strScratch = "PASS" Then
															
														End If ' end of If strScratch <> "<Intentional_Blank_required>" Then
														
														strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CSVNotificationDetail_BodyeRecord3")).value
														
														If strScratch <> "<Intentional_Blank_required>" Then
														
															strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", "", "", "")
															
															If strScratch = "PASS" Then
																' Write the data in all respective columns for all roles
																strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value
																strScratch = strScratch & ",NMI3,METERNUMBER3" 
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = strScratch
																strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
																strScratch = strScratch & "," & objDBRcdSet_TestData_B.Fields("NMI").value & "," & objDBRcdSet_TestData_B.Fields("METERNUMBER").value
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
																
																' Reserve NMI in test_data_key_register table in DB
																int_NMI = objDBRcdSet_TestData_B.Fields("NMI").value
																PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR "	
																fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
															End If ' end of If strScratch = "PASS" Then
															
														End If ' end of If strScratch <> "<Intentional_Blank_required>" Then
														
														
													End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
																											'Else ' Commenting as the user may wish to use the same NMI in multiple rows
																												'fn_set_error_flags true, "Query for row (" & intRowNr_CurrentScenario & ") is missing"
											End If ' end of If strSQL_MTS <> ""  Then

											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												'Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												printmessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Creation"
												'Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												printmessage "p", "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
			            					
											strSQL_MTS = ""
											
										
									Case "XML_Creation"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										
										'Copied from SAR 
										
										If tsNow >= tsNext_ScenarioFunction_canStart_atOrAfter Then
			
											temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
											
									        If temp_XML_Template  <> "" Then
									            
									            If trim(lcase(objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Request_ID_YN")).value)) <> "n" Then
									            	int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution	
									            	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID") ).value = int_UniqueReferenceNumberforXML
									            Else
									            	int_UniqueReferenceNumberforXML = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value
									            End If
									            
									            
									            temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
									            temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("To_Role")).value
									            int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
									            str_ChangeStatusCode = "MXN"
									            
									            str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
									                                                        temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
									                                                        intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, now,  int_UniqueReferenceNumberforXML, _
									                                                        temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
									
									            strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
									            
									            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
									            	'Reporter.ReportEvent micFail,"Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
									            	printmessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
									            Else
									            
										            ' copy the PopulatedXML file from the temp folder to the input folder
										            ' copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
													
													' Store from tag in datasheet
													objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_Value_From_Role")).value = gstr_XML_TAG_FromSrcSQL 
										
										            ' copy the PopulatedXML file from the temp folder to the results folder
										            copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, gFWstr_RunFolder  ' Environment.Value("CATSDestinationFolder")
										            
										            ' ZIP XML File
										            ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
										                    
										            GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
										          

                                                    '####################################### 
										            
										            'Expected_Values in run sheet 
										            'strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "3",  "", "", "", "")
														
														'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Expected_Value")).value
														
														'strScratch = objDBRcdSet_TestData_B.Fields("NMI").value & "," & objDBRcdSet_TestData_B.Fields("NMICHECKSUM").value & "," & objDBRcdSet_TestData_B.Fields("METERNUMBER").value & "," & objDBRcdSet_TestData_B.Fields("Transaction_Details").value & "," & objDBRcdSet_TestData_B.Fields("NOTEBEFIREDATE").value & "," & objDBRcdSet_TestData_B.Fields("NOTAFTERDATE").value & "," & objDBRcdSet_TestData_B.Fields("NOTICEDATE").value
																									
													'	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Expected_Value")).value = strScratch
														
													'####################################	
				
													If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
														'Reporter.ReportEvent micFail,"Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
														printmessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
														gFWbln_ExitIteration = "y"
													    gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													Else
														copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
														'Reporter.ReportEvent micPass, "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
														printmessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
														objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
													End If
									            End If
									            End IF
									            	
									            End If
									            
										

											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												'Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_OWN_DB_Validation"
												'Reporter.ReportEvent micPass, "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												printmessage "p", "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
			            					
									case "MTS_OWN_DB_Validation"
										
											' fn_compare_Current_and_expected_execution_time
											
											tsNow = now() 
											tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
											IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
											End if
			            					
			            					
			            					
			            					
			            					'+++++++++++++++++ Execute SQL to validate data in the DB .......'+++ Validating results in SQL as no MTS screen available. ++++++++++++ 
			            					
			            					'' capture MTS query
											strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DB_SQL_Verification_Template_Name")).value 
											
										
											If strSQL_MTS <> ""  Then
											
											        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											        
											        If lcase(strQueryTemplate) <> "fail" Then
											        	
														strSQL_MTS = strQueryTemplate
														temp_strSQL_MTS = strSQL_MTS 
												
'												msgbox fnTimeStamp(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTBEFOREDATE")).value,"YYYY/MM/DD")
'												msgbox fnTimeStamp(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTAFTERDATE")).value,"YYYY/MM/DD")
'												msgbox fnTimeStamp(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTICEDATE")).value,"YYYY/MM/DD")
'												
												
																		 
												
'														' 'Perform replacements
										            	
										            	'strSQL_MTS = replace( strSQL_MTS , "<Sql_Before_Dt>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTBEFOREDATE")).value, 1, -1, vbTextCompare)
										            		''strSQL_MTS = replace ( strSQL_MTS , "<Sql_Before_Dt>", (fnTimeStamp(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTBEFOREDATE")).value, "DD/MmM/YY")), 1, -1, vbTextCompare)
										              	'strSQL_MTS = replace ( strSQL_MTS , "<Sql_After_Dt>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTAFTERDATE")).value, 1, -1, vbTextCompare)
										            	'strSQL_MTS = replace ( strSQL_MTS , "<Sql_Notice_Dt>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTICEDATE")).value, 1, -1, vbTextCompare)
										            	strSQL_MTS = replace ( strSQL_MTS , "<SQL_NMI>", left(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value,10), 1, -1, vbTextCompare)
										            	' strSQL_MTS = replace ( strSQL_MTS , "<SQL_NMI_CK>", right(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value,1), 1, -1, vbTextCompare)
										            	strSQL_MTS = replace ( strSQL_MTS , "<Runtime_TxnID>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value, 1, -1, vbTextCompare)
										            	' strSQL_MTS = replace ( strSQL_MTS , "<SQL_METER_NO>", (fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "METERNUMBER")), 1, -1, vbTextCompare)
										            	'strSQL_MTS = replace ( strSQL_MTS , "<NOTICEDATE>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTICEDATE")).value, 1, -1, vbTextCompare)
								                       
										            	
														' 'Execute MTS SQL
														'strScratch = Execute_SQL_Populate_DataSheet ((objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
														fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,  "B2B_OWN_MXN NMI1", strSQL_MTS
														strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "3",  "", "", "", "")
													
                                                    
															
														'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Expected_Value")).value
														
														
														
														If objDBRcdSet_TestData_B.recordcount > 0 and lcase(strScratch) = "pass" Then
																																							
														temp_actual_value = left(objDBRcdSet_TestData_B.Fields("NMI").value,10) & "," & objDBRcdSet_TestData_B.Fields("METERSERIALNUMBER").value 
														temp_expected_value = left(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "NMI"),10) _
														& "," & fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "METERNUMBER")
														
															If ucase(trim(temp_actual_value)) =  ucase(trim(temp_expected_value)) Then
																PrintMessage "P", "MTS_OWN_DB_Validation pass", "Expected value ("&  temp_expected_value & ") is SAME as Actual value("&  temp_actual_value & ")"
															Else
																PrintMessage "f", "MTS_OWN_DB_Validation fail", "Expected value ("&  temp_expected_value & ") is DIFFERENT than Actual value("&  temp_actual_value & ")"
																											
														End If
														
														Else
														fn_set_error_flags true, "NMI1 value cannot be found in B2B_OWN_MXN table"
													End If ' end of If objDBRcdSet_TestData_B.recordcount > 0 Then
													
											
											End If '' end of  If lcase(strQueryTemplate) <> "fail" Then
																											
											End If '' end of If strSQL_MTS <> ""  Then
											
													
													'NMI2 Validation
													
											      strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CSVNotificationDetail_BodyeRecord2")).value
												
											If strScratch <> "<Intentional_Blank_required>" Then
																
													
													   If temp_strSQL_MTS <> ""  Then
									
									                    ' strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
									        
									                    ' If lcase(strQueryTemplate) <> "fail" Then
									        	
												        strSQL_MTS = temp_strSQL_MTS
												        'temp_strSQL_MTS = strSQL_MTS 
											
											
																					
																					
																					
											            strSQL_MTS = replace ( strSQL_MTS , "<SQL_NMI>", left(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "NMI2"),10), 1, -1, vbTextCompare)
											            'strSQL_MTS = replace ( strSQL_MTS , "<SQL_NMI_CK>", right(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "NMI2"),1), 1, -1, vbTextCompare)
											            strSQL_MTS = replace ( strSQL_MTS , "<Runtime_TxnID>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value, 1, -1, vbTextCompare)
								                       ' strSQL_MTS = replace ( strSQL_MTS , "<Runtime_TxnID>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value, 1, -1, vbTextCompare)
								                       'strSQL_MTS = replace ( strSQL_MTS , "<SQL_METER_NO>", (fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "METERNUMBER2")), 1, -1, vbTextCompare)
								                       ' strSQL_MTS = replace ( strSQL_MTS , "<NOTICEDATE>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NOTICEDATE")).value, 1, -1, vbTextCompare)
								            	
											     	  
											     	  ' 'Execute MTS SQL
														fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,  "B2B_OWN_MXN NMI2", strSQL_MTS
												      strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "3",  "", "", "", "")
											
																				
											
												        'strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "3",  "", "", "", "")
												
												        'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Expected_Value")).value
												
											         	If objDBRcdSet_TestData_B.recordcount > 0 and lcase(strScratch) = "pass" Then
													
												        
												       ' temp_actual_value = dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values").value
												        temp_actual_value = left(objDBRcdSet_TestData_B.Fields("NMI").value,10) & "," & objDBRcdSet_TestData_B.Fields("METERSERIALNUMBER").value
												       temp_expected_value = left(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "NMI2"),10) _ 
											         	& "," & fn_Fetch_SpecificArrayAttributeValue(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "METERNUMBER2")
												
											        	If ucase(trim(temp_actual_value)) =  ucase(trim(temp_expected_value)) Then
													   PrintMessage "P", "MTS_OWN_DB_Validation pass", "Expected value ("&  temp_expected_value & ") is SAME as Actual value("&  temp_actual_value & ")"
												
												      Else
												     	PrintMessage "f", "MTS_OWN_DB_Validation fail", "Expected value ("&  temp_expected_value & ") is DIFFERENT than Actual value("&  temp_actual_value & ")" 
												      End If
												      
												      Else
												      
														PrintMessage "f", "MTS_OWN_DB_Validation fail", "NMI 2("& left(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "NMI2"),10) &") cannot be found in DB"
												      
												      fn_set_error_flags true, "NMI2 value cannot be found in B2B_OWN_MXN Table"
												 
												      End If ' end of If objDBRcdSet_TestData_B.recordcount > 0 Then
												
									             ' End If  'lcase(strQueryTemplate) <> "fail" for NMI2
									       
									            End If  'end of If temp_strSQL_MTS <> ""  Then
									       
									       End if  'strScratch <> "<Intentional_Blank_required>" 	
	
																								
											'NMI3 Validation										
																										
											strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CSVNotificationDetail_BodyeRecord3")).value
														
											 If strScratch <> "<Intentional_Blank_required>" Then 															
																								
														  If temp_strSQL_MTS <> ""  Then
											
											             'strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											        
											              'If lcase(strQueryTemplate) <> "fail" Then
											        	
														strSQL_MTS = temp_strSQL_MTS
														'temp_strSQL_MTS = strSQL_MTS 	
'													

													    strSQL_MTS = replace ( strSQL_MTS , "<SQL_NMI>", left(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "NMI3"),10), 1, -1, vbTextCompare)
													    strSQL_MTS = replace ( strSQL_MTS , "<Runtime_TxnID>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value, 1, -1, vbTextCompare)
											            ' strSQL_MTS = replace ( strSQL_MTS , "<SQL_NMI_CK>", right(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "NMI3"),1), 1, -1, vbTextCompare)
								                       ' strSQL_MTS = replace ( strSQL_MTS , "<Runtime_TxnID>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID")).value, 1, -1, vbTextCompare)
								                       ' strSQL_MTS = replace ( strSQL_MTS , "<SQL_METER_NO>", (fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "METERNUMBER3")), 1, -1, vbTextCompare)
								            	
														
														'Execute MTS SQL
														fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,  "B2B_OWN_MXN NMI3", strSQL_MTS
														strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "3",  "", "", "", "")
'																													
'														
														If objDBRcdSet_TestData_B.recordcount > 0 and lcase(strScratch) = "pass" Then
															
														temp_actual_value = left(objDBRcdSet_TestData_B.Fields("NMI").value,10) & "," & objDBRcdSet_TestData_B.Fields("METERSERIALNUMBER").value 
														temp_expected_value = left(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "NMI3"),10) _
														& "," & fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "METERNUMBER3")
														
														If ucase(trim(temp_actual_value)) =  ucase(trim(temp_expected_value)) Then
														
															PrintMessage "P", "MTS_OWN_DB_Validation pass", "Expected value ("&  temp_expected_value & ") is SAME as Actual value("&  temp_actual_value & ")"
														Else
															PrintMessage "f", "MTS_OWN_DB_Validation fail", "Expected value ("&  temp_expected_value & ") is DIFFERENT than Actual value("&  temp_actual_value & ")"
														
														End If
														else
														
														PrintMessage "f", "MTS_OWN_DB_Validation fail", "NMI 3("& left(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "NMI3"),10) &") cannot be found in DB"
														fn_set_error_flags true, "NMI3 value cannot be found"
														
														End If ' end of If objDBRcdSet_TestData_B.recordcount > 0 Then
														
												 ' End If  'lcase(strQueryTemplate) <> "fail" for MI3
									       
									              End If  ''end of If temp_strSQL_MTS <> "" for NMI3
														
											End if  'strScratch <> "<Intentional_Blank_required>" 														
'											
														
														' & "," & objDBRcdSet_TestData_B.Fields("Transaction_Details").value & "," & objDBRcdSet_TestData_B.Fields("NOTEBEFIREDATE").value & "," & objDBRcdSet_TestData_B.Fields("NOTAFTERDATE").value & "," & objDBRcdSet_TestData_B.Fields("NOTICEDATE").value
																									
														'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Expected_Value")).value = strScratch
														
													'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$	
													
												
                                              
'											
											
											
																					
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data validation failed in DB because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												''Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												printmessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "END"
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DB Data validation complete"
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												''Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												'printmessage "p", "DB Data validation complete", "Data validate complete for row ("  & intRowNr_CurrentScenario &  ")" 
											'	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
											'	fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												

												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DB Data validation complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_OWN_Screen_Verification"
												'Reporter.ReportEvent micPass, "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												printmessage "p", "DB Data validation complete", "Data validate complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												
												
			            					End If
			            					
											strSQL_MTS = ""
											
											 'MTS_Screen_Verification - code written on 06/06/2017
									    case "MTS_OWN_Screen_Verification"
																					
												' fn_compare_Current_and_expected_execution_time
																						
												tsNow = now() 
												tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
												IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
												End if
												
												'int_NMI = left(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value,10)
												
												
											    ' fn_MTS_MenuNavigation "Transactions;One Way Notification;Meter Exchange Notification;Meter Exchange Notification Received Search"
											    fn_MTS_MenuNavigation  "Transactions;One Way Notification;Meter Exchange Notification;Search Meter Exchange Notification Received"
											
											    'To verify the fields in the panel
											    fn_MTS_Search_Meter_Exchange_Notifications_Received fnTimeStamp(now-7, "DD/MM/YYYY"), fnTimeStamp(now, "DD/MM/YYYY"), "", "", "y", "y", gFW_strRunLog_ScreenCapture_fqFileName
											
											
											
											   Dim temp_Notification_ID, temp_Notification_Received_Date, temp_No_of_NMIs, tmp_From_Participant, tmp_Notification_Status, tmp_Rejection_Code
											   
											   temp_Notification_ID = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_RequestID" )).value
											   ' temp_Notification_Received_Date = 'transaction_detail.gw_file_timestamp - PENDING
											   
		'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%									   
'								          ' temp_Notification_Received_Date validation --transaction_detail.gw_file_timestamp - PENDING
'								          
''											 
'                                           strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Notification_Received_Date")).value 
'											
'										
'											If strSQL_MTS <> ""  Then
'											
'											        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
'											        
'											        If lcase(strQueryTemplate) <> "fail" Then
'											        	
'														strSQL_MTS = strQueryTemplate
'														'temp_strSQL_MTS = strSQL_MTS 
'																							
'																									
'											strSQL_MTS = replace ( strSQL_MTS , "<SQL_REQ_ID>", "TxnID_" & objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID").value), 1, -1, vbTextCompare)
'                                              											
'                                             'strSQL_MTS = replace ( strSQL_MTS , "<SQL_REQ_ID>", left(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr("TxnID_" & Runtime_RequestID"),10).value
'
'                                               ' 'Execute MTS SQL
'												'strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "3",  "", "", "", "")
'
'                                                 'Execute MTS SQL
'												strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("Notification_Received_Date"))
'														
'                                               ' ++++concatenate names++++  
'                                               
''                                              strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value
'											   strScratch = strScratch & ",Notification_Received_Date" 
'											   objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = strScratch
'											   strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
'                                               strScratch = strScratch & "," & objDBRcdSet_TestData_B.Fields("Notification_Received_Date").value 
'											   objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
'											   
'											   End if 'case(strQueryTempla
'                                               End If  'strSQL_MTS <> 
'
'                                              temp_Notification_Received_Date =objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notification_Received_Date")).value
'
'                                               

                                               
      '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                      
											   
											   temp_No_of_NMIs = right(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CSVNotificationDetail_Footer")),1)
											   tmp_From_Participant = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_Value_From_Role"))
											   tmp_Notification_Status = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Notification_Status"))
											   tmp_Rejection_Code = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Code"))
											   tmp_Rejection_Description = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Description"))
											   
											
											    ' Verify on grid
												' fn_MTS_Search_Meter_Exchange_Notifications_Received_Details   "", "TxnID_" & temp_Notification_ID,  temp_Notification_Received_Date, temp_No_of_NMIs,  tmp_From_Participant,  tmp_Notification_Status,  tmp_Rejection_Code,  tmp_Rejection_Description,  "Y",  gFW_strRunLog_ScreenCapture_fqFileName
												fn_MTS_Search_Meter_Exchange_Notifications_Received_Details   "", "TxnID_" & temp_Notification_ID,  "", temp_No_of_NMIs,  tmp_From_Participant,  tmp_Notification_Status,  tmp_Rejection_Code,  tmp_Rejection_Description,  "Y",  gFW_strRunLog_ScreenCapture_fqFileName
											
												' Click Deatils
												'fn_MTS_Click_button_any_screen "Search_Meter_Exchange_Notifications_Received", "btn_Details"
												
												' Verify on screen
												
												Dim temp_transaction_id, temp_retailer, temp_date_sent, temp_status, temp_bus_acc_rej_dt, temp_bus_acc_rej_status, temp_bus_rej_event_cd, temp_bus_rej_explanation
												
												temp_transaction_id = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_RequestID" )).value
												temp_retailer = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_Value_From_Role"))
												'temp_retailer = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("From_Role"))
												'temp_date_sent = 
												temp_status = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Notification_Status"))
												'temp_bus_acc_rej_dt = 
												temp_bus_acc_rej_status = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Ack_File_Status"))
												temp_bus_rej_event_cd = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Rejection_Code"))
												temp_bus_rej_explanation = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Explanation"))  
																												
												
												'fn_MTS_Meter_Exchange_Notifications_Details "", "TxnID_" & temp_transaction_id,  temp_retailer, "temp_date_sent", temp_status,  "temp_bus_acc_rej_dt", temp_bus_acc_rej_status,  temp_bus_rej_event_cd,  temp_bus_rej_explanation, "Y",  gFW_strRunLog_ScreenCapture_fqFileName												
 												fn_MTS_Meter_Exchange_Notifications_Details "TxnID_" & temp_transaction_id,  temp_retailer, "", temp_status,  "", temp_bus_acc_rej_status,  temp_bus_rej_event_cd,  temp_bus_rej_explanation, "Y",  gFW_strRunLog_ScreenCapture_fqFileName
 												
												
												' Close Detail screen
												fn_MTS_Close_any_screen "Meter_Exchange_Notification_Received_Details"
												
											   'Close the MTS screen
											   fn_MTS_Close_any_screen "Search_Meter_Exchange_Notifications_Received"
											
											  temp_Notification_ID = ""
											   temp_Notification_Received_Date = ""
											   temp_No_of_NMIs = ""
											   tmp_From_Participant = ""
											   tmp_Notification_Status = ""
											   tmp_Rejection_Code = ""
											   tmp_Rejection_Description = ""
											
											   ' At the end, check if any error was there and write in corresponding column
											   
											   If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											   objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											   objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Check_status_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											   objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											   Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
											   objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											   int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											   Exit do
											  Else
											  objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
											  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
											  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_OWN_Screen_Verification complete"
											  'Reporter.ReportEvent micPass, "CIS_Check_SOstatus_isRaised complete", "CIS_Check_status_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
											  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
											' Function to write timestamp for next process
											  fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											  int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											  int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
											  Exit Do
											  End If											
																						            			
			            					' End if
											case "FAIL"
											Exit Do
										case else ' incase there is no function (which would NEVER happen) name
											Exit Do
								End Select
					Loop While  (lcase(gFWbln_ExitIteration) = "n" or gFWbln_ExitIteration = false)
					
 
 '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 

				' ################################################################# END of first flow ####################################################################################################


				Case else ' Incase there is no elaboration for a flow, we need to report in datasheet and move on to next row
				    gFWbln_ExitIteration = "Y"
				    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
					'Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
	    End Select ' end of 	Select Case str_ScenarioRow_FlowName
		
		End If ' end of If  (	( strInScope = cY)  and str_ScenarioRow_FlowName <> "" and (trim(str_Next_Function) = "" or lcase(str_Next_Function) <> "fail" or lcase(str_Next_Function) <> "end") ) Then

		' Reset error flags
		fn_reset_error_flags
		
		' first write the data of scratch dictionary in excel
		
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch

	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	

	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows

	' Reset error flags
	fn_reset_error_flags

	i = 2
	objWB_Master.save ' Save the workbook 
Loop While int_ctr_no_of_Eligible_rows > 0

printmessage "i", "Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
' objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= now

objWB_Master.save ' Save the workbook 

fnMTS_WinClose

' Once the test is complete, move the results to network drive
copyfolder  gFWstr_RunFolder , Cstr_OWN_Final_Result_Location


'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%End of First Flow%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  