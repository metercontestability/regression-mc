' v20b was for missing a) AC-verify log-entries  b) restore umesh's logging2

' v13  SIT 
Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

gfwint_DiagnosticLevel = 0 ' CHanging diagnostic level for debugging a particular row

gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScope
intColNr_InScope = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI 
Dim strBarText, strBarText_Save, strMsg
Dim cStr_BarText_Format
cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."


strScratch = fnTimeStamp ( now,  "yyyy`wkWW" )
If strScratch 	= "2017`wk10" Then				'										    ==============================================================================				
									Parameter.Item("Environment")	=	"DEVMC"
	Select Case gobjNet.ComputerName
		Case "CORPVMUFT01" 	:	Parameter.Item("Environment")	=	"SITMC"
		Case else	'	CORPVMUFT06	CORPVMUFT02 PCA19323
	End Select
End if


'			
'			
'			' ReDim gFwAr_strRequestID( cFWint_MaxNrOfProcesses_Required , cFWint_MaxNrOfRoles_Required ) ' no redim preserve required as the Ar has no values in it yet
'

strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciMetaData_Type_and_VersionNr	) = "is_FieldSetVerification_vn01" ' is_ is InformationSet	( 0)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldName						) = "<fn_UnInitialized>"								'	( 1)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldValue						) = "<fv_UnInitialized>"								'	( 2)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciAsOfTimestamp					) = "<aoTS_UnInitialized>"							'	( 3)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldType							) = "<ft_UnInitialized>"								'	( 4)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldVerificationPoint			) =  1													'	( 5)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldSource						) = "gMTS`<tbd_qq>"									'	( 6)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare01					) = "<mt>"												'	( 7)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare02					) = "<mt>"												'	( 8)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare03					) = "<mt>"												'	( 9)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare04					) = "<mt>"												'	(10)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_NameIndexList				) = _
	",ciMetaData_Type_and_VersionNr`0,ciFieldName`1,ciFieldValue`2,ciAsOfTimestamp`3,ciFieldType`4,ciFieldVerificationPoint`5,ciFieldSource`6" & _
	",ciField_spare01`7,ciField_spare02`8,ciField_spare03`9,ciField_spare04`10,ciField_NameIndexList`11,"						' 	(11)


'		On error resume next
'			strAr_Single_VerifyKey_Fields_local_Defaults	= scratchVariant
'		On error goto 0
strAr_Single_VerifyKey_Fields_local_Defaults	= strAr_Single_VerifyKey_Fields_testCase_Defaults	 
strAr_Single_VerifyKey_Fields 					= strAr_Single_VerifyKey_Fields_local_Defaults ' set the work-area strAr from the local defaults-strAr
strAr_Single_VerifyKey_Fields_logXML			= strAr_Single_VerifyKey_Fields_local_Defaults 


'fName = "Dani"
'lName = "Vainstein"
Set hFWobj_SB = DotNetFactory.CreateInstance( "System.Text.StringBuilder" )

Set gFWobj_SystemDate = DotNetFactory.CreateInstance( "System.DateTime" )


' push the RunStats to the DriverWS report-area
Dim cellTopLeft, iTS, iTS_Max, rowTL, colTL

Dim strRunLog_Folder

' -----

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq


' gQtApp, gQtTest, gQtResultsOpt    are all available from in fnLib_AutomationFwk
' gobjNet is available from fnLib_MsWindows to retrieve the computer and username, amongst other things
'dim qtResultsOpt'
'Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")  ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539

' gQtApp in instantiated in fnLib_AutomationFwk.qfl
'If gQtApp.launched <> True then
'  gQtApp.Launch
'End If

'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html

gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
gQtApp.Options.Run.ViewResults                = False


' gQtTest.Settings.Run.OnError = "NextStep" ' Instruct QuickTest to perform next step when error occurs
' gQtTest.Run qtResultsOpt,true ' Run the test
' MsgBox gQtTest.LastRunResults.Status
' gQtTest.Close ' Close the test


Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir = BASE_AUTOMATION_DIR
  'strCaseBaseAutomationDir = "r:\"


gDt_Run_TS = tsOfRun

 
	'  Description:-  Function to replace arguments in a string format
	'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 
	'						cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."
	'						The template should have arguments within {} and the numbering should s
	
	



Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
	str_AnFw_FocusArea = ""
else
	str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
End If


' StringBuilder function replaced with fn_StringFormatReplacement function
'strRunLog_Folder = hFWobj_SB.AppendFormat ("{0}DataSheets\runs\{1}_{2}_{3}_{4}\"          , _
'    fnAttachStr(strCaseBaseAutomationDir,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
'    gQtTest.Name                                 , _
'    Parameter.Item("Environment")                       , _
'    Parameter.Item("Company")                           , _
'    str_AnFw_FocusArea & fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkXX`ddDdD_HH`mm") ).toString
'

strRunLog_Folder = fn_StringFormatReplacement("{0}DataSheets\runs\{1}_{2}_{3}_{4}\", array(fnAttachStr(strCaseBaseAutomationDir,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
    gQtTest.Name                                 , _
    Parameter.Item("Environment")                       , _
    Parameter.Item("Company")                           , _
    str_AnFw_FocusArea & fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))

Path_MultiLevel_CreateComplete strRunLog_Folder
gFWstr_RunFolder = strRunLog_Folder


' ----------------------------------

gQtResultsOpt.ResultsLocation = strRunLog_Folder

'
'\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\runs
'
'\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\runs

' option explicit ' qq reinstate later
'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
' RunCount: Number of iterations

' qq the framework does not use QTP's iteration-management (i.e. via datasheet),
'    instead, it uses the objWS_Driver rowtype=rtSingleScenario, so all row-loops in all stages, need to manage this themselves,
'    using suggested variables like the below :
' fnAFW_wIncrementGlobalCounter()
'                gFwInt_DataParameterRow_ErrorsTotal
'                gFwInt_AbandonIteration_ReasonCount



    '''''''''''   PbWindow("MTS").CaptureBitmap gFwStrScreenCapFqFileName,True 'p2 is overwrite=true/false
    '''''''''''   ' qq wrap these lines into a fnXX, and '
    '''''''''''     reporter.ReportEvent  micWarning, "NMI `" & piStr_NMI & "` not found.", "", gFwStrScreenCapFqFileName
    gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
    gFWbln_ExitIteration = false

    Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")

      dim MethodName
      On error resume next
    ' MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
      MethodName = oDnf_SysDiags.GetMethod().Name
      On error goto 0





' =========
' =========  FrameworkPhase 00   - Setup the Stage-reporting elements =============
' =========

Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
Dim ctSucc, intStageSuccessAr : intStageSuccessAr = split(",0,0,0,0", ",") : intWantedSuccessCount = uBound(intStageSuccessAr)
Dim BASE_XML_Template_DIR

Dim intNrOfEventsToTrack    : intNrOfEventsToTrack      = 10
Dim tsAr_EventStartedAtTS     : tsAr_EventStartedAtTS     = split(string(intNrOfEventsToTrack-1, ","), ",")
Dim tsAr_EventEndedAtTS       : tsAr_EventEndedAtTS       = split(string(intNrOfEventsToTrack-1, ","), ",")
Dim tsAr_EventPermutationsCount : tsAr_EventPermutationsCount = split(replace(string(intNrOfEventsToTrack-1, ","), ",", "0,")&"0", ",")

const iTS_Stage_0 = 0
const iTS_Stage_I = 1
const iTS_Stage_II = 2
const iTS_Stage_III = 3

' =========
' =========  FrameworkPhase 0a   - Expand the Template Rows into SingleScenario Rows  =============
' =========



tsAr_EventStartedAtTS(iTS_Stage_0) = now()

Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")

'   qq  moved to fnLib_MsWindows Dim gObjNet : Set gObjNet = CreateObject("WScript.NetWork") '  gObjNet.UserName gObjNet.ComputerName

' Environment.Value("RUNCOUNT")=Parameter.Item("RunCount") ' not required


    Dim intColNames_RowNr, intColNames_ColNr_Start, intColNames_ColNr_End
    Dim r_rtTemplate, intColNr_RowType, intColNr_DataCol_First, intColNr_DataCol_Last,  intColNr_ScenarioStatus
    Dim intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
    
    

Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"

' Load the MasterWorkBook objWB_Master
Dim strWB_noFolderContext_onlyFileName  
						strWB_noFolderContext_onlyFileName = "MC_Objections_CATS_and_WIGS.v33.xlsm"
						strWB_noFolderContext_onlyFileName = "Automation_Plan_Book.xlsm"

Dim wbScratch

Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
dim int_NrOf_InScope_Rows, int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr



fnExcel_CreateAppInstance  objXLapp, true

Dim x
x = fn_MsOffice_Version_v2_wP ( objXLapp ) 

'LoadData_RunContext objXLapp, objWB_Master, objWS_TestRunContext, 1,  BASE_AUTOMATION_DIR & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr
LoadData_RunContext objXLapp, wbScratch , objWS_TestRunContext, 1,  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder


			' qq 2017`01Jan`24Tue_16`33`22 BrianM Workaround
			Set objWB_Master = wbScratch 
			Set wbScratch = nothing
			
			
			objXLapp.Calculation = xlManual
			objXLapp.screenUpdating=False

			

' load the DataParameter worksheet objWS_DataParameter - this contains the DataScenarios to test
'
' set objWS_DataParameter         = objWB_Master.worksheets("wsDT_MeterCntestblty_Objections")
'set objWS_DataParameter            = objWB_Master.worksheets("wsSsDT_mrCntestbty_Objectns")

' Dim objWB_Master , ' objWS_useInEach_ProcessStage <<== moved to automation framework



Dim objWS_DataParameter 						 
set objWS_DataParameter           					= objWB_Master.worksheets("wsMsDT_mrCntestbty_Objectn") ' qq this must become a parm




Dim objWS_templateXML_TagDataSrc_Tables 	
set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
' Dim objWS_ScenarioOutComesValidations		: set objWS_ScenarioOutComeValidations 		= objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify") ' reference the Scenario ValidationRules WS


' Prepare the runLogAr for processing by loading it from the relevant worksheet
    Set gfwobjWS_LogSheet = objWB_Master.worksheets("ws_RunLog")
    Dim intLogCol_Start, intLogCol_End,  intLogRow_Start, intLogRow_End 
    
    
    If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then
    
        intLogRow_Start        =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_FirstMinusOne").row        +    1
        intLogRow_End         =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_LastPlusOne").row        -    1
        intLogCol_Start        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_First_MinusOne").column    +    1
        intLogCol_End        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_Last_PlusOne").column    -    1
        
        gfwAr_DriverSheet_Log_rangeFor_PullPush = _
            gfwobjWS_LogSheet.range ( gfwobjWS_LogSheet.cells ( intLogRow_Start, intLogCol_Start), gfwobjWS_LogSheet.cells(intLogRow_End, intLogCol_End) )
        intLogRow_End = intLogRow_End         ' qq for debugging    '        else            '    =    gcFw_intLogMethod_WSdirect

    End If


'qq - Temp sheet which need to be removed once verification function is complete
set objWS_Mts_Value_Dump = objWB_Master.worksheets("MTS_Value_Dump")

Dim vntNull, mt
		
		

MethodName = gFWoDnf_SysDiags.GetMethod().Name

gFWrange_NMI_Spec   		= objWS_DataParameter.range("rgWS_dataNMI_Specification")
gFWrange_NMI_SpecResult 	= objWS_DataParameter.range("rgWS_dataNMI_SpecificationResult")

objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 	= "'" & Parameter.Item("Environment")
objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	 = "'" & gObjNet.UserName

' setAll_RequestIDs_Unique objWS_DataParameter

'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula = "'" & fnWindows_UserName(false)'
'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula = "'" & fnFW_GetComputerName()



gDt_Run_TS = now
gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 
'setAll_RequestIDs_to     objWS_DataParameter, gFwTsRequestID_Common 


int_NrOf_InScope_Rows      = objWS_DataParameter.range("rgWS_Count_NrOf_needed_NMIs").value
int_NrOf_InScope_Rows      = cInt(int_NrOf_InScope_Rows * 1.5) + 10 ' get some extra rows in case some of those identified are reserved for other cases
int_NrOf_NMI_rows_required = int_NrOf_InScope_Rows * (intSize_listOldNew + 1)

Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE, cellReportStatus_SingleScenario
set cellReportStatus_FwkProcessStage = objWS_DataParameter.range("rgWS_cellReportStatus_FwkProcessStage")
set cellReportStatus_ROLE            = objWS_DataParameter.range("rgWS_cellReportStatus_ROLE")
On error resume next
' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Set cellReportStatus_SingleScenario = objWS_DataParameter.range("rgWS_cellReportStatus_SingleScenario")
On error goto 0


'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"

set cellTopLeft = objWS_DataParameter.range("rgWS_RunReport_TopLeftCell_Stage0_StartTS")
' rowTL = cellTopLeft.row : colTL = cellTopLeft.column
rowTL = 1 : colTL = 1

'  <<<=========   expand the rows (already done)
'                 expand the rows (already done) - in the objWS_DataParameter in the MasterWB
'                 expand the rows (already done)
'                 expand the rows (already done)
'                 expand the rows (already done)
' sb_rtTemplate_Expand objWS_DataParameter, dictWSDataParameter_KeyName_ColNr


' objExcel.Application.Run "test.xls!sheet1.csi"
' objXLapp.Run gStrWB_noFolderContext_only_RunVersion_FileName & "!sb_rtTemplate_Expand"     ' qqq <<<====   needs fixing, wouldn't run the macro

' objXLapp.Calculation = xlAutomatic qq later, and ensure that    xlAutomatic     is defined



' =========
' =========  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   =============
' =========


Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
dim r

On error resume next
Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
r = 0
set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")


Dim strAr_listOldNew, intSize_listOldNew, str_listOldNew
'str_listOldNew = ucase(Parameter.Item("listOldNew")) ' for debugging
'if left(str_listOldNew,1) <> "," then str_listOldNew = "," & str_listOldNew
'Environment.Value("listOldNew") = str_listOldNew

'strAr_listOldNew = split(Environment.Value("listOldNew"), ",")  ' this list will be one of :  old  new old,new  so we split the list with a comma to find out what permutations we're going to test
'intSize_listOldNew = uBound(strAr_listOldNew)





'fnWS_LoadKey_ColOffsets_toDictionary objWS_TestRunContext, 1, 1, 50, "", dictWSTestRunContext_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
Dim DB_CONNECT_STR_MTS
DB_CONNECT_STR_MTS = "Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & Environment.Value("HOSTNAME") & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & Environment.Value("SERVICENAME") & "))); User ID=" & Environment.Value("USERNAME") & ";Password=" & Environment.Value("PASSWORD") & ";"


' dim strTestName, uftapp : Set uftapp = CreateObject("QuickTest.Application")
' strTestName = uftapp.Test.Name
' Set uftapp = nothing

Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL")&"SORD\"

Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
Dim strQueryTemplate, dtD, Hyph, strNMI

Dim strList_NMIsizes, strAr_NMIsizes, nmiSizes
strList_NMIsizes = ",SMALL,LARGE"
strAr_NMIsizes = split(strList_NMIsizes, ",")

Dim oldnew, oldnewMax, n
oldnewMax = intSize_listOldNew

Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
intCatsCR_ColNr                 = dictWSDataParameter_KeyName_ColNr("CATS_CR")
intColNR_BaseDate       = dictWSDataParameter_KeyName_ColNr("BaseDate")
intColNR_DateOffset       = dictWSDataParameter_KeyName_ColNr("DayOffset")
intColNr_sizeOfNMI        = dictWSDataParameter_KeyName_ColNr("list_tNMI")

Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0

Dim intSQL_Pkg_ColNr, strSQL_RunPackage


Dim intRowNr_DPs_SmallLarge : intRowNr_DPs_SmallLarge = objWS_DataParameter.range("rgWS_RowNr_DPs_SmallLarge").row

Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn


' qq this needs to change so that it is driven from a table which is a sibling to the run log table so that this per run/env/user/machine i.e. context-run config becomes table driven
	
	' Setup the SingleScenario row range to run over - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
	'                                                 - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
	'                                                 - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
	' Setup the SingleScenario row range to run over - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
	intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
	intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
	intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 

			
		'	===>>>   Setting the   intColNr_InScope   colulmn - EXECUTION flow
	sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc

	Dim strColName

	strScratch = fnTimeStamp ( now,  "yyyy`wkWW" )	'											==============================================================================
	If strScratch 	= "2017`wk10" Then				'										    ==============================================================================				
									strColName 	= "InScope_CATS_All"		  			
		'	"InScope_WIGS_All"		"InScope_CATS_All	"
	Select Case gobjNet.ComputerName
		Case "CORPVMUFT02" 	:	strColName 	= "InScope_WIGS_All"
		Case "PCA19323"	 	:	strColName 	= "FrameworkSheetShakeout"
		Case else	'	CORPVMUFT06	CORPVMUFT02 PCA19323
	End Select

'		strColName 	= "InScope_WIGS_All"		  			'	RunParm Overrides are here   <<<<<====================================================================================
	 	intColNr_InScope = dictWSDataParameter_KeyName_ColNr(strColName) ' "FrameworkSheetShakeout")
		objWS_DataParameter.Cells(intWsRowNr_ColumNames, intColNr_InScope).Interior.Color = objWS_DataParameter.range("rgWS_status_CaseStarted").Interior.Color
	 	
'		If 1 = 1 Then
'			intRowNr_LoopWS_RowNr_StartOn 	= 1110  '	1st 2 WIGS cases, investigating    Ab) varAr contsins scalar(PROCESSED) Bb) gui refs not working
'			intRowNr_LoopWS_RowNr_FinishOn 	= 1110 '  112 	'
'		End If
'		If 1 = 100001 Then
'			intRowNr_LoopWS_RowNr_StartOn 	= 1002	'	Cats 4.2 cases
'			intRowNr_LoopWS_RowNr_FinishOn 	= 1003 ' 1272 ' 003 ' 116 ' 002 ' 1006  1116      ' 
'		else
'			intRowNr_LoopWS_RowNr_StartOn 	= 1116	'	 WIGS cases	
'			intRowNr_LoopWS_RowNr_FinishOn 	= 1116 ' 8     single-stage
'		End If
	End If
	objWS_DataParameter.calculate	'	to ensure the filename is updated
				
				
				
Dim  str_xlBitNess : str_xlBitNess = objWS_DataParameter.range("rgWS_XL_BitNess_x32x64").value
				
				' qq also write a tab-deliminted line for pasting into 
				'              \\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\runs\AutomationRun_Register.xlsm
sb_File_WriteContents  _
	gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  


	' sblEnCl_Set_colNrInScope_fromRun_User_and_PcName  intColNr_InScope, dictWSDataParameter_KeyName_ColNr
			'		'	Moved to fnLib_ExecutionControl

			'			' qq  the per-PC, per-User scenario-to-run config (i.e. below) needs to become table-based
			'			  select case gObjNet.ComputerName
			'			    case "PCA18123", "PCA16100", "CORPVMUFT08"    ' Umesh
			'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
			'			    case "PCA15187xxx" '  Brian
			'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("debug_InScope")
			'			    Case else
			'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
			'			  End select
			'			  
			'			  select case gObjNet.UserName
			'				case "brmoloneyQQ"
			'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("debug_InScope")
			'			    Case else
			'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
			'			  End select
	  
	  intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
	  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")

	Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
	If IsEmpty(intColNr_InScope) then	
		blnColumnSet =false
	ElseIf intColNr_InScope = -1 then
		blnColumnSet =false
	End if
	If not blnColumnSet then 
'		qq log this	
'		objXLapp.Calculation = xlManual
		objXLapp.screenUpdating=True
		exittest
	End If

	objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
	objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
	objWS_DataParameter.range("rgWS_Stage_Config_LocalOrGlobal") = "Local"			'		<<<===   should this always be the way the run-scope is managed ?
	objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").formula = "'" & intRowNr_LoopWS_RowNr_StartOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).formula = "'" & intRowNr_LoopWS_RowNr_FinishOn
	objWS_DataParameter.cells ( intRowNr_LoopWS_RowNr_StartOn  , intColNr_InScope ).select
	objWS_DataParameter.calculate
	objxlapp.screenupdating = true
'	objWS_DataParameter.Calculate

	objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
	
tsAr_EventEndedAtTS(iTS_Stage_0) = now()

sbScenarioStageRole_OutcomeList_Verify_vn01 _
	"acSetup"    , _
	objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify").range("rgWsDyn_Table_ScenarioOutcome_Verify"),  _
	mt, _
	mt, mt, mt, mt, mt, _
	gdict_FwkRun_DataContext_AllScenarios , mt, gDict_FieldName_ScenarioOutComeArrayRowNr , _
	gvntAr_fwScenarioOutcome_Verify_tableAr, mt, mt,  mt 
	

' =========
' =========  FrameworkPhase 0c   - Setup the MultiStage Array                                           =============
' =========

Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
Dim  intMasterZoomLevel 

Dim rowStart, rowEnd, colStart, colEnd
Dim RangeAr 


'    ===>>>   Enable HotKeys
objWB_Master.Application.Run "HotKeys_Set"

'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"

' ===>>>   Expand the MasterSheet into the singleProcess Sheets
  objWB_Master.Application.Run "unitTest_WS_MultiProcess_Expand" ' this populates 
'																		  	objWS_DataParameter.Names("rgWsListDyn_MultiStageProcess_WorksheetNames").RefersToRange
'																	  and from that
'																			strAr_listProcessStage_WorkSheetNames

' qq remove the below ??
dim strList_Scratch, m, mMax
Dim strAr_listProcessStage_WorkSheetNames, strAr_Scratch1, strAr_Scratch2, strAr_Scratch3, strAr_Scratch
Dim strAr_listProcessStages_Relevant, strAr_listProcessStages_All
' wsSpecAndData_Expand_CreateMultiStageWorkSheets ActiveWorkbook.Sheets("wsMsDT_mrCntestbty_Objectn")
strAr_Scratch = objWS_DataParameter.Names("rgWsListDyn_MultiStageProcess_WorksheetNames").RefersToRange

mMax = UBound(strAr_Scratch, 1)
strAr_listProcessStage_WorkSheetNames = split ( string(mMax, ","), ",")
For m = 1 to mMax
  strAr_listProcessStage_WorkSheetNames(m) =  strAr_Scratch(m,1)
Next


strAr_Scratch = objWB_Master.Names("rgWbListDyn_SelectedProcess_AllStages").RefersToRange
mMax = UBound(strAr_Scratch, 2)
strAr_listProcessStages_All = split ( string(mMax, ","), ",")
For m = 1 to mMax
  strAr_listProcessStages_All(m) = strAr_Scratch(1,m)
Next
' qq remove the above ??



' create the row (StageNames) and column (XML Template and Tag names) dictionaries
' and populate them with RowNr and ColNr for the XML_ColumnNames, and Process_RowNames, respectively
	Dim dictWsDP_XML_TemplateAndTag_Name_ColNr, dictWsDP_SingleProcess_Name_RowNr
	set dictWsDP_XML_TemplateAndTag_Name_ColNr 			= CreateObject("Scripting.Dictionary")
	set dictWsDP_SingleProcess_Name_RowNr 		 		= CreateObject("Scripting.Dictionary")
	dictWsDP_XML_TemplateAndTag_Name_ColNr.CompareMode 	= vbTextCompare
	dictWsDP_SingleProcess_Name_RowNr.CompareMode       = vbTextCompare
	
	Dim iAr : iAr = Array(0,0,0,0,0) ' array of integers, 1&2 are start and end rowNrs, 3 & 4 are start and end colNrs
	
	iAr(1) = objWS_DataParameter.names("rgWS_XML_TemplateTag_RowNr_and_ColNrStart").row
	iAr(3) = objWS_DataParameter.names("rgWS_XML_TemplateTag_RowNr_and_ColNrStart").column
	iAr(4) = objWS_DataParameter.names("rgWS_XML_TemplateTag_RowNr_and_ColNrEnd"  ).column
'	fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, iAr(1), iAr(3), iAr(4)-iAr(3)+1, "", dictWsDP_XML_TemplateAndTag_Name_ColNr 
	
	iAr(3) = objWS_DataParameter.names("rgWS_ProcessList_ColNr_and_RowNrStart").column
	iAr(1) = objWS_DataParameter.names("rgWS_ProcessList_ColNr_and_RowNrStart").row
	iAr(2) = objWS_DataParameter.names("rgWS_ProcessList_ColNr_and_RowNrEnd"  ).row
'	fnWS_LoadKey_RowOffsets_toDictionary objWS_DataParameter, iAr(3), iAr(1), iAr(2)-iAr(1)+1, "", dictWsDP_SingleProcess_Name_RowNr 
' XML_ColumnName & Process_RowName dictionaries now initialized & ready to use



r_rtSingleScenario = 1115 ' qq - a) move to make this per-row for multi-stage  b) then review the loop structures inside multi-stage for efficiency
objWB_Master.Names("rgWB_ProcessScenario_Selector").formula = _
  "'" & objWS_DataParameter.cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("Scenario_ProcessPath") ).value

' qq
Dim strStageIsOmitted              : strStageIsOmitted = objWB_Master.names("rgWBconst_isAnOmitted_ProcessStage").refersToRange.value
Dim intNrOf_InScope_singleProcesses : intNrOf_InScope_singleProcesses = 0

			'		strAr_Scratch = objWB_Master.Names("rgWB_ProcessStages_InScope").RefersToRange
			'		mMax = UBound(strAr_Scratch, 2)
			'		strAr_listProcessStages_Relevant = split ( string(mMax, ","), ",")
			'		For m = 1 to mMax
			'		  strAr_listProcessStages_Relevant(m) = strAr_Scratch(1,m)
			'		  If strAr_listProcessStages_Relevant(m) <> strStageIsOmitted Then
			'		    intNrOf_InScope_singleProcesses = intNrOf_InScope_singleProcesses + 1
			'		  End If
			'		Next
objWS_DataParameter.calculate
intNrOf_InScope_singleProcesses = objWS_DataParameter.range("rgWS_NrOf_InScope_singleProcesses").value

If intNrOf_InScope_singleProcesses < 1 Then
	Reporter.ReportEvent micFail , "the process is configured with `0` stages to run, fix and rerun", ""
'	objXLapp.Calculation = xlManual
	objXLapp.screenUpdating=True
	exitTest
End IF

'If intNrOf_InScope_singleProcesses = 1 Then
    set objWS_useInEach_ProcessStage = objWS_DataParameter
    

    '	the first one is always used for Phase_I  Phase I
'else
'	set objWS_useInEach_ProcessStage = objWB_Master.sheets(strAr_listProcessStage_WorkSheetNames(iSingleProcess))
'End if

objWS_DataParameter.cells("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope


fnLog_Iteration  cFW_Exactly, 100 '  r_rtSingleScenario ' cFW_Plus cFW_Minus 

' strAr_listProcessStages_Relevant = fnSlice_Array(objXLapp, strAr_Scratch, true, 1)

    ' strAr_Scratch (1) ' objWB_Master.Names("rgWB_ProcessStages_InScope").RefersToRange ' qq


' qqqq Phase 0 needs to check the inputs for whether they are invalid, and if-invalid, whether they are a) validly-configured negative test cases, or b) errors in data-setup made by the SME
' - qqqq  a) can be determined by adopting a format-standard (needs to be clearly visible, e.g. Font.Color=Orange) that denotes a negative test case

' =========
' =========  FrameworkPhase I   - Gather NMI's & other test-data that will serve as inputs to the test =============
' =========            - Begins :
' =========


strPhase = "I_Prepare"

tsAr_EventStartedAtTS(iTS_Stage_I) = tsAr_EventEndedAtTS(iTS_Stage_0)

cellReportStatus_FwkProcessStage.formula = "'" & "=========  Phase I   - Gather NMI's & other test-data that will serve as inputs to the test"
cellReportStatus_ROLE.formula = "'"
cellReportStatus_SingleScenario.formula = "'"


Dim singleProcess_InScope_strAr
Dim rangeProcessControlTable,rangeMultiProcess_ControlTable_Ar 
' rangeProcessControlTable = objWS_DataParameter.names("rgWS_MultiProcess_ControlTable").refersToRange ' same as line below
rangeMultiProcess_ControlTable_Ar =objWS_DataParameter.range("rgWS_MultiProcess_ControlTable")
const cRowNr_RowsThatRun_thisSingleProcess_Count = 2
const cRowNr_SingleProcess_Name = 3


strAr_ProcessStage = strAr_listProcessStages_Relevant
mMax = UBound(rangeMultiProcess_ControlTable_Ar , 2)
intArSz_ProcessStage = mMax


'   qq load the dictionary from the rgWS_ColumName_Row
    intColNr_RowType = dictWSDataParameter_KeyName_ColNr.Item("RowType")
' qq - FrameWork.Rule - retain this single evaluation here so that work can be done concurrently by different test-engineers







' set the state to run the SingleScenarios over, based on the company being tested
  Select Case ucase(Parameter.Item("Company"))
    Case "CITI", "PCOR"
      strScratch = "VIC"
    Case "SAPN"
      strScratch = "SA"
    Case else
    
	' StringBuilder function replaced with fn_StringFormatReplacement function
      ' strScratch =  hFWobj_SB.AppendFormat ("Case `COMPANY` parm was passed, but value was invalid (`{0}`), i.e. not one of {CITI`POWERCOR`SAP}.", Parameter.Item("Company") ).ToString
      
      strScratch =  fn_StringFormatReplacement("Case `COMPANY` parm was passed, but value was invalid (`{0}`), i.e. not one of {CITI`POWERCOR`SAP}.", array(Parameter.Item("Company")))
      
      
      reporter.ReportEvent micFail, strScratch, ""
      ExitAction ' exit the test case
  End Select
  Dim tgtCol_State : tgtCol_State = dictWSDataParameter_KeyName_ColNr("list_tState")
  
  
objXLapp.Calculation = xlManual
objXLapp.screenUpdating=False

  For intScratch = intRowNr_LoopWS_RowNr_StartOn to intRowNr_LoopWS_RowNr_FinishOn
    ' If objWS_DataParameter.cells( intScratch, intColNr_RowType).value = "rtSingleScenario" Then
    If objWS_DataParameter.cells( intScratch, intColNr_RowType).value = "rtSingleScenario" Then
      objWS_DataParameter.cells( intScratch,tgtCol_State ).formula = strScratch
    End If
  Next


' qq select case strMachineName    set     intColNr_InScope
' qq select case strMachineName    set     intColNr_InScope
' qq select case strMachineName    set     intColNr_InScope


    Dim vntAr_RowTypes, vntAR_ColumnTypes

'   Create new SingleScenario rows from the DataTemplate rows



'   <<==  Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
 '            sb_rtTemplate_RemoveDataParm_Rows_WS objWS_DataParameter, dictWSDataParameter_KeyName_ColNr

'   Create new SingleScenario rows from the DataTemplate rows

    Dim intArSz_Role, intArSz_State, intArSz_NorC, intArSz_sizeNMI
    Dim iRole, iState, iNorC, isizeNMI
    Dim colNr_Role, colNr_State, colNr_NorC, colNr_sizeNMI
    Dim strAr_acrossA_Role, strAr_downA_State, strAr_downB_NorC, strAr_downC_sizeNMI
    Dim strRow_SingleScenario_Template, strRow_SingleScenario: strRow_SingleScenario_Template = "tr<TemplateRow>rl<RoleNr>st<State>nc<NorC>nmi<NMI>"
    Dim objCell_NewRowCell
    Dim strRole_NorC_Negative, cStr_OmitThisCombination: cStr_OmitThisCombination = "-"
    Dim intNrOfNewRows: intNrOfNewRows = 0
    Dim intDisplayChanges_Interval: intDisplayChanges_Interval = 0
    Dim intColNr_SQL_DataParameter, strMasterWS_ColumnName
    Dim intColNr_XML_DataTemplate_FileNameQQ
    Dim strSQL_TemplateName_Current, strSQL_TemplateName_Previous, strScratch, intScratch, cellScratch
    Dim strXML_TemplateName_Current, strXML_TemplateName_Previous
    Dim intColNr_NMI, intColNr_TestResult
    Dim strSuffix_Expected, strSuffix_Actual, strSuffix_NMI, strPrefix_RoleIsRelevant, str_Role_Execution_result
    Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true
    Dim qintColNr, qintColsCount, strSqlCols_NamesList, strSqlCols_ValuesList, strSql_Suffix, qintFieldFirst_Nr : qintFieldFirst_Nr = 0
    Dim strAr_SqlCol_NamesAr, strAr_SqlCol_ValuesAr, dictSqlData_forThisRow_ColValues
    Dim colNr_SqlCol_Names, colNr_SqlCol_Values, strPrefixRequestID
    Dim strInScope_SmallLarge_Ar, intCt_AdditionalCriteria_All, cellNMI, cellNMI_data, strCellFormula, cellExpectedValue, cellActualValue
    Dim intNMIdata_ColNr_Start, intNMIdata_ColNr_End, intColNr_NMIdata
    Dim cellTarget

    strSuffix_Expected 			= objWS_DataParameter.range("rgWS_Suffix_Actual").value
    strSuffix_Actual   			= objWS_DataParameter.range("rgWS_Suffix_Expected").value
    ' strSuffix_NMI      			= objWS_DataParameter.range("rgWS_nameNMI").Value ' As the NMI column has chaned hence we need oo refer to new column
    strSuffix_NMI      			= objWS_DataParameter.range("rgWs_prefixNMI").Value
    strPrefixRequestID      		= objWS_DataParameter.range("rgWs_prefixRequestID").Value
    strPrefix_RoleIsRelevant 	= objWS_DataParameter.range("rgWS_RoleIsRelevant_Prefix").Value
    str_Role_Execution_result	= objWS_DataParameter.range("rgWS_Role_ExpectedResult").Value 
    
' this column contains the name of the SQL to be used for this group of Cats_CR's
  intColNr_SQL_DataParameter = dictWSDataParameter_KeyName_ColNr("SQL_Template_Name")

'   the columns that the DataParms appear in, are constant, for    State, NorC and sizeNMI
    colNr_State   = dictWSDataParameter_KeyName_ColNr("list_tState")
  colNr_NorC    = dictWSDataParameter_KeyName_ColNr("this_tNorC")
    colNr_sizeNMI = dictWSDataParameter_KeyName_ColNr("list_tNMI")

    colNr_SqlCol_Names  = dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")
    colNr_SqlCol_Values = dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")
    
	intNMIdata_ColNr_Start = dictWSDataParameter_KeyName_ColNr( "FRMP_NMIdata") ' qq - refine this so that it is dynamic rather than hard-coded
	intNMIdata_ColNr_End   = dictWSDataParameter_KeyName_ColNr( "LNSP_NMIdata")

'   the DataParms for Role are constant for the run
	' Incase 
	If objWS_DataParameter.Range("rgWsConfig_RoleMode_is_Global_or_ScenarioSpecific").value = "Global" Then
    	strAr_acrossA_Role = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_Role_List").value, ",", "L", True), ","): intArSz_Role = UBound(strAr_acrossA_Role)
    ElseIf objWS_DataParameter.Range("rgWsConfig_RoleMode_is_Global_or_ScenarioSpecific").value = "ScenarioSpecific" Then
      '                                                     **    RP becomes MC after MeterContestability
      ' strAr_acrossA_Role  = Split((",FRMP,LR,MDP,MPB,RoLR,RP,LNSP"),","): intArSz_Role = UBound(strAr_acrossA_Role)
	strAr_acrossA_Role  = Split((",FRMP,LR,MDP,MPB,RoLR,MC,LNSP"),",") ' qq - this hardcoding has to be removed
	intArSz_Role = UBound(strAr_acrossA_Role)
    End if

' Retrieve the ColNr for the SQL_Template names
    intColNr_SQL_DataParameter = dictWSDataParameter_KeyName_ColNr("SQL_Template_Name")
    strSQL_TemplateName_Previous = ""

' Retrieve the ColNr for the XML_Template names
  intColNr_XML_DataTemplate_FileNameQQ = dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")


  strAr_downC_sizeNMI = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_listOf_sizeOf_NMI").value, ",", "L", True), ","): intArSz_sizeNMI = UBound(strAr_downC_sizeNMI)
  For isizeNMI = 1 To intArSz_sizeNMI
  

    strSQL_TemplateName_Previous = "<none>"
    strSQL_TemplateName_Current = ""

    ' iterate over all the SingleScenario Rows
      r_rtSingleScenario          = intRowNr_LoopWS_RowNr_StartOn
      Do
      
		str_rowScenarioID = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ScenarioID ).value

        cellReportStatus_SingleScenario.formula = "'" & r_rtSingleScenario
        fnLog_Iteration  cFW_Exactly, r_rtSingleScenario ' cFW_Plus cFW_Minus 

      ' InScope Row
          If ( (UCase(objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_InScope).Value) = "Y") and _
               (objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_InScope).EntireRow.Height <> 0 ) ) _
          Then ' InScope       and RowIsNot_Hidden_ieHeightIsZero       ' qq remove the row.Hidden part ?  or retain for flexibility

             ' objWS_DataParameter.Rows(r_rtSingleScenario).Select


              If objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_RowType).Value = cStr_rtSingleScenario Then

              	objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_InScope).Interior.Color = objWS_DataParameter.range("rgWS_status_CaseStarted").Interior.Color

        ' if the SizeOfNMIs the current query is returning is the same as this row ...
          If strAr_downC_sizeNMI(iSizeNMI) = objWS_DataParameter.Cells( r_rtSingleScenario, colNr_sizeNMI ).value Then
          


          ' report the NMI size being processed ' qq if the below 2 lines are equivalent, then which one is more efficient ?
            ' objWS_DataParameter.range("rgWS_cellReportStatus_NMI_Size").formula = "'" & strAr_downC_sizeNMI(iSizeNMI)
            objWS_DataParameter.range("rgWS_cellReportStatus_NMI_Size").formula = "'" & objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_sizeOfNMI)

            strScratch = objWS_DataParameter.Cells( r_rtSingleScenario, intColNr_SQL_DataParameter ).value
          ' The RULE HERE is that when an SQL is specified, so will be the corresponding list of SQL-sourced-DataTags
		             If ((strScratch <> "") or (strSQL_TemplateName_Previous = "<none>")) Then ' ignore blank cells
		              strSQL_TemplateName_Current = strScratch
				              If strSQL_TemplateName_Current <> strSQL_TemplateName_Previous Then
				
				
				
				                  strMasterWS_ColumnName = strSQL_TemplateName_Current
				                intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr( strMasterWS_ColumnName )  '   "MTS_FIND_NMI_withNo_METER"
				                  strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value '
				                  ' strQueryTemplate       = objWS_TestRunContext.cells(r_rtSingleScenario, intSQL_ColNr).value
				
				
				                strSQL_FindActiveNmi_ofSize = strQueryTemplate
				                ' strSQL_FindActiveNmi_ofSize = replace ( strSQL_FindActiveNmi_ofSize , "<NMI_Size>", strAr_downC_sizeNMI(isizeNMI), 1, -1, vbTextCompare)
				                 strSQL_FindActiveNmi_ofSize = replace ( strSQL_FindActiveNmi_ofSize , "<NMI_Size>", objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_sizeOfNMI), 1, -1, vbTextCompare)
				                strSQL_FindActiveNmi_ofSize = replace ( strSQL_FindActiveNmi_ofSize , "<rownum>"  , int_NrOf_InScope_Rows   , 1, -1, vbTextCompare) ' qq
				
				
				' qq This is a good place to generate the sql content in the worksheet and/or run logs
				' qq This is a good place to generate the sql content in the worksheet and/or run logs
				' qq This is a good place to generate the sql content in the worksheet and/or run logs
				
				              ' write the SQL to the worksheet, save the objWB, and ...
				                Set cellScratch = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated"))
				                With cellScratch
				                  .formula = "'" & strSQL_FindActiveNmi_ofSize
				                  .WrapText = False
				                End With
				                Set cellScratch = nothing
				                objWB_Master.save
				                
				              ' ... run the SQL
				                fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_FindActiveNmi_ofSize, int_NrOf_InScope_Rows
				
				                strSQL_TemplateName_Previous = strSQL_TemplateName_Current
								qintColsCount = objDBRcdSet_TestData_A.Fields.Count
				                           strSqlCols_NamesList = "" : strSql_Suffix = "," ' we'll make the list 1-based
				                        '  strSqlCols_ValuesList = "" : strSql_Suffix = ","  ' we'll make the list 1-based
				
				                For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count-1 ' qq fix this limit
				                  strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
				                ' strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
				                  strSql_Suffix = ","
				                Next
				
				              End If
		            Else' What happens if strScratch = ""
		              strSQL_TemplateName_Current = "" 
		            End If


                '   the DataParms for     State, NorC and sizeNMI    change with every row
                '    strAr_downA_State   = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_listOf_States").Value, ",", "L", True), ","): intArSz_State = UBound(strAr_downA_State)
                '    strAr_downB_NorC    = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_listOf_NorC").Value, ",", "L", True), ","): intArSz_NorC = UBound(strAr_downB_NorC)
		           For iRole = 1 To intArSz_Role
		
		' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array
		' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array\
		' qq -  as part of the setup / initialize / verify process
		' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array
		' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array
		' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array
		
		              cellReportStatus_ROLE.formula = "'"  & strAr_acrossA_Role(iRole)
		              intColNr_ScenarioStatus = dictWSDataParameter_KeyName_ColNr("ps" & strAr_acrossA_Role(iRole))
		
		
		                    '   the columns that the DataParms appear in, for Role, change as the role changes
		                        colNr_Role = dictWSDataParameter_KeyName_ColNr(strAr_acrossA_Role(iRole))
		                        ' colNr_Role+0
		              objWS_DataParameter.range(  objWS_DataParameter.Cells(r_rtSingleScenario, intWsColNr_ParmsStart), _
		                                    objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+3) ).select
		
		'             sbRunningRange_BorderShow objWS_DataParameter.range(  objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+0), _
		'                                   objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+3) )
		              ' objWS_DataParameter.selection
		
		
		              ' strScratch = strAr_acrossA_Role(iRole) & "_" & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected
		              strScratch = strAr_acrossA_Role(iRole) & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected
		                        intColNr_NMI = dictWSDataParameter_KeyName_ColNr(strScratch)
		                        
		
		                        intCt_AdditionalCriteria_All = 0
		                        strInScope_SmallLarge_Ar = split(trim(ucase(objWS_DataParameter.Cells(intRowNr_DPs_SmallLarge, colNr_Role).value)), ",", -1, vbBinaryCompare)
		
		              			set cellNMI = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI)
		              			
		                        Select Case ucase(trim(objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_sizeOfNMI).Value))
		                          case "SMALL" :
		                            If strInScope_SmallLarge_Ar(0) = "SMALL+" Then
		                              intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
		                            else
		                              ' objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI)
		                              cellNMI.Formula = "'nr.SMALL-"
		                              cellNMI.HorizontalAlignment = -4108 ' xlCenter
		                            End if
		                          case "LARGE" :
		                            If strInScope_SmallLarge_Ar(1) = "LARGE+" Then
		                              intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
		                            else
		                              cellNMI.Formula = "'nr.LARGE-"
		                              cellNMI.HorizontalAlignment = -4108 ' xlCenter
		                            End if
		                          case "GENERATR" :
		                            If strInScope_SmallLarge_Ar(2) = "GENERATR+" Then
		                              intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
		                            else
		                              cellNMI.Formula = "'nr.GENERATR-"
		                              cellNMI.HorizontalAlignment = -4108 ' xlCenter
		                            End if
		                          case "INTERCON" :
		                            If strInScope_SmallLarge_Ar(3) = "INTERCON+" Then
		                              intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
		                            else
		                              cellNMI.Formula = "'nr.INTERCON-"
		                              cellNMI.HorizontalAlignment = -4108 ' xlCenter
		                            End if
		                          case "SAMPLE" :
		                            If strInScope_SmallLarge_Ar(4) = "SAMPLE+" Then
		                              intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
		                            else
		                              cellNMI.Formula = "'nr.SAMPLE-"
		                              cellNMI.HorizontalAlignment = -4108 ' xlCenter
		                            End if
		                          case "WHOLESAL" :
		                            If strInScope_SmallLarge_Ar(5) = "WHOLESAL+" Then
		                              intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
		                            else
		                              cellNMI.Formula = "'nr.WHOLESAL-"
		                              cellNMI.HorizontalAlignment = -4108 ' xlCenter
		                            End if
		                          Case else
		                        End Select
		
		
		                        If intCt_AdditionalCriteria_All = 1 Then ' In-Scope for SMALL+-/LARGE+-
		
		                          strScratch = objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role).Value
		
		                          select case ucase(trim(strScratch))
		                          '   SingleScenario rows only ever contain one value in these ROLE columns, in this case,   N  or C   or  -
		
		                            Case "N", "C" : ' this is an In-Scope SingleScenario, so put the testData-NMI into the current Role's NMI-Column
		                              intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
		                            Case else
					                  set cellNMI = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI)
		                              If left(cellNMI.value,3) <> "nr." Then
		                                cellNMI.formula = "nr." & cellNMI.value
		                              End if
		                          End select
		
		                        End If
		                        set cellNMI = nothing
		
		
		'                       objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI).select
		                        if intCt_AdditionalCriteria_All <> 2 then ' the role is out of scope for one or both
		                        
			                        ' shade the cells to indicate OutOfScope
			                        
			                          With objWS_DataParameter.range( objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+0), _
			                                                          objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+3) ).Interior
					                      .Pattern = xlSolid
					                      .PatternColorIndex = xlAutomatic
					                      .ThemeColor = xlThemeColorDark1
					                      .TintAndShade = -0.499984740745262
					                      .PatternTintAndShade = 0
					                  End With
'					                        For each 	thisCell in objWS_DataParameter.range( objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+0), _
'			                                                          objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+3) )
'					                      	thisCell.Interior.Pattern = xlSolid
'					                      	thisCell.Interior.PatternColorIndex = xlAutomatic
'					                      	thisCell.Interior.ThemeColor = xlThemeColorDark1
'					                      	thisCell.Interior.TintAndShade = -0.499984740745262
'					                      	thisCell.Interior.PatternTintAndShade = 0
'			                        Next
			                        Set thisCell = nothing

						else' the row is In-Scope for BOTH    SMALL+-/LARGE+- AND    New+-/Current+-
						
						Do ' synthetic goto

					'	qq     objDBRcdSet_TestData_A.EOF=TRUE   ==>>   has been encountered here (on a CATS-and-WIGS-ALL test), which caused the following statement to fail ...
					'  	qq     objDBRcdSet_TestData_A.EOF=TRUE   ==>>   ... so what is needed here is a test for that with logging and an exittestiteration outcome tgFrameworkLayering
							bln_Scratch = objDBRcdSet_TestData_A.EOF
			                      If 	bln_Scratch  									Then 	'	this line exists to allow EOF-processing to be tested even if EOF has not occurred
			                      	gFWbln_ExitIteration = true 
			                      	objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_NY) = cY
			                      	objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_ReasonContextEtc) = _ 
			                      		fn_StringFormatReplacement (_
			                      			"TestName={0},Scenario{1},RunSheet(2),RowNr{3},Reason={4}", _
									Array(gQtTest.Name, str_rowScenarioID, objWS_DataParameter.Name, r_rtSingleScenario, "a_EOF Reached" ) )
			                      	
			                      	Exit Do	'	synthetic goto
			                      End If
							                      	
		                            strNMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
		                            tsAr_EventPermutationsCount(iTS_Stage_I) = tsAr_EventPermutationsCount(iTS_Stage_I) + 1 ' increment the permutations count
		                          ' set the initial scenario status to fail
		                            objWS_DataParameter.cells(r_rtSingleScenario, intColNr_ScenarioStatus).value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_BusinessProcess_Fail").value
		
		
		                      ' =====>>> NMI Reservation
		                      ' =====>>> NMI Reservation
		                      ' =====>>> NMI Reservation
		                            blnDataReservationIsActive_DefaultTrue = false  ' <<<===  NMI Reservation
		                          '	get a NMI for all singleProcesses/Roles required
		                          '	get a NMI for all singleProcesses/Roles required
		                          '	get a NMI for all singleProcesses/Roles required
		                          ' gFWcell_xferNMI = objWS_DataParameter.range("rgWS_dataNMI_Specification")(0)
		                       '    Populate the source NMI table
		                       '    Populate the source NMI table
		                       '    Populate the source NMI table
		                       '    Populate the source NMI table
							For each gFWcell_xferNMI in objWS_DataParameter.range("rgWS_dataNMI_Specification")
								If left(gFWcell_xferNMI.value, 4) = "<new" Then
							             intReserveDataRC = fnReserveDataForCase_NMI ( strNMI, "CATS_OUT_2001_Objection_a", "", GstrDate_Run_TS_Suffix, "", strReserveData_ResultDesc)
			                            		if blnDataReservationIsActive_DefaultTrue then
								                do While intReserveDataRC < gDataRsvnSt_Approved
							                      objDBRcdSet_TestData_A.MoveNext
							                      If objDBRcdSet_TestData_A.EOF  Then
							                      	gFWbln_ExitIteration = true
			                      					objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_NY) = cY
							                      	objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_ReasonContextEtc) = _ 
							                      		fn_StringFormatReplacement (_
							                      			"TestName={0},Scenario{1},RunSheet(2),RowNr{3},Reason={4}", _
													Array(gQtTest.Name, str_rowScenarioID, objWS_DataParameter.Name, r_rtSingleScenario, "b_EOF Reached" ) )

							                      	Exit Do	'	synthetic goto
							                      End If
							                      strNMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
							                      intReserveDataRC = fnReserveDataForCase_NMI ( strNMI, "CATS_OUT_2001_Objection_a", "", piStr_RunNR, "", strReserveData_ResultDesc) ' qq p2 must be the test case name
							                    loop ' qq also handle Rs.EOF		
							              End if
							              set cellTarget = _
										objWS_DataParameter.range("rgWS_dataNMI_SpecificationResult").cells(_
											1 + gFWcell_xferNMI.row        - objWS_DataParameter.range("rgWS_dataNMI_Specification").cells(1,1).row, _
											1 + gFWcell_xferNMI.column - objWS_DataParameter.range("rgWS_dataNMI_Specification").cells(1,1).column)
									cellTarget.value = strNMI
		'				                    call sbCopyTo_TargetRangeRowCol_Value( _
		'				                    	objWS_DataParameter.range("rgWS_dataNMI_SpecificationResult")                                 , _
		'				                    	gFWcell_xferNMI.row    - objWS_DataParameter.range("rgWS_dataNMI_Specification")(0).row    + 1, _
		'				                    	gFWcell_xferNMI.column - objWS_DataParameter.range("rgWS_dataNMI_Specification")(0).column + 14   , _
		'				                    	strNMI							)
								End If
		                  			Next
							objWS_DataParameter.Calculate
		                  			
		                  	'	==>>>>> setup all the RequestIDs, freshly for each row
		                  			 setAll_RequestIDs_Unique objWS_DataParameter
							objWS_DataParameter.Calculate	'	no harm doing this twice as will only recalc changed-cells since the last ws.Calculate
							objWS_DataParameter.parent.parent.screenupdating = true
							objWS_DataParameter.parent.parent.screenupdating = false

		                  			 
		
							' no longer required qq qwqwqw 
				
		
		         			'         objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI).select
		                     '       objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI).Formula = "'" & strNMI ' objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
		
		                            qintColsCount = objDBRcdSet_TestData_A.Fields.Count
		                          '  strSqlCols_NamesList = "" : strSql_Suffix = ","  ' we'll make the list 1-based
		                            strSqlCols_ValuesList = "" : strSql_Suffix = ","  ' we'll make the list 1-based
		
		                            ' retrieve the SQL_Query's fields and store their values
		                            For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count -1
		
		
		                  ' strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
		                    strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
		                    strSql_Suffix = ","
		                  Next
		
		                  objWS_DataParameter.Cells(r_rtSingleScenario, colNr_SqlCol_Names ).formula = "'" & strSqlCols_NamesList
		
		                  strScratch = strAr_acrossA_Role(iRole)
		                  intScratch = dictWSDataParameter_KeyName_ColNr(strScratch & objWS_DataParameter.range("rgWS_objRS_ColumnValues_roleSuffix").value)
		                  objWS_DataParameter.Cells(r_rtSingleScenario, intScratch         ).formula = "'" & strSqlCols_ValuesList
		                ' objWS_DataParameter.Cells(r_rtSingleScenario, colNr_SqlCol_Values).formula = "'" & strSqlCols_ValuesList
		
		                            ' load the column names and values to the Driver.RunSheet       ' qq PARTICIPANT_NAME
		
		

						' now move to the next row for the next in-Scope row
		                            objDBRcdSet_TestData_A.MoveNext ' move to the next row in the TestData-RecordSet

					Exit do
					Loop ' synthetic goto

		                            ' - qq this is applicable for this script only but need a permanent solution.
		                            ' - We would move the recordset to first row as the NMI can be re-used in this script
		                            If objDBRcdSet_TestData_A.EOF Then
		                            	If objDBRcdSet_TestData_A.RecordCount = 0 Then
		                            		'	qq log this
		                            	else
								objDBRcdSet_TestData_A.MoveFirst	'	this will only work if data is not being reserved
		                            	End If
		                            End If
		
		
		                    end if ' 
		'             sbRunningRange_BorderHide objWS_DataParameter.range(  objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+0), _
		'                                   objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role+3) )
		              ' objWS_DataParameter.selection
		
		
		                '        For iState = 1 To intArSz_State
		                '            For iNorC = 1 To intArSz_NorC
		                  '       strRow_SingleScenario = Replace(strRow_SingleScenario_Template, "<TemplateRow>", r_rtTemplate, 1, -1, vbTextCompare)  ' "rlstnc<NorC>nmi<NMI>"
		                  '       strRow_SingleScenario = Replace(strRow_SingleScenario, "<RoleNr>", iRole, 1, -1, vbTextCompare)        ' "rlstnc<NorC>nmi<NMI>"
		                  '       strRow_SingleScenario = Replace(strRow_SingleScenario, "<State>", iState, 1, -1, vbTextCompare)
		                  '       strRow_SingleScenario = Replace(strRow_SingleScenario, "<NorC>", iNorC, 1, -1, vbTextCompare)
		                  '       strRow_SingleScenario = Replace(strRow_SingleScenario, "<NMI>", isizeNMI, 1, -1, vbTextCompare)
		
		                        '   objWS_DataParameter.Cells(r_rtSingleScenario, 1).Formula = strRow_SingleScenario
		                              '   objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_RowType).Select
		
		                ' not required when not inserting / deleting rows
		                            ' ... intRowNr_End = objWS_DataParameter.Range("rgWS_DataRow_Last_PlusOne").Row - 1
		                            
		                            
					' StringBuilder function replaced with fn_StringFormatReplacement function
'		                            goStrBldr.Clear
'						strBarText = goStrBldr.AppendFormat ( cStr_BarText_Format,  _
'							gQtTest.Name ,	Parameter.Item("Environment") ,	Parameter.Item("Company") ,	r_rtSingleScenario ,	_
'							objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intCatsCR_ColNr).value , strPhase , strAr_acrossA_Role(iRole) , "stgNone", "rqQQ" , strNMI  ).ToString
'							
						strBarText = fn_StringFormatReplacement(cStr_BarText_Format, array(gQtTest.Name ,	Parameter.Item("Environment") ,	Parameter.Item("Company") ,	r_rtSingleScenario ,	_
							objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intCatsCR_ColNr).value , strPhase , strAr_acrossA_Role(iRole) , "stgNone", "rqQQ" , strNMI))

							
						objXLapp.statusbar = strBarText
		
		
		                        '    Next ' NorC
		                    '    Next ' State
		                    
		                    

		                    Next ' Role
		                    
		                    
		                    
		                    
		                    '	.Calculate 				calculates only new, changed and volatile formulas.
					'	.CalculateFull 			calculates all formulas regardless. As a general rule, this will therefore be slower.
					'	.CalculateFullRebuild 	calculates all formulas and rebuilds the entire calculation dependency tree. This will be the slowest of all.
		                    
		                    objWS_DataParameter.Calculate
    				'	objXLapp.Calculation = xlAutomatic 
					objXLapp.screenUpdating=True
				'	objXLapp.Calculation = xlManual
					objXLapp.screenUpdating=False

                    
                    ' the value.locking done here is now done once for the whole row, below, so this indented code below is retired
                    ' the value.locking done here is now done once for the whole row, below, so this indented code below is retired
					'''                  ' now, for all roles, where the NMI is taken from the rgWS_dataNMI_SpecificationResult, then make its value static
					'''					for intColNr_NMIdata = intNMIdata_ColNr_Start to intNMIdata_ColNr_End
					'''					
					'''						set cellNMI_data = objWS_DataParameter.Cells( r_rtSingleScenario, intColNr_NMIdata)
					'''						strCellFormula =  mid(cellNMI_data.formula, 2) ' strCellFormula = """" & mid(cellNMI_data.formula, 2) & """"
					'''					'	if the cell gets its value from the pool of NMIs just retrieved, change the cell value from a formula to a static  value,
					'''					'	to lock the value in 
					'''					'      i.e. leave formulae in place where one SingleScenario data`NMI is set by referencing a nearby NMIcell in another SingleScenario row or column
					'''					' Now make-static all references to the sourceNMI_Table
					'''					' Now make-static all references to the sourceNMI_Table
					'''					' Now make-static all references to the sourceNMI_Table
					'''					' Now make-static all references to the sourceNMI_Table
					'''						If fnBln_Ranges_doIntersect( objWS_DataParameter.range(strCellFormula) , objWS_DataParameter.range("rgWS_dataNMI_SpecificationResult")) Then
					'''							cellNMI_data.Formula = "'" & cellNMI_data.Value ' qq the cell is value-locked here
					'''						End If
					'''					next
                    ' the value.locking done here is now done once for the whole row, below, so this indented code below is retired


							rowStart = r_rtSingleScenario: colStart = objWS_DataParameter.range("rgWS_InScope_SingleProcess_colFirst").column '  - 1 ' to make the array 1-based for the range we're interested in
							rowEnd   = r_rtSingleScenario: colEnd   = objWS_DataParameter.range("rgWS_InScope_SingleProcess_colLast"        ).column
							singleProcess_InScope_strAr = objWS_DataParameter.Range(objWS_DataParameter.Cells(rowStart, colStart), objWS_DataParameter.Cells(rowEnd, colEnd))
							    
					        ' now value.lock the data-configuration on this row for all singleProcess worksheets
					        for iSingleProcess = 1 to intArSz_ProcessStage 
					        
							If singleProcess_InScope_strAr(1, iSingleProcess) <> "<isAnOmitted_ProcessStage>" Then
							
								objWS_DataParameter.range("rgWS_cellReportStatus_singleProcess").formula = strAr_listProcessStages_All(iSingleProcess)
								If intNrOf_InScope_singleProcesses = 1 Then
								    set objWS_useInEach_ProcessStage = objWS_DataParameter
								else
									set objWS_useInEach_ProcessStage = objWB_Master.sheets(strAr_listProcessStage_WorkSheetNames(iSingleProcess))
									' RangeAr = 
						                   objWS_useInEach_ProcessStage.range("rgWS_data_Request_IDandNMI_SpecificationResult_WithColNrOnLeft").value = _
	     								                objWS_DataParameter.range("rgWS_data_Request_IDandNMI_SpecificationResult_WithColNrOnLeft").value 
	     								objWS_useInEach_ProcessStage.calculate
						                   ' =   RangeAr
'									                  objWS_DataParameter.range("rgWS_data_Request_IDandNMI_SpecificationResult_WithColNrOnLeft")
								End If

									
							    rowStart = r_rtSingleScenario: colStart = objWS_DataParameter.range("rgWS_Data_RequestID_colStart").column
							    rowEnd   = r_rtSingleScenario: colEnd   = objWS_DataParameter.range("rgWS_Data_NMI_colEnd"        ).column
							    
								On error resume next
								    Err.Clear
								'	load the cell.values into the Ar
							'	don't set the Xfer variable from the dest range, set it from the src !!! 
							'		RangeAr = objWS_useInEach_ProcessStage.Range(objWS_useInEach_ProcessStage.Cells(rowStart, colStart), objWS_useInEach_ProcessStage.Cells(rowEnd, colEnd))
							
							' no longer required
							' no longer required
								'    	RangeAr = objWS_DataParameter.Range(objWS_useInEach_ProcessStage.Cells(rowStart, colStart), objWS_useInEach_ProcessStage.Cells(rowEnd, colEnd))
							' no longer required

								    If err.number <> 0 Then
								    	' report
								    	' log
								    	' exititeration ?
								    	' exit do ?
								    	err.clear
								    	On error goto 0 ' qq add this style of processing to the coding standards list
								    End If
								    
								'	load the Ar back into the same cells; this replaces formulae with static values, thus value.locking the cells
								    Err.Clear
								    Dim colNr_Key
								    colNr_Key = cInt(0)
								   ' objWS_useInEach_ProcessStage.Range(objWS_useInEach_ProcessStage.Cells(rowStart, colStart), objWS_useInEach_ProcessStage.Cells(rowEnd, colEnd)) = RangeAr
								   For colNr_Key= colStart To colEnd
								   	If left ( objWS_DataParameter.cells ( rowStart, colNr_Key ).formula , 1 ) <> "="  Then ' it's a static value, so transfer it
								   		objWS_useInEach_ProcessStage.cells( rowStart, colNr_Key ).value = objWS_DataParameter.cells( rowStart, colNr_Key ).value
								   	End If
								   Next	'	colNr_Key
						'			objWS_useInEach_ProcessStage.Range(objWS_useInEach_ProcessStage.Cells(rowStart, colStart), objWS_useInEach_ProcessStage.Cells(rowEnd, colEnd)).formula = _
						'		    		objWS_DataParameter.Range(objWS_useInEach_ProcessStage.Cells(rowStart, colStart), objWS_useInEach_ProcessStage.Cells(rowEnd, colEnd)).formula 
								    	objWS_useInEach_ProcessStage.calculate
								 '	the statement below converts the formulae to static values, i.e. it flattens or value-locks them
								   ' objWS_useInEach_ProcessStage.Range(objWS_useInEach_ProcessStage.Cells(rowStart, colStart), objWS_useInEach_ProcessStage.Cells(rowEnd, colEnd)).value = _
								   ' objWS_useInEach_ProcessStage.Range(objWS_useInEach_ProcessStage.Cells(rowStart, colStart), objWS_useInEach_ProcessStage.Cells(rowEnd, colEnd)).value 
								For colNr_Key= colStart To colEnd
								   	If left ( objWS_useInEach_ProcessStage.cells ( rowStart, colNr_Key ).formula , 1 ) = "="  Then ' it's a formula, so make it static, i.e. flattten / valueLock it
								   		objWS_useInEach_ProcessStage.cells( rowStart, colNr_Key ).value = objWS_DataParameter.cells( rowStart, colNr_Key ).value
								   	End If
								   Next	'	colNr_Key

								    If err.number <> 0 Then
								    	' report
								    	' log
								    	' exititeration ?
								    	' exit do ?
								    	err.clear
								    	On error goto 0 
								    End If
								On error goto 0

						do
						Exit do

								    ' -------------------------------------------------------------------------- TEMP CODE TO TEST IF WE CAN WRITE IN MASTER SHEET - START ------------------------------------------------
'								   If iSingleProcess = 1 Then ' Only for once, write data in master sheet as well
									   On error resume next
'										    Err.Clear
'										'	load the cell.values into the Ar
'										    RangeAr = objWS_DataParameter.Range(objWS_DataParameter.Cells(rowStart, colStart), objWS_DataParameter.Cells(rowEnd, colEnd))
'										    If err.number <> 0 Then
'										    	' report
'										    	' log
'										    	' exititeration ?
'										    	' exit do ?
'										    	err.clear
'										    	On error goto 0 ' qq add this style of processing to the coding standards list
'										    End If
'										    
'											load the Ar back into the same cells; this replaces formulae with static values, thus value.locking the cells
										    Err.Clear
										    objWS_DataParameter.Range(objWS_DataParameter.Cells(rowStart, colStart), objWS_DataParameter.Cells(rowEnd, colEnd)) = RangeAr
										    If err.number <> 0 Then
										    	' report
										    	' log
										    	' exititeration ?
										    	' exit do ?
										    	err.clear
										    	On error goto 0 
										    End If
										On error goto 0
										
						Exit do
						loop
	
							End If 	'								If singleProcess_InScope_strAr(1, iSingleProcess) <> "<isAnOmitted_ProcessStage>" Then

								   	
								   
								   
									
								    ' -------------------------------------------------------------------------- TEMP CODE TO TEST IF WE CAN WRITE IN MASTER SHEET - END  ------------------------------------------------
	



'							End if ' if this singleProcess is inScope  (i.e. singleProcess_InScope_strAr(1, iSingleProcess) <> "<isAnOmitted_ProcessStage>"   )
									
					    	Next ' iterating over the singleProcess worksheets to value.lock the NMIs and RequestIDs
    	
					'	objXLapp.Calculation = xlAutomatic 
						objWS_DataParameter.Calculate
						objXLapp.screenUpdating=True
					'	objXLapp.Calculation = xlManual
						objXLapp.screenUpdating=False
    	
    	
    	              End If ' the sizeOfNMI for this row is the same as the query is currently returning
    	              
            End If ' rt = cStr_rtDataParameter
          End If ' InScope Row



	'	move on to the next SingleScenario worksheet row
        r_rtSingleScenario = r_rtSingleScenario + 1

        strNMI = "" ' Clear the last NMI for next data row

      Loop While r_rtSingleScenario <= intRowNr_LoopWS_RowNr_FinishOn ' (piWS_DataSheet.Range("rgWS_DataRow_Last_PlusOne").Row - 1)


  Next ' sizeNMI loop6
  

  objDBRcdSet_TestData_A.Close
  objDBConn_TestData_A.Close

  ' report that all NMI-sizes are finished being processed
  objWS_DataParameter.range("rgWS_cellReportStatus_NMI_Size"         ).formula = "Finished."
  objWS_DataParameter.range("rgWS_cellReportStatus_singleProcess").formula = ""


  objWB_Master.save
'  objWB_Master.Application.Run "unitTest_WS_MultiProcess_Expand" ' qq to delete once this merge Wed.21 is shown to work
  tsAr_EventEndedAtTS(iTS_Stage_I) = now()


objXLapp.Calculation = xlAutomatic 
'objWS_DataParameter.Calculate
objXLapp.screenUpdating=True


iTS_Max = 3 ' Ubound(tsAr_EventEndedAtTS)
For iTS = iTS_Stage_0 To iTS_Stage_III
  cellTopLeft.cells(rowTL    , colTL + iTS).value = tsAr_EventStartedAtTS(iTS)
  cellTopLeft.cells(rowTL + 1, colTL + iTS).value = tsAr_EventEndedAtTS(iTS)
  '        row             +2&3 is the duration, endTS-startTS
  cellTopLeft.cells(rowTL + 4, colTL + iTS).value = tsAr_EventPermutationsCount(iTS)
Next


objXLapp.Calculation = xlAutomatic ' now automatic for the remainder of the run   BrianM_2017`02Feb`w09`28Tue_10`01  
objXLapp.ScreenUpdating = True




' =========            - Ends 
' =========  FrameworkPhase I   - Gather NMI's & other test-data that will serve as inputs to the test =============
' =========            







' =========
' =========
' =========   Multi-Stage start here
' =========     i.e. for each singleProcess, 
' =========              perform FrameworkPhase II and FrameworkPhaseIII
' =========
' =========
'

' exitAction ' qq comment this out

Dim str_IsInScopeYN


objWS_DataParameter.activate
intMasterZoomLevel = objXLapp.ActiveWindow.Zoom

'  <<<<<=====================   Multi-Stage
for iSingleProcess = 1 to intArSz_ProcessStage  ' <<<====  loop over all the singleProcesses


' rgWS_NrOf_InScope_singleProcesses


 ' .firstCell .lastCell .rowcount



  objXLapp.Calculation = xlManual
'  objXLapp.screenUpdating=false


intScratch = rangeMultiProcess_ControlTable_Ar (cRowNr_RowsThatRun_thisSingleProcess_Count, iSingleProcess) 
strScratch = rangeMultiProcess_ControlTable_Ar (cRowNr_SingleProcess_Name, iSingleProcess) 
strStage = strScratch
hFWobj_SB.clear
' StringBuilder function replaced with fn_StringFormatReplacement function
' strScratch = hFWobj_SB.AppendFormat (" singleProcess `{0}` as it has `{1}` rows that invoke it.", strScratch, intScratch  ).ToString

strScratch = fn_StringFormatReplacement(" singleProcess `{0}` as it has `{1}` rows that invoke it.", array(strScratch, intScratch))



  do
' If strAr_listProcessStages_Relevant(iSingleProcess) = strStageIsOmitted then
' If Range("rgWS_MultiStage_InstancesCount").Cells(4, i - 1).Value > 0 Then ' only if 1 or more singleTxns of this type are required
  if rangeMultiProcess_ControlTable_Ar (cRowNr_RowsThatRun_thisSingleProcess_Count, iSingleProcess) < 1 then ' skip performing this singleProcess 
   	reporter.reportevent micDone, "Skipping all " & strScratch, ""
	  Exit do ' qq this is fine while shaking out; eventually the worksheet's non-participating cells need to be greyed-out
	             ' no, since worksheets that do not run a process are not generated
  End if
  reporter.reportevent micDone, "Performing all requested" & strScratch, ""

  
' from here, all rows for this singleProcess will be run for all roles selected for each row
  If intNrOf_InScope_singleProcesses = 1 Then
    set objWS_useInEach_ProcessStage = objWS_DataParameter
  else
    set objWS_useInEach_ProcessStage = objWB_Master.sheets(strAr_listProcessStage_WorkSheetNames(iSingleProcess))
  End If
  
  ' qq :-
  '   align the  position   of the wsProcessStage to the objWS_DataParameter
  '   	align the  ZoomLevel  of the wsProcessStage to the objWS_DataParameter
  
  
  '	objWS_useInEach_ProcessStage.activate	<<==   commented-out so that that masterMultiWS remains visible, where the status info updates are w
  
  objXLapp.ActiveWindow.Zoom = intMasterZoomLevel


' =========
' =========   the MultiProcess now repeatedly, below, performs FrameworkPhase II-submit & III-verify for each singleProcess



' wsSpecAndData_Expand_CreateMultiStageWorkSheets objWB_Master.Sheets("wsMsDT_mrCntestbty_Objectn") ' qq
' qwqwqw


	' =========
  ' =========  FrameworkPhase II  - merge the Harvested Data with the XML Template, and load the txns onto the queue =============
	' =========
	
	
	tsAr_EventStartedAtTS(iTS_Stage_II) = tsAr_EventEndedAtTS(iTS_Stage_I)
	
	  													strScratch = "'" & "=========  Phase II  - merge the Harvested Data with the XML Template, and load the txns onto the queue "
	cellReportStatus_FwkProcessStage.formula 	=	strScratch
	objXLapp.statusbar 							= 	strScratch
	cellReportStatus_ROLE.formula = "'"
	cellReportStatus_SingleScenario.formula = "'"
	strPhase = "II_Execute"

	
	
	Dim dictWSDataParameter_KeyColName_ItemColValue
	Set dictWSDataParameter_KeyColName_ItemColValue = CreateObject("Scripting.Dictionary")
	dictWSDataParameter_KeyColName_ItemColValue.CompareMode = vbTextCompare
	
	Dim StrXMLTemplateLocation, Str_TransactionID, Date_TransactionDate, Str_Role, Str_RoleStatus, Str_Parcipant, Str_ChangeReasonCode
	Dim date_ChgDt_Type_A_P, str_NMI, Str_list_tState, Str_list_tNMI, Int_DayOffset, strJurisdiction
	Dim str_DataParameter_PopulatedXML_FqFileName, int_UniqueReferenceNumberforXML
	
	' load the SQL{ColNames and ColValues) into splitAr's
	Dim strAr_ColumnNames, int_ArSize_ColNames, strAr_ColumnValue, int_ArSize_ColValues, intArSQL_elNr
	Dim vntSqlCell, strSqlCellContents
	
	Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
	Dim tempInt_RoleIdentificationCounter
	' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
	' Get headers of table in a dictionary
	Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	
	
	gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
	Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
	gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
	' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
	
	
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	' iterate over all the SingleScenario Rows
	r_rtSingleScenario = intRowNr_LoopWS_RowNr_StartOn
	
	
	' Load the first/title row in a dictionary
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_RuleTable, 1, int_MaxColumns, gdictWStemplateXML_TagDataSrc_Tables_ColValue
	
	' iterate over all the SingleScenario Rows
	r_rtSingleScenario = intRowNr_LoopWS_RowNr_StartOn
	Do	'	phase II loop begins
	
		str_rowScenarioID = objWS_DataParameter.Cells(r_rtSingleScenario, intColNR_ScenarioID ).value

	
'			do ' synthetic goto - phase II loop
'	qq 	if objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_ExitTestIteration  "ExitTestIteration_Ny"  ).Value) = "Y" then 
'			qq report this rowNr to the log as an error-level message
'			exit do
'		end if
		
		cellReportStatus_SingleScenario.formula = "'" & r_rtSingleScenario
		fnLog_Iteration  cFW_Exactly,  r_rtSingleScenario ' cFW_Plus cFW_Minus 
		
		
		' InScope Row
'		If ( (UCase(objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_InScope).Value) = "Y") And _
'			(objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_InScope).EntireRow.Height <> 0 ) ) _
		' qq should ExitTestIteration=cY be written to the RunLog, for this phase ?	
		If ( (UCase(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_InScope).Value) = cY) and _
			(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_InScope).EntireRow.Height <> 0 ) and _
			(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_NY)  =  cN) ) _
			Then ' InScope       and RowIsNot_Hidden_ieHeightIsZero       ' qq remove the row.Hidden part ?  or retain for flexibility
			
			str_IsInScopeYN = "N"
			'strScratch = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_RowType).Value
			strScratch = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_RowType).Value
			If 	strScratch = cStr_rtSingleScenario Then
				
				'	get the singleProcess name that the MultiProcess loop is currently executing, e.g. any of  INIT,PVAL,REQ,OBJ,REQ^2,OBJ^2,PEND,CAN,REJ,COM
				strScratch = rangeMultiProcess_ControlTable_Ar (cRowNr_SingleProcess_Name, iSingleProcess) ' (strScratch)
				'	now get the current-row version of that singleProcess, i.e. either the same value, if it is InScope for this row, or "<isAnOmitted_ProcessStage>" if it is not InScope for this row
				'strScratch = objWS_DataParameter.Cells( r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strScratch) ).value 
				strScratch = objWS_useInEach_ProcessStage.Cells( r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strScratch) ).value 
				If strScratch <> "<isAnOmitted_ProcessStage>" Then _
				str_IsInScopeYN = "Y"
			End If
			
			If str_IsInScopeYN = "Y" Then
				
				' objWS_useInEach_ProcessStage.Rows(r_rtSingleScenario).Select
				
				' SingleScenario Rows only (i.e. not DataTemplate)
				
				
				' qq - The seven col role array used to determine the role sequence can come from the singlescenario row or the global list. And which one is used must be set based on the state of the range "rgWsConfig_RoleMode_is_Global_or_ScenarioSpecific"
				
				For iRole_SequenceNo = 1 To intArSz_Role ' Outer role array to execute for the number of roles in scope
					
					' We need an inner loop to identify the first role to be executed
					' qq - This loop is overriding the Global role parameter and would run Scenario based for the time being
					
					For tempInt_RoleIdentificationCounter = 1 To intArSz_Role
						strScratch = strPrefix_RoleIsRelevant & strAr_acrossA_Role(tempInt_RoleIdentificationCounter) ' strSuffix_Actual    strSuffix_Expected
						' If  objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strScratch)).Value = iRole_SequenceNo Then
						If  objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strScratch)).Value = iRole_SequenceNo Then
							Str_Role = strAr_acrossA_Role(tempInt_RoleIdentificationCounter)' "LNSP" ' Role parameter for specific scenario
							Exit For
						Else
							Str_Role = ""
						End If
					Next
					
					' Verify that there is a row else raise an error and move on
					If tempInt_RoleIdentificationCounter > intArSz_Role Then
						' No row eligible for run hence move to next role
						Reporter.ReportEvent micDone, "No row selected for execution for row[" & r_rtSingleScenario & "]", "No row selected for execution for row[" & r_rtSingleScenario & "]"
						tempInt_RoleIdentificationCounter = 1 
					Else		
						cellReportStatus_ROLE.formula = "'" & Str_Role
						intColNr_ScenarioStatus = dictWSDataParameter_KeyName_ColNr("ps" & strAr_acrossA_Role(tempInt_RoleIdentificationCounter))
						
						colNr_Role = dictWSDataParameter_KeyName_ColNr(strAr_acrossA_Role(tempInt_RoleIdentificationCounter))
						strXML_TemplateName_Previous = ""
						
						' strScratch = strAr_acrossA_Role(iRole) & "_" & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected ' Underscore needs to be removed
						strScratch = strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected
						
						intColNr_NMI = dictWSDataParameter_KeyName_ColNr(strScratch)
						
						
' ===============>>>>>>>>>>>>>>>>>>
			' NO ! now moved to stage I      -   ' qq to delete once this merge Wed.21 is shown to work
				' setAll_RequestIDs_Unique objWS_DataParameter ' sets up the array of RequestIDs
						' qq also, now convert the the objWS_useInEach_ProcessStage ( current-row ) RequestIDs to static-values from their reference-values
						'    - qq here, or in the stage-I process ?
						' ===============>>>>>>>>>>>>>>>>>>
						
						' If SingleScenario is In-Scope of the data-rules
						' qq - The column need to change
						' Set cellNMI           = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI)
						Set cellNMI           = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_NMI)
							strNMI = cellNMI.value
						Set cellExpectedValue = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_NMI+1)
						Set cellActualValue   = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_NMI+2)
						
						If Left(cellNMI.value,3) <> "nr." Then '    if this is not a no-Run_Permutation          qq            optimal is case-InSensitive
							
							'   this is not a loop, it is the mechanism to do a goto
							Do
							
								gFWbln_ExitIteration = False
								' the SingleScenario-row's data-combination ** IS ** In-Scope of the data-rules
								
								' so now, we :    a) merge the SingleScenario with the TemplateXML, giving the RunXML,
								'                a) merge the SingleScenario with the TemplateXML, giving the RunXML,
								'                a) merge the SingleScenario with the TemplateXML, giving the RunXML,
								'       then ...
								
								' here, now, we use a static XML Template;
								'           in future, we'll choose the XML template from the DataParameter row
								
								
								tsAr_EventPermutationsCount(iTS_Stage_II) = tsAr_EventPermutationsCount(iTS_Stage_II) + 1 ' increment the permutations count
								
								
								BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from framework
								
								' The RULE HERE is that when an SQL is specified, so will be the corresponding list of SQL-sourced-DataTags
								' strScratch = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
								strScratch = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
								If strScratch <> "" Then ' ignore blank cells
									strSQL_TemplateName_Previous = strXML_TemplateName_Current
									strXML_TemplateName_Current = strScratch
								End If
								
								StrXMLTemplateLocation = BASE_XML_Template_DIR & strXML_TemplateName_Current
								'               StrXMLTemplateLocation = BASE_XML_Template_DIR & "CN`1xxx_200BadMeter.xmlTP_AvB.xml" ' qq hardcoded but need to come from framework
								'       int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
								
								' qq - check how this unique reference is generated for single process and multi process
								' int_UniqueReferenceNumberforXML = gFwAr_strRequestID( iSingleProcess, tempInt_RoleIdentificationCounter )
								
								' The unique reference need to come from the cell in corresponding worksheet
								int_UniqueReferenceNumberforXML = "" ' clear previous request ID, if any
								int_UniqueReferenceNumberforXML = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & strPrefixRequestID)).value
								
								' "CCYYMMDDHHmmss99hhmmss"
								Str_TransactionID =  int_UniqueReferenceNumberforXML ' "txnID_" &
								
								' qq here need to detect if the cell already has a comment 
								On Error Resume Next
								Do
									Err.clear
									With cellNMI
										.AddComment
										If Err.number <> 0 Then 
											On Error Goto 0
											Exit Do 
										End If
										.Comment.Visible = False
										.Comment.Text ""
										.Comment.Text "," & int_UniqueReferenceNumberforXML & ","
										' .Select
									End With
									Exit Do
								Loop
								On Error Goto 0
								
								
								' Date_TransactionDate = now ' "<dataRunDt_SrcFWK_fmtAllBut_TimeZone>+10:00" ' sample #11/24/2016 7:12:53 PM#
								Date_TransactionDate = Now ' year(now) & "-" & month(now) & "-" & day(now)
								
								
								' Str_RoleStatus = objWS_DataParameter.Cells(r_rtSingleScenario, colNr_Role).value ' "N" ' with the role status parameter from worksheet
								Str_RoleStatus = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, colNr_Role).value ' "N" ' with the role status parameter from worksheet
								
								
								
								' qq the below code needs to be refactored. Commenting as this is not required for 1xxx
								
								'             ' ColumnNames cell
								'               Set vntSqlCell = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("DsListSqlColumn_Names"))
								'               strSqlCellContents = vntSqlCell.value
								'
								'               strAr_ColumnNames = split ( mid(strSqlCellContents, 2), left(strSqlCellContents,1), -1, vbBinaryCompare)
								'               int_ArSize_ColNames = Ubound(strAr_ColumnNames)
								'               Set vntSqlCell = nothing
								'
								'             ' ColumnValues cell
								'               Set vntSqlCell = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("DsListSqlColumn_Values"))
								'               strSqlCellContents = vntSqlCell.value
								'
								'               strAr_ColumnValue = split ( mid(strSqlCellContents, 2), left(strSqlCellContents,1), -1, vbBinaryCompare)
								'               int_ArSize_ColValues = Ubound(strAr_ColumnValue)
								'               Set vntSqlCell = nothing
								'
								'
								'               If int_ArSize_ColNames <> int_ArSize_ColValues then
								'               ' qq report error
								'
								'               End if
								'
								'               ' Iterate over the splitAr's, loading the value-pairs to the dictionary, as Key=ColName, Item=ColValue
								'
								'               dictWSDataParameter_KeyColName_ItemColValue.RemoveAll
								'
								'               for intArSQL_elNr = 0 to int_ArSize_ColNames
								'
								'                 ' If dictWSDataParameter_KeyColName_ItemColValue.exists (strColName)  Then
								'                 dictWSDataParameter_KeyColName_ItemColValue.Add strAr_ColumnName(intArSQL_elNr), strAr_ColumnValue(intArSQL_elNr)
								'
								'               next
								'
								'
								' set the XML tag-value from the dictionary(ColName)
								' strRoleColumnName = "ROLE_" & strAr_acrossA_Role(iRole)
								'Str_Parcipant = "ROLE_" & strAr_acrossA_Role(iRole) ' As per Brian M, there is no need to execute a SQL for 1xxx
								
								
								' dictWSDataParameter_KeyColName_ItemColValue.RemoveAll
								
								
								'               Str_Parcipant = "SQL_Fetch1" ' <Participant><dataParticipant`A_SrcSQL></Participant> ' with piStr_Parcipant
								' qq GET QUERY FROM BRIAN
								' qq standardize VarNames, in this case, to intColNr_CatsCR
								
								'Str_ChangeReasonCode = objWS_DataParameter.Cells(r_rtSingleScenario, intCatsCR_ColNr).value ' 1000   ' ' with CR_Code parameter from WS
								Str_ChangeReasonCode = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intCatsCR_ColNr).value ' 1000   ' ' with CR_Code parameter from WS
								str_NMI = cellNMI.value ' 12345678901
								
								' = dictWSDataParameter_KeyName_ColNr("CATS_CR")
								
								
								
								
								' str_ChgDt_Type_A_P = "actual" ' with ChgDt_Type_A_P parameter from WS
								' qqrr Does the ** Case **  Matter ?
								'str_ChgDt_Type_A_P = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("ChgDt_Type_A_P")).value
								str_ChgDt_Type_A_P = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("ChgDt_Type_A_P")).value
								
								' qqrr should this be
								date_ChgDt_Type_A_P = Now ' with ChgDt_Type_A_P parameter from WS
								
								
								' Str_list_tState = "VIC" ' ' from WS - list_tState field
								'StrTR_list_tState = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("list_tState")).value
								StrTR_list_tState = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("list_tState")).value
								
								' Str_list_tNMI = "SMALL" ' WS - list_tNMI
								'Str_list_tNMI = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("list_tNMI")).value
								Str_list_tNMI = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("list_tNMI")).value
								
								'             Int_DayOffset = "1" ' WS - DayOffset
								'Int_DayOffset = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("DayOffset")).value
								Int_DayOffset = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("DayOffset")).value
								
								
								'strJurisdiction = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("list_tState")).value
								strJurisdiction = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("list_tState")).value
								
								'Create an XML file
								
								Dim strCaseGroup, strCaseNr, strCsList1_TagsNeedingFlexibleManagement_Current, strCsList1_TagsNeedingFlexibleManagement_Previous
								'strCaseGroup = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value
								strCaseGroup = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value
								' strCaseNr    = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value
								strCaseNr    = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value
								
								
								'strScratch = objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_TagsNeedingFlexiMgt")).value
								strScratch = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_TagsNeedingFlexiMgt")).value
								If strScratch <> "" Then ' ignore blank cells
									strCsList1_TagsNeedingFlexibleManagement_Previous = strCsList1_TagsNeedingFlexibleManagement_Current
									strCsList1_TagsNeedingFlexibleManagement_Current = strScratch
								End If
								
								
								'C:\_data\XMLs\MC
								Dim strFolderNameWithSlashSuffix_Scratch
								Err.clear
								strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"
								'strScratch = strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & objWS_DataParameter.range("rgWS_objRS_ColumnValues_roleSuffix").value
								strScratch = strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & objWS_useInEach_ProcessStage.range("rgWS_objRS_ColumnValues_roleSuffix").value
								
								' strFolderNameWithSlashSuffix_Scratch = "F:\qqq\"
								gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file = 0
								
								str_ChangeStatusCode = strAr_listProcessStages_All(iSingleProcess)
								'	"REQ" ' qq - This is hardcoded for this script but in a multistage process, this should be the process name. A tag in XML would be updated with this value
								
								
								' fnCreateCATSXML Str_CaseGroup & "_" & Str_CATS_CR, StrXMLTemplate_Folder, StrXMLTemplate_FileNameWoFolder, objWS_useInEach_ProcessStage, "1", gvntAr_RuleTable, gdictWStemplateXML_TagDataSrc_Tables_ColValue
								'                      fnCreateCATSXML strCaseGroup & "_" & strCaseNr, BASE_XML_Template_DIR, strXML_TemplateName_Current, objWS_useInEach_ProcessStage, r_rtSingleScenario, gvntAr_RuleTable, gdictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate, int_UniqueReferenceNumberforXML, Str_Role, str_NMI
								
								' set cellScratch to the current-row XML_TemplateName and retrieve the value referred to by the cell from the configuration table
								' Set cellScratch = objWS_DataParameter.cells( r_rtSingleScenario , dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name"))
								Set cellScratch = objWS_useInEach_ProcessStage.cells( r_rtSingleScenario , dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name"))
								' qq qq qqqq
								
								
								If fnElement(cellScratch,"_",0,-1,"L") <> "tplt_" Then					
									strXML_TemplateName_Current = cellScratch.value
								Else
									strXML_TemplateName_Current = _
										objWS_useInEach_ProcessStage.cells(dictWsDP_SingleProcess_Name_RowNr(strAr_listProcessStages_All(iSingleProcess)) , dictWsDP_XML_TemplateAndTag_Name_ColNr(cellScratch.value)).value
									'objWS_DataParameter.cells(dictWsDP_SingleProcess_Name_RowNr(strAr_listProcessStages_All(iSingleProcess)) , dictWsDP_XML_TemplateAndTag_Name_ColNr(cellScratch.value)).value
									
								End If
								
'								str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML(Environment.Value("COMPANY"), strCaseGroup & "_" & strCaseNr, str_ChangeStatusCode, BASE_XML_Template_DIR, strXML_TemplateName_Current, _
'								strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, r_rtSingleScenario, gvntAr_RuleTable, _
'								gdictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate, int_UniqueReferenceNumberforXML, _
'								Str_Role, str_NMI, "csListSqlColumn_Names", strScratch)
'
'	===>>> Create the XML file
								dim str_DataParameter_PopulatedXML_FileName_withoutFolder
								str_DataParameter_PopulatedXML_FileName_withoutFolder = _
									fnCreateCATSXML_V1(  _
										Environment.Value("COMPANY"), strCaseGroup & "_" & strCaseNr, str_ChangeStatusCode, BASE_XML_Template_DIR,  _
									       strXML_TemplateName_Current, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_useInEach_ProcessStage, _
									       dictWSDataParameter_KeyName_ColNr, 	r_rtSingleScenario, _
									       	gvntAr_RuleTable, _
									       gdictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate,  _
									       int_UniqueReferenceNumberforXML,  Str_Role, str_NMI, "csListSqlColumn_Names", strScratch )
									       
							       If gFWbln_ExitIteration = True Then
							       	Exit do
							       End If

								'str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML(Environment.Value("COMPANY"), strCaseGroup & "_" & strCaseNr, str_ChangeStatusCode, BASE_XML_Template_DIR, strXML_TemplateName_Current, _
								'strFolderNameWithSlashSuffix_Scratch, objWS_useInEach_ProcessStage, dictWSDataParameter_KeyName_ColNr, r_rtSingleScenario, gvntAr_RuleTable, _
								'gdictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate, int_UniqueReferenceNumberforXML, _
								'Str_Role, str_NMI, "csListSqlColumn_Names", strScratch)
								
								'                    case else
								'                      str_DataParameter_PopulatedXML_FqFileName = fnUpdate_CNxxxx_2_3_400_xml( _
								'                        strCaseGroup, strCaseNr, _
								'                        objWS_useInEach_ProcessStage, r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", strScratch, _
								'                        strCsList1_TagsNeedingFlexibleManagement_Current, _
								'                        BASE_XML_Template_DIR, strXML_TemplateName_Current , strFolderNameWithSlashSuffix_Scratch, "", _
								'                        int_UniqueReferenceNumberforXML, Str_TransactionID, Date_TransactionDate, Str_Role, Str_RoleStatus, Str_ChangeReasonCode,_
								'                        str_ChgDt_Type_A_P, date_ChgDt_Type_A_P, str_NMI, strJurisdiction, STR_list_tState, Str_list_tNMI, Int_DayOffset)
								'                  End Select
								'
								
								
								' ActiveSheet.Hyperlinks.Add Anchor:=Selection, Address:= _
								' "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\runs\61030290223`20161201101518`CN`1xxx`200BadMeter`xml`1101542.xml"
								
								' Set cellScratch = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ScenarioStatus )
								Set cellScratch = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_ScenarioStatus )
								str_DataParameter_PopulatedXML_FqFileName = strFolderNameWithSlashSuffix_Scratch & str_DataParameter_PopulatedXML_FileName_withoutFolder
								strScratch = str_DataParameter_PopulatedXML_FqFileName
								
								' qq - need to fix as hyperlinks are not working
								
								'objWS_useInEach_ProcessStage.Hyperlinks.Add cellScratch   , strScratch
								'objWS_useInEach_ProcessStage.Hyperlinks.Add cellExpectedValue, strScratch
								' cellScratch.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_InProgress").value
								cellScratch.value = objWS_useInEach_ProcessStage.range("rgWS_SingleScenario_RunStatus_InProgress").value
								
								Dim intErrCount
								
								intErrCount = 0
								
								
								intScratch = Len ( str_DataParameter_PopulatedXML_FileName_withoutFolder )
								If  intScratch > gFWint_AEMO_HUB_MaxFileNameLength_Limit Then
									strScratch = ""
									intErrCount = intErrCount + 1
									With cellScratch
										.value = "TF"
										On Error Resume Next
										strScratch = .Comment.Text
										On Error Goto 0
										.AddComment
										.Comment.Visible = False
										' StringBuilder function replaced with fn_StringFormatReplacement function
										' strScratch = strScratch & hFWobj_SB.AppendFormat ("HUB.Parameter`FileName.MaximumLength` was exceeded, value is `{0}`, `{1}` was submitted.", gFWint_AEMO_HUB_MaxFileNameLength_Limit, intScratch  ).ToString
										strScratch = strScratch & fn_StringFormatReplacement("HUB.Parameter`FileName.MaximumLength` was exceeded, value is `{0}`, `{1}` was submitted.", array(gFWint_AEMO_HUB_MaxFileNameLength_Limit, intScratch))
										.Comment.Text strScratch
										Reporter.ReportEvent micFail, strScratch, str_DataParameter_PopulatedXML_FqFileName
										' set the scenario status to TechFail
										.value = objWS_useInEach_ProcessStage.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value
										gFWbln_ExitIteration = True
										objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_NY) = cY
						                      	objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_ReasonContextEtc) = _ 
						                      		fn_StringFormatReplacement (_
						                      			"TestName={0},Scenario{1},RunSheet(2),RowNr{3},Reason={4},LimitName={5},Limit={6},Name={7},ActualLen={9}", _
												Array(gQtTest.Name, str_rowScenarioID, objWS_DataParameter.Name, r_rtSingleScenario, "c_FolderNameTooLong" , _
															"gFWint_AEMO_HUB_MaxFileNameLength_Limit", gFWint_AEMO_HUB_MaxFileNameLength_Limit, _
															str_DataParameter_PopulatedXML_FileName_withoutFolder, intScratch ) )
										
									End With
									' Set cellScratch = nothing
								End If
								
								If gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file > 0 Then
									strScratch = ""
									intErrCount = intErrCount + 1
									With cellScratch
										.value = "TF"
										On Error Resume Next
										strScratch = .Comment.Text
										'On error goto 0
										.AddComment
										.Comment.Visible = False
										' StringBuilder function replaced with fn_StringFormatReplacement function
										' strScratch = strScratch & hFWobj_SB.AppendFormat ("The PopulatedXML still contains `{0}` Un-Replaced tags; iteration abandoned.  Fix and rerun.", gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file).ToString
										strScratch = strScratch & fn_StringFormatReplacement("The PopulatedXML still contains `{0}` Un-Replaced tags; iteration abandoned.  Fix and rerun.", array(gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file))
										'On error resume next ' a) in case the cell already has a hyperlink b) qq move this to a function that handles that concern
										On Error Goto 0
										.Comment.Text strScratch
										Reporter.ReportEvent micFail, strScratch, str_DataParameter_PopulatedXML_FqFileName
										' set the scenario status to TechFail
										.value = objWS_useInEach_ProcessStage.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value ' qq should this move to the fnLib_Automation, if so, push the value to this range as part of FW.TestCase_Setup
										gFWbln_ExitIteration = True
										objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_NY) = cY
						                      	objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_ReasonContextEtc) = _ 
						                      		fn_StringFormatReplacement (_
						                      			"TestName={0},Scenario{1},RunSheet(2),RowNr{3},Reason={4},FileName={5},Limit={6},Name={7},ActualLen={9}", _
												Array(gQtTest.Name, str_rowScenarioID, objWS_DataParameter.Name, r_rtSingleScenario, "d_UnReplacedTags_Exist_in_PopulatedXML_File" , _
															strFolderNameWithSlashSuffix_Scratch & str_DataParameter_PopulatedXML_FileName_withoutFolder ) )
										
									End With
									' Set cellScratch = nothing
								End If
								
							'	if intErrCount > 0 then
							'		cellScratch.Hyperlinks.Add  cellScratch, str_DataParameter_PopulatedXML_FqFileName 
							'	End if
								copyfile str_DataParameter_PopulatedXML_FileName_withoutFolder , strFolderNameWithSlashSuffix_Scratch , strRunLog_Folder
								cellScratch.Hyperlinks.Add  cellScratch  ,  strRunLog_Folder & str_DataParameter_PopulatedXML_FileName_withoutFolder 

							' StringBuilder function replaced with fn_StringFormatReplacement function
'								goStrBldr.Clear
'								strBarText = goStrBldr.AppendFormat ( cStr_BarText_Format,  _
'									gQtTest.Name ,	Parameter.Item("Environment") ,	Parameter.Item("Company") ,	r_rtSingleScenario ,	_
'									objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intCatsCR_ColNr).value , strPhase , Str_Role , strStage, int_UniqueReferenceNumberforXML , str_NMI  ).ToString
								strBarText = fn_StringFormatReplacement(cStr_BarText_Format, array(gQtTest.Name ,	Parameter.Item("Environment") ,	Parameter.Item("Company") ,	r_rtSingleScenario ,	_
									objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intCatsCR_ColNr).value , strPhase , Str_Role , strStage, int_UniqueReferenceNumberforXML , str_NMI  ))

								objXLapp.statusbar = strBarText

								Set cellScratch = Nothing
								strScratch_YN =  fnSetIfTrueFalse ( gFWbln_ExitIteration  , "EQ", True, cY, cN )

								strAr_Single_VerifyKey_Fields_logXML ( ciField_HyperLink 			 		) = strRunLog_Folder & str_DataParameter_PopulatedXML_FileName_withoutFolder 
								strAr_Single_VerifyKey_Fields_logXML ( ciField_or_Object_Type		 		) = "ft_txnXML" 		'	qq should this become an enumerated list ?
								strAr_Single_VerifyKey_Fields_logXML ( ciField_ValueWasUsed_YN	 		) = strScratch_YN
								strAr_Single_VerifyKey_Fields_logXML ( ciField_Value_orObject_orBlob	) = gstr_generated_txnXML
								strAr_Single_VerifyKey_Fields_logXML ( ciAsOfTimestamp 					) = now
		
								strScratch_dictionary_TemplateKey = _
									fnCreate_Run_DataKey_Name ( _	
										cFormatStr_I_guiMTS, _
										mt, mt, mt, mt, mt, "InBound", objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intCatsCR_ColNr).value,  _
											strStage, str_Role   , int_UniqueReferenceNumberforXML , mt, mt, mt, "fw_PhaseII", gcStr_toReplace_FieldName )
									'	IO - taken care of by the template, i.e. I
									' 	       RunNr    ScenarioNr               CatsCR                                                                                     
									'      	       ScenarioList   InOutBound                                                                                                        
									'      	stage        role                                             RequestID                                                     txStat   VerificationPtSeqNr    
									'                                                                                                                                                             raisedByRetailerNm
									'                                                                                                                                                                           FieldSource    FieldName
									'            qqqq RunNr needs a value, from the external generator in the common Oracle DB
									'              so do ScenarioList and Scenario
										
								strScratch_dictionary_FieldName = "txnClob_XML" 
																																																				  strScratch_dictionary_FieldName = strScratch_dictionary_FieldName 
								gstrFW_dictionaryKey_fwPhase_IIExecute = replace ( 													strScratch_dictionary_TemplateKey ,	 gcStr_toReplace_FieldName	, strScratch_dictionary_FieldName , 1	, cInt_Split_or_Replace_ProcessOrReturn_AllElements , vbTextCompare )
								strAr_Single_VerifyKey_Fields_logXML ( ciField_Key_storedWith			) = gstrFW_dictionaryKey_fwPhase_IIExecute
				'				sbDictLoad_GlobalRunContext_Item_Key_n_Value  	gdict_FwkRun_DataContext_AllScenarios, 	strScratch_dictionary_TemplateKey ,	 gcStr_toReplace_FieldName	, strAr_Single_VerifyKey_Fields_logXML, strAr_Single_VerifyKey_Fields_logXML 
								
							'	sbDictLoad_GlobalRunContext_Item_Key_n_Value ( 	pio_Dictionary								, piStr_Dictionary_KeyName_Template	, piStr_Item_Name , piVnt_Item_Value_or_Instance		, 	piVnt_Item								)
							'	sbDictLoad_GlobalRunContext_Item_Key_n_Value 	( 	pio_Dictionary 								, piStr_Dictionary_KeyName_Template 	, piStr_Item_Name , piVnt_Item_Value_or_Instance , piVnt_Item 						)
								
				'				sbPushValuesToLog_wsTable _
				'					cstrLogEntryType_DataFile ,	"",  _
				'						objWS_useInEach_ProcessStage ,		r_rtSingleScenario							, _
				'						objWB_Master.worksheets("CatsCrRoleStage_VerifyDump")						, _
				'						str_dictDataContext_Key															, _
				'						gdict_FwkRun_DataContext_AllScenarios.item (  str_dictDataContext_Key )		, _
				'						piRgWsDyn_Table_ScenarioOutcome_Verify	,	intRowNr_of_FieldVerify_rangeTableAr	,	 strPass_YN

					If 			gFWbln_ExitIteration = True Then Exit Do ' gFWbln_ExitIteration is reset to true at the beginning of the iteration
								
								strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
								' copy the PopulatedXML file from the temp folder to the input folder
								copyfile str_DataParameter_PopulatedXML_FileName_withoutFolder, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
								
								' ZIP XML File
								Dim vntDuration : vntDuration = now
								ZipFile str_DataParameter_PopulatedXML_FileName_withoutFolder , BASE_XML_Template_DIR & "runs\"
								vntDuration = now - vntDuration
								strBarText_Save = objXLapp.StatusBar
								strMsg = "Zip duration was " & fnTimeStamp ( vntDuration, "mm'ss")
								objXLapp.StatusBar = strMsg & " - " & strBarText_Save
								  ' wait 0, 750
'								objXLapp.StatusBar = strBarText_Save
'								objXLapp.StatusBar = ""
						
								GetCATSDestinationFolder_V1 Environment.Value("COMPANY"),Environment.Value("ENV"),Str_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
						
'								GetCATSDestinationFolder Environment.Value("COMPANY"),Environment.Value("ENV"),Str_Role ' qq - this needs to move out. We need to get rid of this and read from framework (DS)
								'msgbox Environment.Value("CATSDestinationFolder")
								copyfile Left(str_DataParameter_PopulatedXML_FileName_withoutFolder, Len(str_DataParameter_PopulatedXML_FileName_withoutFolder)-4) &".zip",BASE_XML_Template_DIR & "runs\" ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
							
							Exit Do
							Loop	'	just-inside   "if this is not a no-Run_Permutation", 	for 			IterationExit management
							
						End If 	'	if this is not a no-Run_Permutation 
						Set cellNMI = Nothing
						
					End If ' if this role-column matches the required sequence
					
				Next ' Role Sequence  (from role_01 to role_07)
			End If ' InScopeYN Row
		End If ' SingleScenario Rows only (i.e. not DataTemplate)
		
	'	Exit do
	'	end loop '  synthetic goto - phase II loop              is this required, given that just above, an 			IterationExit management loop already exists ?

		
		r_rtSingleScenario = r_rtSingleScenario + 1
	Loop While r_rtSingleScenario <= intRowNr_LoopWS_RowNr_FinishOn ' (piWS_DataSheet.Range("rgWS_DataRow_Last_PlusOne").Row - 1)
'	phase II loop ends ( do all rt_SingleScenario rows ) 

    objWB_Master.save	'	at end of Phase II
    
    
'	refresh the information display
	objWS_useInEach_ProcessStage.calculate
	If objWS_useInEach_ProcessStage.name <> objWS_DataParameter.name Then
		objWS_DataParameter.calculate
	End If
	objXLapp.screenUpdating=true
'	objXLapp.screenUpdating=False


    tsAr_EventEndedAtTS(iTS_Stage_II) = now()


  iTS_Max = 3 ' Ubound(tsAr_EventEndedAtTS)
  For iTS = iTS_Stage_0 To iTS_Stage_III
    cellTopLeft.cells(rowTL    , colTL + iTS).value = tsAr_EventStartedAtTS(iTS)
    cellTopLeft.cells(rowTL + 1, colTL + iTS).value = tsAr_EventEndedAtTS(iTS)
    '        row             +2&3 is the duration, endTS-startTS
    cellTopLeft.cells(rowTL + 4, colTL + iTS).value = tsAr_EventPermutationsCount(iTS)
  Next


  ' =========
  ' =========  FrameworkPhase III  - verify in the AUT GUI that the AUT gave a reject{RC=CatsCrCode) response  =============
  ' =========

  ' Set dnSysDtTm = DotNetFactory.CreateInstance("System.DateTime")
'Do	'	for POC for Raymond & higher-ups
'Exit do
	
  strPhase = "III_Verify"
  tsAr_EventStartedAtTS(iTS_Stage_III) = tsAr_EventEndedAtTS(iTS_Stage_II)

  														strScratch = "'" & "=========  Phase III  - verify in the AUT GUI that the AUT gave a reject{RC=CatsCrCode) response  "
  cellReportStatus_FwkProcessStage.formula 	=	strScratch
  objXLapp.statusbar 								= 	strScratch
  cellReportStatus_ROLE.formula					= 	"'"
  cellReportStatus_SingleScenario.formula 		= 	"'"
  Dim intSeconds
  
  objXlApp.statusbar = ""

  'While (now()-tsAr_EventStartedAtTS(iTS_Stage_II)) < DateAdd("n", 90, 0)) ' 90 seconds since the first XML was sent
  do
    intSeconds = DatePart("s", now()-tsAr_EventStartedAtTS(iTS_Stage_II))
    If intSeconds >= 48 then exit do ' seconds since the first XML was sent ' qq fix seconds
    objXlApp.statusbar = _
	fn_StringFormatReplacement(_
		"Waited `{0}` of `{1}` seconds to allow the phase_II transaction(s) to reach MTS."	,	_
		array(intSeconds, 48) )
    
    'if gFWobj_SystemDate.TimeSpan(now()-tsAr_EventStartedAtTS(iTS_Stage_II)).TotalSeconds > 60 then exit do
    wait 5 : intScratch = Extern.MessageBeep(&HFFFFFFFF) ' intScratch is 1 on success
  loop
  
  objXlApp.statusbar = ""

  ' wait 300

  Dim str_MTS_Objection_Reason_Code, str_MTS_Txn_Status
  Dim intColNr_CatsCrCode_Actual

  ' Close any open instance of the application
  fnMTS_WinClose ' qq cleanup RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration

  ' Open MTS aplication
  fnMTS_Open_wEnvVars ' qq remove following :   RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration  ' qq we need to detect and notify if login fails
  
  
  
 @@ hightlight id_;_466584_;_script infofile_;_ZIP::ssf3.xml_;_
 @@ hightlight id_;_466584_;_script infofile_;_ZIP::ssf4.xml_;_
  
  

    ' We need Environment.Value("COMPANY")
    ' We need Environment.Value("ENV")
    ' strSource = "C:\_data\GUILoader\MTS\" ' this source folder needs to be there
    ' "C:\_data\GUILoader\MTS\" needs to have ini files

' iterate over all the SingleScenario Rows
r_rtSingleScenario = intRowNr_LoopWS_RowNr_StartOn

' gVarDict_MTSAttribute_HeadersPlusValues - is a global dictionary which will hold all MTS values

Set gVarDict_MTSAttribute_HeadersPlusValues = CreateObject("Scripting.Dictionary")
gVarDict_MTSAttribute_HeadersPlusValues.CompareMode = vbTextCompare

  
  '------------------------------------------------------------------ Changed Starts here --------------------------------------------------------------------------

Do	'	phase III loop begins
	
	str_rowScenarioID = objWS_DataParameter.Cells(r_rtSingleScenario, intColNR_ScenarioID ).value

'		do ' synthetic goto - phase III loop
'	qq 	if objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_ExitTestIteration  "ExitTestIteration_Ny"  ).Value) = "Y" then 
'			qq report this rowNr to the log as an error-level message
'			exit do
'		end if

	
	cellReportStatus_SingleScenario.formula = "'" & r_rtSingleScenario
	fnLog_Iteration  cFW_Exactly,  r_rtSingleScenario ' cFW_Plus cFW_Minus 
	
	gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
	
	' qq pull the gFwInt_DataParameterRow_ErrorsTotal up to the WS.SingleScenario
	'  = gFwInt_DataParameterRow_ErrorsTotal = objWSqqq
	
	str_IsInScopeYN = cN
	' InScope Row
	'If ( (UCase(objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_InScope).Value) = "Y") And _
	'	(objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_InScope).EntireRow.Height <> 0 ) ) _
	' qq should ExitTestIteration=cY be written to the RunLog ?
	If ( (UCase(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_InScope).Value) = "Y") And _
		(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_InScope).EntireRow.Height <> 0 )   and _
		(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_NY)  =  cN) )  _
		Then ' InScope       and RowIsNot_Hidden_ieHeightIsZero       ' qq remove the row.Hidden part ?  or retain for flexibility
		'	get the singleProcess name that the MultiProcess loop is currently executing, e.g. any of  INIT,PVAL,REQ,OBJ,REQ^2,OBJ^2,PEND,CAN,REJ,COM
		strScratch = rangeMultiProcess_ControlTable_Ar (cRowNr_SingleProcess_Name, iSingleProcess) ' (strScratch)
		'	now get the current-row version of that singleProcess, i.e. either the same value, if it is InScope for this row, or "<isAnOmitted_ProcessStage>" if it is not InScope for this row
		'strScratch = objWS_DataParameter.Cells( r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strScratch) ).value 
		strScratch = objWS_useInEach_ProcessStage.Cells( r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strScratch) ).value 
		If (objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_ExitTestIteration_NY)  =  cY) Then
		else
			str_IsInScopeYN = str_IsInScopeYN ' qq
			' qq report phase III skipped for this row as it is not in scope
			If strScratch <> "<isAnOmitted_ProcessStage>" Then _
				str_IsInScopeYN = "Y"
		End If
	End If
	If str_IsInScopeYN = "Y" Then
	
		For iRole_SequenceNo = 1 To intArSz_Role
		
			For tempInt_RoleIdentificationCounter = 1 To intArSz_Role
				strScratch = strPrefix_RoleIsRelevant & strAr_acrossA_Role(tempInt_RoleIdentificationCounter) ' strSuffix_Actual    strSuffix_Expected
				' If  objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strScratch)).Value = iRole_SequenceNo Then
				If  objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(strScratch)).Value = iRole_SequenceNo Then
					Str_Role = strAr_acrossA_Role(tempInt_RoleIdentificationCounter)' "LNSP" ' Role parameter for specific scenario
					Exit For
				Else
					Str_Role = ""
				End If
			Next
			' Verify that there is a row else raise an error and move on
			If tempInt_RoleIdentificationCounter > intArSz_Role Then
				' No row eligible for run hence move to next role
				tempInt_RoleIdentificationCounter = 1 
			Else		
				' qq we should perhaps verify that all the WS and rows are present, required, as a shakeout set of activities, before the run proper begins
				' qq also, the framework really needs as major features    a) restartability    b) borrow failed list from earlier run, where runtype=rerunOnlyFails
				sbScenarioStageRole_OutcomeList_Verify_vn01 _
					"acSetup"    , _
						objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify").range("rgWsDyn_Table_ScenarioOutcome_Verify"),  _
						mt, _
						mt, mt, mt, mt, mt, _
						gdict_FwkRun_DataContext_AllScenarios , mt, gDict_FieldName_ScenarioOutComeArrayRowNr , _
						gvntAr_fwScenarioOutcome_Verify_tableAr, mt, mt,  mt 

				Str_Role = strAr_acrossA_Role(tempInt_RoleIdentificationCounter)' "LNSP" ' Role parameter from framework
				
				cellReportStatus_ROLE.formula = "'" & Str_Role
				intColNr_ScenarioStatus = dictWSDataParameter_KeyName_ColNr("ps" & strAr_acrossA_Role(tempInt_RoleIdentificationCounter))
				
				
				' strScratch = strAr_acrossA_Role(iRole) & "_" & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected
				strScratch = strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected
				
				
				intColNr_NMI = dictWSDataParameter_KeyName_ColNr(strScratch)
				' Set cellNMI = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI)
				Set cellNMI = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_NMI)
				
				
				intColNr_CatsCrCode_Actual = _
					dictWSDataParameter_KeyName_ColNr( strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & objWS_useInEach_ProcessStage.range("rgWS_Suffix_Actual").value )
				' dictWSDataParameter_KeyName_ColNr( strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & objWS_DataParameter.range("rgWS_Suffix_Actual").value )
				
				
				
				' SingleScenario Rows only (i.e. not DataTemplate)
				'If objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_RowType).Value = cStr_rtSingleScenario Then
				If objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_RowType).Value = cStr_rtSingleScenario Then
					
					
					' qq  if the ScenarioStatus is TechFail, the exit the iteration
					' qq  if the ScenarioStatus is TechFail, the exit the iteration
					' qq  if the ScenarioStatus is TechFail, the exit the iteration
					
					' If SingleScenario is In-Scope of the data-rules
					' Set cellNMI = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_NMI)
					Set cellNMI = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_NMI)
					If Left(cellNMI.value,3) <> "nr." Then
						Do   ' synthetic goto
						
							strScratch = ""
							' the SingleScenario-row's data-combination ** IS ** In-Scope of the data-rules
							
							' launch MTS application and get the reason code
							
							'        str_MTS_Objection_Reason_Code = fnCheckCats_MTS(3301125155331, 6103029390, "", "", "")
							
							' ==>>>> here the GUI check is invoked
							' ==>>>> here the GUI check is invoked
							
							
							' Set cellScratch = objWS_DataParameter.Cells(r_rtSingleScenario, intColNr_ScenarioStatus )
							Set cellScratch = objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_ScenarioStatus )
							'If cellScratch.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value Then
							If cellScratch.value = objWS_useInEach_ProcessStage.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value Then
								Set cellScratch = Nothing
								intScratch = Extern.MessageBeep(&HFFFFFFFF)
								wait 0, 200
								intScratch = Extern.MessageBeep(&HFFFFFFFF)
								Exit Do
							End If
							Set cellScratch = Nothing
							
							If objXLapp.activesheet.name <> objWS_useInEach_ProcessStage.name Then
'								objWS_useInEach_ProcessStage.activate
'								objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intColNr_NMI).Select
							End If
							' cellNMI.select
							tsAr_EventPermutationsCount(iTS_Stage_III) = tsAr_EventPermutationsCount(iTS_Stage_III) + 1 ' increment the permutations count
							
							' ====>>>>
							' str_MTS_Objection_Reason_Code = fnCheckCats_MTS(Split(cellNMI.Comment.Text , ",")(1), cellNMI.value, "", "", "") ' first parameter is the request ID
							
							
						'	str_MTS_Txn_Status = fnCheckCatsNotificationReceived_MTS_V2(rangeMultiProcess_ControlTable_Ar(cRowNr_SingleProcess_Name,iSingleProcess), cellNMI.value, piStr_CRCode, Split(cellNMI.Comment.Text , ",")(1) , Str_Role, "", "")
							Dim str_CR_Code, str_RequestID
								  str_CR_Code 		= objWS_useInEach_ProcessStage.Cells( r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR") ).value 
								  str_Scratch 		= objWS_useInEach_ProcessStage.range("rgWs_prefixRequestID").value
								  str_RequestID 	= objWS_useInEach_ProcessStage.Cells( r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr(Str_Role & str_Scratch)).value
								  		
							str_MTS_Txn_Status = _
								fnCheckCatsNotificationReceived_MTS_vn03( _
									rangeMultiProcess_ControlTable_Ar(cRowNr_SingleProcess_Name, iSingleProcess), _
									cellNMI.value, str_CR_Code, str_RequestID						 , Str_Role, "", "")	
									
								'	cellNMI.value, str_CR_Code, Split(cellNMI.Comment.Text , ",")(1) , Str_Role, "", "")	'	qq retrieve the comment.text value more transparently
								
' Function fnAppendData_WS_MTS_Value_Dump(piobjWS_Mts_Value_Dump, piobjWS_RequestID, piobjWS_NMI,  pi_Master_File_Row, pi_CR_Code, pi_Role, pi_Stage, pi_gDict_FieldName_ValueActual_allForSingleScenarioStageRole)
								
'							fnAppendData_WS_MTS_Value_Dump objWS_Mts_Value_Dump,(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("" & Str_Role & "_RequestID")).value), _
'															(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("" & Str_Role & "_NMIdata")).value),  _
'															r_rtSingleScenario,(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value), _ 
'															Str_Role,rangeMultiProcess_ControlTable_Ar(cRowNr_SingleProcess_Name,iSingleProcess), gDict_FieldName_ValueActual_allForSingleScenarioStageRole 
'															
'							fnAppendData_WS_MTS_Value_Dump objWS_Mts_Value_Dump,(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("" & Str_Role & "_RequestID")).value), (objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("" & Str_Role & "_NMIdata")).value),  r_rtSingleScenario,(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value),Str_Role,rangeMultiProcess_ControlTable_Ar(cRowNr_SingleProcess_Name,iSingleProcess), gDict_FieldName_ValueActual_allForSingleScenarioStageRole 
	'						fnAppendData_WS_MTS_Value_Dump_V2 objWS_Mts_Value_Dump,(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("" & Str_Role & "_RequestID")).value), _
	'														(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("" & Str_Role & "_NMIdata")).value),  _
	'														r_rtSingleScenario,(objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value), _ 
	'														Str_Role,rangeMultiProcess_ControlTable_Ar(cRowNr_SingleProcess_Name,iSingleProcess), gdict_FwkRun_DataContext_AllScenarios  
							
							' Write this objection code in the actual parameter
							' objWS_useInEach_ProcessStage.cells( r_rtSingleScenario, intColNr_CatsCrCode_Actual ).formula = "'" & str_MTS_Objection_Reason_Code
							
							' Write this objection code in the actual parameter
							If Trim(g_strMTSObjectionCode) = "" Then
							' If Trim(str_MTS_Objection_Reason_Code) = "" Then
								objWS_useInEach_ProcessStage.cells( r_rtSingleScenario, intColNr_CatsCrCode_Actual ).formula = "'<empty>"
							Else
								objWS_useInEach_ProcessStage.cells( r_rtSingleScenario, intColNr_CatsCrCode_Actual ).formula = "'" & g_strMTSObjectionCode
								' objWS_useInEach_ProcessStage.cells( r_rtSingleScenario, intColNr_CatsCrCode_Actual ).formula = "'" & str_MTS_Objection_Reason_Code
							End If
							
							Set cellScratch = objWS_useInEach_ProcessStage.cells( r_rtSingleScenario, intColNr_CatsCrCode_Actual )
							
							With cellScratch
								On Error Resume Next
								strScratch = .Comment.Text
								On Error Goto 0
								.AddComment
								.Comment.Visible = False
								strScratch = strScratch & ",ORC Explanation[" & g_strObjectionExplanation & "],"
								.Comment.Text strScratch
							End With
							
							'           If g_strObjectionExplanation <> "" Then
							'             objWS_useInEach_ProcessStage.cells( r_rtSingleScenario, intColNr_CatsCrCode_Actual ).Comment.Text ",ORC Explanation[" & g_strObjectionExplanation & "],"  ' Write g_strObjectionExplanation in comments
							'           Else
							'             objWS_useInEach_ProcessStage.cells( r_rtSingleScenario, intColNr_CatsCrCode_Actual ).Comment.Text ",ORC Explanation[blank],"  ' Write g_strObjectionExplanation in comments
							'           End If
							'
							
							'		qq delete below as all results-verification is now delegated to the relevant routine;
							'             aa - HowEver - ensure that the Actual stringSet is maintained so that the high-level A/E PassFail comparison test is maintained
							'							' qq screen-capture the WHOLE set of results
							'							' If  str_MTS_Objection_Reason_Code = _
							'							If Trim(g_strMTSObjectionCode) = _
							'								objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value Then
							'								'objWS_DataParameter.Cells(r_rtSingleScenario, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value Then
							'								
							'								' set the scenario status to BusinessProcess-Pass
							'								objWS_useInEach_ProcessStage.cells(r_rtSingleScenario, intColNr_ScenarioStatus).value = objWS_useInEach_ProcessStage.range("rgWS_SingleScenario_RunStatus_BusinessProcess_Pass").value
							'							Else
							'								objWS_useInEach_ProcessStage.cells(r_rtSingleScenario, intColNr_ScenarioStatus).value = objWS_useInEach_ProcessStage.range("rgWS_SingleScenario_RunStatus_BusinessProcess_Fail").value
							'							End If
							
							' qq - need to fix as hyperlinks are not worknig
							' objWS_useInEach_ProcessStage.Hyperlinks.Add cellActualValue    , gFW_strRunLog_ScreenCapture_fqFileName
							
							
						'	sbScenarioStageRole_OutcomeList_Verify_vn01 ( _
						'		piStr_RunMode, _
						'		piRgWsDyn_Table_ScenarioOutcome_Verify , _            qqqqqqqqqq
						'			pi2DstrPreSufList_VerificationsToPerform   , _
						'			   piStrList_stagesAll, piStr_thisStage, piStr_thisRole, _
						'				pioDict_FieldName_ValueActual_allForSingleScenarioStageRole, _
						'				  piDict_FieldName_ValueActual__ClearOnCompletionYn, _
						'					poDict_FieldName_ScenarioOutComeArrayRowNr, _
						'					poAr_ScenarioOutcomeArray , _
						'					poStrList_Verification_FieldNames ,  _
						'					poStrList_Verification_FieldValues_Expected , _
						'					poStrList_Verification_FieldValues_Actual
						

				
						'							                                                                                                                                                OutOfScope              In          Out      InScope
						'        For Citipower / Powercor / SAP,   I is InScope roles, O are OutOfScope roles                                                           O         O      I             I            O         I       I
							gstr_CurrentRole 			= strAr_acrossA_Role( tempInt_RoleIdentificationCounter )	' gives one of    FRMP	LR	MDP	MPB	RoLR	RP	LNSP


							Dim strHeader_Role_VerificationList, strHeader_Role_RequestID
						'	construct the ExpectedResult row-header string for this role
							strHeader_Role_VerificationList 	= str_Role_Execution_result  & gstr_CurrentRole  ' qq remove range rgWsDyn_Table_ScenarioOutcome_Verify
							strHeader_Role_RequestID		= gstr_CurrentRole & strPrefixRequestID

						' 	                                                                       wsMsDT_mrCntestbty_COM_10  ................................................ ***********
'							gstr_CurrentStageName 	= fnElement( objWS_useInEach_ProcessStage.name, "_", 1, -1, "R")	'	gives    COM
'							gstr_CurrentStageNumber	= fnElement( objWS_useInEach_ProcessStage.name, "_", 0, -1, "R") '      gives     10
							

							gstr_CurrentStageName 	= rangeMultiProcess_ControlTable_Ar (cRowNr_SingleProcess_Name, iSingleProcess) 	'	qq need to move from Process to Stage in varNames
							gstr_CurrentStageNumber	= iSingleProcess

								' StringBuilder function replaced with fn_StringFormatReplacement function
							'goStrBldr.Clear
'								strBarText = goStrBldr.AppendFormat ( cStr_BarText_Format,  _
'									gQtTest.Name ,	Parameter.Item("Environment") ,	Parameter.Item("Company") ,	r_rtSingleScenario ,	_
'									objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intCatsCR_ColNr).value , strPhase , gstr_CurrentRole   ,  gstr_CurrentStageName ,  "rqQQ" , strNMI  ).ToString
								strBarText = fn_StringFormatReplacement( cStr_BarText_Format,  _
									array(gQtTest.Name ,	Parameter.Item("Environment") ,	Parameter.Item("Company") ,	r_rtSingleScenario ,	_
									objWS_useInEach_ProcessStage.Cells(r_rtSingleScenario, intCatsCR_ColNr).value , strPhase , gstr_CurrentRole   ,  gstr_CurrentStageName ,  "rqQQ" , strNMI  ))
								objXLapp.statusbar = strBarText
								
								
								
							if PbWindow("w_frame").Dialog("CitiPower (CMTSMD) MTS").Static("label_ApplicationTimedOut_PleaseRestart").exist(0) then 
								PbWindow("w_frame").Dialog("CitiPower (CMTSMD) MTS").WinButton("btnOK").Click	'	closes the app 
											  ' Close any open instance of the application
											 ' fnMTS_WinClose ' qq cleanup RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration
								  ' Open MTS aplication
								  fnMTS_Open_wEnvVars 
							End if


							sbScenarioStageRole_OutcomeList_Verify_vn01  _
								"bcRun" , _
								objWB_Master.Names("rgWBdyn_Table_ScenarioOutcome_Verify").RefersToRange, _
									objWS_useInEach_ProcessStage.Cells ( r_rtSingleScenario , dictWSDataParameter_KeyName_ColNr(strHeader_Role_VerificationList) ).Value, _
										objWS_useInEach_ProcessStage.Cells ( r_rtSingleScenario , dictWSDataParameter_KeyName_ColNr("CATS_CR") ).Value, _
										objWB_Master.Names("rgWB_fwStage_MasterTxnList_CATS").RefersToRange.cells(1,1).value, _
										gstr_CurrentStageName , _
										gstr_CurrentRole   ,  _
										objWS_useInEach_ProcessStage.Cells ( r_rtSingleScenario , dictWSDataParameter_KeyName_ColNr ( strHeader_Role_RequestID ) ).Value, _	
											gdict_FwkRun_DataContext_AllScenarios, _
											cY, _
												gDict_FieldName_ScenarioOutComeArrayRowNr , _
												gvntAr_fwScenarioOutcome_Verify_tableAr , _
												gStrList_Verification_FieldNames ,  _
												gStrList_VerificationValues_Expected , _
												gStrList_VerificationValues_Actual 

						Exit Do
						Loop ' synthetic goto
					End If
					Set cellNMI = Nothing
					' qq push the gFwInt_DataParameterRow_ErrorsTotal up to the WS.SingleScenario
					' qq = gFwInt_DataParameterRow_ErrorsTotal
					' qq and append the reasonList to the WS.reasonList
				End If ' if this role-column matches the required sequence
				
			End If ' SingleScenario Rows only (i.e. not DataTemplate)

		Next ' Role Sequence
		
	End If ' InScopeYN Row
	
'	Exit do
'	end loop '  synthetic goto - phase III loop

	
	r_rtSingleScenario = r_rtSingleScenario + 1
Loop While r_rtSingleScenario <= intRowNr_LoopWS_RowNr_FinishOn ' (piWS_DataSheet.Range("rgWS_DataRow_Last_PlusOne").Row - 1)
'	phase III loop ends  ( do all rt_SingleScenario rows ) 


  fnMTS_WinClose
'Loop 

'	refresh the information display
	objWS_useInEach_ProcessStage.calculate
	If objWS_useInEach_ProcessStage.name <> objWS_DataParameter.name Then
		objWS_DataParameter.calculate
	End If
	objXLapp.screenUpdating=true
'	objXLapp.screenUpdating=False


    objWB_Master.save
  ' objWB_Master.close

  tsAr_EventEndedAtTS(iTS_Stage_III) = now()



  '     FrameworkPhase III - ends here


' =========
' =========   MultiProcesses has just done stages II-submit & III-verify, for a singleProcess, above,
' =========     and so now moves onto the next process-stage


  Exit do ' this loop is solely used for skipping Omitted singleProcesses
  loop

Next ' perform the next singleProcess in the MultiProcess
objWS_DataParameter.activate


  objXLapp.Calculation = xlAutomatic
  objXLapp.screenUpdating=true


' =========
' =========
' =========   MultiProcesses ends here
' =========
' =========
'
objXLapp.StatusBar = ""


    If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then    '    now push the logAr into the logWS
        objXLapp.Calculation     =    xlManual 
'        gfwobjWS_LogSheet.range(gfwobjWS_LogSheet.cells ( intLogRow_Start, intLogCol_Start), gfwobjWS_LogSheet.cells(intLogRow_End, intLogCol_End) ) = 
        gfwobjWS_LogSheet.cells ( intLogRow_Start , intLogCol_Start ) =  _
            gfwAr_DriverSheet_Log_rangeFor_PullPush     '    https://usefulgyaan.wordpress.com/2013/06/12/vba-trick-of-the-week-slicing-an-array-without-loop-application-index/
        objXLapp.Calculation     =    xlAutomatic
    End if


fnLog_Iteration  cFW_Exactly, 200 ' cFW_Plus cFW_Minus 


sbScenarioStageRole_OutcomeList_Verify_vn01  _
	"ccFinalCleanup"    , mt, mt, mt, mt, mt, mt, mt, _
	gdict_FwkRun_DataContext_AllScenarios , mt, gDict_FieldName_ScenarioOutComeArrayRowNr , gvntAr_fwScenarioOutcome_Verify_tableAr, mt, mt,  mt 


Set objWS_useInEach_ProcessStage = nothing
	gfwobjWS_LogSheet.activate	'	activating the logSheet helps show that the test case is finished
set	gfwobjWS_LogSheet = nothing ' qqqqq and the others   e.g. DataParameter, any others ?

' now push the execution iterations and elapsedTimes into the metrics table
' qq this should be inside multi-process, and again at the end for overall stats

iTS_Max = 3 ' Ubound(tsAr_EventEndedAtTS)
For iTS = iTS_Stage_0 To iTS_Stage_III
  cellTopLeft.cells(rowTL    , colTL + iTS).value = tsAr_EventStartedAtTS(iTS)
  cellTopLeft.cells(rowTL + 1, colTL + iTS).value = tsAr_EventEndedAtTS(iTS)
  '        row             +2&3 is the duration, endTS-startTS
  cellTopLeft.cells(rowTL + 4, colTL + iTS).value = tsAr_EventPermutationsCount(iTS)
Next


Set cellTopLeft = nothing

Set gQtResultsOpt = Nothing ' Release the Run Results Options object
set gQtApp   = nothing
Set gQtTest  = nothing

' qq Save the UFT.RunLog to the RunFolder (or just configure the runlog.folder @ startOfScript, to the RunFolder ? - is that possible )

' ExitAction
